<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text"/>
<!DOCTYPE html>
<html lang="${language}">
<head>
    <title>All users</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">
<link rel="shortcut icon" href="/img/cloud.ico">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <meta name="google-signin-client_id"
          content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
    <script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script>
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target=".navbar-collapse">
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/home">Clouds Merger</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li style="margin-top: -0.6rem";><a href="/admin/home"><i class="fa fa-home fa-2x"></i></a></li>
                <li class="dropdown"><a class="dropdown-toggle"
                                        data-toggle="dropdown" href="#"><fmt:message key="all_users.profile"></fmt:message> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>${user.userName}</li>
                        <li>${user.email}</li>
                        <li><a href="/user/profile"><fmt:message key="all_users.account"></fmt:message> </a></li>
                        <li><a id="signOutSocial" href="" class="portfolio-link"><fmt:message key="all_users.log_out"></fmt:message> </a></li>
                    </ul>
                </li>
                <li>
                    <form>
                        <select class="form-control" id="language" name="language"
                                onchange="submit()" style="margin-top: 6px">
                            <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                            <option value="uk_UA" ${language == 'uk_UA' ? 'selected' : ''}>Українська</option>
                        </select>
                    </form>
                </li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
<div id="blue_cloud">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-8 col-lg-offset-2"></div>
        </div>
        <!-- row -->
    </div>
    <!-- container -->
</div>

<div class="col-lg-9" style="padding: 3em 5em 0 5em;
    margin: 0px !important;">
    <table id="users" class="table table-striped table-bordered"
           cellspacing="0" width="100%" data-id="${user.id}">
        <thead>
        <tr>
            <th>Id</th>
            <th>VK</th>
            <th>G+</th>
            <th>Drive</th>
            <th>Drop</th>
            <th>Box</th>
            <th><fmt:message key="profile.username"></fmt:message></th>
            <th><fmt:message key="mainpage.modalwindow_register.email"></fmt:message> </th>
            <th><fmt:message key="all_users.table.fullname"></fmt:message> </th>
            <th><fmt:message key="all_users.table.locale"></fmt:message> </th>
            <th><fmt:message key="all_users.table.blocked"></fmt:message> </th>
            <th><fmt:message key="all_users.table.confirmed"></fmt:message> </th>
            <th><fmt:message key="all_users.table.role"></fmt:message> </th>
            <th></th>
        </tr>
        </thead>
        <tbody id="tableBody" style="text-align: left;">
        <%--jQuery append td and tr to table--%>
        </tbody>
    </table>
</div>
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap-treeview.js"></script>
<script src="/js/bootbox.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/signOutSocial.js"></script>
<script>
    $(document).ready(function () {
        tableCreate();
    });

    function tableCreate() {
        var table = $('#users').DataTable({ "language": {
            search: "<fmt:message key="datatable.search"></fmt:message>",
            paginate: {
                first:      "<fmt:message key="datatable.first"></fmt:message>",
                previous:   "<fmt:message key="datatable.previous"></fmt:message>",
                next:       "<fmt:message key="datatable.next"></fmt:message>",
                last:       "<fmt:message key="datatable.last"></fmt:message>"
            },
            emptyTable:     "<fmt:message key="datatable.empty_table"></fmt:message>",
            lengthMenu:    "<fmt:message key="datatable.length_menu"></fmt:message>",
            info:           "<fmt:message key="datatable.info"></fmt:message>",
            infoEmpty:      "<fmt:message key="datatable.info_empty"></fmt:message>",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "<fmt:message key="datatable.loading_records"></fmt:message>",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher"
        }});
        table.clear();
        $.ajax({
            type: 'POST',
            url: '/admin/users',
            data: {},
            success: function (resp) {
//                console.log(resp);
                var users = JSON.parse(resp);
                users.forEach(function (user) {
                    var blocked = $('#users')[0].getAttribute('data-id') === user.id ? 'disabled' : '';
                    table.row.add(
                            [user.id,
                                user.vkId !== 'null' ? '<span class="glyphicon glyphicon-ok"aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove"aria-hidden="true"></span>',
                                user.googleId !== 'null' ? '<span class="glyphicon glyphicon-ok"aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove"aria-hidden="true"></span>',
                                user.driveRefreshToken !== 'null' ? '<span class="glyphicon glyphicon-ok"aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove"aria-hidden="true"></span>',
                                user.drbxToken !== 'null' ? '<span class="glyphicon glyphicon-ok"aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove"aria-hidden="true"></span>',
                                user.boxRefreshToken !== 'null' ? '<span class="glyphicon glyphicon-ok"aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove"aria-hidden="true"></span>',
                                user.userName, user.email, user.fullname, user.locale,
                                user.blocked == 'true' ? '<span class="glyphicon glyphicon-ok"aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove"aria-hidden="true"></span>',
                                user.confirmed == 'true' ? '<span class="glyphicon glyphicon-ok"aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-remove"aria-hidden="true"></span>',
                                user.role,
                                '<button type="button" onclick="cr(this)" class="btn btn-default change-role" ' + blocked +' id="c' + user.id + '"><fmt:message key="all_users.script.role"></fmt:message></button>'
                            ])
                            .draw(false);
                });
            }
        });
    }

    function cr(th) {
        var userId = th.id.slice(1);
        var tds = th.parentElement.parentElement.childNodes;
        var curRole = tds[tds.length-2].textContent;
        var newRole = curRole == 'ADMIN' ? 'USER' : 'ADMIN';
        $.ajax({
            type: "POST",
            url: "/admin/role",
            data: {
                'userId': userId,
                'newRole': newRole
            },
            success: function (res) {
                console.log(res);
                res = JSON.parse(res);
                if (res['redirect'] !== undefined) {
                    tableCreate();
                }
                else {
                    alert(res);
                }
            }
        });
    }

</script>
</body>
</html>
