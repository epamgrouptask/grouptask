<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" />
<!DOCTYPE html lang=${language}>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="/img/cloud.ico">

<title>Clouds Merger</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/main.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="/css/tipso.css">
<link rel="stylesheet" href="/css/animate.css"> -->
<!-- <script src="/js/tipsy.js"></script> -->
<meta name="google-signin-client_id"
	content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async
	defer>
	
</script>

</head>
<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/home">Clouds Merger</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li style="margin-top: -0.6rem";><a href="home"><i
							class="fa fa-home fa-2x"></i></a></li>
					<li><a href="#" id="signOutSocial" class="portfolio-link"><fmt:message
								key="user_home.log_out" /></a></li>
					<li>
						<form>
							<select class="form-control" id="language" name="language"
								onchange="submit()" style="margin-top: 6px">
								<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
								<option value="uk_UA" ${language == 'uk_UA' ? 'selected' : ''}>Українська</option>
							</select>
						</form>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>

	<div id="blue_cloud">
		<div class="container">
			<div class="row centered">
				<div class="col-lg-8 col-lg-offset-2"></div>
			</div>
			<!-- row -->
		</div>
	</div>
	<div id="dg" style="padding: 40px;">
		<div class="container">
			<div class="row centered">
				<div>
					<div class="col-lg-6">
						<div class="pricing-option">
							<div class="pricing-top" style="background: darkgray;">
								<span class="pricing-edition"><fmt:message
										key="admin_home.view_statistic"></fmt:message> </span>
								<div class="row">
									<div class="row image">
										<img alt="" src="/img/Statistics.png" style="height: 20em"
											onclick="window.location.href='/admin/stat'">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="pricing-option">
							<div class="pricing-top" style="background: darkgray;">
								<span class="pricing-edition"><fmt:message
										key="admin_home.all_users"></fmt:message> </span>
								<div class="row">
									<div class="row image">
										<img alt="" src="/img/beneficiario.png" style="height: 20em"
											onclick="window.location.href='/admin/users'">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container w">
		<div class="row marg">
			<div class="col-lg-4 col-lg-offset-1">
				<div class="row centered marg">
					<h2 class="title">
						<fmt:message key="profile.header"></fmt:message>
					</h2>
					<table>
						<tr>
							<th><fmt:message key="profile.fullname"></fmt:message></th>
							<td>${user.fullname }</td>
							<td></td>
						</tr>
						<tr>
							<th><fmt:message key="profile.username"></fmt:message></th>
							<td>${user.userName }</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>${user.email }</td>
						</tr>
					</table>
				</div>
				<div class="row marg">
					<button type="button" class="btn my1"
						style="width: 100%; background-color: cornflowerblue;"
						onclick="showPassChange()">
						<fmt:message key="profile.change_password"></fmt:message>
					</button>
				</div>
				<div class="row marg">
					<button type="button" class="btn my2"
						style="width: 100%; background-color: steelblue;"
						onclick="editData()">
						<fmt:message key="profile.edit_info"></fmt:message>
					</button>
				</div>
			</div>
			<!-- <div class="row"></div> -->
			<div class="row marg">
				<div class="row centered">
					<h2 class="title">
						<fmt:message key="profile.social_networks"></fmt:message>
					</h2>

					<div class="col-lg-4 col-lg-offset-1">
						<div class="row marg"></div>
						<c:if test="${user.vkId == null}">
							<button id="login_button" type="button" class="btn myVK"
								onclick="VK.Auth.login(authInfo);"
								style="width: 100% !important; background-color: steelblue;">
								<fmt:message key="profile.link_vk"></fmt:message>
							</button>
						</c:if>
						<c:if test="${user.vkId != null}">
							<button id="unlinkVK" type="button" class="btn myVK"
								style="width: 100%; margin-top: 1em; background-color: steelblue;"
								value="Link VK">
								<fmt:message key="profile.unlink_vk"></fmt:message>
							</button>
						</c:if>
					</div>
					<div class="col-lg-4 col-lg-offset-1">
						<c:if test="${user.googleId == null}">
							<div class="btn g-signin2" data-onsuccess="onSignIn"
								style="width: 100%; margin-top: 1em;">
								<fmt:message key="user_home.login_g+"></fmt:message>
							</div>
						</c:if>
						<c:if test="${user.googleId != null}">
							<button id="unlinkGooglePlus" type="button" class="btn my1"
								style="width: 100%; margin-top: 1em;">
								<fmt:message key="profile.unlink_g+"></fmt:message>
							</button>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="r">
		<div class="container">
			<div class="row centered">
				<div class="col-lg-8 col-lg-offset-2">

					<p><fmt:message key="mainpage.footer"></fmt:message></p>
				</div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- r wrap -->


	<div class="modal fade" id="editProfile" role="dialog">
		<div class="modal-dialog" style="width: 60vw;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3>
						<fmt:message key="profile.edit_info"></fmt:message>
					</h3>
					<input type="hidden" value="${user.id }" id="userId">
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h3 class="thin text-center">
								<fmt:message key="profile.edit_info"></fmt:message>
							</h3>

							<div class="top-margin  form-group has-feedback" id="fullNE">
								<label><fmt:message key="profile.fullname"></fmt:message>
								</label> <input type="text" class="form-control" id="fullname"
									value="${user.fullname }">
							</div>
							<div class="top-margin  form-group has-feedback" id="userNE">
								<label><fmt:message key="profile.username"></fmt:message>
								</label> <input type="text" class="form-control" id="username"
									value="${user.userName }">
							</div>
							<div class="top-margin form-group has-feedback" id="emailE">
								<label>Email</label> <input type="email" class="form-control"
									id="Email" value="${user.email }">
							</div>
							<div class="row top-margin  form-group has-feedback" id="passE">
								<label><fmt:message
										key="mainpage.modalwindow_login.password"></fmt:message> <span
									class="text-danger">*</span></label> <input type="password"
									id="password" class="form-control">
							</div>
							<hr>
							<div class="row">
								<div class="col-lg-4 text-right">
									<button class="btn btn-action" type="button"
										style="background: yellowgreen;" onclick="sendRegisterData()"
										id="change">
										<fmt:message key="profile.save"></fmt:message>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="changingPass" role="dialog">
		<div class="modal-dialog" style="width: 60vw;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3>
						<fmt:message key="profile.change_password"></fmt:message>
					</h3>
					<input type="hidden" value="${user.id }" id="userId">
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h3 class="thin text-center">
								<fmt:message key="profile.edit_info"></fmt:message>
							</h3>

							<div class="top-margin form-group has-feedback" id="oldP">
								<label><fmt:message key="profile.old_password"></fmt:message>
								</label> <input type="password" class="form-control" id="oldPass">
							</div>
							<div class="top-margin form-group has-feedback" id="newP">
								<label><fmt:message key="profile.new_password"></fmt:message>
								</label> <input type="password" class="form-control" id="pass">
							</div>
							<div class="top-margin form-group has-feedback" id="newCP">
								<label><fmt:message key="profile.confirm_new_password"></fmt:message>
								</label> <input type="password" class="form-control" id="cPassword">
							</div>
							<div class="row">
								<div class="col-lg-4 text-right">
									<button class="btn btn-action" type="button"
										style="background: yellowgreen; margin: 20px;"
										onclick="changePass()" id="send">
										<fmt:message key="profile.change_password"></fmt:message>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- MODAL FOR CONTACT -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">contact us</h4>
				</div>
				<div class="modal-body">
					<div class="row centered">
						<p>We are available 24/7, so don't hesitate to contact us.</p>

						<p>
							Somestreet Ave, 987<br /> London, UK.<br /> +44 8948-4343<br />
							hi@blacktie.co
						</p>

						<div id="mapwrap">
							<iframe height="300" width="100%" frameborder="0" scrolling="no"
								marginheight="0" marginwidth="0"
								src="https://www.google.es/maps?t=m&amp;ie=UTF8&amp;ll=52.752693,22.791016&amp;spn=67.34552,156.972656&amp;z=2&amp;output=embed"></iframe>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Save
						& Go</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
	<script src="/js/bootbox.min.js"></script>
	<script src="/js/signOutSocial.js"></script>
	<script src="/js/changeProfileData.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/socialLogin.js"></script>
	<script src="/js/linkSocial.js"></script>
	<%--<script src="/js/unlinkGoogleDrive.js"></script>--%>
	<script src="/js/unlinkSocialScript.js"></script>
	<%--<script src="/js/linkBox.js"></script>--%>
	<script type="text/javascript">
		function editData() {
			$("#editProfile").modal('show');
		}

		function sendRegisterData() {
			$.ajax({
				url : "/admin/profile",
				type : "POST",
				data : {
					username : $('#username').val(),
					id : $('#userId').val(),
					password : $('#password').val(),
					email : $('#Email').val(),
					fullname : $('#fullname').val()
				},
				success : function(data) {
					switch (data) {
					case 'ok':
						window.location.reload(true);
						break;
					case 'nameFail':
						$('#userNE')[0].setAttribute('class', 'has-error');
						$('#change').attr("disabled", "disabled");
						break;
					case 'emailFail':
						$('#emailE')[0].setAttribute('class', 'has-error');
						$('#change').attr("disabled", "disabled");
						break;
					case 'passFail':
						$('#passE')[0].setAttribute('class', 'has-error');
						$('#change').attr("disabled", "disabled");
						break;
					}
				}
			});
		}

		function showPassChange() {
			$("#changingPass").modal('show');
		}
		$('#pass').keyup(function() {
			var pass = $('#pass').val();
			var confirm = $('#cPassword').val();
			if (confirm !== "") {
				if (pass !== confirm) {
					$('#newP')[0].setAttribute('class', 'has-error');
					$('#newCP')[0].setAttribute('class', 'has-error');
					$('#send').attr("disabled", "disabled");
				} else {
					$('#newP')[0].setAttribute('class', 'has-success');
					$('#newCP')[0].setAttribute('class', 'has-success');
					$('#send').removeAttr("disabled");
				}
			}
		});
		$('#cPassword').keyup(function() {
			var pass = $('#pass').val();
			var confirm = $('#cPassword').val();
			if (pass !== confirm) {
				$('#newP')[0].setAttribute('class', 'has-error');
				$('#newCP')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#newP')[0].setAttribute('class', 'has-success');
				$('#newCP')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
		});
		$('#oldPass').keyup(function() {
			var pass = $('#oldPass').val();
			if (pass == "") {
				$('#oldP')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#oldP')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
		});
		function changePass() {
			console.log("df");
			$.ajax({
				url : "/admin/profile",
				type : "POST",
				data : {
					password : $('#oldPass').val(),
					newPass : $('#pass').val(),
					id : $('#userId').val()
				},
				success : function(data) {
					console.log(data);
					switch (data) {

					case 'okPass':
						$('#changingPass').modal('toggle');
						break;
					case 'failPass':
						$('#oldP')[0].setAttribute('class', 'has-error');
						$('#send').removeAttr("disabled");
						break;
					}
				}
			});
		}
		function emptyChecker(id) {
			var parentDivId;
			switch (id) {
			case '#fullname':
				parentDivId = '#fullNE';
				break;
			case '#username':
				parentDivId = '#userNE';
				break;
			case '#password':
				parentDivId = '#passE';
				break;
			}
			var val = $(id).val();
			if (val == '') {
				$(parentDivId)[0].setAttribute('class', 'has-error');
				$('#change').attr("disabled", "disabled");
			} else {
				$(parentDivId)[0].setAttribute('class', 'has-success');
				$('#change').removeAttr("disabled");
			}
			$(id).keyup(function() {
				var val = $(id).val();
				if (val == '') {
					$(parentDivId)[0].setAttribute('class', 'has-error');
					$('#change').attr("disabled", "disabled");
				} else {
					$(parentDivId)[0].setAttribute('class', 'has-success');
					$('#change').removeAttr("disabled");
				}
			});
		}
		function validateEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}
		$('#Email').keyup(function() {
			var email = $('#Email').val();
			if (email == "" || !validateEmail(email)) {
				$('#emailE')[0].setAttribute('class', 'has-error');
				$('#change').attr("disabled", "disabled");
			} else {
				$('#emailE')[0].setAttribute('class', 'has-success');
				$('#change').removeAttr("disabled");
			}
		});
		window.onload = function() {
			 var el = [ '#password', '#fullname', '#username' ];
			el.forEach(function(item, el) {
				emptyChecker(item);
			});
			var val = $('#Email').val();
			if (val == '') {
				$('#emailE')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#emailE')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
			val = $('#cPassword').val();
			if (val == '') {
				$('#newCP')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#newCP')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
			val = $('#pass').val();
			if (val == '') {
				$('#newP')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#newP')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
			val = $('#oldPass').val();
			if (val == '') {
				$('#oldP')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#oldP')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
			$('.abcRioButton.abcRioButtonLightBlue').removeAttr('style');
			$('.abcRioButton.abcRioButtonLightBlue').css({
				'background-color' : 'cornflowerblue',
				'color' : 'white',
				'height' : '30px',
				'width' : '100%'
			});
			$('.abcRioButtonContents').text('Link G+');
		}
	</script>
</body>
</html>