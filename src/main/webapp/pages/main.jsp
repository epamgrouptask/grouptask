<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text"/>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="shortcut icon" href="/img/cloud.ico">

    <title>Clouds Merger</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script
            src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <meta name="google-signin-client_id"
          content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
    <script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <script src="https://apis.google.com/js/platform.js?onload=onLoad" async
            defer></script>
</head>
<body style="background: url(/img/clouds.jpg) no-repeat center top; margin: 0">
<style>
    *{
        font: 16px "Open Sans","Helvetica Neue", Helvetica, Arial, sans-serif !important;
    }
</style>
                <ul class="nav navbar-nav navbar-right">
                <li>
                    <a data-toggle="dropdown"
                       class="dropdown-toggle" style="width: 60px"><img src="/img/gb.svg" onclick="setEng()" style="width: 100%"></a>
                </li>
                <li><a data-toggle="dropdown"
                       class="dropdown-toggle" style="width: 60px"><img src="/img/ua.svg" onclick="setUkr()" style="width: 100%"></a></li>
                </ul>
         
	<div class="container">
		<div class="row" style="margin-top: 2em;">
			<div class="col-sm-8 col-sm-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">${bundle.getString("main.panel_title")}</h3>
					</div>
					<div class="panel-body">
						<fieldset>
							<div class="form-group">
								<div class="alert alert-danger" role="alert"
									style="display: none;">
									<span id="errorLogin"></span>
								</div>
								<input class="form-control" placeholder="<fmt:message key="mainpage.modalwindow_login.login"></fmt:message>" type="text"
									class="form-control" id="login">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="<fmt:message key="mainpage.modalwindow_login.password"></fmt:message>"
									type="password" value="" class="form-control" id="pass">
							</div>
							
							<input class="btn btn-lg btn-info btn-block" type="button"
								onclick="sendLoginData()" value="<fmt:message key="mainpage.modalwindow_login.login"></fmt:message>">
								<input class="btn btn-lg btn-primary btn-block"  type="button" value="<fmt:message key="user_home.login_vk"></fmt:message>"
									class="btn myVK" style="width: 100% !important;"
									onclick="VK.Auth.login(authInfo);">
									
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Placed at the end of the document so the pages load faster -->
<script src="/js/jquery.js"></script>
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- <script src="js/jquery.js"></script> -->
<!-- 	<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
<script
        src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="/js/classie.js"></script>
<script src="/js/cbpAnimatedHeader.js"></script>

<!-- Contact Form JavaScript -->
<script src="/js/jqBootstrapValidation.js"></script>
<script src="/js/contact_me.js"></script>
<script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>

<!-- Custom Theme JavaScript -->

<script src="/js/agency.js"></script>
<script src="/js/agency.js"></script>
<script src="/js/signOutSocial.js"></script>
<script src="/js/clientSocial.js"></script>
<script type="text/javascript">
    function closeModal(modName) {
        $(modName).modal('toggle');
    }

    var language = 'en';

    function setEng(){
        language = 'en';
        //app.changeLocale('en');
        window.location.href = '/client/login?language='+language;
    }

    function setUkr(){
        language = 'uk_UA';
        window.location.href = '/client/login?language='+language;
    }

    function sendDataPass() {
        $.ajax({
                    url : "/restorepass",
                    type : "POST",
                    data : {
                        email : $('#mailPass').val(),
                        username : $('#loginPass').val()
                    },
                    success : function(data) {
                        if (data == 'sent') {
                            $('.alert-success').show();
                            $('#sendResult').text("${bundle.getString("main.script.send")}" );
                        }
                        if (data == 'nouser') {
                            $('.alert-success').show();
                            $('#sendResult').text("<fmt:message key="main.script.user_not_found"></fmt:message>");
                            $('#sendResult').text("${bundle.getString("main.script.user_not_found")}");
                        }
                    }
                });
    }
    

    function sendLoginData() {
        $.ajax({
            url : "/client/login",
            type : "POST",
            data : {
                username : $('#login').val(),
                password : $('#pass').val()
            },
            success : function(data) {
                switch (data) {
                    case 'no user by login':
                        $('.alert-danger').show();
                        $('#errorLogin').text(
                                "${bundle.getString("main.script.user_doesn`t_exists")}" );
                        break;
                    case 'not correct password':
                        $('.alert-danger').show();
                        $('#errorLogin').text("${bundle.getString("main.script.wrong_pass")}" );
                        break;
                    case 'not confirmed':
                        $('.alert-danger').show();
                        $('#errorLogin').text("${bundle.getString("main.script.profile_confirmed")} ");
                        break;
                    default :
                        var login = $('#login').val();
                        var pass = $('#pass').val();
                        app.sendId(data, login, pass);
                        /* window.location.replace("/client/paths"); */
                        break;
                }
            }
        });
    }
</script>
<script>
    function signOut() {
        VK.Auth.logout();
        var auth3 = gapi.auth2.getAuthInstance();
        auth3.signOut().then(function () {
            console.log('User signed out.');
        });
        $.get('logout');
        window.location.reload();
    }
    function onLoad() {
        gapi.load('auth2', function () {
            gapi.auth2.init();
        });
    }
    $(document).ready(onLoad());
</script>
</body>
</html>