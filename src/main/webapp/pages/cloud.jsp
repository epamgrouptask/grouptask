<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" />
<!DOCTYPE html lang="${language}">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="/img/cloud.ico">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/manni.css">
<link rel="stylesheet" href="/css/fileSystem.css">
<title>Clouds Merger</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/main.css" rel="stylesheet">

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script
	src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<meta name="google-signin-client_id"
	content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async
	defer></script>
</head>

<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/home">Clouds Merger</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li style="margin-top: -0.6rem";><a href="/user/home"><i
							class="fa fa-home fa-2x"></i></a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><fmt:message
								key="all_users.profile"></fmt:message> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li style="margin-bottom: 3%">${user.userName}</li>
							<li style="margin-bottom: 5%">${user.email}</li>
							<li style="margin-bottom: 5%"><a href="/user/profile"><fmt:message
										key="all_users.account"></fmt:message> </a></li>
							<li><a id="signOutSocial" href="" class="portfolio-link"><fmt:message
										key="all_users.log_out"></fmt:message> </a></li>
						</ul></li>
					<li>
						<form>
							<select class="form-control" id="language" name="language"
								onchange="submit()" style="margin-top: 6px">
								<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
								<option value="uk_UA" ${language == 'uk_UA' ? 'selected' : ''}>Українська</option>
							</select>
						</form>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<div class="modal fade" id="treeviewShow" role="dialog">
		<div class="modal-dialog" style="width: 60vw;">
			<div id="treeview1" class="treeview"></div>
		</div>
	</div>
	<div id="blue_cloud">
		<div class="container">
			<div class="row centered">
				<div class="col-lg-8 col-lg-offset-2"></div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- blue wrap -->

	<div class="container w">
		<div class="row centered">
			<div class="row">
				<div class="hidden-lg">
					<div class="btn-group pull-right">
						<button class="btn btn-default dropdown-toggle" type="button"
							data-toggle="dropdown"
							style="position: fixed; background: cadetblue; z-index: 200; float: right; margin-left: -50%; right: 0;">
							Menu<span class="caret"></span>
						</button>
						<ul class="dropdown-menu " style="top: 260px; position: fixed;">
							<li><a href="#" onclick="goBack()"><span
									class="glyphicon glyphicon-step-backward"></span> <fmt:message
										key="cloud.files"></fmt:message></a></li>
							<li><a href="#" onclick="files()"><fmt:message
										key="cloud.list"></fmt:message></a></li>
							<li><a href="#" onclick="tableCreate()"><fmt:message
										key="cloud.update"></fmt:message></a></li>
							<li><a href="#" onclick="uploadFile()"><fmt:message
										key="cloud.upload"></fmt:message></a></li>
							<li><a href="#" onclick="createFolder()"><fmt:message
										key="cloud.create"></fmt:message></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-3 hidden-md hidden-sm hidden-xs">
				<div class="list-group" style="position: fixed">
					<%--todo fixed group like navbar--%>
					<button class="list-group-item hover_effect" onclick="goBack()"
						style="width: 25rem;">
						<span class="glyphicon glyphicon-step-backward"></span>
						<fmt:message key="cloud.files"></fmt:message>
					</button>
					<button class="list-group-item hover_effect" onclick="files()"
						style="width: 25rem;">
						<fmt:message key="cloud.list"></fmt:message>
					</button>
					<button class="list-group-item hover_effect"
						onclick="tableCreate()" style="width: 25rem;">
						<fmt:message key="cloud.update"></fmt:message>
					</button>
					<button class="list-group-item hover_effect btn-hide"
						onclick="uploadFile()" style="width: 25rem;">
						<fmt:message key="cloud.upload"></fmt:message>
					</button>
					<button onclick="createFolder()"
						class="list-group-item hover_effect btn-hide"
						style="width: 25rem;">
						<fmt:message key="cloud.create"></fmt:message>
					</button>
					<div class="loader2" id="spinner_under_list" style="display: none"></div>
				</div>

			</div>
			<%--<input type="hidden" id="fileId" value="">--%>

			<div class="loader" id="loaderTable">
				<fmt:message key="cloud.loading"></fmt:message>
			</div>
			<div class="col-lg-9" id="rowWithTable">
				<table id="example" class="table table-striped table-bordered"
					cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><fmt:message key="cloud.name"></fmt:message></th>
							<th><fmt:message key="cloud.size"></fmt:message></th>
							<th class="dateOrCloud"><fmt:message key="cloud.storage"></fmt:message></th>
						</tr>
					</thead>
					<tbody id="tableBody" style="text-align: left;">
						<%--jQuery append td and tr to table--%>
					</tbody>
					<tfoot>
						<tr>
							<th><fmt:message key="cloud.name"></fmt:message></th>
							<th><fmt:message key="cloud.size"></fmt:message></th>
							<th class="dateOrCloud"><fmt:message key="cloud.storage"></fmt:message></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
	</div>

	<div id="context-menu-fol">
		<ul class="dropdown-menu" role="menu">
			<li><a href="#" tabindex="-1" onclick="deleteFile(event)"><fmt:message
						key="cloud.dropdown_menu.delete"></fmt:message> </a></li>
			<li><a href="#" tabindex="-1" onclick="renameFile(event)"><fmt:message
						key="cloud.left_menu.header_rename"></fmt:message></a></li>
			<li><a href="#" tabindex="-1" onclick="copyDir(event)"><fmt:message
						key="cloud.dropdown_menu.copy"></fmt:message> </a></li>
		</ul>
	</div>
	<div id="context-menu-fil">
		<ul class="dropdown-menu" role="menu">
			<li><a href="#" tabindex="-1" onclick="deleteFile(event)"><fmt:message
						key="cloud.dropdown_menu.delete"></fmt:message></a></li>
			<li><a href="#" tabindex="-1" onclick="renameFile(event)"><fmt:message
						key="cloud.left_menu.header_rename"></fmt:message></a></li>
			<li><a href="#" tabindex="-1" onclick="copyFile(event)"><fmt:message
						key="cloud.dropdown_menu.copy"></fmt:message></a></li>
			<li><a href="#" tabindex="-1" onclick="copyFileTo(event)"><fmt:message
						key="cloud.dropdown_menu.copy_to"></fmt:message></a></li>
			<li><a href="#" tabindex="-1" onclick="downloadFile(event)"><fmt:message
						key="cloud.dropdown_menu.download"></fmt:message> </a></li>
			<li class="divider"></li>
			<li><a href="#" tabindex="-1" onclick="moveFile(event)"><fmt:message
						key="cloud.dropdown_menu.move"></fmt:message> </a></li>
			<li><a href="#" tabindex="-1" onclick="moveFileTo(event)"><fmt:message
						key="cloud.dropdown_menu.move_to"></fmt:message> </a></li>
		</ul>
	</div>
	<div id="context-menu-complex">
		<ul class="dropdown-menu" role="menu">
			<li><a href="#" tabindex="-1" onclick="deleteFile(event)"><fmt:message
						key="cloud.dropdown_menu.delete"></fmt:message></a></li>
			<li><a href="#" tabindex="-1" onclick="renameFile(event)"><fmt:message
						key="cloud.left_menu.header_rename"></fmt:message></a></li>
			<li><a href="#" tabindex="-1" onclick="downloadFile(event)"><fmt:message
						key="cloud.dropdown_menu.download"></fmt:message> </a></li>
		</ul>
	</div>

	<script src="/js/jquery.js"></script>
	<script src="/js/bootstrap-treeview.js"></script>
	<script src="/js/bootbox.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/signOutSocial.js"></script>
	<script src="/js/uploadBigFile.js"></script>
	<script type="text/javascript">
    /*$(document)
     .ajaxStart(function () {
     $('#rowWithTable').hide();
     $(".loader").show();
     })
     .ajaxStop(function () {
     $('#rowWithTable').show();
     $(".loader").hide();
     });*/

    var currentPath, parameters, cloud, clientId, directoryCloud, pathToDirectory, fileId;
    var selectedFileId;
    var userId;
    var treeData, options, cloudCount, freeSpace, fileSize;
    function setSelectedFileId(fId) {
        selectedFileId = fId;
        $("#context-menu-fol").hide();
        $("#context-menu-fil").hide();
    }

    function goBack() {
        window.history.go(-1);
        console.log(tableCreate);
        if (tableCreate == 'undefined') {
            window.location.reload();
        } else {
            tableCreate();
        }
    }
	
    function uploadFile() {
        //event.preventDefault();
        var upload = '<h3 class="thin text-center"><fmt:message key="cloud.left_menu.upload_file"></fmt:message></h3> <hr><div class="top-margin" id="uploadFileDiv"><form name="upload_file" method="post" enctype="multipart/form-data"><input type="file" id="file" name="file" onchange="complex()" class="btn btn-default btn-file"></form>';
        if (cloud === 'COMPLEX') {
            upload += '<select class="form-control" id="sel1" onchange="complex()" style="width: 60%">';
            options = '';
            $.ajax({
                url: "/user/cloud/free",
                success: function (data) {
                    freeSpace = JSON.parse(data);
                }
            });
            $.ajax({
                beforeSend: function () {
                    $("#spinner_under_list").show();
                },
                complete: function () {
                    $("#spinner_under_list").hide();
                    //tableCreate();
                },
                url: "/user/active",
                type: "GET",
                data: {},
                success: function (data) {
                    var data = JSON.parse(data);
                    var counter = 0;
                    if (data['dropbox']) {
                        counter++;
                        options += '<option value="DROPBOX">Dropbox</option>';
                    }
                    if (data['box']) {
                        counter++;
                        options += '<option value="BOX">Box</option>';
                    }
                    if (data['drive']) {
                        counter++;
                        options += '<option value="DRIVE">Drive</option>';
                    }
                    if (data.clients != undefined) {
                        for (var i = 0; i < data.clients.length; i++) {
                            if (data.clients[i].status == true) {
                                upload += '<option value="' + data.clients[i].clientId + '">' + data.clients[i].name + '</option>';
                            }
                        }
                    }
                    if (counter > 1) {
                        upload += '<option value="COMPLEX">Complex</option>';
                    }
                    upload += options;
                    upload += '</select></div>';
                    bootbox.confirm(upload,
                            function (result) {
                                if (result != null && result != false) {
                                    getsetParameters();
                                    var size = $('#file')[0].files[0].size;
                                    var complexFile = false;
                                    var selected = $("#sel1").val();
                                    if (selected === 'COMPLEX' || selected === 'DRIVE' || selected === 'DROPBOX' || selected === 'BOX') {
                                        var cloudToSave = selected;
                                        var clientId = userId;
                                        path = '/';
                                    } else {
                                        var cloudToSave = 'CLIENT';
                                        var clientId = selected;
                                        path = $("#sel3").val();
                                    }
                                    var formData = new FormData();
                                    var pathEls = (window.location.pathname).split('/');
                                    formData.append('file', $("#file")[0].files[0]);
                                    formData.append('path', path);
                                    formData.append('cloud', cloudToSave);
                                    formData.append('clientId', clientId);
                                    formData.append('size', size);
                                    if (selected === 'COMPLEX') {
                                        var json = '{"parts" : [ ';
                                        var childs = $(".filePartDiv");
                                        for (var i = 0; i < childs.length; i++) {
                                            json += ' { "cloud" : "' + childs[i].childNodes[0].value;
                                            json += '", "percent" : ' + childs[i].childNodes[1].value;
                                            json += '},';
                                        }
                                        json = json.substring(0, json.length - 1);
                                        json += ']}';
                                        formData.append('clouds', json);
                                        $.ajax({
                                            beforeSend: function () {
                                                $("#spinner_under_list").show();
                                            },
                                            complete: function () {
                                                $("#spinner_under_list").hide();
                                                tableCreate();
                                            },
                                            type: 'POST',
                                            url: '/user/cloud/uploadComplexFile',
                                            data: formData,
                                            cache: false,
                                            contentType: false,//"multipart/form-data"
                                            processData: false,
                                            success: function (data) {
                                                switch (data) {
                                                    case 'ok':
                                                        bootbox.alert("<fmt:message key="cloud.scripts.upload_success"></fmt:message>");
                                                        break;
                                                    case 'err':
                                                        bootbox.alert("<fmt:message key="cloud.scripts.upload_filed"></fmt:message>");
                                                        break;
                                                }
                                            }
                                        });
                                    } else {
                                        if (size > 1024 * 1024 * 200) {
                                            var uploader = ShowForm();
                                            uploader.cloud = cloudToSave;
                                            uploader.clientId = clientId;
                                            uploader.path = '/';
                                            uploader.name = $("#file")[0].files[0].name;
                                            uploader.Upload();
                                        }
                                        $.ajax({
                                            beforeSend: function () {
                                                $("#spinner_under_list").show();
                                            },
                                            complete: function () {
                                                $("#spinner_under_list").hide();
                                                tableCreate();
                                            },
                                            type: 'POST',
                                            url: '/user/cloud/getdeleteupload',
                                            data: formData,
                                            cache: false,
                                            contentType: false,//"multipart/form-data"
                                            processData: false,
                                            success: function (data) {
                                                switch (data) {
                                                    case 'ok':
                                                        bootbox.alert("<fmt:message key="cloud.scripts.upload_success"></fmt:message>");
                                                        break;
                                                    case 'err':
                                                        bootbox.alert("<fmt:message key="cloud.scripts.upload_filed"></fmt:message>");
                                                        break;
                                                }
                                            }
                                        });
                                    }

                                }
                            }
                    );

                }
            });
        } else {
            upload += '</div>';
            bootbox.confirm(upload,
                    function (result) {
                        if (result != null && result != false) {
                            getsetParameters();
                            var cloudToSave = cloud;
//                            var clientId = clientId;
                            var pathEls = (window.location.pathname).split('/');
                            if (cloud.toUpperCase() == 'DRIVE') {
                                path = pathEls[pathEls.length - 1];
                            } else {
                                path = pathToDirectory;
                            }
                            if (path.toUpperCase() == 'DRIVE' || path.toUpperCase() == 'BOX' || path.toUpperCase() == 'DROPBOX') {
                                path = '/';
                            }
                            var size = $('#file')[0].files[0].size;
                            if (size > 1024 * 1024 * 100) {
                                var uploader = ShowForm();
                                uploader.cloud = cloudToSave;
                                uploader.clientId = clientId;
                                uploader.path = path;
                                uploader.name = $("#file")[0].files[0].name;
                                uploader.Upload();
                            } else {
                                var complexFile = false;
                                var formData = new FormData();
                                formData.append('file', $("#file")[0].files[0]);
                                formData.append('path', path);
                                formData.append('cloud', cloud);
                                formData.append('clientId', clientId);
                                formData.append('size', size);
                                $.ajax({
                                    beforeSend: function () {
                                        $("#spinner_under_list").show();
                                    },
                                    complete: function () {
                                        $("#spinner_under_list").hide();
                                        tableCreate();
                                    },
                                    type: 'POST',
                                    url: '/user/cloud/getdeleteupload',
                                    data: formData,
                                    cache: false,
                                    contentType: false,//"multipart/form-data"
                                    processData: false,
                                    success: function (data) {
                                        switch (data) {
                                            case 'ok':
                                                bootbox.alert("<fmt:message key="cloud.scripts.upload_success"></fmt:message>");
                                                break;
                                            case 'err':
                                                bootbox.alert("<fmt:message key="cloud.scripts.upload_filed"></fmt:message>");
                                                break;
                                        }
                                    }
                                });

                            }
                        }
                    }
            )
        }
    }

    function deleteFile(event) {
        event.preventDefault();
        getsetParameters();
        // pathToDirectory = $('#fileId').val();
        console.log(selectedFileId + " " + pathToDirectory);
        var fileName = $('#' + selectedFileId).get(0).innerText;
        var type = $('#' + selectedFileId).get(0).className;

        if (cloud == 'COMPLEX') {
            var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
            var storageType = tds[tds.length - 1].innerText;
            cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
            cloud.toUpperCase();
        }
        $.ajax({
            beforeSend: function () {
                $("#spinner_under_list").show();
            },
            complete: function () {
                $("#spinner_under_list").hide();
                //tableCreate();
            },
            type: 'DELETE',
            url: '/user/cloud/getdeleteupload',
            data: {
                'type': type,
                'path': $("#" + selectedFileId).attr('fileId'),
                'cloud': cloud,
                'fileId': $("#" + selectedFileId).attr('fileId'),
                'clientId': clientId,
                'name': fileName
            },
            success: function (resp) {
                switch (resp) {
                    case 'err':
                        bootbox.alert("<fmt:message key="cloud.scripts.cant_delete"></fmt:message>", function () {

                        });
                        break;
                    case 'blocked':
                        bootbox.prompt({
                            title: "<fmt:message key="cloud.scripts.enter_code"></fmt:message>",
                            callback: function (result) {
                                if (result == null) {

                                } else {
                                    var code = result;
                                    $.ajax({
                                        url: '/checkcode/',
                                        type: 'POST',
                                        data: {
                                            'code': code
                                        },
                                        success: function (response) {
                                            if (response == 'bad') {
                                                return deleteFile();
                                            } else {
                                                return deleteFile();
                                            }
                                        }
                                    });
                                }
                            }
                        });
                        break;
                    default:
                        bootbox.alert("<fmt:message key="cloud.scripts.delete_success"></fmt:message>", function () {

                        });
                        var child = $("#" + selectedFileId).parent().parent().get(0);
                        child.parentElement.removeChild(child);
                        break;
                }
            }
        });
    }

    function renameFile(event) {
        event.preventDefault();
        bootbox.prompt({
            title: "<fmt:message key="cloud.left_menu.rename"></fmt:message>",
            value: $("#" + selectedFileId).text(),
            callback: function (result) {
                if (result == null) {

                } else {
                    var newName = result;
                    if (!checkNewName(newName)) {
                    	renameFile();
                        return;
                    }
                    getsetParameters();
                    console.log(selectedFileId + " " + pathToDirectory);
//                    var n = $('#fileId').val().split('/');
//                    var name = n[n.length - 1];
                    var type = $('#' + selectedFileId).get(0).className;
//                    console.log("path " + pathToDirectory + " name " + name);

                    if (cloud == 'COMPLEX') {
                        var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
                        var storageType = tds[tds.length - 1].innerText;
                        cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
                    }
                    $.ajax({
                        type: 'POST',
                        url: '/user/cloud/createrenamedownload',
                        data: {
                            'type': type,
                            'path': $("#" + selectedFileId).attr('fileId'),
                            'cloud': cloud,
                            'clientId': clientId,
                            'newName': newName,
                            'fileId': $("#" + selectedFileId).attr('fileId')
                        },
                        success: function (resp) {
                            switch (resp) {
                                case 'err':
                                    $('.alert-success').hide();
                                    $('.alert-danger').show();
                                    $('#errorRF').text("<fmt:message key="cloud.scripts.error_rename"></fmt:message>");
                                    break;
                                case 'blocked':
                                    bootbox.prompt({
                                        title: "<fmt:message key="cloud.scripts.enter_code"></fmt:message>",
                                        callback: function (result) {
                                            if (result == null) {

                                            } else {
                                                var code = result;
                                                $.ajax({
                                                    url: '/checkcode/',
                                                    type: 'POST',
                                                    data: {
                                                        'code': code
                                                    },
                                                    success: function (response) {
                                                        if (response == 'bad') {
                                                            return renameFile();
                                                        } else {
                                                            return renameFile();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    });
                                    break;
                                default:
                                    bootbox.alert("<fmt:message key="cloud.scripts.success_rename"></fmt:message>");
                                    $("#" + selectedFileId).text(newName);
                                        var hrf = $('#' + selectedFileId).attr('href');
                                        hrf = hrf.slice(0, hrf.lastIndexOf('/')+1);
                                        hrf+=newName;
                                    $('#' + selectedFileId).attr('href',hrf);
                                    var hrf = $('#' + selectedFileId).attr('fileid');
                                    hrf = hrf.slice(0, hrf.lastIndexOf('/')+1);
                                    hrf+=newName;
                                    $('#' + selectedFileId).attr('fileid',hrf);
                                    $('.alert-danger').hide();
                                    $('#successRF').text("<fmt:message key="cloud.scripts.success_rename"></fmt:message>");
                                    $('.alert-success').show();
                                    break;
                            }
                        }
                    });
                }
            }
        });
    }

    function downloadFile(event) {
        event.preventDefault();
        getsetParameters();
        console.log(selectedFileId + " " + pathToDirectory);
        //var n = $('#fileId').val().split('/');
        //var name = n[n.length - 1];
        //console.log("path " + pathToDirectory + " name " + name);
        var fileName = $('#' + selectedFileId).get(0).innerText;
        var a = document.createElement('a');

        if (cloud == 'COMPLEX') {
            var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
            var storageType = tds[tds.length - 1].innerText;
            cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
        }
        a.href = "/user/cloud/createrenamedownload?name=" + fileName + "&clientId=" + clientId + "&path=" + pathToDirectory + "&fileId=" + $("#" + selectedFileId).attr('fileId') + "&cloud=" + cloud;
        a.click();
    }

    function copyDir(event) {
        event.preventDefault();
        getsetParameters();
        if (cloud == 'COMPLEX') {
            var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
            var storageType = tds[tds.length - 1].innerText;
            cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
        }
        $.ajax({
            beforeSend: function () {
                $("#spinner_under_list").show();
            },
            complete: function () {
                $("#spinner_under_list").hide();
            },
            url: "/fileTree",
            type: "get",
            data: {
                cloud: cloud,
                clientId: clientId
            },
            success: function (dataF) {
                treeData = dataF;
                bootbox.confirm('<div id="treeview2" class="treeview" onload="setTreeViewData(treeData)"></div>',
                        function (result) {
                            if (result) {
                                var path = $("#treeview2").treeview('getSelected')[0].path;
                                if (path != 'undefined') {
                                    var type = $('#' + selectedFileId).get(0).className;
                                    if (cloud == 'COMPLEX') {
                                        var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
                                        var storageType = tds[tds.length - 1].innerText;
                                        cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
                                    }
                                    $.ajax({
                                        url: '/user/cloud/relocatecopy',
                                        type: 'PUT',
                                        data: {
                                            'cloud': cloud,
                                            'path': $("#" + selectedFileId).attr('fileId'),
                                            'fileId': $("#" + selectedFileId).attr('fileId'),
                                            'clientId': clientId,
                                            'toCloud': cloud,
                                            'toPath': path,
                                            'name': $("#" + selectedFileId).get(0).innerText,
                                            'type': type
                                        },
                                        success: function (data) {
                                            switch (data) {
                                                case 'blocked':
                                                    bootbox.prompt({
                                                        title: "<fmt:message key="cloud.scripts.enter_code"></fmt:message>",
                                                        callback: function (result) {
                                                            if (result == null) {

                                                            } else {
                                                                var code = result;
                                                                $.ajax({
                                                                    url: '/checkcode/',
                                                                    type: 'POST',
                                                                    data: {
                                                                        'code': code
                                                                    },
                                                                    success: function (response) {
                                                                        if (response == 'bad') {
                                                                            return copyDir();
                                                                        } else {
                                                                            return copyDir();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                    break;
                                                case "ok":
                                                    bootbox.alert("<fmt:message key="cloud.scripts.copy_dir_success"></fmt:message>", function () {

                                                    });
                                                    break;
                                                case "err":
                                                    bootbox.alert("<fmt:message key="cloud.scripts.copy_dir_failed"></fmt:message>", function () {

                                                    });
                                                    break;
                                            }
                                        },
                                        error: function () {

                                        }
                                    });
                                }
                            } else {

                            }
                        }
                );
                setTreeViewData(dataF);
            }
        });
    }

    function copyFile(event) {
        event.preventDefault();
        getsetParameters();
        $.ajax({
            beforeSend: function () {
                $("#spinner_under_list").show();
            },
            complete: function () {
                $("#spinner_under_list").hide();
            },
            url: "/fileTree",
            type: "get",
            data: {
                cloud: cloud,
                clientId: clientId
            },
            success: function (dataF) {
                treeData = dataF;
                bootbox.confirm('<div id="treeview2" class="treeview" onload="setTreeViewData(treeData)"></div>',
                        function (result) {
                            if (result) {
                                var path = $("#treeview2").treeview('getSelected')[0].path;
                                if (path != 'undefined') {
                                    var type = $('#' + selectedFileId).get(0).className;
                                    if (cloud == 'COMPLEX') {
                                        var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
                                        var storageType = tds[tds.length - 1].innerText;
                                        cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
                                    }
                                    $.ajax({
                                        url: '/user/cloud/relocatecopy',
                                        type: 'PUT',
                                        data: {
                                            'cloud': cloud,
                                            'path': $("#" + selectedFileId).attr('fileId'),
                                            'fileId': $("#" + selectedFileId).attr('fileId'),
                                            'clientId': clientId,
                                            'toCloud': cloud,
                                            'toPath': path,
                                            'name': $("#" + selectedFileId).get(0).innerText,
                                            'type': type
                                        },
                                        success: function (data) {
                                            switch (data) {
                                                case 'blocked':
                                                    bootbox.prompt({
                                                        title: "<fmt:message key="cloud.scripts.enter_code"></fmt:message>",
                                                        callback: function (result) {
                                                            if (result == null) {

                                                            } else {
                                                                var code = result;
                                                                $.ajax({
                                                                    url: '/checkcode/',
                                                                    type: 'POST',
                                                                    data: {
                                                                        'code': code
                                                                    },
                                                                    success: function (response) {
                                                                        if (response == 'bad') {
                                                                            return copyFile();
                                                                        } else {
                                                                            return copyFile();
                                                                        }
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    });
                                                    break;
                                                case 'ok':
                                                    bootbox.alert("<fmt:message key="cloud.scripts.copy_successful"></fmt:message>");
                                                    break;
                                                case 'err':
                                                    bootbox.alert("<fmt:message key="cloud.scripts.copy_filed"></fmt:message>");
                                                    break;
                                            }
                                        },
                                    });
                                }
                            }
                        }
                );
                setTreeViewData(dataF);
            }
        });
    }

    function moveFile(event) {
        event.preventDefault();
        getsetParameters();
        $.ajax({
            beforeSend: function () {
                $("#spinner_under_list").show();
            },
            complete: function () {
                $("#spinner_under_list").hide();
                //tableCreate();
            },
            url: "/fileTree",
            type: "get",
            data: {
                cloud: cloud,
                clientId: clientId
            },
            success: function (dataF) {
                treeData = dataF;
                bootbox.confirm('<div id="treeview2" class="treeview" onload="setTreeViewData(treeData)"></div>',
                        function (result) {
                            if (result) {
                                var path = $("#treeview2").treeview('getSelected')[0].path;
                                if (path != 'undefined') {
                                    var type = $('#' + selectedFileId).get(0).className;
                                    if (cloud == 'COMPLEX') {
                                        var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
                                        var storageType = tds[tds.length - 1].innerText;
                                        cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
                                    }
                                    $.ajax({
                                        beforeSend: function () {
                                            $("#spinner_under_list").show();
                                        },
                                        complete: function () {
                                            $("#spinner_under_list").hide();
                                            //tableCreate();
                                        },
                                        url: '/user/cloud/relocatecopy',
                                        type: 'POST',
                                        data: {
                                            'cloud': cloud,
                                            'clientId': clientId,
                                            'path': $("#" + selectedFileId).attr('fileId'),
                                            'fileId': $("#" + selectedFileId).attr('fileId'),
                                            'toCloud': cloud,
                                            'toPath': path,
                                            'type': type
                                        },
                                        success: function (data) {
                                            switch (data) {
                                                case "ok":
                                                    bootbox.alert("<fmt:message key="cloud.scripts.move_success"></fmt:message>", function () {
                                                    });
                                                    $("#" + selectedFileId).parent().parent().hide();
                                                        //tableCreate();
                                                    break;
                                                case "err":
                                                    bootbox.alert("<fmt:message key="cloud.scripts.move_error"></fmt:message>", function () {
                                                    });
                                                    break;
                                            }
                                        }

                                    });
                                }
                            } else {

                            }
                        }
                );
                setTreeViewData(dataF);
            }
        });
    }

    function copyFileTo(event) {
        event.preventDefault();
        getsetParameters();
        $.ajax({
            beforeSend: function () {
                $("#spinner_under_list").show();
            },
            complete: function () {
                $("#spinner_under_list").hide();
                //tableCreate();
            },
            url: "/fileTree",
            type: "get",
            data: {
                cloud: "COMPLEX"
            },
            success: function (dataF) {
                bootbox.confirm('<div id="treeview2" class="treeview" onload="setTreeViewData(treeData)"></div>',
                        function (result) {
                            if (result) {
                                var node = $("#treeview2").treeview('getSelected')[0];
                                var path = node.path;
                                if (node.parentId != 'undefined') {
                                    while (node.parentId != undefined) {
                                        console.log(node);
                                        node = $("#treeview2").treeview('getNode', node.parentId);
                                    }
                                }
                                var toCloud = node.text;
                                toCloud = toCloud.toUpperCase();
                                if (path != 'undefined') {
                                    var type = $('#' + selectedFileId).get(0).className;
                                    var name = $('#' + selectedFileId).get(0).innerText;
                                    console.log(name);
                                    if (cloud == 'COMPLEX') {
                                        var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
                                        var storageType = tds[tds.length - 1].innerText;
                                        cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
                                    }
                                    $.ajax({
                                        beforeSend: function () {
                                            $("#spinner_under_list").show();
                                        },
                                        complete: function () {
                                            $("#spinner_under_list").hide();
                                            //tableCreate();
                                        },
                                        url: '/user/cloud/relocatecopy',
                                        type: 'PUT',
                                        data: {
                                            'cloud': cloud,
                                            'clientId': clientId,
                                            'path': $("#" + selectedFileId).attr('fileId'),
                                            'name': name,
                                            'fileId': $("#" + selectedFileId).attr('fileId'),
                                            'toCloud': toCloud,
                                            'toPath': path,
                                            'type': type
                                        },
                                        success: function (data) {
                                            switch (data) {
                                                case 'ok':
                                                    bootbox.alert("<fmt:message key="cloud.scripts.copy_successful"></fmt:message>");
                                                    break;
                                                case 'err':
                                                    bootbox.alert("<fmt:message key="cloud.scripts.copy_filed"></fmt:message>");
                                                    break;
                                            }
                                        },
                                        error: function () {

                                        }
                                    });
                                }
                            } else {

                            }
                        }
                );
                setTreeViewData(dataF);
            }
        });
    }

    function moveFileTo(event) {
        event.preventDefault();
        getsetParameters();
        $.ajax({
            beforeSend: function () {
                $("#spinner_under_list").show();
            },
            complete: function () {
                $("#spinner_under_list").hide();
                //tableCreate();
            },
            url: "/fileTree",
            type: "get",
            data: {
                cloud: "COMPLEX"
            },
            success: function (dataF) {
                bootbox.confirm('<div id="treeview2" class="treeview" onload="setTreeViewData(treeData)"></div>',
                        function (result) {
                            if (result) {
                                var node = $("#treeview2").treeview('getSelected')[0];
                                var path = node.path;
                                if (node.parentId != 'undefined') {
                                    while (node.parentId != undefined) {
                                        console.log(node);
                                        node = $("#treeview2").treeview('getNode', node.parentId);
                                    }
                                }
                                var toCloud = node.text;
                                /*if(toCloud !== 'drive' && toCloud !== 'dropbox' || toCloud !== 'box'){
                                    clientId =
                                            toCloud = 'CLIENT';
                                }*/
                                toCloud = toCloud.toUpperCase();
                                if (path != 'undefined') {
                                    var type = $('#' + selectedFileId).get(0).className;
                                    var name = $('#' + selectedFileId).get(0).innerText;
                                    console.log(name);
                                    if (cloud == 'COMPLEX') {
                                        var tds = $('#' + selectedFileId).parent().parent().get(0).childNodes;
                                        var storageType = tds[tds.length - 1].innerText;
                                        cloud = storageType.substring(storageType.lastIndexOf(' ') + 1);
                                    }
                                    $.ajax({
                                        beforeSend: function () {
                                            $("#spinner_under_list").show();
                                        },
                                        complete: function () {
                                            $("#spinner_under_list").hide();
                                        },
                                        url: '/user/cloud/relocatecopy',
                                        type: 'post',
                                        data: {
                                            'cloud': cloud,
                                            'clientId': clientId,
                                            'path': $("#" + selectedFileId).attr('fileId'),
                                            'name': name,
                                            'fileId': $("#" + selectedFileId).attr('fileId'),
                                            'toCloud': toCloud,
                                            'toPath': path,
                                            'type': type
                                        },
                                        success: function (data) {
                                            switch (data) {
                                                case "ok":
                                                    bootbox.alert("<fmt:message key="cloud.scripts.move_success"></fmt:message>");
                                                    $("#" + selectedFileId).parent().parent().hide();
                                                    tableCreate();
                                                    break;
                                                case "err":
                                                    bootbox.alert("<fmt:message key="cloud.scripts.move_error"></fmt:message>");
                                                    break;
                                            }
                                        }
                                    });
                                }
                            } else {

                            }
                        }
                );
                setTreeViewData(dataF);
            }
        });
    }

    function setTreeViewData(standartData) {
        $('#treeview2').treeview({
            data: standartData,
            levels: 1
        });
        $('#treeview2').css("font-family", "helvetica");
    }

    function getsetParameters() {
        //event.preventDefault();
        currentPath = window.location.pathname;
        parameters = currentPath.split('/');
        cloud = parameters[4].toUpperCase();
        if (cloud != 'DROPBOX' && cloud != 'DRIVE' && cloud != 'COMPLEX' && cloud != 'BOX') {
            clientId = cloud.toLowerCase();
            cloud = 'CLIENT';
        }
        if (cloud == 'COMPLEX') {
            directoryCloud = parameters[4].toUpperCase();
            cloud = parameters[5].toUpperCase();
            if (cloud != 'DROPBOX' && cloud != 'DRIVE' && cloud != 'COMPLEX' && cloud != 'BOX') {
                clientId = cloud.toLowerCase();
                cloud = 'CLIENT';
            }
            parameters = parameters.slice(6, parameters.length);
        } else {
            parameters = parameters.slice(5, parameters.length);
        }
        pathToDirectory = parameters.join('/');
    }

    function checkNewName(name) {
    	var indycator=0;
        $("#example tr td").each(function () {
        	console.log(this.innerText);
            if (this.innerText == " "+name) {
            	indycator=1;
                return false;
            }
           
        }); 
        if(indycator==0){
        	return true;
        }
        return false;
    }

    function files() {
        getsetParameters();
        window.location.href = '/user/cloud/storage/' + ( directoryCloud === 'COMPLEX' ? 'complex/complex' : cloud === 'CLIENT' ? clientId : cloud.toLowerCase());
    }

    function selectChanged(e) {
        var selectors = $("select.form-control.uploadDiv");
        for (var i = 0; i < selectors.length; i++) {
            if (this != selectors[i]) {
                if (this.value === selectors[i].value) {
                    $(this).css("color", "#AA0000");
                    $(this).css("background-color", "#FFAAAA");
                    $(".btn.btn-primary")[0].disabled = true;
                    return;
                }
            }
        }
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).css("color", "#555555");
            $(selectors[i]).css("background-color", "white");
        }
        percentChanged();
    }

    function checkPartSizes() {
        var inputs = $("input.form-control.uploadDiv");
        var cloud;
        var partSize;
        var bool = true;
        for (var i = 0; i < inputs.length; i++) {
            cloud = inputs[i].parentElement.getElementsByTagName("select")[0].value;
            partSize = fileSize / 100 * inputs[i].value;
            if (cloud === "DROPBOX") {
                if (partSize <= freeSpace.dropbox) {
                    (bool = bool & true)
                } else {
                    bool = false
                };
            } else if (cloud === "BOX") {
                if (partSize <= freeSpace.box) {
                    (bool = bool & true)
                } else {
                    bool = false
                };
            } else if (cloud === "DRIVE") {
                if (partSize <= freeSpace.drive) {
                    (bool = bool & true)
                } else {
                    bool = false
                };
            }
            if (!bool) {
                $(inputs[i]).css("color", "#AA0000");
                $(inputs[i]).css("background-color", "#FFAAAA");
            }
        }
        return bool;
    }

    function percentChanged() {
        var inputs = $("input.form-control.uploadDiv");
        var sum = 0;
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].value === "" || inputs[i].value < 0 || inputs[i].value >= 100 || !checkPartSizes()) {
                $(".btn.btn-primary")[0].disabled = true;
                return;
            }
            sum += parseFloat(inputs[i].value);
        }
        if (sum == 100 && checkPartSizes()) {
            if (checkSelects() && checkPartSizes() == true) {
                $(".btn.btn-primary")[0].disabled = false;
            } else {
                $(".btn.btn-primary")[0].disabled = true;
            }
            for (var i = 0; i < inputs.length; i++) {
                $(inputs[i]).css("color", "#555555");
                $(inputs[i]).css("background-color", "white");
            }
        } else {
            $(".btn.btn-primary")[0].disabled = true;
            for (var i = 0; i < inputs.length; i++) {
                $(inputs[i]).css("color", "#AA0000");
                $(inputs[i]).css("background-color", "#FFAAAA");
            }
        }
    }

    function checkSelects() {
        var selectors = $("select.form-control.uploadDiv");
        for (var i = 0; i < selectors.length; i++) {
            for (var j = i + 1; j < selectors.length; j++) {
                if (selectors[i].value === selectors[j].value) {
                    return false;
                }
            }
        }
        return true;
    }

    function complex() {
        var lastElement = $("#uploadFileDiv")[0].lastChild;
        while (lastElement.tagName === "DIV") {
            lastElement.remove();
            lastElement = $("#uploadFileDiv")[0].lastChild;
        }
        if ($("#file")[0].files[0] != undefined) {
            fileSize = $("#file")[0].files[0].size;
            if ($("#sel1").val() === 'COMPLEX') {
                $(".btn.btn-primary")[0].disabled = true;
                var div = document.createElement("div");
                div.className = "filePartDiv";
                var select = document.createElement("select");
                select.className = "form-control";
                select.className += " uploadDiv";
                select.innerHTML = options;
                select.addEventListener("change", selectChanged);
                div.appendChild(select);
                var inputNumber = document.createElement("input");
                inputNumber.type = "number";
                inputNumber.className = "form-control";
                inputNumber.className += " uploadDiv";
                inputNumber.style = "width: 20%";
                inputNumber.min = 0;
                inputNumber.max = 100;
                inputNumber.addEventListener("change", percentChanged);
                div.appendChild(inputNumber);
                var label = document.createElement("label");
                label.setAttribute("for", inputNumber);
                label.innerHTML = "%";
                div.appendChild(label);
                $("#uploadFileDiv")[0].appendChild(div);
                var div1 = document.createElement("div");
                div1.className = "filePartDiv";
                var select = document.createElement("select");
                select.className = "form-control";
                select.className += " uploadDiv";
                select.innerHTML = options;
                select.selectedIndex = 1;
                select.addEventListener("change", selectChanged);
                div1.appendChild(select);
                var inputNumber = document.createElement("input");
                inputNumber.type = "number";
                inputNumber.className = "form-control";
                inputNumber.className += " uploadDiv";
                inputNumber.style = "width: 20%";
                inputNumber.min = 0;
                inputNumber.max = 100;
                inputNumber.addEventListener("change", percentChanged);
                div1.appendChild(inputNumber);
                var label = document.createElement("label");
                label.setAttribute("for", inputNumber);
                label.innerHTML = "%";
                div1.appendChild(label);
                $("#uploadFileDiv")[0].appendChild(div1);
                var count = $("#uploadFileDiv")[0].childNodes.length - 1;
                cloudCount = select.length;
                if (cloudCount > 3) {
                    cloudCount = 3
                }
                if (count <= cloudCount) {
                    var button = document.createElement("button");
                    button.id = "addRowButton";
                    button.className = "btn";
                    button.className += " btn-default";
                    button.innerText = "+";
                    button.addEventListener("click", addRow);
                    $("#uploadFileDiv")[0].appendChild(button);
                }
            } else {
                var lastElement = $("#uploadFileDiv")[0].lastChild;
                $(".btn.btn-primary")[0].disabled = false;
                while (lastElement.tagName === "DIV") {
                    lastElement.remove();
                    lastElement = $("#uploadFileDiv")[0].lastChild;
                }
            }
        } else {

        }
        clientDir();
    }

    function clientDir() {
        if ($("#sel1").val() === 'COMPLEX' || $("#sel1").val() === 'BOX' || $("#sel1").val() === 'DRIVE' || $("#sel1").val() === 'DROPBOX') {
            var sel3 = $("#sel3")[0];
            if (sel3 != undefined) {
                sel3.parentNode.removeChild(sel3);
            }
        } else {
            $.ajax({
                url: '/getClientPaths',
                type: 'post',
                data: {
                    clientId: $("#sel1").val()
                },
                success: function (data) {
                    jsonArray = JSON.parse(data);
                    var selectPaths = document.createElement("select");
                    selectPaths.className = "form-control";
                    selectPaths.id = "sel3";
                    selectPaths.style.width = "80%";
                    for (var i = 0; i < jsonArray.length; i++) {
                        var option = document.createElement("option");
                        option.value = jsonArray[i];
                        option.text = jsonArray[i];
                        selectPaths.add(option);
                    }
                    $("#uploadFileDiv")[0].appendChild(selectPaths);
                }
            });
        }
    }

    function addRow() {
        var div = document.createElement("div");
        div.className = "filePartDiv";
        var select = document.createElement("select");
        select.className = "form-control";
        select.className += " uploadDiv";
        select.innerHTML = options;
        select.addEventListener("change", selectChanged);
        div.appendChild(select);
        var inputNumber = document.createElement("input");
        inputNumber.type = "number";
        inputNumber.className = "form-control";
        inputNumber.className += " uploadDiv";
        inputNumber.style = "width: 20%";
        inputNumber.min = 0;
        inputNumber.max = 100;
        inputNumber.addEventListener("change", percentChanged);
        div.appendChild(inputNumber);
        var label = document.createElement("label");
        label.setAttribute("for", inputNumber);
        label.innerHTML = "%";
        div.appendChild(label);
        $("#uploadFileDiv")[0].insertBefore(div, $("#uploadFileDiv")[0].lastChild);
        var count = $("#uploadFileDiv")[0].childNodes.length - 2;
        if (count > cloudCount) {
            $("#addRowButton").hide();
        }
        percentChanged();
    }

    function createFolder() {
        //event.preventDefault();
        var selects = '<div id="uploadFileDiv">';
        if (cloud === 'COMPLEX') {
            selects += '<select class="form-control" id="sel1" onChange="clientDir()"  style="width: 100%">';
            $.ajax({
                beforeSend: function () {
                    $("#spinner_under_list").show();
                },
                complete: function () {
                    $("#spinner_under_list").hide();
                },
                url: "/user/active",
                type: "GET",
                data: {},
                success: function (data) {
                    var data = JSON.parse(data);
                    if (data['dropbox']) {
                        selects += '<option value="DROPBOX">Dropbox</option>';
                    }
                    if (data['box']) {
                        selects += '<option value="BOX">Box</option>';
                    }
                    if (data['drive']) {
                        selects += '<option value="DRIVE">Drive</option>';
                    }
                    if (data.clients != undefined) {
                        for (var i = 0; i < data.clients.length; i++) {
                            if (data.clients[i].status == true) {
                                selects += '<option  value="' + data.clients[i].clientId + '">' + data.clients[i].name + '</option>';
                            }
                        }
                    }
                    selects += "</select></div>";
                    bootbox.confirm("Cloud" + selects + '<br>Folder name<br> <input id="folderName" type="text" class="form-control">', function (result) {
                        if (result != null) {
                            if (result != '') {
                                var newName = $("#folderName").val();
                                if (!checkNewName(newName)) {
                                    $('.alert-success').hide();
                                    $('#errorCF').text("<fmt:message key="cloud.scripts.create_folder_error"></fmt:message>");
                                    $('.alert-danger').show();
                                    return;
                                }
                                var selected = $("#sel1").val();
                                if (selected === 'DRIVE' || selected === 'DROPBOX' || selected === 'BOX') {
                                    var cloudToSave = selected;
                                    var clientId = userId;
                                    var path = "/";
                                } else {
                                    var cloudToSave = 'CLIENT';
                                    var clientId = selected;
                                    var path = $("#sel3").val();
                                }
                                $.ajax({
                                    beforeSend: function () {
                                        $("#spinner_under_list").show();
                                    },
                                    complete: function () {
                                        $("#spinner_under_list").hide();
                                    },
                                    type: 'PUT',
                                    url: '/user/cloud/createrenamedownload',
                                    data: {
                                        'path': path,
                                        'cloud': cloudToSave,
                                        'clientId': clientId,
                                        'fileId': pathToDirectory,
                                        'newName': newName
                                    },
                                    success: function (data) {
                                        switch (data) {
                                            case 'created':
                                                bootbox.alert("<fmt:message key="cloud.scripts.success_folder"></fmt:message>");
                                                tableCreate();
                                                break;
                                            case 'blocked':
                                                bootbox.prompt({
                                                    title: "<fmt:message key="cloud.scripts.enter_code"></fmt:message>",
                                                    callback: function (result) {
                                                        if (result == null) {

                                                        } else {
                                                            var code = result;
                                                            $.ajax({
                                                                url: '/checkcode/',
                                                                type: 'POST',
                                                                data: {
                                                                    'code': code
                                                                },
                                                                success: function (response) {
                                                                    if (response == 'bad') {
                                                                        return createFolder();
                                                                    } else {
                                                                        return createFolder();
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                                break;
                                            case 'err':
                                                bootbox.alert("<fmt:message key="cloud.scripts.erro_folder"></fmt:message>");
                                                $('.alert-success').hide();
                                                $('.alert-danger').show();
                                                $('#errorCF').text("<fmt:message key="cloud.scripts.erro_folder"></fmt:message>");
                                                break;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }

            });
        } else {
            bootbox.prompt("Folder name", function (result) {
                if (result != null) {
                    if (result != '') {
                        var newName = result;
                        if (!checkNewName(newName)) {
                            bootbox.alert("<fmt:message key="cloud.scripts.create_folder_error"></fmt:message>");
                            $('.alert-success').hide();
                            $('#errorCF').text("<fmt:message key="cloud.scripts.create_folder_error"></fmt:message>");
                            $('.alert-danger').show();
                            return;
                        }
                        $.ajax({
                            beforeSend: function () {
                                $("#spinner_under_list").show();
                            },
                            complete: function () {
                                $("#spinner_under_list").hide();
                            },
                            type: 'PUT',
                            url: '/user/cloud/createrenamedownload',
                            data: {
                                'path': pathToDirectory,
                                'cloud': cloud,
                                'clientId': clientId,
                                'fileId': pathToDirectory,
                                'newName': newName
                            },
                            success: function (data) {
                                switch (data) {
                                    case 'created':
                                        bootbox.alert("<fmt:message key="cloud.scripts.success_folder"></fmt:message>");
                                        tableCreate();
                                        break;
                                    case 'blocked':
                                        bootbox.prompt({
                                            title: "<fmt:message key="cloud.scripts.enter_code"></fmt:message>",
                                            callback: function (result) {
                                                if (result == null) {

                                                } else {
                                                    var code = result;
                                                    $.ajax({
                                                        url: '/checkcode/',
                                                        type: 'POST',
                                                        data: {
                                                            'code': code
                                                        },
                                                        success: function (response) {
                                                            if (response == 'bad') {
                                                                return createFolder();
                                                            } else {
                                                                return createFolder();
                                                            }
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                        break;
                                    case 'err':
                                            bootbox.alert("<fmt:message key="cloud.scripts.erro_folder"></fmt:message>");
                                        $('.alert-success').hide();
                                        $('.alert-danger').show();
                                        $('#errorCF').text("<fmt:message key="cloud.scripts.erro_folder"></fmt:message>");
                                        break;
                                }
                            }
                        });
                    }
                }
            });
        }
    }

    $.fn.dataTableExt.oSort['type-name-asc'] = function (a, b) {
        var pA = $(a).find('p').prevObject[2];
        var pB = $(b).find('p').prevObject[2];
        var clA = pA.className;
        var clB = pB.className;
        var text1 = pA.textContent;
        var text2 = pB.textContent;
        var compare = text1.localeCompare(text2);
        if (clA === clB) {
            return compare;
        } else if (clA === 'fol' && clB === 'fil') {
            return -1;
        } else if (clA === 'fil' && clB === 'fol') {
            return 1;
        }
        return compare;
    };

    $.fn.dataTableExt.oSort['type-name-desc'] = function (a, b) {
        var pA = $(a).find('p').prevObject[2];
        var pB = $(b).find('p').prevObject[2];
        var clA = pA.className;
        var clB = pB.className;
        var text1 = pA.textContent;
        var text2 = pB.textContent;
        var compare = text2.localeCompare(text1);
        if (clA === clB) {
            return compare;
        } else if (clA === 'fol' && clB === 'fil') {
            return -1;
        } else if (clA === 'fil' && clB === 'fol') {
            return 1;
        }
        return compare;
    };

    $.fn.dataTableExt.oSort['file-size-asc'] = function (a, b) {
        var pA = $(a).find('span').prevObject[0];
        var pB = $(b).find('span').prevObject[0];
        var clA = pA.className;
        var clB = pB.className;
        var text1 = pA.textContent;
        var text2 = pB.textContent;

        var size1 = clA === 'fil' ? sizeTransformer(text1) : 0;
        var size2 = clB === 'fil' ? sizeTransformer(text2) : 0;
        var compare = size1 < size2 ? -1 : size1 === size2 ? 0 : 1;
        if (clA === clB) {
            return compare;
        } else if (clA === 'fol' && clB === 'fil') {
            return -1;
        } else if (clA === 'fil' && clB === 'fol') {
            return 1;
        }
        return compare;
    };

    $.fn.dataTableExt.oSort['file-size-desc'] = function (a, b) {
        var pA = $(a).find('span').prevObject[0];
        var pB = $(b).find('span').prevObject[0];
        var clA = pA.className;
        var clB = pB.className;
        var text1 = pA.textContent;
        var text2 = pB.textContent;

        var size1 = clA === 'fil' ? sizeTransformer(text1) : 0;
        var size2 = clB === 'fil' ? sizeTransformer(text2) : 0;
        var compare = size1 < size2 ? 1 : size1 === size2 ? 0 : -1;
        if (clA === clB) {
            return compare;
        } else if (clA === 'fol' && clB === 'fil') {
            return 1;
        } else if (clA === 'fil' && clB === 'fol') {
            return -1;
        }
        return compare;
    };

    function sizeTransformer(data) {
        var matches = data.match(/^(\d+(?:\.\d+)?)\s*([a-z]+)/i);
        var multipliers = {
            b: 1,
            kb: 1000,
            mb: 1000000,
            gb: 1000000000,
            tb: 1000000000000,
            pb: 1000000000000000
        };
        if(matches == null){
            return 0;
        }
        var multiplier = multipliers[matches[2].toLowerCase()];
        return parseFloat(matches[1]) * multiplier;
    }

    $.fn.dataTableExt.oSort['date-cloud-asc'] = function (a, b) {
        var dateCmp = function (x, y) {
            var pA = $(a).find('span').prevObject[0];
            var pB = $(b).find('span').prevObject[0];
            var clA = pA.className;
            var clB = pB.className;
            var text1 = pA.textContent;
            var text2 = pB.textContent;

            var d1 = text1 === '' ? Date.UTC() : strToDate(text1);
            var d2 = text2 === '' ? Date.UTC() : strToDate(text2);
            var compare = ((d1 < d2) ? -1 : ((d1 > d2) ? 1 : 0));
            if (clA === clB) {
                return compare;
            } else if (clA === 'fol' && clB === 'fil') {
                return -1;
            } else if (clA === 'fil' && clB === 'fol') {
                return 1;
            }
            return compare;
        }

        var cloudCmp = function (x, y) {
            var pA = $(a).find('span').prevObject[0];
            var pB = $(b).find('span').prevObject[0];
            var clA = pA.className;
            var clB = pB.className;
            var text1 = pA.textContent;
            var text2 = pB.textContent;

            var compare = text1.localeCompare(text2);
            if (clA === clB) {
                return compare;
            } else if (clA === 'fol' && clB === 'fil') {
                return -1;
            } else if (clA === 'fil' && clB === 'fol') {
                return 1;
            }
            return compare;
        }

        if (cloud === 'COMPLEX') {
            return cloudCmp(a, b);
        }
        return dateCmp(a, b);
    };

    $.fn.dataTableExt.oSort['date-cloud-desc'] = function (a, b) {
        var dateCmp = function (x, y) {
            var pA = $(a).find('span').prevObject[0];
            var pB = $(b).find('span').prevObject[0];
            var clA = pA.className;
            var clB = pB.className;
            var text1 = pA.textContent;
            var text2 = pB.textContent;

            var d1 = text1 === '' ? Date.UTC() : strToDate(text1);
            var d2 = text2 === '' ? Date.UTC() : strToDate(text2);
            var compare = ((d1 < d2) ? 1 : ((d1 > d2) ? -1 : 0));
            if (clA === clB) {
                return compare;
            } else if (clA === 'fol' && clB === 'fil') {
                return -1;
            } else if (clA === 'fil' && clB === 'fol') {
                return 1;
            }
            return compare;
        };

        var cloudCmp = function (x, y) {
            var pA = $(a).find('span').prevObject[0];
            var pB = $(b).find('span').prevObject[0];
            var clA = pA.className;
            var clB = pB.className;
            var text1 = pA.textContent;
            var text2 = pB.textContent;

            var compare = text2.localeCompare(text1);
            if (clA === clB) {
                return compare;
            } else if (clA === 'fol' && clB === 'fil') {
                return -1;
            } else if (clA === 'fil' && clB === 'fol') {
                return 1;
            }
            return compare;
        };

        if (cloud === 'COMPLEX') {
            return cloudCmp(a, b);
        }
        return dateCmp(a, b);
    };

    function strToDate(a) {
        var dateParts = a.split('.'),
                time = a.split(' ')[1].split(':'),
                year = parseInt(dateParts[2]) - 1900,
                month = parseInt(dateParts[1]) - 1,
                day = parseInt(dateParts[0]),
                hours = parseInt(time[0]),
                mins = parseInt(time[1]),
                secs = parseInt(time[2]);
        return Date.UTC(year, month, day, hours, mins, secs);
    }

    var table = $('#example').DataTable({
        "columns": [
            {"sType": "type-name"},
            //{"orderDataType":"type-name"},
            {"sType": "file-size"},
            {"sType": "date-cloud"}
        ],
        "order": [[0, "asc"]],
        "language": {
            search: "<fmt:message key="datatable.search"></fmt:message>",
            paginate: {
                first:      "<fmt:message key="datatable.first"></fmt:message>",
                previous:   "<fmt:message key="datatable.previous"></fmt:message>",
                next:       "<fmt:message key="datatable.next"></fmt:message>",
                last:       "<fmt:message key="datatable.last"></fmt:message>"
            },
            emptyTable:     "<fmt:message key="datatable.empty_table"></fmt:message>",
            lengthMenu:    "<fmt:message key="datatable.length_menu"></fmt:message>",
            info:           "<fmt:message key="datatable.info"></fmt:message>",
            infoEmpty:      "<fmt:message key="datatable.info_empty"></fmt:message>",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "<fmt:message key="datatable.loading_records"></fmt:message>",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher"
        }
    });

    $('#example').on('draw.dt', function () {
        //console.log('');
    });
    function tableCreate() {
        //preventDef();
        var fileType;
        table.clear();
        getsetParameters();
        currentPath = window.location.pathname;
        parameters = currentPath.split('/');
        var cl = parameters[4] == 'undefined' ? '' : parameters[4].toUpperCase();
        var cl1 = parameters[5] == undefined ? '' : parameters[5].toUpperCase();
        if (cl == 'COMPLEX' && cl1 == 'COMPLEX') {
            $('.dateOrCloud').text('<fmt:message key="cloud.scripts.cloud_text"></fmt:message>');
        } else {
            $('.dateOrCloud').text('<fmt:message key="cloud.scripts.last_mod"></fmt:message>');
        }
        if (cloud.toLowerCase() === 'client' && parameters.length == 5) {
            $('.btn-hide').hide();
        } else {
            $('.btn-hide').show();
        }

        $.ajax({
            beforeSend: function () {
                $('#rowWithTable').hide();
                $('#loaderTable').show();
            },
            complete: function () {
                console.log('complete');
                $('#loaderTable').hide();
                $('#rowWithTable').show();
            },
            type: 'GET',
            url: '/user/cloud/getdeleteupload',
            data: {
                'path': pathToDirectory,
                'cloud': cloud,
                //'directoryCloud': directoryCloud,
                'clientId': clientId
            },
            success: function (resp) {
                var directory = JSON.parse(resp);
                var clazz;
//                $('#fileId').val(directory[0].path);
                userId = directory[0] == undefined ? null : directory[0].userId;
                var count = 0;
                directory.forEach(function (value) {
                	
                    var nameEl;
                    var type = value.type;
                    if (value.type == 'application/vnd.google-apps.folder' || value.type == 'folder') {
                        type = 'folder';
                        fileType = '<i class="fa fa-folder fa-lg"></i> ';
                        clazz = 'fol" data-toggle="context" data-target="#context-menu-fol" oncontextmenu="setSelectedFileId(this.id)"';
                        nameEl = 'a';
                        if ((cloud.toLowerCase() == 'complex' && parameters.length == 6 && value.storageType.toLowerCase() == 'null')
                                || (cloud.toLowerCase() !== 'complex' && parameters.length == 5 && value.storageType.toLowerCase() == 'null')) {
                            clazz = 'fol"';
                        }
                    } else {
                        type = '';
                        nameEl = 'p style="display:inline;"';
                        fileType = '<i class="fa fa-file-o fa-lg"></i> ';
                        clazz = 'fil" data-toggle="context" data-target="#context-menu-fil" oncontextmenu="setSelectedFileId(this.id)"';
                        if (cloud.toLowerCase() === 'complex' && value.storageType.toLowerCase() === 'complex') {
                            clazz = 'fil" data-toggle="context" data-target="#context-menu-complex" oncontextmenu="setSelectedFileId(this.id)"';
                        }
                    }
                    if (type != 'folder') {
                        type = '';
                    }
                    var space_ = '';
                    if (cloud.toLowerCase() == 'dropbox') {
                        space_ = '';
                    } else {
                        space_ = '/';
                    }
                    var path;
                    var p;
                    var storageType;
                    var cloudIco = '<i class="fa fa-cloud fa-lg"></i> ';
                    var clientIco = '<i class="fa fa-desktop fa-lg"></i> ';
                    switch (cloud.toLowerCase()) {
                        case 'drive':
                            p = '/user/cloud/storage/drive';
                            path = value.fileId;
                            storageType = cloudIco + value.storageType.toLowerCase();
                            if(parameters[4] == 'complex'){
                                p = '/user/cloud/storage/complex/drive'
                            }
                            if (value.lastChange == 'null') {
                                storageType = '';
                            } else {
                                storageType = value.lastChange;
                            }
                            break;
                        case 'dropbox':
                            p = '/user/cloud/storage/dropbox';
                            if(parameters[4] == 'complex'){
                                p = '/user/cloud/storage/complex/dropbox'
                            }
                            path = value.path;
                            storageType = cloudIco + value.storageType.toLowerCase();
                            if (value.lastChange == 'null') {
                                storageType = '';
                            } else {
                                storageType = value.lastChange;
                            }
                            break;
                        case 'box':
                            p = '/user/cloud/storage/box';
                            if(parameters[4] == 'complex'){
                                p = '/user/cloud/storage/complex/box'
                            }
                            path = value.fileId;
                            storageType = cloudIco + value.storageType.toLowerCase();
                            if (value.lastChange == 'null') {
                                storageType = '';
                            } else {
                                storageType = value.lastChange;
                            }
                            break;
                        case 'complex':
                            p = '/user/cloud/storage/complex/' + value.storageType.toLowerCase();
                            path = (value.storageType == 'BOX' || value.storageType == 'DRIVE') ? value.fileId : value.path;
                            space_ = value.storageType == 'DROPBOX' ? '' : '/';
                            storageType = value.storageType.toLowerCase();
                            if (storageType !== 'drive' && storageType !== 'dropbox' && storageType !== 'box' && storageType !== 'complex') {
                                storageType = clientIco + value.fileId;  //name of user's pc
                                p = '/user/cloud/storage/complex/' + value.clientId;
                            } else {
                                storageType = cloudIco + value.storageType.toLowerCase();
                            }
                            break;
                        default :
                            p = '/user/cloud/storage/' + clientId;
                            if(parameters[4] == 'complex'){
                                p = '/user/cloud/storage/complex/'+clientId;
                            }
                            path = value.path;
                            if (value.lastChange == 'null') {
                                storageType = '';
                            } else {
                                storageType = value.lastChange;
                            }
                    }
                    table.row.add(
                            [
                                fileType + '<' + nameEl + ' href="' + p + space_ + path + '"' + 'fileId="' + path + '"class="' + clazz + ' id="fileId' + count + '" >'
                                + value.name
                                + '</' + nameEl + '>',
                                '<span class="' + clazz + '">' + displaySize(value.size) + '</span>',
                                '<span class="' + clazz + '">' + storageType + '</span>'
                            ]).draw(false);
                    count++;
                });
//                $('.loader').hide();
            }
        });
    }

    function sortTableToType() {
        var start = new Date();
        var rows = $('tbody [role="row"]');
        var table = $('tbody')[0];
        rows.sort(folFIlcomparator);
        rows.remove();
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            table.appendChild(row);
        }
        console.log(new Date().getTime() - start.getTime());
    }

    function folFIlcomparator(a, b) {
        var p1 = $(a).find('p');
        var p2 = $(b).find('p');
        var cl1 = p1.classList[0];
        var cl2 = p2.classList[0];
        if (cl1 === 'fil' && cl2 === 'fol') {
            return 1;
        } else if (cl1 === 'fol' && cl2 === 'fil') {
            return -1;
        } else return 0;
    }

    $(document).ready(function () {
        tableCreate();
        //sortTableToType();
    });
</script>
	<script src="/js/SizeDisplayer.js"></script>
	<script src="/js/bootstrap-contextmenu.js"></script>
</body>
</html>