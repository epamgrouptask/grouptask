<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" />
<!DOCTYPE html lang=${language}>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="/img/cloud.ico">

<title>Clouds Merger</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="/css/jquery-ui.css">

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script
	src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<meta name="google-signin-client_id"
	content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async
	defer>

    </script>
</head>
<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/home">Clouds Merger</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li style="margin-top: -0.6rem";><a href="home"><i
							class="fa fa-home fa-2x"></i></a></li>
					<li><a href="#" id="signOutSocial" class="portfolio-link"><fmt:message
								key="user_home.log_out" /></a></li>
					<li>
						<form>
							<select class="form-control" id="language" name="language"
								onchange="submit()" style="margin-top: 6px">
								<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
								<option value="uk_UA" ${language == 'uk_UA' ? 'selected' : ''}>Українська</option>
							</select>
						</form>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>

	<div id="blue_profile">
		<div class="container">
			<div class="row centered">
				<div class="col-lg-8 col-lg-offset-2"></div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- blue wrap -->

	<div class="container w">
		<div class="row marg">
			<div class="col-sm-4 col-sm-offset-1 col-xs-12">
				<div class="row centered marg">
					<h2 class="title">
						<fmt:message key="profile.header"></fmt:message>
					</h2>
					<table>
						<tr>
							<th><fmt:message key="profile.fullname"></fmt:message></th>
							<td>${user.fullname }</td>
							<td></td>
						</tr>
						<tr>
							<th><fmt:message key="profile.username"></fmt:message></th>
							<td>${user.userName }</td>
						</tr>
						<tr>
							<th>Email</th>
							<td>${user.email }</td>
						</tr>
					</table>

				</div>
				<div class="row marg">
					<button type="button" class="btn my1" style="width: 100%;"
						onclick="showPassChange()">
						<fmt:message key="profile.change_password"></fmt:message>
					</button>
				</div>
				<div class="row marg">
					<button type="button" class="btn my2" style="width: 100%;"
						onclick="editData()">
						<fmt:message key="profile.edit_info"></fmt:message>
					</button>
				</div>
			</div>
			<!-- <div class="row"></div> -->
			<div class="row marg">
				<div class="row centered">
					<div class="col-sm-4 col-sm-offset-1 col-xs-12">

						<h2 class="title">
							<fmt:message key="profile.social_networks"></fmt:message>
						</h2>

						<div class="row marg"></div>
						<c:choose>
							<c:when test="${user.vkId == null || user.vkId == ''}">
								<button id="login_button" type="button" class="btn myVK"
									onclick="VK.Auth.login(authInfo);"
									style="width: 100% !important;">
									<fmt:message key="profile.link_vk"></fmt:message>
								</button>
							</c:when>
							<c:otherwise>
								<%--test="${user.vkId != null}"--%>
								<button id="unlinkVK" type="button" class="btn myVK"
									style="width: 100%; margin-top: 1em;" value="Link VK">
									<fmt:message key="profile.unlink_vk"></fmt:message>
								</button>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-sm-4 col-sm-offset-1 col-xs-12">
						<c:choose>
							<c:when test="${user.googleId == null || user.googleId == ''}">
								<div class="btn g-signin2" data-onsuccess="onSignIn"
									style="width: 100%; margin-top: 1em;">
									<fmt:message key="user_home.login_g+"></fmt:message>
								</div>
							</c:when>
							<c:otherwise>
								<%--test="${user.googleId != null}"--%>
								<button id="unlinkGooglePlus" type="button" class="btn my1"
									style="width: 100%; margin-top: 1em;">
									<fmt:message key="profile.unlink_g+"></fmt:message>
								</button>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- PRICING SECTION -->
	<div id="dg">
		<div class="container">
			<div class="row centered">
				<h4 style="font-size: 30px">
					<fmt:message key="user_home.profile"></fmt:message>
				</h4>
				<br>
				<div id="clientStorages"></div>

			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- DG -->

	<div class="container">
		<h4 style="font-size: 30px; text-align: center">
			<fmt:message key="profile.diagrams.header"></fmt:message>
		</h4>
		<div class="row">
			<div class="col-lg-3">
				<label><fmt:message key="profile.diagrams.from"></fmt:message>
					<input id="date1" class="form-control date datepicker" type="text"
					style="display: inline-block; width: 200px;"></label>
			</div>
			<div class="col-lg-3">
				<label><fmt:message key="profile.diagrams.to"></fmt:message>
					<input id="date2" class="form-control date datepicker" type="text"
					style="display: inline-block; width: 200px;"></label>
			</div>
			<div class="col-lg-6">
				<label class="radio-inline"><input type="radio"
					name="optradio" id="pie" checked> <fmt:message
						key="profile.diagrams.pie"></fmt:message> </label> <label
					class="radio-inline"><input type="radio" name="optradio"
					id="column"> <fmt:message key="profile.diagrams.column"></fmt:message>
				</label>
				<button class="btn btn-info" onclick="drawChart()"
					style="margin-left: 2rem; width: 10rem;">
					<fmt:message key="profile.diagrams.show"></fmt:message>
				</button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-5" id="donutchart_box"
				style="height: 50%; width: 50%; display: none"></div>
			<div class="col-lg-5" id="donutchart_dropbox"
				style="height: 50%; width: 50%; display: none"></div>
			<div class="col-lg-5" id="donutchart_drive"
				style="height: 50%; width: 50%; display: none"></div>
			<div class="col-lg-5" id="donutchart_complex"
				style="height: 50%; width: 50%; display: none"></div>
			<div class="col-lg-5" id="donutchart_client"
				style="height: 50%; width: 50%; display: none"></div>
			<div class="col-lg-5" id="donutchart_all"
				style="height: 50%; width: 50%; display: none"></div>
			<div class="col-sm-12 col-lg-5 columnChart" id="divBox"
				style="display: none">
				<b
					style="margin: auto; display: block; width: 0%; font-size: 20px; margin-bottom: 10px;">Box</b>
				<canvas id="barchart_box" width="487" height="390"
					style="display: none"></canvas>
			</div>
			<div class="col-sm-12 col-lg-5 columnChart" id="divDropbox"
				style="display: none">
				<b
					style="margin: auto; display: block; width: 0%; font-size: 20px; margin-bottom: 10px;">Dropbox</b>
				<canvas id="barchart_dropbox" width="487" height="390"
					style="display: none"></canvas>
			</div>
			<div class="col-sm-12 col-lg-5 columnChart" id="divDrive"
				style="display: none">
				<b
					style="margin: auto; display: block; width: 0%; font-size: 20px; margin-bottom: 10px;">Drive</b>
				<canvas id="barchart_drive" width="487" height="390"
					style="display: none"></canvas>
			</div>
			<div class="col-sm-12 col-lg-5 columnChart" id="divComplex"
				style="display: none">
				<b
					style="margin: auto; display: block; width: 0%; font-size: 20px; margin-bottom: 10px;">Complex</b>
				<canvas id="barchart_complex" width="487" height="390"
					style="display: none"></canvas>
			</div>
			<div class="col-sm-12 col-lg-5 columnChart" id="divClient"
				style="display: none">
				<b
					style="margin: auto; display: block; width: 0%; font-size: 20px; margin-bottom: 10px;">Client</b>
				<canvas id="barchart_client" width="487" height="390"
					style="display: none"></canvas>
			</div>
			<div class="col-sm-12 col-lg-5 columnChart" id="divAll"
				style="display: none">
				<b
					style="margin: auto; display: block; width: 0%; font-size: 20px; margin-bottom: 10px;">All</b>
				<canvas id="barchart_all" width="487" height="390"
					style="display: none"></canvas>
			</div>
		</div>
	</div>

	<div id="r">
		<div class="container">
			<div class="row centered">
				<div class="col-lg-8 col-lg-offset-2">
					<%-- <h4>WE ARE STORYTELLERS. BRANDS ARE OUR SUBJECTS. DESIGN IS
                    OUR VOICE.</h4>--%>

					<p><fmt:message key="mainpage.footer"></fmt:message> </p>
				</div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- r wrap -->


	<div class="modal fade" id="editProfile" role="dialog">
		<div class="modal-dialog" style="width: 60vw;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3>
						<fmt:message key="profile.edit_info"></fmt:message>
					</h3>
					<input type="hidden" value="${user.id }" id="userId">
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h3 class="thin text-center">
								<fmt:message key="profile.edit_info"></fmt:message>
							</h3>
							<div id="personalInfo">
								<div class="top-margin  form-group has-feedback" id="fullNE">
									<label><fmt:message key="profile.fullname"></fmt:message>
									</label> <input type="text" class="form-control" id="fullname"
										value="${user.fullname }">
								</div>
								<div class="top-margin  form-group has-feedback" id="userNE">
									<label><fmt:message key="profile.username"></fmt:message>
									</label> <input type="text" class="form-control" id="username"
										value="${user.userName }">
								</div>
								<div class="top-margin form-group has-feedback" id="emailE">
									<label>Email</label> <input type="email" class="form-control"
										id="Email" value="${user.email }">
								</div>
								<hr>
								<div class="row">
									<div class="col-lg-4 text-right">
										<button class="btn btn-action" type="button"
											style="background: yellowgreen;" onclick="sendConfirmCode()"
											id="change">
											<fmt:message key="profile.save"></fmt:message>
										</button>
									</div>
								</div>
							</div>
							<div id="conf">
								<div class="row top-margin  form-group has-feedback" id="codeE">
									<label>Confirmation code <span class="text-danger">*</span></label>
									<input type="password" id="confCode" class="form-control">
								</div>
								<hr>
								<div class="row">
									<div class="col-lg-4 text-right">
										<button class="btn btn-action" type="button"
											style="background: yellowgreen;" onclick="sendRegisterData()"
											id="codeSend">
											<fmt:message key="profile.save"></fmt:message>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="changingPass" role="dialog">
		<div class="modal-dialog" style="width: 60vw;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3>
						<fmt:message key="profile.change_password"></fmt:message>
					</h3>
					<input type="hidden" value="${user.id }" id="userId">
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<h3 class="thin text-center">
								<fmt:message key="profile.edit_info"></fmt:message>
							</h3>

							<div class="top-margin form-group has-feedback" id="oldP">
								<label><fmt:message key="profile.old_password"></fmt:message>
								</label> <input type="password" class="form-control" id="oldPass">
							</div>
							<div class="top-margin form-group has-feedback" id="newP">
								<label><fmt:message key="profile.new_password"></fmt:message>
								</label> <input type="password" class="form-control" id="pass">
							</div>
							<div class="top-margin form-group has-feedback" id="newCP">
								<label><fmt:message key="profile.confirm_new_password"></fmt:message>
								</label> <input type="password" class="form-control" id="cPassword">
							</div>
							<div class="row">
								<div class="col-lg-4 text-right">
									<button class="btn btn-action" type="button"
										style="background: yellowgreen; margin: 20px;"
										onclick="changePass()" id="send">
										<fmt:message key="profile.change_password"></fmt:message>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- MODAL FOR CONTACT -->
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">contact us</h4>
				</div>
				<div class="modal-body">
					<div class="row centered">
						<p>We are available 24/7, so don't hesitate to contact us.</p>

						<p>
							Somestreet Ave, 987<br /> London, UK.<br /> +44 8948-4343<br />
							hi@blacktie.co
						</p>

						<div id="mapwrap">
							<iframe height="300" width="100%" frameborder="0" scrolling="no"
								marginheight="0" marginwidth="0"
								src="https://www.google.es/maps?t=m&amp;ie=UTF8&amp;ll=52.752693,22.791016&amp;spn=67.34552,156.972656&amp;z=2&amp;output=embed"></iframe>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Save
						& Go</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="/js/jquery-ui.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript"
		src="https://www.gstatic.com/charts/loader.js"></script>
	<script src="/js/Chart.js"></script>
	<script src="/js/bootbox.min.js"></script>
	<script src="/js/signOutSocial.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/socialLogin.js"></script>
	<script src="/js/linkSocial.js"></script>
	<%--<script src="/js/unlinkGoogleDrive.js"></script>--%>
	<script src="/js/unlinkSocialScript.js"></script>
	<%--<script src="/js/linkBox.js"></script>--%>
	<script type="text/javascript">
											function sendConfirmCode(){
												$.ajax({
										            url: "/login",
										            type: "GET",
										            data:{
										            	email: $('#Email').val(),
										            	username: $('#username').val()
										            },
										            success: function(response){
										            	switch(response){
										            	case 'nameFail':
									                        $('#userNE')[0].setAttribute('class', 'has-error');
									                        $('#change').attr("disabled", "disabled");
									                        break;
									                    case 'emailFail':
									                        $('#emailE')[0].setAttribute('class', 'has-error');
									                        $('#change').attr("disabled", "disabled");
									                        break;
									                    case 'success':
									                    	$( "#personalInfo" ).hide();
											            	$( "#conf" ).show();
											            	break;
										            	}
										            	

										            }
										        });
											}
										</script>
	<script type="text/javascript">
    function getActiveStorages() {
        $
                .ajax({
                    url: "/user/active",
                    type: "GET",
                    data: {},
                    success: function (data) {
                        var dropboxButton;
                        var driveButton;
                        var boxButton;
                        var data = JSON.parse(data);
                        if (data['dropbox']) {
                            dropboxButton = '<button id="unlinkDropbox" onclick="unlinkDropbox()" class="pricing-signup" style="width: 100%"><fmt:message key="profile.unlink"></fmt:message></button>';
                        } else {
                            dropboxButton = '<button id="linkDropbox" onclick="linkDropbox()" class="pricing-signup" style="width: 100%"><fmt:message key="profile.link"></fmt:message></button>';
                        }
                        if (data['box']) {
                            boxButton = '<button id="unlinkBox" class="pricing-signup" onclick="unlinkBox()" style="width: 100%"><fmt:message key="profile.unlink"></fmt:message></button>';
                        } else {
                            boxButton = '<button id="boxLink" class="pricing-signup" onclick="linkBoxCloud()" style="width: 100%"><fmt:message key="profile.link"></fmt:message></button>';
                        }
                        if (data['drive']) {
                            driveButton = '<button id="unlinkDrive" onclick="unlinkDrive()" class="pricing-signup" style="width: 100%"><fmt:message key="profile.unlink"></fmt:message> </button>';
                        } else {
                            driveButton = '<button id="linkDrive" onclick="linkDrive()" class="pricing-signup" style="width: 100%"><fmt:message key="profile.link"></fmt:message></button>';
                        }
                        var box = '<div class="col-xs-12 col-sm-6 col-lg-4"><div class="pricing-option"><div class="pricing-top">'
                                + '<span class="pricing-edition">BOX</span><div class="row"><img alt="" src="/img/cloud.png" style="height: 210px">'
                                + '</div></div><ul style="height: 9em; border-bottom: none;"><li style="border-bottom: none;"><fmt:message key="profile.box"></fmt:message> </li></ul>'
                                + boxButton + '</div></div>';
                        var drive = '<div class="col-xs-12 col-sm-6 col-lg-4"><div class="pricing-option"><div class="pricing-top">'
                                + '<span class="pricing-edition">Google drive </span><div class="row"><img alt="" src="/img/googledrive.png" style="height: 210px">'
                                + '</div></div><ul style="height: 9em;"><li style="border-bottom: none;"><fmt:message key="profile.google_drive"></fmt:message></li>'
                                + '</ul>' + driveButton + '</div></div>';
                        var dropbox = '<div class="col-xs-12 col-sm-6 col-lg-4"><div class="pricing-option"><div class="pricing-top">'
                                + '<span class="pricing-edition">DROPBOX</span><div class="row"><img alt="" src="/img/drop.png" style="height: 210px">'
                                + '</div></div><ul style="height: 9em; border-bottom: none;"><li style="border-bottom: none;"><fmt:message key="profile.dropbox"></fmt:message></li>'
                                + '</ul>' + dropboxButton + '</div></div>';
                        $('#clientStorages').append(dropbox);
                        $('#clientStorages').append(box);
                        $('#clientStorages').append(drive);
                    }
                });
    }
    function editData() {
        $("#editProfile").modal('show');
    }

    $("#linkDropbox").click(linkDropbox);
    function linkDropbox(){
        $.ajax({
            url: "/linkDropbox",
            type: "GET",
            success: function(response){
                window.open(response, "_self");
            }
        });
    }

    $("#linkDrive").click(linkDrive);
    function linkDrive(){
        window.open("https://accounts.google.com/o/oauth2/v2/auth?scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fdrive&response_type=code&client_id=452446586683-r37qjcd14vsn1trcatt7as5133ebgesf.apps.googleusercontent.com&redirect_uri=http://localhost:8080/linkGoogle&access_type=offline", "_self");
    }

    function sendRegisterData() {
    	$.ajax({
            url: "/user/profile",
            type: "POST",
            data: {
                username: $('#username').val(),
                id: $('#userId').val(),
                password: $('#password').val(),
                email: $('#Email').val(),
                fullname: $('#fullname').val(),
                code: $('#confCode').val()
            },
            success: function (data) {
                switch (data) {
                    case 'ok':
                        window.location.reload(true);
                        break;
                    case 'nameFail':
                        $('#userNE')[0].setAttribute('class', 'has-error');
                        $('#change').attr("disabled", "disabled");
                        break;
                    case 'emailFail':
                        $('#emailE')[0].setAttribute('class', 'has-error');
                        $('#change').attr("disabled", "disabled");
                        break;
                    case 'codeFail':
                        $('#codeE')[0].setAttribute('class', 'has-error');
                        $('#codeSend').attr("disabled", "disabled");
                        break;
                }
            }
        });
    }

    function showPassChange() {
        $("#changingPass").modal('show');
    }
    $('#pass').keyup(function () {
        var pass = $('#pass').val();
        var confirm = $('#cPassword').val();
        if (confirm !== "") {
            if (pass !== confirm) {
                $('#newP')[0].setAttribute('class', 'has-error');
                $('#newCP')[0].setAttribute('class', 'has-error');
                $('#send').attr("disabled", "disabled");
            } else {
                $('#newP')[0].setAttribute('class', 'has-success');
                $('#newCP')[0].setAttribute('class', 'has-success');
                $('#send').removeAttr("disabled");
            }
        }
    });
    $('#cPassword').keyup(function () {
        var pass = $('#pass').val();
        var confirm = $('#cPassword').val();
        if (pass !== confirm) {
            $('#newP')[0].setAttribute('class', 'has-error');
            $('#newCP')[0].setAttribute('class', 'has-error');
            $('#send').attr("disabled", "disabled");
        } else {
            $('#newP')[0].setAttribute('class', 'has-success');
            $('#newCP')[0].setAttribute('class', 'has-success');
            $('#send').removeAttr("disabled");
        }
    });
    
    $('#oldPass').keyup(function () {
        var pass = $('#oldPass').val();
        if (pass == "") {
            $('#oldP')[0].setAttribute('class', 'has-error');
            $('#send').attr("disabled", "disabled");
        } else {
            $('#oldP')[0].setAttribute('class', 'has-success');
            $('#send').removeAttr("disabled");
        }
    });
    $('#confCode').keyup(function () {
        var pass = $('#confCode').val();
        if (pass == "") {
            $('#codeE')[0].setAttribute('class', 'has-error');
            $('#codeSend').attr("disabled", "disabled");
        } else {
            $('#codeE')[0].setAttribute('class', 'has-success');
            $('#codeSend').removeAttr("disabled");
        }
    });
    function changePass() {
        $.ajax({
            url: "/user/profile",
            type: "POST",
            data: {
                password: $('#oldPass').val(),
                newPass: $('#pass').val(),
                id: $('#userId').val()
            },
            success: function (data) {
                switch (data) {
				    case 'okPass':
                        $('#changingPass').modal('toggle');
                        break;
                    case 'failPass':
                        $('#oldP')[0].setAttribute('class', 'has-error');
                        $('#send').removeAttr("disabled");
                        break;
                }
            }
        });
    }
    window.onload = function () {
    	$( "#conf" ).hide();
        getActiveStorages();
        var el = ['#fullname', '#username','#confCode'];
        el.forEach(function (item, el) {
            emptyChecker(item);
        }); 
        
        var val = $('#Email').val();
        if (val == '') {
            $('#emailE')[0].setAttribute('class', 'has-error');
            $('#send').attr("disabled", "disabled");
        } else {
            $('#emailE')[0].setAttribute('class', 'has-success');
            $('#send').removeAttr("disabled");
        }
        val = $('#cPassword').val();
        if (val == '') {
            $('#newCP')[0].setAttribute('class', 'has-error');
            $('#send').attr("disabled", "disabled");
        } else {
            $('#newCP')[0].setAttribute('class', 'has-success');
            $('#send').removeAttr("disabled");
        }
        val = $('#pass').val();
        if (val == '') {
            $('#newP')[0].setAttribute('class', 'has-error');
            $('#send').attr("disabled", "disabled");
        } else {
            $('#newP')[0].setAttribute('class', 'has-success');
            $('#send').removeAttr("disabled");
        }
        val = $('#oldPass').val();
        if (val == '') {
            $('#oldP')[0].setAttribute('class', 'has-error');
            $('#send').attr("disabled", "disabled");
        } else {
            $('#oldP')[0].setAttribute('class', 'has-success');
            $('#send').removeAttr("disabled");
        }
        $( "#" ).hide();
        $('.abcRioButton.abcRioButtonLightBlue').removeAttr('style');

        $('.abcRioButton.abcRioButtonLightBlue').css({
            'background-color': 'RGB(177, 92, 131)',
            'color': 'white',
            'height': '30px',
            'width': '100%'
        });
        $('.abcRioButtonContents').text('<fmt:message key="user_home.script.link_G+"></fmt:message>');
    }
    /* Function to edit profile info */
   function emptyChecker(id) {
        var parentDivId;
        switch (id) {
            case '#fullname':
                parentDivId = '#fullNE';
                break;
            case '#username':
                parentDivId = '#userNE';
                break;
            case '#confCode':
				parentDivId='#codeE';
				break;
        }
        var val = $(id).val();
        if (val == '') {
            $(parentDivId)[0].setAttribute('class', 'has-error');
            $('#change').attr("disabled", "disabled");
        } else {
            $(parentDivId)[0].setAttribute('class', 'has-success');
            $('#change').removeAttr("disabled");
        }
        $(id).keyup(function () {
            var val = $(id).val();
            if (val == '') {
                $(parentDivId)[0].setAttribute('class', 'has-error');
                $('#change').attr("disabled", "disabled");
            } else {
                $(parentDivId)[0].setAttribute('class', 'has-success');
                $('#change').removeAttr("disabled");
            }
        });
    } 
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    $('#Email').keyup(function () {
        var email = $('#Email').val();
        if (email == "" || !validateEmail(email)) {
            $('#emailE')[0].setAttribute('class', 'has-error');
            $('#change').attr("disabled", "disabled");
        } else {
            $('#emailE')[0].setAttribute('class', 'has-success');
            $('#change').removeAttr("disabled");
        }
    });
</script>
	<script>
    function linkBoxCloud() {
        window.open('https://app.box.com/api/oauth2/authorize?response_type=code&client_id=b0s23iynxyvxh8ntspe65jl352chum8z&state=security_token%3DKnhMJatFipTAnM0nHlZA', 'link BOX');
        bootbox.prompt({
            title: "<fmt:message key="profile.script.enter_link"></fmt:message>",
            callback: function (result) {
                if (result == null) {

                } else {
                    var link = result;
                    $.ajax({
                        url: '/linkBox',
                        type: 'get',
                        data: {
                            request: link
                        },
                        success: function (response) {
                            $.ajax({
                                url: "/linkBox",
                                type: "POST",
                                success: function (response) {
                                    response = JSON.parse(response);
                                    if (response['redirect'] !== undefined) {
                                        $('#boxLink').attr('id','unlinkBox');
                                        $('#unlinkBox').attr('onclick','unlinkBox()');
                                        $('#unlinkBox').text('<fmt:message key="profile.unlink"></fmt:message>');
                                        //window.location.href = response['redirect'];
                                    }
                                }
                            });
                        }
                    });
                }

            }
        });
    }

    $("#boxLink").click(linkBoxCloud);
</script>
	<script>
    $("#unlinkBox").click(unlinkBox);

    function sendUnlinkBox(response) {
        $.ajax({
            url: "/unlinkBox",
            type: "GET",
            success: function (response) {
                var clientId = JSON.parse(response).clientId;
                var clientSecret = JSON.parse(response).clientSecret;
                var refreshToken = JSON.parse(response).refreshToken;
                $.ajax({
                    url: "https://www.box.com/api/oauth2/revoke",
                    type: "POST",
                    data: {
                        "client_id": clientId,
                        "client_secret": clientSecret,
                        "token": refreshToken
                    }
                });
                $('#unlinkBox').attr('id','boxLink');
                $('#boxLink').attr('onclick','linkBoxCloud()');
                $('#boxLink').text('<fmt:message key="profile.link"></fmt:message>');
                //window.location.reload();
            }
        });
    }

    function unlinkBox() {
        $.ajax({
            url: "/unlinkBox",
            type: "POST",
            success: function (response) {
                var parsResp = JSON.parse(response);
                if (parsResp.length === 0) {
                    sendUnlinkBox(response);
                } else {
                    var html = "<div><strong><p><fmt:message key="profile.script.unlinkBox"></fmt:message></p></strong><hr>";
                    for (var i = 0; i < parsResp.length; i++) {
                        var name = parsResp[i].name;
                        html += "<div class=\"row\"><div class=\"col-lg-8\"><label>" + name + "</label></div>" + "<div class=\"col-lg-4\"><a href='/user/cloud/createrenamedownload?name=" + name + "&cloud=COMPLEX'><fmt:message key="profile.script.download"></fmt:message> </a></div></div>";
                    }
                    html += "</div>";
                    bootbox.confirm(html, function (result) {
                        if (result == true) {
                            sendUnlinkBox(response);
                        } else {
                            //window.location.reload();
                        }
                    });
                }
            }
        });
    }
</script>
	<script>
    $("#unlinkDrive").click(unlinkDrive);

    function sendUnlinkDrive() {

        $.ajax({
            url: "/unlinkDrive",
            type: "GET",
            success: function (refreshToken) {
                $.get("https://accounts.google.com/o/oauth2/revoke?token=" + refreshToken);
                $('#unlinkDrive').attr('id','linkDrive');
                $('#linkDrive').attr('onclick','linkDrive()');
                $('#linkDrive').text('<fmt:message key="profile.link"></fmt:message>');
            }
        });
    }

    function unlinkDrive() {
        $.ajax({
            url: "/unlinkDrive",
            type: "POST",
            success: function (response) {
                var parsResp = JSON.parse(response);
                if (parsResp.length === 0) {
                    sendUnlinkDrive();
                } else {
                    var html = "<div><strong><p><fmt:message key="profile.script.unlinkBox"></fmt:message></p></strong><hr>";
                    for (var i = 0; i < parsResp.length; i++) {
                        var name = parsResp[i].name;
                        html += "<div class=\"row\"><div class=\"col-lg-8\"><label>" + name + "</label></div>" + "<div class=\"col-lg-4\"><a href='/user/cloud/createrenamedownload?name=" + name + "&cloud=COMPLEX'><fmt:message key="profile.script.download"></fmt:message></a></div></div>";
                    }
                    html += "</div>";
                    bootbox.confirm(html, function (result) {
                        if (result == true) {
                            sendUnlinkDrive();
                        } else {
                            //window.location.reload();
                        }
                    });
                }
            }
        });
    }
</script>
	<script>
    $("#unlinkDropbox").click(unlinkDropbox);

    function sendUnlinkDropbox() {
        $.ajax({
            url: "/unlinkDropbox",
            type: "GET",
            success: function () {
                $('#unlinkDropbox').attr('id','linkDropbox');
                $('#linkDropbox').attr('onclick','linkDropbox()');
                $('#linkDropbox').text('<fmt:message key="profile.link"></fmt:message>');
            }
        });
    }

    function unlinkDropbox() {
        $.ajax({
            url: "/unlinkDropbox",
            type: "POST",
            success: function (response) {
                var parsResp = JSON.parse(response);
                if (parsResp.length === 0) {
                    sendUnlinkDropbox();
                } else {
                    var html = "<div><strong><p><fmt:message key="profile.script.unlinkBox"></fmt:message></p></strong><hr>";
                    for (var i = 0; i < parsResp.length; i++) {
                        var name = parsResp[i].name;
                        html += "<div class=\"row\"><div class=\"col-lg-8\"><label>" + name + "</label></div>" + "<div class=\"col-lg-4\"><a href='/user/cloud/createrenamedownload?name=" + name + "&cloud=COMPLEX'><fmt:message key="profile.script.download"></fmt:message></a></div></div>";
                        //var a = document.createElement("a");
                        //a.href = "/user/cloud/createrenamedownload?name=" + name + "&cloud=COMPLEX";
                        //a.click();
                    }
                    html += "</div>";
                    bootbox.confirm(html, function (result) {
                        if (result == true) {
                            sendUnlinkDropbox();
                        } else {
//                            window.location.reload();
                        }
                    });
                }
            }
        });
    }
</script>
	<script>
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });
</script>
	<script>
    google.charts.load('current', {packages: ['corechart', 'bar']});
    //google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var x1 = $("#date1").val();
        var x2 = $("#date2").val();
        var currentDate = new Date();
        var date1 = new Date(x1);
        var date2 = new Date(x2);

        if(x1 == null || x1 == "" || x2 == null || x2 == "" || date2.getTime() < date1.getTime() ||
                date1.getTime() > currentDate.getTime()){
            $("#date1").addClass("alert alert-danger").css("margin-bottom", "0px");
            $("#date2").addClass("alert alert-danger").css("margin-bottom", "0px");
            hideCharts();
            return false;
        }else{
            $("#date1").removeClass("alert alert-danger");
            $("#date2").removeClass("alert alert-danger");
        }
        var choosePie = document.getElementById("pie").checked;
        var chooseColumn = document.getElementById("column").checked;
        $.ajax({
            url: "/userstat",
            method: "GET",
            dataType: "json",
            data: {
                "date1": x1,
                "date2": x2
            },
            success: function (response) {
                console.log(response);
                var box, dropbox, drive, complex, client, allStorages;
                var getDirSumBox = 0, deleteSumBox = 0, uploadSumBox = 0, createSumBox = 0, renameSumBox = 0,
                        moveSumBox = 0, copySumBox = 0, downloadSumBox = 0, allSumBox;
                var getDirSumDropbox = 0, deleteSumDropbox = 0, uploadSumDropbox = 0, createSumDropbox = 0, renameSumDropbox = 0,
                        moveSumDropbox = 0, copySumDropbox = 0, downloadSumDropbox = 0, allSumDropbox = 0;
                var getDirSumDrive = 0, deleteSumDrive = 0, uploadSumDrive = 0, createSumDrive = 0, renameSumDrive = 0,
                        moveSumDrive = 0, copySumDrive = 0, downloadSumDrive = 0, allSumDrive;
                var getDirSumComplex = 0, deleteSumComplex = 0, uploadSumComplex = 0, createSumComplex = 0, renameSumComplex = 0,
                        moveSumComplex = 0, copySumComplex = 0, downloadSumComplex = 0, allSumComplex;
                var getDirSumClient = 0, deleteSumClient = 0, uploadSumClient = 0, createSumClient = 0, renameSumClient = 0,
                        moveSumClient = 0, copySumClient = 0, downloadSumClient = 0, allSumClient = 0;

                var startDate = new Date(); //todo continue with this date
                for (var key in response) {
                    var date = new Date(+key);
                    box = response[key].BOX;
                    dropbox = response[key].DROPBOX;
                    drive = response[key].DRIVE;
                    complex = response[key].COMPLEX;
                    client = response[key].CLIENT;

                    if (box != undefined) {
                        getDirSumBox += +JSON.parse(box).getDir;
                        deleteSumBox += +JSON.parse(box).delete;
                        uploadSumBox += +JSON.parse(box).upload;
                        createSumBox += +JSON.parse(box).create;
                        renameSumBox += +JSON.parse(box).rename;
                        moveSumBox += +JSON.parse(box).move;
                        copySumBox += +JSON.parse(box).copy;
                        downloadSumBox += +JSON.parse(box).download;
                    }
                    if (dropbox != undefined) {
                        getDirSumDropbox += +JSON.parse(dropbox).getDir;
                        deleteSumDropbox += +JSON.parse(dropbox).delete;
                        uploadSumDropbox += +JSON.parse(dropbox).upload;
                        createSumDropbox += +JSON.parse(dropbox).create;
                        renameSumDropbox += +JSON.parse(dropbox).rename;
                        moveSumDropbox += +JSON.parse(dropbox).move;
                        copySumDropbox += +JSON.parse(dropbox).copy;
                        downloadSumDropbox += +JSON.parse(dropbox).download;
                    }
                    if (drive != undefined) {
                        getDirSumDrive += +JSON.parse(drive).getDir;
                        deleteSumDrive += +JSON.parse(drive).delete;
                        uploadSumDrive += +JSON.parse(drive).upload;
                        createSumDrive += +JSON.parse(drive).create;
                        renameSumDrive += +JSON.parse(drive).rename;
                        moveSumDrive += +JSON.parse(drive).move;
                        copySumDrive += +JSON.parse(drive).copy;
                        downloadSumDrive += +JSON.parse(drive).download;
                    }
                    if (complex != undefined) {
                        getDirSumComplex += +JSON.parse(complex).getDir;
                        deleteSumComplex += +JSON.parse(complex).delete;
                        uploadSumComplex += +JSON.parse(complex).upload;
                        createSumComplex += +JSON.parse(complex).create;
                        renameSumComplex += +JSON.parse(complex).rename;
                        moveSumComplex += +JSON.parse(complex).move;
                        copySumComplex += +JSON.parse(complex).copy;
                        downloadSumComplex += +JSON.parse(complex).download;
                    }
                    if (client != undefined) {
                        getDirSumClient += +JSON.parse(client).getDir;
                        deleteSumClient += +JSON.parse(client).delete;
                        uploadSumClient += +JSON.parse(client).upload;
                        createSumClient += +JSON.parse(client).create;
                        renameSumClient += +JSON.parse(client).rename;
                        moveSumClient += +JSON.parse(client).move;
                        copySumClient += +JSON.parse(client).copy;
                        downloadSumClient += +JSON.parse(client).download;
                    }

                }
                var getDirSumAllStorages = getDirSumBox + getDirSumDropbox + getDirSumDrive + getDirSumComplex + getDirSumClient;
                var uploadSumAllStorages = uploadSumBox + uploadSumDropbox + uploadSumDrive + uploadSumComplex + uploadSumClient;
                var deleteSumAllStorages = deleteSumBox + deleteSumDropbox + deleteSumDrive + deleteSumComplex + deleteSumClient;
                var createSumAllStorages = createSumBox + createSumDropbox + createSumDrive + createSumComplex + createSumClient;
                var renameSumAllStorages = renameSumBox + renameSumDropbox + renameSumDrive + renameSumComplex + renameSumClient;
                var moveSumAllStorages = moveSumBox + moveSumDropbox + moveSumDrive + moveSumComplex + moveSumClient;
                var copySumAllStorages = copySumBox + copySumDropbox + copySumDrive + copySumComplex + copySumClient;
                var downloadSumAllStorages = downloadSumBox + downloadSumDropbox + downloadSumDrive + downloadSumComplex + downloadSumClient;

                allSumBox = uploadSumBox + deleteSumBox + createSumBox + renameSumBox + moveSumBox + copySumBox + downloadSumBox;
                allSumDropbox = uploadSumDropbox + deleteSumDropbox + createSumDropbox + renameSumDropbox + moveSumDropbox + copySumDropbox + downloadSumDropbox;
                allSumDrive = uploadSumDrive + deleteSumDrive + createSumDrive + renameSumDrive + moveSumDrive + copySumDrive + downloadSumDrive;
                allSumComplex = uploadSumComplex + deleteSumComplex + createSumComplex + renameSumComplex + moveSumComplex + copySumComplex + downloadSumComplex;
                allSumClient = uploadSumClient + deleteSumClient + createSumClient + renameSumClient + moveSumClient + copySumClient + downloadSumClient;
                var allSum = allSumBox + allSumDropbox + allSumDrive + allSumComplex + allSumClient;

                var jsonBox = {
                    "cols": [
                        {"id": "", "label": "Topping", "pattern": "", "type": "string"},
                        {"id": "", "label": "Slices", "pattern": "", "type": "number"}
                    ],
                    "rows": [
                        //{"c": [{"v": "Get directory", "f": null}, {"v": getDirSumBox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="clientprofile.delete"></fmt:message>", "f": null}, {"v": deleteSumBox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.upload"></fmt:message>", "f": null}, {"v": uploadSumBox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>", "f": null}, {"v": createSumBox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>", "f": null}, {"v": renameSumBox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>", "f": null}, {"v": moveSumBox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>", "f": null}, {"v": copySumBox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>", "f": null}, {"v": downloadSumBox, "f": null}]},
                    ]
                };
                for (var i = 0; i < jsonBox['rows'].length; i++) {
                    if (jsonBox['rows'][i]['c'][1]['v'] == 0) {
                        jsonBox['rows'].splice(i, 1);
                        i--;
                    }
                }

                var jsonDropbox = {
                    "cols": [
                        {"id": "", "label": "Topping", "pattern": "", "type": "string"},
                        {"id": "", "label": "Slices", "pattern": "", "type": "number"}
                    ],
                    "rows": [
                        //{"c": [{"v": "Get directory", "f": null}, {"v": getDirSumDropbox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="clientprofile.delete"></fmt:message>", "f": null}, {"v": deleteSumDropbox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.upload"></fmt:message>", "f": null}, {"v": uploadSumDropbox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>", "f": null}, {"v": createSumDropbox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>", "f": null}, {"v": renameSumDropbox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>", "f": null}, {"v": moveSumDropbox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>", "f": null}, {"v": copySumDropbox, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>", "f": null}, {"v": downloadSumDropbox, "f": null}]},
                    ]
                };
                for (var i = 0; i < jsonDropbox['rows'].length; i++) {
                    if (jsonDropbox['rows'][i]['c'][1]['v'] == 0) {
                        jsonDropbox['rows'].splice(i, 1);
                        i--;
                    }
                }

                var jsonDrive = {
                    "cols": [
                        {"id": "", "label": "Topping", "pattern": "", "type": "string"},
                        {"id": "", "label": "Slices", "pattern": "", "type": "number"}
                    ],
                    "rows": [
                        // {"c": [{"v": "Get directory", "f": null}, {"v": getDirSumDrive, "f": null}]},
                        {"c": [{"v": "<fmt:message key="clientprofile.delete"></fmt:message>", "f": null}, {"v": deleteSumDrive, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.upload"></fmt:message>", "f": null}, {"v": uploadSumDrive, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>", "f": null}, {"v": createSumDrive, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>", "f": null}, {"v": renameSumDrive, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>", "f": null}, {"v": moveSumDrive, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>", "f": null}, {"v": copySumDrive, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>", "f": null}, {"v": downloadSumDrive, "f": null}]},
                    ]
                };
                for (var i = 0; i < jsonDrive['rows'].length; i++) {
                    if (jsonDrive['rows'][i]['c'][1]['v'] == 0) {
                        jsonDrive['rows'].splice(i, 1);
                        i--;
                    }
                }

                var jsonComplex = {
                    "cols": [
                        {"id": "", "label": "Topping", "pattern": "", "type": "string"},
                        {"id": "", "label": "Slices", "pattern": "", "type": "number"}
                    ],
                    "rows": [
                        {"c": [{"v": "<fmt:message key="profile.script.getdir"></fmt:message>", "f": null}, {"v": getDirSumComplex, "f": null}]},
                        {"c": [{"v": "<fmt:message key="clientprofile.delete"></fmt:message>", "f": null}, {"v": deleteSumComplex, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.upload"></fmt:message>", "f": null}, {"v": uploadSumComplex, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>", "f": null}, {"v": createSumComplex, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>", "f": null}, {"v": renameSumComplex, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>", "f": null}, {"v": moveSumComplex, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>", "f": null}, {"v": copySumComplex, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>", "f": null}, {"v": downloadSumComplex, "f": null}]},
                    ]
                };
                for (var i = 0; i < jsonComplex['rows'].length; i++) {
                    if (jsonComplex['rows'][i]['c'][1]['v'] == 0) {
                        jsonComplex['rows'].splice(i, 1);
                        i--;
                    }
                }

                var jsonClient = {
                    "cols": [
                        {"id": "", "label": "Topping", "pattern": "", "type": "string"},
                        {"id": "", "label": "Slices", "pattern": "", "type": "number"}
                    ],
                    "rows": [
                        //{"c": [{"v": "Get directory", "f": null}, {"v": getDirSumClient, "f": null}]},
                        {"c": [{"v": "<fmt:message key="clientprofile.delete"></fmt:message>", "f": null}, {"v": deleteSumClient, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.upload"></fmt:message>", "f": null}, {"v": uploadSumClient, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>", "f": null}, {"v": createSumClient, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>", "f": null}, {"v": renameSumClient, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>", "f": null}, {"v": moveSumClient, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>", "f": null}, {"v": copySumClient, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>", "f": null}, {"v": downloadSumClient, "f": null}]},
                    ]
                };
                for (var i = 0; i < jsonClient['rows'].length; i++) {
                    if (jsonClient['rows'][i]['c'][1]['v'] == 0) {
                        jsonClient['rows'].splice(i, 1);
                        i--;
                    }
                }

                var jsonAllStorages = {
                    "cols": [
                        {"id": "", "label": "Topping", "pattern": "", "type": "string"},
                        {"id": "", "label": "Slices", "pattern": "", "type": "number"}
                    ],
                    "rows": [
                        //{"c": [{"v": "Get directory", "f": null}, {"v": getDirSumAllStorages, "f": null}]},
                        {"c": [{"v": "<fmt:message key="clientprofile.delete"></fmt:message>", "f": null}, {"v": deleteSumAllStorages, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.upload"></fmt:message>", "f": null}, {"v": uploadSumAllStorages, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>", "f": null}, {"v": createSumAllStorages, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>", "f": null}, {"v": renameSumAllStorages, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>", "f": null}, {"v": moveSumAllStorages, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>", "f": null}, {"v": copySumAllStorages, "f": null}]},
                        {"c": [{"v": "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>", "f": null}, {"v": downloadSumAllStorages, "f": null}]}
                    ]
                };
                for (var i = 0; i < jsonAllStorages['rows'].length; i++) {
                    if (jsonAllStorages['rows'][i]['c'][1]['v'] == 0) {
                        jsonAllStorages['rows'].splice(i, 1);
                        i--;
                    }
                }

                var optionsBox = {
                    title: 'Box',
                    backgroundColor: "#f2f2f2",
                    is3D: true,
                    chartArea: {width: '90%', height: '85%'},
                };

                var optionsDropbox = {
                    title: 'Dropbox',  //todo add date FROM and TO ,
                    backgroundColor: "#f2f2f2",
                    is3D: true,
                    chartArea: {width: '90%', height: '85%'}
                };

                var optionsDrive = {
                    title: 'Drive',
                    backgroundColor: "#f2f2f2",
                    is3D: true,
                    chartArea: {width: '90%', height: '85%'}
                };

                var optionsComplex = {
                    title: 'Complex',
                    backgroundColor: "#f2f2f2",
                    is3D: true,
                    chartArea: {width: '90%', height: '85%'}
                };

                var optionsClient = {
                    title: 'Client',
                    backgroundColor: "#f2f2f2",
                    is3D: true,
                    chartArea: {width: '90%', height: '85%'}
                };

                var optionsAllStorages = {
                    title: '<fmt:message key="profile.script.all_storages"></fmt:message>',
                    backgroundColor: "#f2f2f2",
                    is3D: true,
                    chartArea: {width: '90%', height: '85%'}
                };

                if (choosePie == true) {

                    if (allSumBox != 0) {
                        $("#divBox").hide();
                        $("#barchart_box").hide();
                        $("#donutchart_box").show();
                        var dataBox = new google.visualization.DataTable(jsonBox);
                        var chartBox = new google.visualization.PieChart(document.getElementById('donutchart_box'));
                        chartBox.draw(dataBox, optionsBox);
                    }

                    if (allSumDropbox != 0) {
                        $("#divDropbox").hide();
                        $("#barchart_dropbox").hide();
                        $("#donutchart_dropbox").show();
                        var dataDropbox = new google.visualization.DataTable(jsonDropbox);
                        var chartDropbox = new google.visualization.PieChart(document.getElementById('donutchart_dropbox'));
                        chartDropbox.draw(dataDropbox, optionsDropbox);
                    }

                    if (allSumDrive != 0) {
                        $("#divDrive").hide();
                        $("#barchart_drive").hide();
                        $("#donutchart_drive").show();
                        var dataDrive = new google.visualization.DataTable(jsonDrive);
                        var chartDrive = new google.visualization.PieChart(document.getElementById('donutchart_drive'));
                        chartDrive.draw(dataDrive, optionsDrive);
                    }

                    if(allSumComplex != 0) {
                        $("#divComplex").hide();
                        $("#barchart_complex").hide();
                        $("#donutchart_complex").show();
                        var dataComplex = new google.visualization.DataTable(jsonComplex);
                        var chartComplex = new google.visualization.PieChart(document.getElementById('donutchart_complex'));
                        chartComplex.draw(dataComplex, optionsComplex);
                    }

                    if(allSumClient != 0) {
                        $("#divClient").hide();
                        $("#barchart_client").hide();
                        $("#donutchart_client").show();
                        var dataClient = new google.visualization.DataTable(jsonClient);
                        var chartClient = new google.visualization.PieChart(document.getElementById('donutchart_client'));
                        chartClient.draw(dataClient, optionsClient);
                    }

                    if(allSum != 0) {
                        $("#divAll").hide();
                        $("#barchart_all").hide();
                        $("#donutchart_all").show();
                        var dataAllStorages = new google.visualization.DataTable(jsonAllStorages);
                        var chartAllStorages = new google.visualization.PieChart(document.getElementById('donutchart_all'));
                        chartAllStorages.draw(dataAllStorages, optionsAllStorages);
                    }

                } else {
                    hideCharts();
                    if (allSumBox != 0) {
                        $("#divBox").show();
                        $("#barchart_box").show();
                        var boxColumns = {
                            labels: [
                                "<fmt:message key="clientprofile.delete"></fmt:message>",
                                "<fmt:message key="cloud.upload"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>"
                            ],
                            datasets: [
                                {
                                    label: "Box",
                                    fillColor: "#dcdcbc",
                                    strokeColor: "#54542c",
                                    highlightFill: "rgba(151,187,205,0.75)",
                                    highlightStroke: "rgba(151,187,205,1)",
                                    data: [deleteSumBox, uploadSumBox, createSumBox, renameSumBox, moveSumBox,
                                        copySumBox, downloadSumBox]
                                }
                            ]

                        };

                        var counterBox = boxColumns.datasets[0].data.length;
                        for (var i = 0; i < counterBox; i++) {
                            if (boxColumns.datasets[0].data[i] == 0) {
                                boxColumns.datasets[0].data.splice(i, 1);
                                boxColumns.labels.splice(i, 1);
                                //delete boxColumns[i];
                                i--;
                            }
                        }
                        console.log(boxColumns);
                        var contextBox = document.getElementById("barchart_box").getContext('2d');
                        new Chart(contextBox).Bar(boxColumns, {
                            scaleOverride: true,
                            scaleSteps: 1 + Math.max(deleteSumBox, uploadSumBox, createSumBox, renameSumBox, moveSumBox,
                                    copySumBox, downloadSumBox),
                            scaleStepWidth: 1,
                            responsive: true,
                            maintainAspectRatio: true,
                            scaleStartValue: 0
                        });
                    }

                    if (allSumDropbox != 0) {
                        //$("#donutchart_dropbox").hide();
                        $("#divDropbox").show();
                        $("#barchart_dropbox").show();
                        var dropboxColumns = {
                            labels: [
                                "<fmt:message key="clientprofile.delete"></fmt:message>",
                                "<fmt:message key="cloud.upload"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>"
                            ],
                            datasets: [
                                {
                                    label: "Dropbox",
                                    fillColor: "#ff99ff",
                                    strokeColor: "#660066",
                                    highlightFill: "rgba(151,187,205,0.75)",
                                    highlightStroke: "rgba(151,187,205,1)",
                                    data: [deleteSumDropbox, uploadSumDropbox, createSumDropbox, renameSumDropbox, moveSumDropbox,
                                        copySumDropbox, downloadSumDropbox]
                                }
                            ]

                        };
                        var counterDropbox = dropboxColumns.datasets[0].data.length;
                        for (var i = 0; i < counterDropbox; i++) {
                            if (dropboxColumns.datasets[0].data[i] == 0) {
                                dropboxColumns.datasets[0].data.splice(i, 1);
                                dropboxColumns.labels.splice(i, 1);
                                //delete dropboxColumns[i];
                                i--;
                            }
                        }
                        console.log(dropboxColumns);
                        var contextDropbox = document.getElementById("barchart_dropbox").getContext('2d');
                        new Chart(contextDropbox).Bar(dropboxColumns, {
                            scaleOverride: true,
                            scaleSteps: 1 + Math.max(deleteSumDropbox, uploadSumDropbox, createSumDropbox, renameSumDropbox, moveSumDropbox,
                                    copySumDropbox, downloadSumDropbox),
                            scaleStepWidth: 1,
                            responsive: true,
                            maintainAspectRatio: true,
                            scaleStartValue: 0
                        });
                    }

                    if (allSumDrive != 0) {
                        //$("#donutchart_drive").hide();
                        $("#divDrive").show();
                        $("#barchart_drive").show();
                        var driveColumns = {
                            labels: [
                                "<fmt:message key="clientprofile.delete"></fmt:message>",
                                "<fmt:message key="cloud.upload"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>"
                            ],
                            datasets: [
                                {
                                    label: "Drive",
                                    fillColor: "rgba(151,187,205,0.5)",
                                    strokeColor: "#002699",
                                    highlightFill: "rgba(151,187,205,0.75)",
                                    highlightStroke: "rgba(151,187,205,1)",
                                    data: [deleteSumDrive, uploadSumDrive, createSumDrive, renameSumDrive, moveSumDrive,
                                        copySumDrive, downloadSumDrive]
                                }
                            ]

                        };

                        var counterDrive = driveColumns.datasets[0].data.length;
                        for (var i = 0; i < counterDrive; i++) {
                            if (driveColumns.datasets[0].data[i] == 0) {
                                driveColumns.datasets[0].data.splice(i, 1);
                                driveColumns.labels.splice(i, 1);
                                //delete driveColumns[i];
                                i--;
                            }
                        }
                        console.log(driveColumns);
                        var contextDrive = document.getElementById("barchart_drive").getContext('2d');
                        new Chart(contextDrive).Bar(driveColumns, {
                            scaleOverride: true,
                            scaleSteps: 1 + Math.max(deleteSumDrive, uploadSumDrive, createSumDrive, renameSumDrive, moveSumDrive,
                                    copySumDrive, downloadSumDrive),
                            scaleStepWidth: 1,
                            responsive: true,
                            maintainAspectRatio: true,
                            scaleStartValue: 0
                        });
                    }

                    if(allSumComplex != 0) {
                       // $("#donutchart_complex").hide();
                        $("#divComplex").show();
                        $("#barchart_complex").show();
                        var complexColumns = {
                            labels: [
                                "<fmt:message key="clientprofile.delete"></fmt:message>",
                                "<fmt:message key="cloud.upload"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>"
                            ],
                            datasets: [
                                {
                                    label: "Complex",
                                    fillColor: "#ffffb3",
                                    strokeColor: "#ffff00",
                                    highlightFill: "rgba(151,187,205,0.75)",
                                    highlightStroke: "rgba(151,187,205,1)",
                                    data: [deleteSumComplex, uploadSumComplex, createSumComplex, renameSumComplex, moveSumComplex,
                                        copySumComplex, downloadSumComplex]
                                }
                            ]

                        };
                        var counterComplex = complexColumns.datasets[0].data.length;
                        for (var i = 0; i < counterComplex; i++) {
                            if (complexColumns.datasets[0].data[i] == 0) {
                                complexColumns.datasets[0].data.splice(i, 1);
                                complexColumns.labels.splice(i, 1);
                                //delete complexColumns[i];
                                i--;
                            }
                        }
                        console.log(complexColumns);
                        var contextComplex = document.getElementById("barchart_complex").getContext('2d');
                        new Chart(contextComplex).Bar(complexColumns, {
                            scaleOverride: true,
                            scaleSteps: 1 + Math.max(deleteSumComplex, uploadSumComplex, createSumComplex, renameSumComplex, moveSumComplex,
                                    copySumComplex, downloadSumComplex),
                            scaleStepWidth: 1,
                            responsive: true,
                            maintainAspectRatio: true,
                            scaleStartValue: 0
                        });
                    }

                    if(allSumClient != 0) {
                        //$("#donutchart_client").hide();
                        $("#divClient").show();
                        $("#barchart_client").show();
                        var clientColumns = {
                            labels: [
                                "<fmt:message key="clientprofile.delete"></fmt:message>",
                                "<fmt:message key="cloud.upload"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>"
                            ],
                            datasets: [
                                {
                                    label: "Client",
                                    fillColor: "#ccffcc",
                                    strokeColor: "#00b300",
                                    highlightFill: "rgba(151,187,205,0.75)",
                                    highlightStroke: "rgba(151,187,205,1)",
                                    data: [deleteSumClient, uploadSumClient, createSumClient, renameSumClient, moveSumClient,
                                        copySumClient, downloadSumClient]
                                }
                            ]

                        };
                        var counterClient = clientColumns.datasets[0].data.length;
                        for (var i = 0; i < counterClient; i++) {
                            if (clientColumns.datasets[0].data[i] == 0) {
                                clientColumns.datasets[0].data.splice(i, 1);
                                clientColumns.labels.splice(i, 1);
                                //delete clientColumns[i];
                                i--;
                            }
                        }
                        console.log(clientColumns);
                        var contextClient = document.getElementById("barchart_client").getContext('2d');
                        new Chart(contextClient).Bar(clientColumns, {
                            scaleOverride: true,
                            scaleSteps: 1 + Math.max(deleteSumClient, uploadSumClient, createSumClient, renameSumClient, moveSumClient,
                                    copySumClient, downloadSumClient),
                            scaleStepWidth: 1,
                            responsive: true,
                            maintainAspectRatio: true,
                            scaleStartValue: 0
                        });
                    }

                    if(allSum != 0) {
                        //$("#donutchart_all").hide();
                        $("#divAll").show();
                        $("#barchart_all").show();
                        var allStoragesColumns = {
                            labels: [
                                "<fmt:message key="clientprofile.delete"></fmt:message>",
                                "<fmt:message key="cloud.upload"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.create_folder"></fmt:message>",
                                "<fmt:message key="cloud.left_menu.header_rename"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.move"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.copy"></fmt:message>",
                                "<fmt:message key="cloud.dropdown_menu.download"></fmt:message>"
                            ],
                            datasets: [
                                {
                                    label: "AllStorages",
                                    fillColor: "#ffcccc",
                                    strokeColor: "#990000",
                                    highlightFill: "rgba(151,187,205,0.75)",
                                    highlightStroke: "rgba(151,187,205,1)",
                                    data: [deleteSumAllStorages, uploadSumAllStorages, createSumAllStorages, renameSumAllStorages, moveSumAllStorages,
                                        copySumAllStorages, downloadSumAllStorages]
                                }
                            ]

                        };
                        var counterAllStorages = allStoragesColumns.datasets[0].data.length;
                        for (var i = 0; i < counterAllStorages; i++) {
                            if (allStoragesColumns.datasets[0].data[i] == 0) {
                                allStoragesColumns.datasets[0].data.splice(i, 1);
                                allStoragesColumns.labels.splice(i, 1);
                                //delete allStoragesColumns[i];
                                i--;
                            }
                        }
                        console.log(allStoragesColumns);

                        var contextAllStorages = document.getElementById("barchart_all").getContext('2d');
                        new Chart(contextAllStorages).Bar(allStoragesColumns, {
                            scaleOverride: true,
                            scaleSteps: 1 + Math.max(deleteSumAllStorages, uploadSumAllStorages, createSumAllStorages, renameSumAllStorages, moveSumAllStorages,
                                    copySumAllStorages, downloadSumAllStorages),
                            scaleStepWidth: 1,
                            responsive: true,
                            maintainAspectRatio: true,
                            scaleStartValue: 0
                        });
                    }
                }
                //chart.draw(data, options);
                //console.log(date.toDateString());
            }
        });
    }

    function hideCharts(){
        $("#donutchart_box").hide();
        $("#donutchart_dropbox").hide();
        $("#donutchart_drive").hide();
        $("#donutchart_complex").hide();
        $("#donutchart_client").hide();
        $("#donutchart_all").hide();
    }
</script>
</body>
</html>