<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>Finish</h1>
<div id="d"></div>
<div id="c"></div>
<input type="button" id="butt" value="Butt">
</body>
<script src="/js/jquery.js"></script>
<script>
    $('#butt').click(function(){
        token();
    });
    function token() {
        $.ajax({
            url: 'https://www.googleapis.com/oauth2/v4/token',
            type: 'POST',
            dataType: 'application/x-www-form-urlencoded',
            data: {
                code: '${code}',
                client_id: '448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com',
                client_secret: 'Cll5xWA2kinCOBj6qfJeM_bL',
                redirect_uri: 'http://localhost:8080/clientGoogleLogin',
                grant_type: 'authorization_code',
                access_type: 'offline'
            },
            success: function (e) {
                $('#d').text(JSON.stringify(e));
            },
            error: function (e) {
                $('#d').text(JSON.stringify(e));
                var json = JSON.parse(e);
                $('#c').text(json);
                // alert("access: "+json.access_token+"\n+ refresh: "+json.refresh_token);
                $.ajax({
                    url: 'https://www.googleapis.com/oauth2/v1/userinfo',
                    type: 'get',
                    data: {
                        access_token: json.access_token
                    },
                    success: function (e) {
                        var json = JSON.parse(e.responseText);
                        $.ajax({
                            url: '/client/google/user',
                            type: 'GET',
                            data: {
                                'user_id': json.id
                            },
                            success: function () {
                                window.location.href = '/client/google/user'
                            }
                        });
                    },
                    error: function(e){
                        $('body').innerHTML(e);
                    }
                });
            }
        });
    }
    /*$(document).ready(function () {
        token();
    });*/
</script>
</html>
