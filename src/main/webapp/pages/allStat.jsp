<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" />
<!DOCTYPE html>
<html lang="${language}">
<head>
<title>All stat</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="/css/jquery-ui.css">
<link rel="shortcut icon" href="/img/cloud.ico">
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script
	src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<meta name="google-signin-client_id"
	content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async
	defer></script>
</head>
<body>

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/home">Clouds Merger</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li style="margin-top: -0.6rem";><a href="home"><i
							class="fa fa-home fa-2x"></i></a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><fmt:message
								key="all_users.profile"></fmt:message> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li>${user.userName}</li>
							<li>${user.email}</li>
							<li><a href="/admin/users">All users</a></li>
							<li><a id="signOutSocial" href="" class="portfolio-link"><fmt:message
										key="all_users.log_out"></fmt:message> </a></li>
						</ul></li>
					<li>
						<form>
							<select class="form-control" id="language" name="language"
								onchange="submit()" style="margin-top: 6px">
								<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
								<option value="uk_UA" ${language == 'uk_UA' ? 'selected' : ''}>Українська</option>
							</select>
						</form>
					</li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<div id="blue_cloud"></div>
<div id="idHint" style=" margin-top:2.5em; width:1px"></div>
	<div class="row" style="padding: 4em; margin: 0px !important;">
	
		<div class="col-lg-offset-1 col-lg-10">
			<table id="users" class="table table-striped table-bordered"
				cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Id</th>
						<th><fmt:message key="clientprofile.delete"></fmt:message></th>
						<th><fmt:message key="cloud.dropdown_menu.download"></fmt:message>
						</th>
						<th><fmt:message key="cloud.upload"></fmt:message></th>
						<th><fmt:message key="all_stat.open"></fmt:message></th>
						<th><fmt:message key="cloud.dropdown_menu.move"></fmt:message>
						</th>
						<th><fmt:message key="cloud.dropdown_menu.copy"></fmt:message>
						</th>
						<th><fmt:message key="all_stat.create_dir"></fmt:message></th>
						<th><fmt:message key="cloud.left_menu.header_rename"></fmt:message>
						</th>
						<th></th>
					</tr>
				</thead>
				<tbody id="tableBody" style="text-align: left;">
				</tbody>
			</table>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true" data-id="">
		<div class="modal-dialog" style="width: 75%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">
						<fmt:message key="all_stat.statistics_period"></fmt:message>
					</h4>
				</div>
				<div class="modal-body" style="padding: 40px">
					<div class="row centered">
						<label><fmt:message key="profile.diagrams.from"></fmt:message>
							<input id="date1" class="form-control date datepicker"
							type="text"
							style="display: inline-block; width: 200px; margin-right: 10px;"></label>
						<label><fmt:message key="profile.diagrams.to"></fmt:message>
							<input id="date2" class="form-control date datepicker"
							type="text"
							style="display: inline-block; width: 200px; margin-right: 10px;"></label>
						<button class="btn btn-info" onclick="getStat()"
							style="margin-left: 2rem; width: 10rem;">
							<fmt:message key="profile.diagrams.show"></fmt:message>
						</button>

						<hr style="margin-top: 25px; margin-bottom: 15px">
						<table id="cloudStat" class="table table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th><fmt:message key="all_stat.storage"></fmt:message></th>
									<th><fmt:message key="clientprofile.delete"></fmt:message></th>
									<th><fmt:message key="cloud.dropdown_menu.download"></fmt:message></th>
									<th><fmt:message key="cloud.upload"></fmt:message></th>
									<th><fmt:message key="all_stat.open"></fmt:message></th>
									<th><fmt:message key="cloud.dropdown_menu.move"></fmt:message></th>
									<th><fmt:message key="cloud.dropdown_menu.copy"></fmt:message></th>
									<th><fmt:message key="all_stat.create_dir"></fmt:message></th>
									<th><fmt:message key="cloud.left_menu.header_rename"></fmt:message></th>
								</tr>
							</thead>
							<tbody id="cloudStatBody" style="text-align: center;">
							</tbody>
						</table>
						<hr>
						<button class="btn btn-danger pull-left" onclick="block()">
							<fmt:message key="all_stat.block"></fmt:message>
						</button>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	<script src="/js/jquery.js"></script>
	<script src="/js/jquery-ui.js"></script>
	<script src="/js/bootstrap-treeview.js"></script>
	<script src="/js/bootbox.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/signOutSocial.js"></script>
	<script>
    $(".datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });

    var table = $('#users').DataTable({"order": [[1, "desc"]],
        "language": {
            search: "<fmt:message key="datatable.search"></fmt:message>",
            paginate: {
                first:      "<fmt:message key="datatable.first"></fmt:message>",
                previous:   "<fmt:message key="datatable.previous"></fmt:message>",
                next:       "<fmt:message key="datatable.next"></fmt:message>",
                last:       "<fmt:message key="datatable.last"></fmt:message>"
            },
            emptyTable:     "<fmt:message key="datatable.empty_table"></fmt:message>",
            lengthMenu:    "<fmt:message key="datatable.length_menu"></fmt:message>",
            info:           "<fmt:message key="datatable.info"></fmt:message>",
            infoEmpty:      "<fmt:message key="datatable.info_empty"></fmt:message>",
            infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix:    "",
            loadingRecords: "<fmt:message key="datatable.loading_records"></fmt:message>",
            zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher"
        }});
    var cloudTable;

    $('#myModal').on('hidden.bs.modal', function () {
        $('.datepicker').datepicker('setDate', null);
        //cloudTable.state.clear();
        cloudTable.clear();
        cloudTable.destroy();
    });
    
    $(document).ready(function () {
        tableCreate();
        $("div#idHint").popover({
	        trigger: 'manual',
	        placement: 'right',
	        animation: true,
	        popupDelay: 0,
	        appendToBody: false,
	    });
    	$("div#idHint").attr({ 'data-content':'<fmt:message key="all_stat.script.more_information"></fmt:message>','width':'10em'}).popover('show');
    });
    var prevId;
    function checkUndef(val){
    	if(val=='' || typeof val=='undefined'){return '';}
    	return val;
    }
    $(document).click(function(e){
    	e.preventDefault; 
    	$(".popa").popover("hide");
    	$("div#idHint").popover('hide');
    });
	function showPersonalInfo(id){
		$(".popa:not(#"+id+")").popover("hide");
		$("div#idHint").popover('hide');
		var userMessage;
		$.ajax({
            type: 'POST',
            url: '/userstat',
            data: {id:id},
            success: function (resp) {
            	var user = JSON.parse(resp);
            	userMessage =checkUndef(user['login'])+" "+ checkUndef(user['fullname'])+" "+checkUndef(user['email']);
            	$("div#"+id).popover({
    		        trigger: 'manual',
    		        placement: 'right',
    		        animation: true,
    		        popupDelay: 0,
    		        appendToBody: false,
    		        content: function() {
    		          return userMessage;
    		        }
    		    });
            	$("div#"+id).attr({ 'data-content':userMessage}).popover('show');
    		   /*  $("div#"+id).popover("show"); */
            	
            },
            error:function(resp){
            	console.log("error");
            }
        });
		 
	}
    function tableCreate() {

        table.clear();
        $.ajax({
            type: 'POST',
            url: '/admin/stat',
            data: {},
            success: function (resp) {
                var stat = JSON.parse(resp);
                for (var st in stat) {
                    var op = JSON.parse(stat[st]);
                    table.row.add(
                            [
                                '<div id="'+st+'" onclick="showPersonalInfo(this.id)" class="popa">'+st,
                                op['delete'],
                                op['download'],
                                op['upload'],
                                op['getDir'],
                                op['move'],
                                op['copy'],
                                op['create'],
                                op['rename'],
                                '<a href="#myModal" id="' + st + '" data-toggle="modal" onclick="getUserId(this)"><fmt:message key="all_stat.script.details"></fmt:message></a>'
                            ])
                            .draw(false);
                }
            }
        });
    }

    function block(){
        var userId = $('#myModal')[0].getAttribute('data-id');
        $.ajax({
            url: "/admin/block",
            method: "POST",
            data: {
                'userId': userId
            },
            success: function(){

            }
        })
    }

    function getUserId(th) {
        $('#myModal')[0].setAttribute('data-id', th.id);

        cloudTable = $('#cloudStat').DataTable({
            "sPaginationType": "full_numbers",
            "bSearchable": false,
            "bInfo": false,
            "bLengthChange": false,
            "language": {
                search: "<fmt:message key="datatable.search"></fmt:message>",
                paginate: {
                    first:      "<fmt:message key="datatable.first"></fmt:message>",
                    previous:   "<fmt:message key="datatable.previous"></fmt:message>",
                    next:       "<fmt:message key="datatable.next"></fmt:message>",
                    last:       "<fmt:message key="datatable.last"></fmt:message>"
                },
                emptyTable:     "<fmt:message key="datatable.empty_table"></fmt:message>",
                lengthMenu:    "<fmt:message key="datatable.length_menu"></fmt:message>",
                info:           "<fmt:message key="datatable.info"></fmt:message>",
                infoEmpty:      "<fmt:message key="datatable.info_empty"></fmt:message>",
                infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix:    "",
                loadingRecords: "<fmt:message key="datatable.loading_records"></fmt:message>",
                zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher"
            },
            "fnDrawCallback": function () {
                $('#cloudStat div.dataTables_paginate').css('display', "none");
            }
        });
    }


    function getStat() {
        var d1 = $('#date1').val();
        var d2 = $('#date2').val();
        var userId = $('#myModal')[0].getAttribute('data-id');

        $.ajax({
            url: "/userstat",
            method: "GET",
            dataType: "json",
            data: {
                "date1": d1,
                "date2": d2,
                "userId": userId
            },
            success: function (response) {
                cloudTable.clear();
                console.log(response);
                var box, dropbox, drive, complex, client, allStorages;
                var getDirSumBox = 0, deleteSumBox = 0, uploadSumBox = 0, createSumBox = 0, renameSumBox = 0,
                        moveSumBox = 0, copySumBox = 0, downloadSumBox = 0, allSumBox;
                var getDirSumDropbox = 0, deleteSumDropbox = 0, uploadSumDropbox = 0, createSumDropbox = 0, renameSumDropbox = 0,
                        moveSumDropbox = 0, copySumDropbox = 0, downloadSumDropbox = 0, allSumDropbox = 0;
                var getDirSumDrive = 0, deleteSumDrive = 0, uploadSumDrive = 0, createSumDrive = 0, renameSumDrive = 0,
                        moveSumDrive = 0, copySumDrive = 0, downloadSumDrive = 0, allSumDrive;
                var getDirSumComplex = 0, deleteSumComplex = 0, uploadSumComplex = 0, createSumComplex = 0, renameSumComplex = 0,
                        moveSumComplex = 0, copySumComplex = 0, downloadSumComplex = 0, allSumComplex;
                var getDirSumClient = 0, deleteSumClient = 0, uploadSumClient = 0, createSumClient = 0, renameSumClient = 0,
                        moveSumClient = 0, copySumClient = 0, downloadSumClient = 0, allSumClient = 0;

                var startDate = new Date();
                for (var key in response) {
                    var date = new Date(+key);
                    box = response[key].BOX;
                    dropbox = response[key].DROPBOX;
                    drive = response[key].DRIVE;
                    complex = response[key].COMPLEX;
                    client = response[key].CLIENT;

                    if (box != undefined) {
                        getDirSumBox += +JSON.parse(box).getDir;
                        deleteSumBox += +JSON.parse(box).delete;
                        uploadSumBox += +JSON.parse(box).upload;
                        createSumBox += +JSON.parse(box).create;
                        renameSumBox += +JSON.parse(box).rename;
                        moveSumBox += +JSON.parse(box).move;
                        copySumBox += +JSON.parse(box).copy;
                        downloadSumBox += +JSON.parse(box).download;
                    }
                    if (dropbox != undefined) {
                        getDirSumDropbox += +JSON.parse(dropbox).getDir;
                        deleteSumDropbox += +JSON.parse(dropbox).delete;
                        uploadSumDropbox += +JSON.parse(dropbox).upload;
                        createSumDropbox += +JSON.parse(dropbox).create;
                        renameSumDropbox += +JSON.parse(dropbox).rename;
                        moveSumDropbox += +JSON.parse(dropbox).move;
                        copySumDropbox += +JSON.parse(dropbox).copy;
                        downloadSumDropbox += +JSON.parse(dropbox).download;
                    }
                    if (drive != undefined) {
                        getDirSumDrive += +JSON.parse(drive).getDir;
                        deleteSumDrive += +JSON.parse(drive).delete;
                        uploadSumDrive += +JSON.parse(drive).upload;
                        createSumDrive += +JSON.parse(drive).create;
                        renameSumDrive += +JSON.parse(drive).rename;
                        moveSumDrive += +JSON.parse(drive).move;
                        copySumDrive += +JSON.parse(drive).copy;
                        downloadSumDrive += +JSON.parse(drive).download;
                    }
                    if (complex != undefined) {
                        getDirSumComplex += +JSON.parse(complex).getDir;
                        deleteSumComplex += +JSON.parse(complex).delete;
                        uploadSumComplex += +JSON.parse(complex).upload;
                        createSumComplex += +JSON.parse(complex).create;
                        renameSumComplex += +JSON.parse(complex).rename;
                        moveSumComplex += +JSON.parse(complex).move;
                        copySumComplex += +JSON.parse(complex).copy;
                        downloadSumComplex += +JSON.parse(complex).download;
                    }
                    if (client != undefined) {
                        getDirSumClient += +JSON.parse(client).getDir;
                        deleteSumClient += +JSON.parse(client).delete;
                        uploadSumClient += +JSON.parse(client).upload;
                        createSumClient += +JSON.parse(client).create;
                        renameSumClient += +JSON.parse(client).rename;
                        moveSumClient += +JSON.parse(client).move;
                        copySumClient += +JSON.parse(client).copy;
                        downloadSumClient += +JSON.parse(client).download;
                    }

                }
                var getDirSumAllStorages = getDirSumBox + getDirSumDropbox + getDirSumDrive + getDirSumComplex + getDirSumClient;
                var uploadSumAllStorages = uploadSumBox + uploadSumDropbox + uploadSumDrive + uploadSumComplex + uploadSumClient;
                var deleteSumAllStorages = deleteSumBox + deleteSumDropbox + deleteSumDrive + deleteSumComplex + deleteSumClient;
                var createSumAllStorages = createSumBox + createSumDropbox + createSumDrive + createSumComplex + createSumClient;
                var renameSumAllStorages = renameSumBox + renameSumDropbox + renameSumDrive + renameSumComplex + renameSumClient;
                var moveSumAllStorages = moveSumBox + moveSumDropbox + moveSumDrive + moveSumComplex + moveSumClient;
                var copySumAllStorages = copySumBox + copySumDropbox + copySumDrive + copySumComplex + copySumClient;
                var downloadSumAllStorages = downloadSumBox + downloadSumDropbox + downloadSumDrive + downloadSumComplex + downloadSumClient;

                allSumBox = uploadSumBox + deleteSumBox + createSumBox + renameSumBox + moveSumBox + copySumBox + downloadSumBox + getDirSumBox;
                allSumDropbox = uploadSumDropbox + deleteSumDropbox + createSumDropbox + renameSumDropbox + moveSumDropbox + copySumDropbox + downloadSumDropbox + getDirSumDropbox;
                allSumDrive = uploadSumDrive + deleteSumDrive + createSumDrive + renameSumDrive + moveSumDrive + copySumDrive + downloadSumDrive + getDirSumDrive;
                allSumComplex = uploadSumComplex + deleteSumComplex + createSumComplex + renameSumComplex + moveSumComplex + copySumComplex + downloadSumComplex + getDirSumComplex;
                allSumClient = uploadSumClient + deleteSumClient + createSumClient + renameSumClient + moveSumClient + copySumClient + downloadSumClient + getDirSumClient;
                var allSum = allSumBox + allSumDropbox + allSumDrive + allSumComplex + allSumClient;

                if (allSumBox > 0) {
                    cloudTable.row.add(['Box', deleteSumBox, downloadSumBox, uploadSumBox, getDirSumBox,
                        moveSumBox, copySumBox, createSumBox, renameSumBox]).draw(false);
                }
                if (allSumDrive > 0) {
                    cloudTable.row.add(['Drive', deleteSumDrive, downloadSumDrive, uploadSumDrive, getDirSumDrive,
                        moveSumDrive, copySumDrive, createSumDrive, renameSumDrive]).draw(false);
                }
                if (allSumDropbox > 0) {
                    cloudTable.row.add(['Dropbox', deleteSumDropbox, downloadSumDropbox, uploadSumDropbox, getDirSumDropbox,
                        moveSumDropbox, copySumDropbox, createSumDropbox, renameSumDropbox]).draw(false);
                }
                if (allSumClient > 0) {
                    cloudTable.row.add(['Client', deleteSumClient, downloadSumClient, uploadSumClient, getDirSumClient,
                        moveSumClient, copySumClient, createSumClient, renameSumClient]).draw(false);
                }
                if (allSumComplex > 0){
                    cloudTable.row.add(['Complex', deleteSumComplex, downloadSumComplex, uploadSumComplex, getDirSumComplex,
                        moveSumComplex, copySumComplex, createSumComplex, renameSumComplex]).draw(false);
                }
            }
        })
    }

</script>
</body>
</html>
