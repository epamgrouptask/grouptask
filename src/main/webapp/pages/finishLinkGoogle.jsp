<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text"/>
<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 21.01.2016
  Time: 16:30
  To change this template use File | Settings | File Templates.
--%>

<html lang="${language}">
<head>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="/img/cloud.ico">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script
            src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <title></title>
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
</body>
<script>
    $(document).ready(function () {
        $("#google1").click(token);
    });
    function token() {
        $.ajax({
            url: 'https://www.googleapis.com/oauth2/v4/token',
            type: 'POST',
            dataType: 'application/x-www-form-urlencoded',
            data: {
                code: '${code}',
                client_id: '452446586683-r37qjcd14vsn1trcatt7as5133ebgesf.apps.googleusercontent.com',
                client_secret: 'UZALQD5bWXMHeMVHA8REWk0N',
                redirect_uri: 'http://localhost:8080/linkGoogle',
                grant_type: 'authorization_code',
                access_type: 'offline'
            },
            success: function (e) {
                console.log(e);
            },
            error: function (e) {
                console.log(e);
                var json = JSON.parse(e.responseText);
                // alert("access: "+json.access_token+"\n+ refresh: "+json.refresh_token);
                if(json.refresh_token == undefined){
                    $('body').append('<div id="headerwrap"><div class="container"><div class="row centered">'+
                            '<div class="col-lg-8 col-lg-offset-2"><h1><b>CLOUDS MERGER</b></h1>'+
                    '</div></div></div><div class="container"><div class="row centered">' +
                            '</div></div></div></div>' +
                            '<div class="col-lg-8 col-lg-offset-2">'+
                            '<h3><fmt:message key="drive_error"></fmt:message> <a href="/user/home"><fmt:message key="page404.page"></fmt:message></a></h3></div>');
                } else {
                    $.ajax({
                        url: 'linkGoogle',
                        type: 'post',
                        data: {
                            access: json.access_token,
                            refresh: json.refresh_token
                        },
                        success: function () {
                            location.href = "/user/profile";
                        }
                    });
                }
            }
        });
    }
    $(document).ready(function () {
        token();
    });
</script>
</html>
