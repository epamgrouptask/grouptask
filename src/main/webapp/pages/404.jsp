<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text"/>
<!DOCTYPE html>
<html>
<head>
    <title>Clouds Merger</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
<link rel="shortcut icon" href="/img/cloud.ico">
    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script
            src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
</head>
<body>
<div id="headerwrap">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-8 col-lg-offset-2">
                <h1>
                    <b>CLOUDS MERGER</b>
                </h1>

                <h2>404</h2>
            </div>
        </div>
    </div>
    <!-- container -->
</div>
<div class="container">
    <div class="row centered">
        <div class="col-lg-8 col-lg-offset-2">

            <h3><fmt:message key="page404.message"></fmt:message> <a href="/home"><fmt:message key="page404.page"></fmt:message> </a></h3>
            <iframe src="http://www.youtube.com/embed/SIaFtAKnqBU?vq=hd720&amp;rel=0&amp;showinfo=0&amp;controls=0&amp;iv_load_policy=3&amp;loop=1&amp;playlist=SIaFtAKnqBU&amp;modestbranding=1&amp;autoplay=1" frameborder="0" webkitallowfullscreen="" allowfullscreen="" id="fitvid489417"></iframe>
        </div>
    </div>
    <!-- row -->
</div>
</body>
</html>
