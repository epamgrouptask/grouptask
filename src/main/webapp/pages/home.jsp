<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="/img/cloud.ico">

<title>Clouds Merger</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/main.css" rel="stylesheet">

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script
	src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<meta name="google-signin-client_id"
	content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async
	defer></script>
</head>
<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<%--  <a class="navbar-brand" href="#"><fmt:message key="mainpage.title"></fmt:message> </a> --%>
				<a class="navbar-brand" href="/home">Clouds Merger</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<c:if test="${user!=null}">
						<li style="margin-top: -0.6rem;"><a href="/user/home"><i
								class="fa fa-home fa-2x"></i></a></li>
					</c:if>
					<c:choose>
						<c:when test="${user!=null}">
							<li><a id="signOutSocial" href="" class="portfolio-link"><fmt:message
										key="user_home.log_out"></fmt:message> </a></li>
						</c:when>
						<c:when test="${user==null}">
							<li><a class="page-scroll" href="#portfolioModal0"
								class="portfolio-link" data-toggle="modal"><fmt:message
										key="user_home.log_in"></fmt:message> </a></li>
						</c:when>
					</c:choose>
					<%--<li><a data-toggle="modal" data-target="#myModal"
                       href="#myModal"><i class="fa fa-envelope-o"></i></a></li>--%>
					<li>
						<form>
							<select class="form-control" id="language" name="language"
								onchange="submit()" style="margin-top: 6px; width: 100px;">
								<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
								<option value="uk_UA" ${language == 'uk_UA' ? 'selected' : ''}>Українська</option>
							</select>
						</form>
					</li>
				</ul>
			</div>

			<!--/.nav-collapse -->
		</div>
	</div>

	<div id="headerwrap">
		<div class="container">
			<div class="row centered">
				<div class="col-lg-8 col-lg-offset-2 col-sm-10 col-sm-offset-1">
					<h1>
						<b>CLOUDS MERGER</b>
					</h1>

					<h2>
						<fmt:message key="mainpage.subtext"></fmt:message>
					</h2>
				</div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- headerwrap -->


	<div class="container w">
		<div class="row centered">
			<br> <br>

			<div class="col-md-4">
				<i class="fa fa-cloud"></i>
				<h4>
					<fmt:message key="mainpage.easy_header"></fmt:message>
				</h4>

				<p>
					<fmt:message key="mainpage.easy"></fmt:message>
				</p>
			</div>
			<!-- col-lg-4 -->

			<div class="col-md-4">
				<i class="fa fa-random"></i>
				<h4>
					<fmt:message key="mainpage.integration_header"></fmt:message>
				</h4>

				<p>
					<fmt:message key="mainpage.integration"></fmt:message>
				</p>
			</div>
			<!-- col-lg-4 -->

			<div class="col-md-4">
				<i class="fa fa-scissors"></i>
				<h4>
					<fmt:message key="mainpage.divide_header"></fmt:message>
				</h4>

				<p>
					<fmt:message key="mainpage.divide"></fmt:message>
				</p>
			</div>
			<!-- col-lg-4 -->
		</div>
		<!-- row -->
		<br> <br>
	</div>
	<!-- container -->


	<!-- PORTFOLIO SECTION -->
	<div id="dg">
		<div class="container">
			<div class="row centered">
				<h4 style="font-size: 30px; color: cadetblue;">
					<fmt:message key="user_home.supported_cloud"></fmt:message>
				</h4>
				<br>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-2">
						<div class="col-sm-4">
							<div class="tilt">
								<a href="#"><img src="/img/googledrive.png" alt=""
									style="width: 200px;"></a>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="tilt">
								<a href="#"><img src="/img/dropbox_1.png" alt=""
									style="width: 200px;"></a>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="tilt">
								<a href="#"><img src="/img/cloud.png" alt=""
									style="width: 150px; margin-top: 20px"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>
	<!-- DG -->


	<!-- FEATURE SECTION -->
	<div class="container wb"
		style="background: rgb(0, 0, 0); background: #f2f2f2; margin: 0px; width: 100%; margin-bottom: 30px;">
		<div class="row centered">
			<br> <br>

			<div class="col-sm-8 col-sm-offset-2">
				<h4>
					<fmt:message key="mainpage.convinient_program"></fmt:message>
				</h4>

				<p>
					<fmt:message key="mainpage.client_description"></fmt:message>
				</p>

				<br> <br>
			</div>



		</div>
		<div class="row">
			<div class='slider' style="box-shadow: -10px 10px 10px 5px #888888;">
				<div class='slide1'></div>
				<div class='slide2'></div>
				<div class='slide3'></div>
			</div>
		</div>
	</div>
	<!-- container -->


	<div id="lg">
		<div class="container">
			<div class="row centered">
				<h4>
					<fmt:message key="mainpage.technologies"></fmt:message>
				</h4>
				<ul class="list-inline">
					<li><img src="/img/java-logo-transparent.png" alt=""
						width="150" height="100"></li>
					<li><img src="/img/mysql.png" alt="" width="150" height="100"></li>
					<li><img src="/img/m2w.png" alt=""
						style="width: 150px; height: 100px; background-color: #f2f2f2"
						width="170" height="100"></li>
					<li><img src="/img/apple-touch-icon@2.png" width="170"
						height="140" style="width: 100px; height: 100px;"></li>
					<li><img src="/img/Jenkins_logo.svg" style="width: 70px;"></li>
					<li><img src="/img/bitbucket.png" alt=""
						style="width: 90px; height: 90px;"></li>
					<br>
					<li><img src="/img/frontend-01.png" style="width: 350px;"></li>
				</ul>
			</div>
			<!-- row -->
		</div>
	</div>
	<!-- dg -->


	<div id="r">
		<div class="container">
			<div class="row centered">
				<div class="col-sm-8 col-sm-offset-2">
					<%--<h4>WE ARE STORYTELLERS. BRANDS ARE OUR SUBJECTS. DESIGN IS
						OUR VOICE.</h4>--%>

					<p>
						<fmt:message key="mainpage.footer"></fmt:message>
					</p>
				</div>
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>

	<c:if test="${user==null}">
		<div class="modal fade" id="portfolioModal0" role="dialog">
			<div class="modal-dialog" style="width: 60vw;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h3>
							<fmt:message key="mainpage.modalwindow_login.do_login"></fmt:message>
						</h3>
					</div>
					<div class="modal-body"
						style="background: url(../img/clouds3.jpg); background-size: cover;">
						<div class="row">
							<div class="col-lg-7">
								<div class="panel panel-default">
									<div class="panel-body">
										<h3 class="thin text-center">
											<fmt:message key="mainpage.modalwindow_login.justlogin"></fmt:message>
										</h3>

										<p class="text-center text-muted">
											<fmt:message key="mainpage.modalwindow_login.not_register"></fmt:message>
											<a class="portfolio-link" data-toggle="modal"
												href="#portfolioModalReg" name="#portfolioModal0"
												onclick="closeModal(this.name)"><fmt:message
													key="mainpage.modalwindow_login.do_register"></fmt:message>
											</a>
										</p>
										<hr>
										<form>
											<div class="top-margin">
												<div class="form-group has-feedback" id="Login">
													<label><fmt:message
															key="mainpage.modalwindow_login.login"></fmt:message> <span
														class="text-danger">*</span></label> <input type="text"
														class="form-control" id="login">
												</div>
											</div>

											<div class="top-margin">
												<div class="form-group has-feedback" id="Pass">
													<label><fmt:message
															key="mainpage.modalwindow_login.password"></fmt:message>
														<span class="text-danger">*</span></label> <input type="password"
														class="form-control" id="pass">
												</div>
											</div>

											<hr>
											<div class="row">
												<div class="col-lg-8">
													<b><a href="#modalPassForgot" class="page-scroll"
														class="portfolio-link" data-toggle="modal"
														name="#portfolioModal0" onclick="closeModal(this.name)">
															<fmt:message
																key="mainpage.modalwindow_login.forgot_password"></fmt:message>
													</a></b>
												</div>
												<div class="col-lg-4 text-right">
													<button class="btn btn-action" type="button"
														style="background: antiquewhite;"
														onclick="sendLoginData()">
														<fmt:message key="mainpage.modalwindow_login.login"></fmt:message>
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-lg-5">
								<!-- 	<h3 class="thin text-center">Вхід через соцілаьні мережі</h3> -->

								<%--<div id="login_button" onclick="VK.Auth.login(authInfo);" style="height: 10px"></div>--%>
								<button id="login_button" type="button" value="Login vk"
									class="btn myVK" style="width: 100% !important;"
									onclick="VK.Auth.login(authInfo);">
									<fmt:message key="user_home.login_vk"></fmt:message>
								</button>
								<div class="btn g-signin2" data-onsuccess="onSignIn"
									style="width: 100%; margin-top: 1em;">
									<fmt:message key="user_home.login_g+"></fmt:message>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</c:if>
	<div class="modal fade" id="portfolioModalReg" role="dialog">
		<div class="modal-dialog" style="width: 60vw;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3>
						<fmt:message key="mainpage.modalwindow_register.register"></fmt:message>
					</h3>
				</div>
				<div class="modal-body" style="padding-top: 0px;">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2">
							<h3 class="thin text-center">
								<fmt:message
									key="mainpage.modalwindow_register.register_new_user"></fmt:message>
							</h3>

							<p class="text-center text-muted">
								<fmt:message
									key="mainpage.modalwindow_register.already_have_account"></fmt:message>
								<a class="portfolio-link" data-toggle="modal"
									href="#portfolioModal0" name="#portfolioModalReg"
									onclick="closeModal(this.name)"><fmt:message
										key="mainpage.modalwindow_register.log_in"></fmt:message> </a>
							</p>
							<hr>
							<form>
								<div class="row top-margin">
									<div class="col-sm-12 col-lg-12">
										<div class="form-group has-feedback" id="userF">
											<label><fmt:message
													key="mainpage.modalwindow_register.name"></fmt:message><span
												class="text-danger">*</span> </label> <input type="text"
												class="form-control" id="fullname" required>
										</div>
									</div>
								</div>
								<div class="row top-margin">
									<div class="col-sm-12">
										<div class="form-group has-feedback" id="loginF">
											<label><fmt:message
													key="mainpage.modalwindow_register.logIn"></fmt:message> <span
												class="text-danger">*</span></label> <input type="text"
												class="form-control" id="username" required>
										</div>
									</div>
								</div>
								<div class="row top-margin">
									<div class="col-sm-12">
										<div class="form-group has-feedback" id="emailF">
											<label><fmt:message
													key="mainpage.modalwindow_register.email"></fmt:message> <span
												class="text-danger">*</span></label> <input type="email"
												class="form-control" id="Email" required>
										</div>
									</div>
								</div>
								<div class="row top-margin">
									<div class="col-sm-12">
										<div class="form-group has-feedback" id="passw">
											<label><fmt:message
													key="mainpage.modalwindow_login.password"></fmt:message> <span
												class="text-danger">*</span> </label> <input type="password"
												id="password" class="form-control" required>
										</div>
									</div>
								</div>
								<div class="row top-margin">
									<div class="col-sm-12">
										<div class="form-group has-feedback" id="confirm">
											<label><fmt:message
													key="mainpage.modalwindow_register.confirm"></fmt:message>
												<span class="text-danger">*</span> </label> <input type="password"
												id="cPassword" class="form-control" required>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-4 text-right pull-right"
										style="margin-top: 2%">
										<button class="btn btn-action" type="button" id="send"
											onclick="sendRegisterData()" style="width: 130px;">
											<fmt:message key="mainpage.modalwindow_login.do_register"></fmt:message>
										</button>
									</div>
									<div class="col-lg-4 text-right pull-right"
										style="margin-top: 2%">
										<button class="btn btn-action" type="button" id="reset"
											onclick="resetData()" style="width: 130px;">
											<fmt:message key="mainpage.modalwindow_login.reset"></fmt:message>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="modalRegComplete" role="dialog"
		name="modalRegComplete">
		<div class="modal-dialog" style="width: 60vw;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3>
						<fmt:message key="mainpage.modalwindow_register.register"></fmt:message>
					</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<p style="margin: 10px; font-family: 'Open Sans' !important;">
							<fmt:message
								key="mainpage.modalwindow_forgot_password.send_email"></fmt:message>
						</p>
					</div>
					<div class="row">
						<p style="margin: 10px; font-family: 'Open Sans' !important;">
							<fmt:message
								key="mainpage.modalwindow_forgot_password.send_again"></fmt:message>
						</p>
					</div>
					<div class="top-margin">
						<label>Email<span class="text-danger">*</span></label>
						<div class="form-group has-feedback" id="mailCPA">
							<input type="email" class="form-control" id="complEmail">
						</div>
					</div>
					<div class="alert alert-success" role="alert" id="successr"
						style="display: none;">
						<strong></strong><span id="succ"></span>
					</div>
					<div class="alert alert-danger" role="alert" id="errorR"
						style="display: none;">
						<strong></strong><span id="againFail"></span>
					</div>
					<button class="btn btn-action" type="button"
						style="margin-top: 20px;" onclick="sendAgainData()" id="sendbyE">
						<fmt:message key="mainpage.modalwindow_forgot_password.send_link"></fmt:message>
					</button>
				</div>
			</div>

		</div>
	</div>

	<div class="modal fade" id="modalPassForgot" role="dialog"
		name="modalRegComplete">
		<div class="modal-dialog" style="width: 40vw;">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h3>
						<fmt:message
							key="mainpage.modalwindow_forgot_password.forgot_title"></fmt:message>
					</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<p style="margin: 10px; font-family: 'Open Sans' !important;">
							<fmt:message
								key="mainpage.modalwindow_forgot_password.input_login"></fmt:message>
						</p>
					</div>
				<div class="top-margin">
						<label><fmt:message key="mainpage.modalwindow_login.login"></fmt:message>
							<span class="text-danger">*</span></label> <input type="text"
							class="form-control" id="loginPass">

					</div> 
					<div class="top-margin">
						<label>Email<span class="text-danger">*</span></label>
						<div class="form-group has-feedback" id="mailPA">
							<input type="email"
								class="form-control" id="mailPass">
						</div>
					</div>
					<div class="alert alert-success" role="alert" id="sr"
						style="display: none;">
						<strong></strong><span id="successResp"></span>
					</div>
					<div class="alert alert-danger" role="alert" id="er"
						style="display: none;">
						<strong></strong><span id="errorResp"></span>
					</div>
					<button class="btn btn-action" type="button"
						onclick="sendDataPass()" style="margin-top: 20px;">
						<fmt:message
							key="mainpage.modalwindow_forgot_password.restore_password"></fmt:message>
					</button>
				</div>
			</div>

		</div>
	</div>
	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/js/jquery.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<!-- <script src="js/jquery.js"></script> TODO validation - reg not work corectly-->
	<!-- 	<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
	<script
		src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="/js/socialLogin.js"></script>
	<script src="/js/signOutSocial.js"></script>
	<script>
		var bitmap = [ false, false, false, false, false ];
		$('#password').keyup(function() {
			var pass = $('#password').val();
			var confirm = $('#cPassword').val();
			if (confirm !== "") {
				if (pass !== confirm) {
					$('#passw')[0].setAttribute('class', 'has-error');
					$('#confirm')[0].setAttribute('class', 'has-error');
					$('#send').attr("disabled", "disabled");
					bitmap[3] = false;
					bitmap[4] = false;
				} else {
					$('#passw')[0].setAttribute('class', 'has-success');
					$('#confirm')[0].setAttribute('class', 'has-success');
					var confirmed = true;
					bitmap[3] = true;
					bitmap[4] = true;
					for ( var i in bitmap) {
						confirmed = confirmed & bitmap[i];
					}
					if (confirmed) {
						$('#send').removeAttr("disabled");
					}
				}
			}
		});
		$('#cPassword').keyup(function() {
			var pass = $('#password').val();
			var confirm = $('#cPassword').val();
			if (pass !== confirm) {
				$('#passw')[0].setAttribute('class', 'has-error');
				$('#confirm')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
				bitmap[3] = false;
				bitmap[4] = false;
			} else {
				$('#passw')[0].setAttribute('class', 'has-success');
				$('#confirm')[0].setAttribute('class', 'has-success');
				var confirmed = true;
				bitmap[3] = true;
				bitmap[4] = true;
				for ( var i in bitmap) {
					confirmed = confirmed & bitmap[i];
				}
				if (confirmed) {
					$('#send').removeAttr("disabled");
				}
			}
		});
		$('#mailPass').keyup(function() {
			var mail = $('#mailPass').val();
			if (mail == '' || !validateEmail(mail)) {
				$('#mailPA')[0].setAttribute('class', 'has-error');
				$('#sendbyE').attr("disabled", "disabled");
			} else {
				$('#mailPA')[0].setAttribute('class', 'has-success');
				$('#sendbyE').removeAttr("disabled");
			}
			if(mail==''){
				$('#mailPA')[0].setAttribute('class', 'has-success');
				$('#sendbyE').removeAttr("disabled");
			}
		});
		$('#username')
				.keyup(
						function() {
							var username = $('#username').val();
							if (username == ""
									|| username
											.search('[\/\\\<\> \"\"\'\'\=\?\:\.\@\$\+\)\(\,\_\!\;\{\}\^\*]+') != -1) {
								$('#loginF')[0].setAttribute('class',
										'has-error');
								$('#send').attr("disabled", "disabled");
								bitmap[0] = false;
							} else {
								$('#loginF')[0].setAttribute('class',
										'has-success');
								var confirmed = true;
								bitmap[0] = true;
								for ( var i in bitmap) {
									confirmed = confirmed & bitmap[i];
								}
								if (confirmed) {
									$('#send').removeAttr("disabled");
								}
							}
						});
		$('#complEmail').keyup(function() {
			var mail = $('#complEmail').val();
			if (mail == '' || !validateEmail(mail)) {
				$('#mailCPA')[0].setAttribute('class', 'has-error');
				$('#sendbyE').attr("disabled", "disabled");
			} else {
				$('#mailCPA')[0].setAttribute('class', 'has-success');
				$('#sendbyE').removeAttr("disabled");
			}
		});
		$('#Email').keyup(function() {
			var email = $('#Email').val();
			if (email == "" || !validateEmail(email)) {
				$('#emailF')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
				bitmap[1] = false;
			} else {
				$('#emailF')[0].setAttribute('class', 'has-success');
				var confirmed = true;
				bitmap[1] = true;
				for ( var i in bitmap) {
					confirmed = confirmed & bitmap[i];
				}
				if (confirmed) {
					$('#send').removeAttr("disabled");
				}
			}
		});

		$('#fullname')
				.keyup(
						function() {
							var fullname = $('#fullname').val();
							if (fullname == ""
									|| fullname
											.search('[\/\\\<\>\"\"\'\'\=\?\:\.\@\$\+\)\(\,\_\!\;\{\}\^\*]+') != -1) {
								$('#userF')[0].setAttribute('class',
										'has-error');
								$('#send').attr("disabled", "disabled");
								bitmap[2] = false;
							} else {
								$('#userF')[0].setAttribute('class',
										'has-success');
								var confirmed = true;
								bitmap[2] = true;
								for ( var i in bitmap) {
									confirmed = confirmed & bitmap[i];
								}
								if (confirmed) {
									$('#send').removeAttr("disabled");
								}
							}
						});

		$('#login')
				.keyup(
						function() {
							var un = $('#login').val();
							if (un == ""
									|| un
											.search('[\/\\\<\> \"\"\'\'\=\?\:\.\@\$\+\)\(\,\_\!\;\{\}\^\*]+') != -1) {
								$('#Login')[0].setAttribute('class',
										'has-error');
								$('#send').attr("disabled", "disabled");
							} else {
								$('#Login')[0].setAttribute('class',
										'has-success');
								$('#send').removeAttr("disabled");
							}
						});
		$('#pass').keyup(function() {
			var pa = $('#pass').val();
			if (pa == "") {
				$('#Pass')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#Pass')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
		});
		function closeModal(modName) {
			$(modName).modal('toggle');
		}
		var login;
		function validateEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}

		function resetData() {
			$('#username').val("");
			$('#fullname').val("");
			$('#Email').val("");
			$('#password').val("");
			$('#cPassword').val("");
		}

		function sendDataPass() {
			//check if have mail or login
			var mail = $('#mailPass').val();
			var user = $('#loginPass').val();
			if (mail == '' && user == '') {
				$('#er').show();
				$('#sr').hide();
				$('#errorResp')
						.text(
								"<fmt:message key="user_home.script.error_resp"></fmt:message>");
			} else {
				$
						.ajax({
							url : "/restorepass",
							type : "POST",
							data : {
								email : $('#mailPass').val(),
								username : $('#loginPass').val()
							},
							success : function(data) {
								if (data == 'sent') {
									$('#er').hide();
									$('#successResp')
											.text(
													"<fmt:message key="mainpage.scripts.send_script"></fmt:message>");
									$('#sr').show();

								}
								if (data == 'no user') {
									$('#sr').hide();
									$('#errorResp')
											.text(
													"<fmt:message key="mainpage.scripts.not_found"></fmt:message>");
									$('#er').show();

								}
								if (data == 'no email') {
									$('#sr').hide();
									$('#errorResp')
											.text(
													"<fmt:message key="user_home.script.no_email"></fmt:message>");
									$('#er').show();

								}
							}
						});
			}

		}
		window.onload = function() {
			$('.abcRioButton.abcRioButtonLightBlue').removeAttr('style');
			;
			$('.abcRioButton.abcRioButtonLightBlue').css({
				'background-color' : 'RGB(177, 92, 131)',
				'color' : 'white',
				'height' : '30px',
				'width' : '100%'
			});
			$('.abcRioButtonContents').text(
					'<fmt:message key="user_home.login_g+"></fmt:message>');

		}

		function sendAgainData() {
			$
					.ajax({
						url : "/registration",
						type : "POST",
						data : {
							email : $('#complEmail').val(),
							username : login
						},
						success : function(data) {
							switch (data) {
							case 'sent':
								$('#errorR').hide();
								$('#succ')
										.text(
												"<fmt:message key="mainpage.scripts.send_letter"></fmt:message>");
								$('#successr').show();
								break;
							case 'emailFail':
								$('#successr').hide();
								$('#againFail').text("<fmt:message key="user_home.script.email_already_use"></fmt:message>");
								$('#errorR').show();
								break;
							}
						}
					});

		}
		function sendRegisterData() {
			login = $('#username').val();
			var username = $('#username').val();
			var pass = $('#password').val();
			var confirm = $('#cPassword').val();
			var email = $('#Email').val();
			var fullname = $('#fullname').val();
			var someErr = 0;
			if (username == "") {
				$('#loginF')[0].setAttribute('class', 'has-error');
				someErr = 1;
			} else {
				$('#loginF')[0].setAttribute('class', 'has-success');
			}
			if (email == "") {
				$('#emailF')[0].setAttribute('class', 'has-error');
				someErr = 1;
			} else {
				$('#emailF')[0].setAttribute('class', 'has-success');
			}
			if (fullname == "") {
				$('#userF')[0].setAttribute('class', 'has-error');
				someErr = 1;
			} else {
				$('#userF')[0].setAttribute('class', 'has-success');
			}
			if (pass == "") {
				$('#passw')[0].setAttribute('class', 'has-error');
				someErr = 1;
			} else {
				$('#passw')[0].setAttribute('class', 'has-success');
			}
			if (confirm == "") {
				$('#confirm')[0].setAttribute('class', 'has-error');
				someErr = 1;
			} else {
				$('#confirm')[0].setAttribute('class', 'has-success');
			}
			if (someErr == 1) {
				return;
			}
			$.ajax({
				url : "/registration",
				type : "POST",
				data : {
					username : username,
					password : pass,
					email : email,
					fullname : fullname
				},
				success : function(data) {
					switch (data) {
					case 'sent':
						$('#portfolioModalReg').modal('toggle');
						$('#modalRegComplete').modal();
						break;
					case 'nameFail':
						$('#loginF')[0].setAttribute('class', 'has-error');
						break;
					case 'emailFail':
						$('#emailF')[0].setAttribute('class', 'has-error');
						break;
					}
				}
			});

		}

		function sendLoginData() {
			var un = $('#login').val();
			var pw = $('#pass').val();
			if (un == '' || pw == '') {
				$('#Login')[0].setAttribute('class', 'has-error');
				$('#Pass')[0].setAttribute('class', 'has-error');
			} else {
				$.ajax({
					url : "/login",
					type : "POST",
					data : {
						username : un,
						password : pw
					},
					success : function(data) {
						var res = JSON.parse(data);
						if (res['redirect'] !== undefined) {
							window.location.href = res['redirect'];
						} else {
							$('#Login')[0].setAttribute('class', 'has-error');
							$('#Pass')[0].setAttribute('class', 'has-error');
						}
					}
				});
			}
		}
	</script>
</body>
</html>