<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restore password</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="/img/cloud.ico">

    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script
            src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

</head>
<body>
<div id="headerwrap">
    <div class="container">
        <div class="row centered">
            <div class="col-lg-8 col-lg-offset-2">
                <h1>
                    <b>CLOUDS MERGER</b>
                </h1>
            </div>
        </div>
    </div>
    <!-- container -->
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
    <h2>Set new password</h2>
    <form action="checkrestore" method="POST">
        <input type="hidden" value="${login }" name="username">
        <input type="hidden" value="${code }" name="code">

            <div class="col-sm-6">
                <div class="form-group has-feedback" id="passw">
                    <label><fmt:message key="mainpage.modalwindow_login.password"></fmt:message>
                        <span class="text-danger">*</span>
                    </label>
                    <input type="password" id="password" class="form-control" name="password">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group has-feedback" id="confirm">
                    <label><fmt:message key="mainpage.modalwindow_register.confirm"></fmt:message>
                        <span class="text-danger">*</span>
                    </label>
                    <input type="password" id="cPassword" class="form-control">
                </div>
            </div>
        <button type="submit" id="send" class="btn btn-primary" style="margin-top: 1%"><fmt:message key="restore_servlet.Restore"></fmt:message> </button>
    </form>
    </div>
</div>
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script>
    $('#password').keyup(function () {
        var pass = $('#password').val();
        var confirm = $('#cPassword').val();
        if (confirm !== "") {
            if (pass !== confirm) {
                $('#passw')[0].setAttribute('class', 'has-error');
                $('#confirm')[0].setAttribute('class', 'has-error');
                $('#send').attr("disabled", "disabled");
            } else {
                $('#passw')[0].setAttribute('class', 'has-success');
                $('#confirm')[0].setAttribute('class', 'has-success');
                $('#send').removeAttr("disabled");
            }
        }
    });
    $('#cPassword').keyup(function () {
        var pass = $('#password').val();
        var confirm = $('#cPassword').val();
        if (pass !== confirm) {
            $('#passw')[0].setAttribute('class', 'has-error');
            $('#confirm')[0].setAttribute('class', 'has-error');
            $('#send').attr("disabled", "disabled");
        } else {
            $('#passw')[0].setAttribute('class', 'has-success');
            $('#confirm')[0].setAttribute('class', 'has-success');
            $('#send').removeAttr("disabled");
        }
    });
</script>
</body>
</html>