<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="Text" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta name="google-signin-client_id"
	content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async
	defer></script>
<link rel="shortcut icon" href="assets/ico/favicon.png">

<title>Clouds Merger</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/main.css" rel="stylesheet">
<link rel="shortcut icon" href="/img/cloud.ico">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/home">Clouds Merger </a>
			</div>

			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li style="margin-top: -0.6rem;"><a href="/user/home"><i
							class="fa fa-home fa-2x"></i></a></li>
					<li class="dropdown"><a class="dropdown-toggle"
						data-toggle="dropdown" href="#"><fmt:message
								key="all_users.profile"></fmt:message> <span class="caret"></span></a>
						<ul class="dropdown-menu"
							style="padding: 10px; font-weight: lighter; color: cadetblue;">
							<li style="margin-bottom: 3%">${user.fullname}</li>
							<li style="margin-bottom: 5%">${user.email}</li>
							<li style="color: black; font-weight: initial; margin-bottom: 5%"><fmt:message
									key="user_home.occupied_space"></fmt:message></li>
							<div class="progress" style="margin-bottom: 5%">
								<div class="progress-bar progress-bar-striped active"
									role="progressbar" aria-valuenow="40" aria-valuemin="0"
									aria-valuemax="100" style="width: 0%" id="pbAllProf"></div>
							</div>
							<li><a href="/user/profile"><fmt:message
										key="all_users.account"></fmt:message></a></li>
							<li><a id="signOutSocial" href="" class="portfolio-link"><fmt:message
										key="all_users.log_out"></fmt:message> </a></li>
						</ul></li>
					<li>
						<form>
							<select class="form-control" id="language" name="language"
								onchange="submit()" style="margin-top: 6px">
								<option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
								<option value="uk_UA" ${language == 'uk_UA' ? 'selected' : ''}>Українська</option>
							</select>
						</form>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="cloudsAnime"></div>
	<div class="sky">
		<div class="clouds_one"></div>
		<div class="clouds_two"></div>
		<div class="clouds_three"></div>
	</div>
	<!-- <div id="blue_cloud">
		<div class="container">
			
		</div>
		container
	</div> -->


	<!-- PRICING SECTION -->
	<div id="dg">
		<div class="container">
			<div class="row centered">
				<h4 id="store">
					<fmt:message key="user_home.profile"></fmt:message>
				</h4>
				<br>
			</div>
			<!-- row -->
			<div class="row centered" id="clientStorages"></div>
		</div>
		<!-- container -->
	</div>
	<!-- DG -->
	<div id="r">
		<div class="container">
			<div class="row centered">
				<div class="col-lg-8 col-lg-offset-2">

					<p><fmt:message key="mainpage.footer"></fmt:message></p>
				</div>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/SizeDisplayer.js"></script>
	<script type="text/javascript">
		$(document)
				.ready(
						function() {
							$
									.ajax({
										url : "/user/cloud/free",
										type : "GET",
										success : function(data) {
											console.log(data);
											var free = JSON.parse(data);
											var usedDrive = free.totalDrive
													- free.drive;
											var usedDrop = free.totalDrop
													- free.dropbox;
											var usedBox = free.totalBox
													- free.box;
											var usedSummary = free.summaryTotal
													- free['summary'];
											//											var usedSummary = usedDrive + usedDrop + usedBox;
											$('#boxSpace').text(
													displaySize(free.box));
											$('#googleSpace').text(
													displaySize(free.drive));
											$('#dropboxSpace').text(
													displaySize(free.dropbox));
											$('#allSpace')
													.text(
															displaySize(free['summary']));
											$('#allTotal')
													.text(
															displaySize(free.summaryTotal));
											$('#totalDrive')
													.text(
															displaySize(free.totalDrive));
											$('#totalDrop')
													.text(
															displaySize(free.totalDrop));
											$('#totalBox').text(
													displaySize(free.totalBox));

											if (free.clientTotal !== undefined) {
												for ( var client in free.clientTotal) {
													$('#' + client + 'free')
															.text(
																	displaySize(free.clientFree[client]));
													$('#' + client + 'total')
															.text(
																	displaySize(free.clientTotal[client]));

													var percents = parseFloat(
															Math
																	.round((free.clientTotal[client] - free.clientFree[client])
																			/ free.clientTotal[client]
																			* 100))
															.toFixed(2)
															+ '%';
													$('#' + client + 'pb').css(
															'width', percents);
													$('#' + client + 'text')
															.text(
																	percents
																			+ ' <fmt:message key="user_home.used"></fmt:message>');
												}
											}
											var usedDrivePerc = parseFloat(
													Math.round(usedDrive
															/ free.totalDrive
															* 100)).toFixed(2);
											var usedDropPerc = parseFloat(
													Math.round(usedDrop
															/ free.totalDrop
															* 100)).toFixed(2);
											var usedAllPerc = parseFloat(
													Math.round(usedSummary
															/ free.summaryTotal
															* 100)).toFixed(2);
											var usedBoxPerc = parseFloat(
													Math.round(usedBox
															/ free.totalBox
															* 100)).toFixed(2);
											if (isNaN(usedAllPerc)) {
												usedAllPerc = 0;
											}
											$('#pbAllProf').css('width',
													usedAllPerc + '%');
											if (usedDrivePerc <= 65) {
												usedDrivePerc += "%";
												$('#pbDrive').css('width',
														usedDrivePerc);
												$('#DriveText')
														.text(
																usedDrivePerc
																		+ " <fmt:message key="user_home.used"></fmt:message>");
											} else {
												if (usedDrivePerc < 85) {
													usedDrivePerc += "%";
													$('#pbDrive').css('width',
															usedDrivePerc);
													$('#DriveText')
															.text(
																	usedDrivePerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbDrive').css(
															'background-color',
															'#5cb85c');
												} else {
													usedDrivePerc += "%";
													$('#pbDrive').css('width',
															usedDrivePerc);
													$('#DriveText')
															.text(
																	usedDrivePerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbDrive').css(
															'background-color',
															'#5cb85c');
												}
											}
											if (usedDropPerc <= 65) {
												usedDropPerc += "%";
												$('#pbDrop').css('width',
														usedDropPerc);
												$('#DropText')
														.text(
																usedDropPerc
																		+ " <fmt:message key="user_home.used"></fmt:message>");
											} else {
												if (usedDropPerc <= 85) {
													usedDropPerc += "%";
													$('#pbDrop').css('width',
															usedDropPerc);
													$('#DropText')
															.text(
																	usedDropPerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbDrop').css(
															'background-color',
															'#5cb85c');
												} else {
													usedDropPerc += "%";
													$('#pbDrop').css('width',
															usedDropPerc);
													$('#DropText')
															.text(
																	usedDropPerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbDrop').css(
															'background-color',
															'#5cb85c');
												}
											}
											if (usedBoxPerc <= 65) {
												usedBoxPerc += "%";
												$('#pbBox').css('width',
														usedBoxPerc);
												$('#boxText')
														.text(
																usedBoxPerc
																		+ " <fmt:message key="user_home.used"></fmt:message>");
											} else {
												if (usedBoxPerc <= 85) {
													usedBoxPerc += "%";
													$('#pbBox').css('width',
															usedBoxPerc);
													$('#boxText')
															.text(
																	usedBoxPerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbBox').css(
															'background-color',
															'#5cb85c');
												} else {
													usedBoxPerc += "%";
													$('#boxText')
															.text(
																	usedBoxPerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbBox').css(
															'background-color',
															'#5cb85c');
												}
											}
											if (usedAllPerc <= 65) {
												usedAllPerc += "%";
												$('#pbAll').css('width',
														usedAllPerc);
												$('#AllText')
														.text(
																usedAllPerc
																		+ " <fmt:message key="user_home.used"></fmt:message>");
											} else {
												if (usedAllPerc <= 85) {
													usedAllPerc += "%";
													$('#pbAll').css('width',
															usedAllPerc);
													$('#AllText')
															.text(
																	usedAllPerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbAll').css(
															'background-color',
															'#5cb85c');
												} else {
													usedAllPerc += "%";
													$('#pbAll').css('width',
															usedAllPerc);
													$('#AllText')
															.text(
																	usedAllPerc
																			+ " <fmt:message key="user_home.used"></fmt:message>");
													$('#pbAll').css(
															'background-color',
															'#5cb85c');
												}
											}
										}
									});
							//todo: for with clients

						});
		window.onload = function() {
			getActiveStorages();

		};
		function getActiveStorages() {
			var removeclient = '<i class="fa fa-times fa-2" style="color: cadetblue; float:right;"></i>';
			$
					.ajax({
						url : "/user/active",
						type : "GET",
						data : {},
						success : function(data) {
							var all = '<div class="col-sm-12 col-md-6 col-lg-4"><div class="pricing-option"><div class="pricing-top">'
									+ '<span class="pricing-edition">All </span><div class="row image"><a href="/user/cloud/storage/complex/complex"><img alt="" src="/img/all.png" style="height: 10em;"></a>'
									+ '</div></div><ul><li><fmt:message key="user_home.free_space"></fmt:message><p id="allSpace"></p></li>'
									+ '<li><fmt:message key="user_home.total_space"></fmt:message><p id="allTotal"></p></li></ul><div class="progress">'
									+ '<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0"'
									+ ' aria-valuemax="100" style="width: 100%" id="pbAll"></div><div id="AllText" style="position: absolute; width: 100%; left: 0px;"></div>'
									+ '</div><a href="/user/cloud/storage/complex/complex" class="pricing-signup"><fmt:message key="user_home.view"></fmt:message></a> </div></div>';
							var box = '<div class="col-sm-12 col-md-6 col-lg-4"><div class="pricing-option"><div class="pricing-top"><span class="pricing-edition">BOX</span>'
									+ '<div class="row image"><a href="/user/cloud/storage/box"><img alt="" src="/img/cloud.png" style="height: 10em;"></a></div></div><ul>'
									+ '<li><fmt:message key="user_home.free_space"></fmt:message><p id="boxSpace"></p></li>'
									+ '<li><fmt:message key="user_home.total_space"></fmt:message><p id="totalBox"></p></li></ul><div class="progress">'
									+ '<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0"'
									+ ' aria-valuemax="100" style="width: 100%" id="pbBox"></div> <div id="boxText" style="position: absolute; width: 100%; left: 0px;"></div>'
									+ '</div><a href="/user/cloud/storage/box" class="pricing-signup"><fmt:message key="user_home.view"></fmt:message> </a></div></div>';
							var drive = ' <div class="col-sm-12 col-md-6 col-lg-4">'
									+ '<div class="pricing-option">'
									+ '<div class="pricing-top"> <span class="pricing-edition">Google drive </span>'
									+ '<div class="row image"><a href="/user/cloud/storage/drive"><img alt="" src="../img/googledrive.png" style="height: 10em;"></a>'
									+ '</div></div><ul><li><fmt:message key="user_home.free_space"></fmt:message>'
									+ '<p id="googleSpace"></p></li><li><fmt:message key="user_home.total_space"></fmt:message>'
									+ '<p id="totalDrive"></p></li></ul><div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped"'
									+ ' role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%" id="pbDrive"></div>'
									+ '<div id="DriveText" style="position: absolute; width: 100%; "></div></div>'
									+ '<a href="/user/cloud/storage/drive" class="pricing-signup"><fmt:message key="user_home.view"></fmt:message></a></div>'
									+ '</div>';
							var dropbox = '<div class="col-sm-12 col-md-6 col-lg-4"><div class="pricing-option"><div class="pricing-top">'
									+ '<span class="pricing-edition">DROPBOX</span><div class="row image"><a href="/user/cloud/storage/dropbox"><img alt="" src="../img/drop.png" style="height: 10em;"></a>'
									+ '</div></div><ul><li><fmt:message key="user_home.free_space"></fmt:message><p id="dropboxSpace"></p></li>'
									+ '<li><fmt:message key="user_home.total_space"></fmt:message><p id="totalDrop"></p></li></ul><div class="progress">'
									+ '<div class="progress-bar progress-bar-success progress-bar-striped" ole="progressbar" aria-valuenow="40" aria-valuemin="0"'
									+ ' aria-valuemax="100" style="width: 100%" id="pbDrop"></div><div id="DropText" style="position: absolute; width: 100%; left: 0px;"></div>'
									+ '</div><a href="/user/cloud/storage/dropbox" class="pricing-signup"><fmt:message key="user_home.view"></fmt:message> </a></div></div>';
							var data = JSON.parse(data);
							if (!data['dropbox']
									& !data['drive']
									& !data['box']
									& (data['clients'] == undefined || data['clients'].length == 0)) {
								/* $('#cloudsAnime')
										.append(
												'<div class="sky"><div style="position: fixed;margin-top: 50vh;z-index: 90;'+
							    'margin-left: 37vw;color: white;font-size: 50px;">NO STORAGES</div><div class="clouds_one"></div>'
														+ '<div class="clouds_two"></div><div class="clouds_three"></div></div>'); */
								/* $('#dg').hide(); */
								$('.sky').css('height','70vh');
								$('.sky').prepend('<div style="position: fixed;margin-top: 50vh;z-index: 90;'+
									    'margin-left: 37vw;color: white;font-size: 50px;">NO STORAGES</div>');
								$('#store')
										.text(
												'YOU CAN LINK YOUR STORAGES IN YOUR PROFILE');
								$('#blue_cloud').hide();
								return;
							}
							if (data['dropbox']
									|| data['drive']
									|| data['box']
									|| (data['clients'] !== undefined || data['clients'].length !== 0)) {
								$('#clientStorages').append(all);
							}

							if (data['dropbox']) {
								$('#clientStorages').append(dropbox);
							}
							if (data['box']) {
								$('#clientStorages').append(box);
							}
							if (data['drive']) {
								$('#clientStorages').append(drive);
							}
							for (var i = 0; i < data.clients.length; i++) { //clients
								var el = '<div class="col-sm-12 col-md-6 col-lg-4"><div class="pricing-option" style="height: 33em;"><div class="pricing-top">'
										+ '<span class="pricing-edition">'
										+ data.clients[i].name
										+ '</span><div class="row image">'
										+ '<a href="/user/cloud/storage/' + data.clients[i].clientId + '"><img alt="" src="../img/cloud_client.ico" style="height: 10em;"></a></div>'
										+ '</div><ul><li><fmt:message key="user_home.free_space"></fmt:message><p id="' + data.clients[i].clientId + 'free"></p></li><li><fmt:message key="user_home.total_space"></fmt:message><p id="' + data.clients[i].clientId + 'total"></p>'
										+ '</li></ul><div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped"'
										+ 'role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%" id="'
										+ data.clients[i].clientId
										+ 'pb"></div>'
										+ '<div id="'
										+ data.clients[i].clientId
										+ 'text" style="position: absolute; width: 100%; left: 0px;"></div></div>'
										+ '<a href="/user/cloud/storage/' + data.clients[i].clientId + '" class="pricing-signup"><fmt:message key="user_home.view"></fmt:message></a></div></div>';
								if (data.clients[i].status == true) {
									$('#clientStorages').append(el);
								} else {
									var elHidden = '<div class="col-sm-12 col-md-6 col-lg-4" style="opacity: 0.7;height: 33em;" id="'+data.clients[i].clientId+'"><div class="pricing-option" style="opacity: 0.7"><div class="pricing-top" style="opacity: 0.7">'
											+ '<i class="fa fa-times fa-3 removeCP" style="color: cornflowerblue; " ></i>'
											+ '<span class="pricing-edition" style="opacity: 0.7">'
											+ data.clients[i].name
											+ '</span><div class="row" style="opacity: 0.7">'
											+ '<img alt="" src="../img/cloud_client.ico" style="height: 8.5em;"></div>'
											+ '</div><ul><li><fmt:message key="user_home.free_space"></fmt:message><p id="allSpace"></p><div>&nbsp;</div></li><li><fmt:message key="user_home.total_space"></fmt:message><p id="allTotal"><div>&nbsp;</div></p>'
											+ '</li></ul><div class="progress" style="opacity: 0.7"><div class="progress-bar progress-bar-success progress-bar-striped"'
											+ 'role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;opacity: 0.7;" id="pbAll"></div>'
											+ '<div id="AllText" style="position: absolute; width: 100%; left: 0px; opacity: 0.7;"></div></div>'
											+ '<a href="/user/cloud/storage/' + data.clients[i].clientId + '" class="pricing-signup not-active"><fmt:message key="user_home.script.not_active"></fmt:message></a></div></div>';
									$('#clientStorages').append(elHidden);
								}
							}
							$(document).on(
									'click',
									'.removeCP',
									function(event) {
										var id = $(this).parent().parent()
												.parent().attr('id');
										$.ajax({
											url : "/client/reg",
											type : "DELETE",
											data : {
												clientId : id
											},
											success : function(rsp) {
												$('#' + id).hide();
											}
										})
									});
						}
					});
		}
	</script>

	<script src="/js/signOutSocial.js"></script>
</body>
</html>