<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta name="google-signin-client_id"
	content="448803943450-echa6mdbg8rjceogilc34imkom31ttdv.apps.googleusercontent.com">
<script src="//vk.com/js/api/openapi.js" type="text/javascript"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="https://apis.google.com/js/platform.js?onload=onLoad" async
	defer></script>
<link rel="shortcut icon" href="/img/cloud.ico">

<title>Clouds Merger</title>

<!-- Bootstrap core CSS -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link href="/css/font-awesome.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="/css/main.css" rel="stylesheet">

</head>
<style type="text/css">
* {
	font: 16px "Helvetica Neue", "Open Sans", Arial, sans-serif !important;
}

.glyphicon {
	font: 15px 'Glyphicons Halflings' !important;
}

.fa {
	font: 95px FontAwesome !important;
}

/* added custom font-family  */
.blabla table.one {
	margin-bottom: 3em;
	border-collapse: collapse;
}

.blabla td {
	/* removed the border from the table data rows  */
	text-align: center;
	width: 10em;
	padding: 1em;
}

.blabla th {
	/* removed the border from the table heading row  */
	text-align: center;
	padding: 0.5em;
	background-color: #428bca;
	width: 10em;
	/* added a red background color to the heading cells  */
	color: white;
}

/* added a white font color to the heading text */
.blabla tr {
	height: 1em;
}

.blabla table tr:nth-child(even) {
	/* added all even rows a #eee color  */
	background-color: #eee;
}

.blabla table tr:nth-child(odd) {
	/* added all odd rows a #fff color  */
	background-color: #fff;
}
</style>
<body>
	<ul class="nav nav-tabs" style="margin-top: -4.6em;" id="mainTab">
		<li id="1"><a href="#generalInfo" data-toggle="dropdown"
			class="dropdown-toggle" id="prof" onclick=" $(this).tab('show'); ">${bundle.getString("profile.profile")}
		</a></li>
		<li id="2"><a href="#storagesInfo" data-toggle="dropdown"
			class="dropdown-toggle" id="storages"
			onclick=" $(this).tab('show'); ">${bundle.getString("clientprofile.storages")}</a></li>
		<li id="3"><a href="#pathsInfo" data-toggle="dropdown"
			class="dropdown-toggle" id="pathSh" onclick="$(this).tab('show');">${bundle.getString("profile.shared_paths")}</a></li>
		<li id="4"><a onclick="signOut()" href="#" data-toggle="dropdown"
			class="dropdown-toggle">${bundle.getString("user_home.log_out")}</a></li>
		<li style="margin-left: 30%;"><a data-toggle="dropdown"
			class="dropdown-toggle" style="width: 60px"><img
				src="/img/gb.svg" onclick="setEng()" style="width: 100%"></a></li>
		<li><a data-toggle="dropdown" class="dropdown-toggle"
			style="width: 60px"><img src="/img/ua.svg" onclick="setUkr()"
				style="width: 100%"></a></li>
	</ul>

	<div class="tab-content">
		<div id="blue" style="height: 9em;">
			<div class="container">
				<div class="row centered">
					<div class="col-lg-8 col-lg-offset-2">
						<h4></h4>

						<p></p>
					</div>
				</div>
				<!-- row -->
			</div>
			<!-- container -->
		</div>
		<div class="${tabProfile }" id="generalInfo">
			<div class="container w_client">
				<div class="col-sm-5 col-sm-offset-1">
					<div class="row centered">
						<h2 style="padding-top: 0px !important;">
							<input type="hidden" id="cId" value="${clientId }"> <input
								type="hidden" id="userId" value="${user.id }">
							${bundle.getString("profile.header")}
						</h2>
						<table>
							<tr>
								<th>${bundle.getString("profile.fullname")}</th>
								<td>${user.fullname }</td>
								<td></td>
							</tr>
							<tr>
								<th>${bundle.getString("profile.username")}</th>
								<td>${user.userName }</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>${user.email }</td>
							</tr>
							<tr>
								<th>${bundle.getString("clientprofile.client_name")}</th>
								<td id="compN">${clientname }<a onclick="changeName()"><span
										class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="${tabStorages}" id="storagesInfo">
			<div class="container w_client">
				<div id="clientStorages"></div>
			</div>
		</div>
		<div class="${tabPaths}" id="pathsInfo">
			<div class="container w">
				<!-- <i id="tableLoader" class="fa fa-spinner fa-spin" style="font-size:14px; padding: 15%;"></i> -->
				<div id="table">
					<table class="blabla">
						<caption>${bundle.getString("clientprofile.not_shared")}</caption>
						<thead>
							<tr>
								<th>${bundle.getString("clientprofile.not_shared")}</th>
								<th>${bundle.getString("clientprofile.edit")}</th>
								<th>${bundle.getString("clientprofile.delete")}</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="path" items="${paths}" varStatus="theCount">
								<c:if test="${paths == null}">
									<tr>
										<td>${bundle.getString("clientprofile.no_paths")}</td>
										<td></td>
										<td></td>
									</tr>
								</c:if>
								<!-- <tr class="bkkl"> -->
								<tr class="divIDNo${theCount.index}" id="${path }">
									<td><c:out value="${path}" /><i
										class="fa fa-spinner fa-spin" id="divIDNo${theCount.index}"
										style="font-size: 7px; display: none;"></i></td>
									<td><a id="<c:out value='${path}'/>"
										onclick="sendData(this.id)"><span
											class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
									<td><a class="boo" onclick="removePaths(this.id)"
										id="divIDNo${theCount.index}"><span
											class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
									<!-- </tr> -->
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<input type="button" onclick="addPath()"
						value="${bundle.getString("clientprofile.add_path")}" class="btn btn-primary">
				</div>
			</div>
		</div>
	</div>
</tbody>
	<!-- Bootstrap core JavaScript
================================================== -->
	<script src="/js/jquery.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/SizeDisplayer.js"></script>
	<script type="text/javascript">
    $(document).ready(function () {
        $.ajax({
            url: "/user/cloud/free",
            type: "GET",
            success: function (data) {
                var free = JSON.parse(data);
                var usedDrive = free.totalDrive - free.drive;
                var usedDrop = free.totalDrop - free.dropbox;
                var usedBox = free.totalBox - free.box;
                var usedSummary = usedDrive + usedDrop + usedBox;
                $('#boxSpace').text(displaySize(free.box));
                $('#googleSpace').text(displaySize(free.drive));
                $('#dropboxSpace').text(displaySize(free.dropbox));
                $('#allSpace').text(
                        displaySize(free.drive + free.dropbox + free.box));
                $('#allTotal').text(displaySize(free.summaryTotal));
                $('#totalDrive').text(displaySize(free.totalDrive));
                $('#totalDrop').text(displaySize(free.totalDrop));
                $('#totalBox').text(displaySize(free.totalBox));
                var usedDrivePerc = parseFloat(
                        Math.round(usedDrive / free.totalDrive * 100))
                        .toFixed(2);
                var usedDropPerc = parseFloat(
                        Math.round(usedDrop / free.totalDrop * 100))
                        .toFixed(2);
                var usedAllPerc = parseFloat(
                        Math.round(usedSummary / free.summaryTotal * 100))
                        .toFixed(2);
                var usedBoxPerc = parseFloat(
                        Math.round(usedBox / free.totalBox * 100)).toFixed(
                        2);
                if (isNaN(usedAllPerc)) {
                    usedAllPerc = 0;
                }
                if (usedDrivePerc <= 65) {
                    usedDrivePerc += "%";
                    $('#pbDrive').css('width', usedDrivePerc);
                    $('#DriveText').text(usedDrivePerc + " ${bundle.getString("user_home.used")}");
                } else {
                    if (usedDrivePerc < 85) {
                        usedDrivePerc += "%";
                        $('#pbDrive').css('width', usedDrivePerc);
                        $('#DriveText').text(usedDrivePerc + " ${bundle.getString("user_home.used")}");
                        $('#pbDrive').css('background-color', 'cornflowerblue');
                    } else {
                        usedDrivePerc += "%";
                        $('#pbDrive').css('width', usedDrivePerc);
                        $('#DriveText').text(usedDrivePerc + " ${bundle.getString("user_home.used")}");
                        $('#pbDrive').css('background-color', 'cornflowerblue');
                    }
                }
                if (usedDropPerc <= 65) {
                    usedDropPerc += "%";
                    $('#pbDrop').css('width', usedDropPerc);
                    $('#DropText').text(usedDropPerc + " ${bundle.getString("user_home.used")}");
                } else {
                    if (usedDropPerc <= 85) {
                        usedDropPerc += "%";
                        $('#pbDrop').css('width', usedDropPerc);
                        $('#DropText').text(usedDropPerc + " ${bundle.getString("user_home.used")}");
                        $('#pbDrop').css('background-color', 'cornflowerblue');
                    } else {
                        usedDropPerc += "%";
                        $('#pbDrop').css('width', usedDropPerc);
                        $('#DropText').text(usedDropPerc + " ${bundle.getString("user_home.used")}");
                        $('#pbDrop').css('background-color', 'cornflowerblue');
                    }
                }
                if (usedBoxPerc <= 65) {
                    usedBoxPerc += "%";
                    $('#pbBox').css('width', usedBoxPerc);
                    $('#boxText').text(usedBoxPerc + " ${bundle.getString("user_home.used")}");
                } else {
                    if (usedBoxPerc <= 85) {
                        usedBoxPerc += "%";
                        $('#pbBox').css('width', usedBoxPerc);
                        $('#boxText').text(usedBoxPerc + " ${bundle.getString("user_home.used")}");
                        $('#pbBox').css('background-color', 'cornflowerblue');
                    } else {
                        usedBoxPerc += "%";
                        $('#boxText').text(usedBoxPerc + " ${bundle.getString("user_home.used")}");
                        $('#pbBox').css('background-color', 'cornflowerblue');
                    }
                }
                if (usedAllPerc <= 65) {
                    usedAllPerc += "%";
                    $('#pbAll').css('width', usedAllPerc);
                    $('#AllText').text(usedAllPerc + " ${bundle.getString("user_home.used")}");
                } else {
                    if (usedAllPerc <= 85) {
                        usedAllPerc += "%";
                        $('#pbAll').css('width', usedAllPerc);
                        $('#AllText').text(usedAllPerc + " ${bundle.getString("user_home.used")}");
                        $('#pbAll').css('background-color', 'cornflowerblue');
                    } else {
                        usedAllPerc += "%";
                        $('#pbAll').css('width', usedAllPerc);
                        $('#AllText').text(usedAllPerc + " ${bundle.getString("user_home.used")}");
                        $('#pbAll').css('background-color', 'cornflowerblue');
                    }
                }
            }
        });
    });
   
    var language = 'en';

    function setEng(){
        language = 'en';
        app.changeLocale('en');
    }

    function setUkr(){
        language = 'uk_UA';
        app.changeLocale('uk_UA');
    }

    function getActiveStorages() {
        $.ajax({
            url: "/user/active",
            type: "GET",
            data: {},
            success: function (data) {
                var box = '<div class="col-sm-4"><div class="pricing-option"><div class="pricing-top"><span class="pricing-edition">BOX</span>'
                        + '<div class="row"><img alt="" src="/img/cloud.png" style="height: 6em;"></div></div><ul>'
                        + '<li>${bundle.getString("user_home.free_space")}<p id="boxSpace"></p></li>'
                        + '<li>${bundle.getString("user_home.total_space")}<p id="totalBox"></p></li></ul><div class="progress">'
                        + '<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0"'
                        + ' aria-valuemax="100" style="width: 100%; background-color: cornflowerblue;"  id="pbBox"></div> <div id="boxText" style="position: absolute; width: 100%; text-align: center"></div>'
                        + '</div></div></div>';
                var drive = ' <div class="col-sm-4">'
                        + '<div class="pricing-option">'
                        + '<div class="pricing-top"> <span class="pricing-edition">Google drive </span>'
                        + '<div class="row"><img alt="" src="../img/googledrive.png" style="height: 5em;">'
                        + '</div></div><ul><li>${bundle.getString("user_home.free_space")}'
                        + '<p id="googleSpace"></p></li><li>${bundle.getString("user_home.total_space")}'
                        + '<p id="totalDrive"></p></li></ul><div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped"'
                        + ' role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%; background-color: cornflowerblue; text-align: center;" id="pbDrive"></div>'
                        + '<div id="DriveText" style="position: absolute; width: 100%; text-align: center"></div></div>'
                        + '</div>'
                        + '</div>';
                var dropbox = '<div class="col-sm-4"><div class="pricing-option"><div class="pricing-top">'
                        + '<span class="pricing-edition">DROPBOX</span><div class="row"><img alt="" src="../img/drop.png" style="height: 6em;">'
                        + '</div></div><ul><li>${bundle.getString("user_home.free_space")}<p id="dropboxSpace"></p></li>'
                        + '<li>${bundle.getString("user_home.total_space")}<p id="totalDrop"></p></li></ul><div class="progress">'
                        + '<div class="progress-bar progress-bar-success progress-bar-striped" ole="progressbar" aria-valuenow="40" aria-valuemin="0"'
                        + ' aria-valuemax="100" style="width: 100%; background-color: cornflowerblue;" id="pbDrop"></div><div id="DropText" style="position: absolute; width: 100%; text-align: center;"></div>'
                        + '</div></div></div>';
                var data = JSON.parse(data);
                if (data['dropbox']) {
                    $('#clientStorages').append(dropbox);
                }
                if (data['box']) {
                    $('#clientStorages').append(box);
                }
                if (data['drive']) {
                    $('#clientStorages').append(drive);
                }

            }
        });
    }

    function signOut() {
        VK.Auth.logout();
        var auth3 = gapi.auth2.getAuthInstance();
        auth3.signOut().then(function () {
            console.log('User signed out.');
        });
        app.logout();
        window.location.reload();
    }

    function onLoad() {
        gapi.load('auth2', function () {
            gapi.auth2.init();
        });
        getActiveStorages();
    }
    $(document).ready(onLoad());

    function changeName() {
        var oldName = $('#compN').text();
        
        $('#compN')
                .html(
                ' <div class="form-group has-feedback" id="compF">'+
    '<input style="float:left;" type="text" value=' + oldName + ' id="compname" onclick="checker()">'+
'</div><a onclick="renameComp()"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span></a>');
        $('#compF')[0].setAttribute('class',
		'has-error form-group has-feedback');
        $('#compname')
    	.click(
    			function() {
    				var compname = $('#compname').val();
    				if (compname == ""
    						|| compname
    								.search('[\/\\\<\> \"\"\'\'\=\?\:\.\@\$\+\)\(\,\_\!\;\{\}\^\*]+') != -1) {
    					$('#compF')[0].setAttribute('class',
    							'has-error');   					
    				}  					
    			});
    }
    
    function checker() {
		var compname = $('#compname').val();
		if (compname == ""
				|| compname
						.search('[\/\\\<\> \"\"\'\'\=\?\:\.\@\$\+\)\(\,\_\!\;\{\}\^\*]+') != -1) {
			$('#compname').css('color','black');			
		}			
	};
    
    function renameComp() {
        var newName = $('#compname').val();
        var clientId = $('#cId').val();
        if(newName == ""
			|| newName.search('[\/\\\<\> \"\"\'\'\=\?\:\.\@\$\+\)\(\,\_\!\;\{\}\^\*]+') != -1){
        	$('#compname').css('color','red');
        	return;
        }
        var lang = $('#language').val();
        $.ajax({
            url: '/client/profile',
            type: 'POST',
            data: {
                'newName': newName,
                'clientId': clientId,
                'language': lang
            },
            success: function (data) {

            }
        });
        $('#changeNameMod').modal('hidden');
        $('#compN')
                .html(
                newName
                + '<a onclick="changeName()"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>');
        app.changeName(newName);
    }

    function removePaths(id) {
        added = 0;
        var paths = $("." + id).children('td:first').text();
        $('.' + id).remove();
        var clientId = $('#cId').val();
        $.ajax({
            url: '/client/paths',
            type: 'PUT',
            data: {
                'path': paths,
                'clientId': clientId,
            },
            success: function (data) {
                $(this).parent().parent().remove();
                app.removePath(paths);
                
            },
            error: function () {
          
            }
        });
        $(this).parent().parent().remove();
    }

    var added = 0;
    function sendData(id) {
         if(id==''){
        	 $('#loadingSpinner').show();
         }else{
             $('tr').each(function () {
                 if(this.id == id){
            	 	$(this).html('<td><i class="fa fa-spinner fa-spin" style="font-size: 37px !important; "></i></td><td><a id="'+id+'" onclick="sendData(this.id)"><span'+
					' class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td><td><a class="boo" onclick="removePaths(this.id)"'+
				' id="divIDNo${theCount.index}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>');
             	};
             });
        }
        var userId = $('#userId').val();
        var path = '';
        $('td:first-child').each(function () {
            path += ($(this).text()) + ",";
        });
        if ($('tr:last-child td:first-child').text() == "") {
            added = 0;
        }
        app.changePath(userId, path, id, added);
        added = 0;
    }

    function addPath() {
        var tr = '<tr><td><i class="fa fa-spinner fa-spin" style="font-size:37px !important;  display: none;" id="loadingSpinner"></i></td><td><a id="<c:out value='${path}'/>" onclick="sendData(this.id)"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td><td><a class="boo"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td></tr>';
        $('#table tbody').append(tr);
        added = 1;
    }

    function allPath(){
    	var path = '';
        $('td:first-child').each(function () {
            path += ($(this).text()) + ",";
        });
    	app.getPaths(path);
    }
</script>
</body>
</html>