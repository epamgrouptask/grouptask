<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="Text"/>
<!DOCTYPE html>
<html lang="${language}">
<head>
  <title></title>
  <!-- Bootstrap core CSS -->
  <link href="/css/bootstrap.css" rel="stylesheet">
  <link href="/css/font-awesome.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="/css/main.css" rel="stylesheet">
<link rel="shortcut icon" href="/img/cloud.ico">
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script
          src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
</head>
<body>
<div id="headerwrap">
  <div class="container">
    <div class="row centered">
      <div class="col-lg-8 col-lg-offset-2">
        <h1>
          <b>CLOUDS MERGER</b>
        </h1>

        <h2>For confirmation your account in system enter email address</h2>
      </div>
    </div>
  </div>
  <!-- container -->
</div>
<div class="container">
  <div class="row centered">
    <div class="col-lg-8 col-lg-offset-2">

      <h3></h3>
      <input class="form-control" type="text" id="email" placeholder="Email" style="display: inline; width: 60%">
      <button id="codeClick" class="btn btn-success"><fmt:message key="blocked.submit"></fmt:message> </button>
      <%--<div class="alert alert-danger" role="alert" style="display: none; margin: 10px 15% 0px 15%; height: 45px;">
      <!-- <button type="button" class="close myClose">
                        <span aria-hidden="true">&times;</span>
                    </button> -->
        <strong></strong><span id="err"></span>
      </div>--%>
    </div>
  </div>
  <!-- row -->
</div>
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script>
  $('#codeClick').click(function () {
    var email = $('#email').val();
    $.ajax({
        url: '/socialLogin',
        type: 'PUT',
        data: {
            'email': email,
            'id': '${id}',
            'social': '${social}'
        },
        success: function (response) {
            if (response == 'bad') {
                //$('.alert').show();
                /*$('#err').text('<fmt:message key="blocked.invalid_code"></fmt:message>');*/
            } else {
                window.location.href = '/user/home';
            }
        }
    });
  });
</script>
</body>
</html>
