/**
 * Created by Олег on 31.01.2016.
 */
$("#unlinkBox").click(unlinkBox);

function unlinkBox(){

    bootbox.confirm()
    $.ajax({
        url:"/unlinkBox",
        type:"POST",
        success:function(response){
            var clientId = JSON.parse(response).clientId;
            var clientSecret = JSON.parse(response).clientSecret;
            var refreshToken = JSON.parse(response).refreshToken;
            $.ajax({
                url:"https://www.box.com/api/oauth2/revoke",
                type:"POST",
                data:{
                    "client_id": clientId,
                    "client_secret":clientSecret,
                    "token":refreshToken
                }
            });
            window.location.reload();
        }
    });
}