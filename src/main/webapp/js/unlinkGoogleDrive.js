/**
 * Created by 1 on 25.01.2016.
 */
function sendUnlinkDropbox(){
    $.ajax({
        url:"/unlinkDropbox",
        type:"GET",
        success: function(){
            window.location.reload();
        }
    });
}

function unlinkDropbox() {
    $.ajax({
        url: "/unlinkDropbox",
        type: "POST",
        success: function (response) {
            var parsResp = JSON.parse(response);
            if (parsResp.length === 0) {
                sendUnlinkDropbox();
            } else {
                var html = "<div>";
                for(var i = 0; i < parsResp.length; i++){
                    var name = parsResp[i].name;
                    html.append("<label>" + name + "</label>" +"<a href='/user/cloud/createrenamedownload?name=" + name + "&cloud=COMPLEX'></a>");
                    //var a = document.createElement("a");
                    //a.href = "/user/cloud/createrenamedownload?name=" + name + "&cloud=COMPLEX";
                    //a.click();
                }
                html.append("</div>");
                bootbox.confirm(html, function(result){
                    if(result == false){
                        sendUnlinkDropbox();
                    }
                })
            }
        }
    });
}


