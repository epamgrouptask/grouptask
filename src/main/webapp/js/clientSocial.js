/**
 * Created by 1 on 19.01.2016.
 */
$(document).ready(function () {
    /*VK.UI.button('login_button');*/
    VK.init({
        apiId: 5231954
    });
});
function authInfo(response) {
    if (response.session) {
        var id = response.session.mid;
        VK.Api.call('users.get', {user_ids: id}, function (r) {
            if (r.response) {
                var fullname = r.response[0].first_name + ' ' + r.response[0].last_name;
                var email = r.response[0].email;
                SocialLogin('vk', id, fullname, email);
            }
        });
    } else {
        //alert('not auth');
    }
}


function SocialLogin(social, id, fullname, email) {
    console.log('as');
    $.ajax({
        url: '/socialLogin',
        type: 'post',
        data: {
            id: id,
            fullname: fullname,
            email: email,
            social: social
        },
        success: function (e) {
            console.log(e);
            app.sendId(e, 1,1);
        }
    });
}
function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var id = profile.getId();
    var fullname = profile.getName();
    var email = profile.getEmail();
    SocialLogin('google', id, fullname, email);
}