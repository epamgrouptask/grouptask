function editData() {
			$("#editProfile").modal('show');
		}

		function sendRegisterData() {
			$.ajax({
				url : "/user/profile",
				type : "POST",
				data : {
					username : $('#username').val(),
					id : $('#userId').val(),
					password : $('#password').val(),
					email : $('#Email').val(),
					fullname : $('#fullname').val()
				},
				success : function(data) {
					switch (data) {
					case 'ok':
						window.location.reload(true);
						break;
					case 'nameFail':
						$('#userNE')[0].setAttribute('class', 'has-error');
						$('#change').attr("disabled", "disabled");
						break;
					case 'emailFail':
						$('#emailE')[0].setAttribute('class', 'has-error');
						$('#change').attr("disabled", "disabled");
						break;
					case 'passFail':
						$('#passE')[0].setAttribute('class', 'has-error');
						$('#change').attr("disabled", "disabled");
						break;
					}
				}
			});
		}

		function showPassChange() {
			$("#changingPass").modal('show');
		}
		$('#pass').keyup(function() {
			var pass = $('#pass').val();
			var confirm = $('#cPassword').val();
			if (confirm !== "") {
				if (pass !== confirm) {
					$('#newP')[0].setAttribute('class', 'has-error');
					$('#newCP')[0].setAttribute('class', 'has-error');
					$('#send').attr("disabled", "disabled");
				} else {
					$('#newP')[0].setAttribute('class', 'has-success');
					$('#newCP')[0].setAttribute('class', 'has-success');
					$('#send').removeAttr("disabled");
				}
			}
		});
		$('#cPassword').keyup(function() {
			var pass = $('#pass').val();
			var confirm = $('#cPassword').val();
			if (pass !== confirm) {
				$('#newP')[0].setAttribute('class', 'has-error');
				$('#newCP')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#newP')[0].setAttribute('class', 'has-success');
				$('#newCP')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
		});
		$('#oldPass').keyup(function() {
			var pass = $('#oldPass').val();
			if (pass == "") {
				$('#oldP')[0].setAttribute('class', 'has-error');
				$('#send').attr("disabled", "disabled");
			} else {
				$('#oldP')[0].setAttribute('class', 'has-success');
				$('#send').removeAttr("disabled");
			}
		});
		function changePass() {
			console.log("df");
			$.ajax({
				url : "/user/profile",
				type : "POST",
				data : {
					password : $('#oldPass').val(),
					newPass : $('#pass').val(),
					id : $('#userId').val()
				},
				success : function(data) {
					console.log(data);
					switch (data) {

					case 'okPass':
						$('#changingPass').modal('toggle');
						break;
					case 'failPass':
						$('#oldP')[0].setAttribute('class', 'has-error');
						$('#send').removeAttr("disabled");
						break;
					}
				}
			});
		}
		function emptyChecker(id) {
			var parentDivId;
			switch (id) {
			case '#fullname':
				parentDivId = '#fullNE';
				break;
			case '#username':
				parentDivId = '#userNE';
				break;
			case '#password':
				parentDivId = '#passE';
				break;
			case '#confCode':
				parentDivId='#codeE';
				break;
			}
			var val = $(id).val();
			if (val == '') {
				$(parentDivId)[0].setAttribute('class', 'has-error');
				$('#change').attr("disabled", "disabled");
			} else {
				$(parentDivId)[0].setAttribute('class', 'has-success');
				$('#change').removeAttr("disabled");
			}
			$(id).keyup(function() {
				var val = $(id).val();
				if (val == '') {
					$(parentDivId)[0].setAttribute('class', 'has-error');
					$('#change').attr("disabled", "disabled");
				} else {
					$(parentDivId)[0].setAttribute('class', 'has-success');
					$('#change').removeAttr("disabled");
				}
			});
		}
		function validateEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}
		$('#Email').keyup(function() {
			var email = $('#Email').val();
			if (email == "" || !validateEmail(email)) {
				$('#emailE')[0].setAttribute('class', 'has-error');
				$('#change').attr("disabled", "disabled");
			} else {
				$('#emailE')[0].setAttribute('class', 'has-success');
				$('#change').removeAttr("disabled");
			}
		});