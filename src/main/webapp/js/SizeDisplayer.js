function displaySize(size) {
    var sizeDimention = ["kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    if (size == 'undefined' || isNaN(size) || size == 0) {
        return '';
    }
    var i = 0;
    var sizeIn = size;
    if(size <= 1024){
        return size + " B";
    }
    while (sizeIn > 1024) {
        if (i > sizeDimention.length) {
            break;
        }
        sizeIn /= 1024;
        i++;
    }
    i--;
    result = size / Math.pow(1024, i + 1);
    result = parseFloat(Math.round(result * 100) / 100).toFixed(2);
    return result + " " + sizeDimention[i];
}