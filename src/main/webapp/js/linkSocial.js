function authInfo(response) {
    if (response.session) {
        var id = response.session.mid;
        VK.Api.call('users.get', {user_ids: id}, function (r) {
            if (r.response) {
                var fullname = r.response[0].first_name + ' ' + r.response[0].last_name;
                var email = r.response[0].email;
                SocialLogin('vk',id,fullname,email);
            }
        });
    } else {
        //alert('not auth');
    }
}/**
 * Created by 1 on 24.01.2016.
 */

function SocialLogin(social, id, fullname, email) {
    $.ajax({
        url: '/linkSocial',
        type: 'post',
        data: {
            id: id,
            fullname: fullname,
            email: email,
            social: social
        },
        success: function(e) {
            console.log(e);
         location.reload();
             }
        });
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    var id = profile.getId();
    var fullname = profile.getName();
    var email = profile.getEmail();
    SocialLogin('google',id,fullname,email);
}

$(document).ready(function(){
    $("#linkVK").click(authInfo);
});