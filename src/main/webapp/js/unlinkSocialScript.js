/**
 * Created by 1 on 26.01.2016.
 */
function unlinkVK(){
    $.ajax({
        url:"/unlinkSocial",
        type: "post",
        data:{
            social:"vk"
        },
        success:function(){
            VK.Auth.logout();
          location.reload();
        }
    });
}
function onLoad() {
    gapi.load('auth2', function () {
        gapi.auth2.init();
    });
}
function unlinkGooglePlus(){
    $.ajax({
        url:"/unlinkSocial",
        type: "post",
        data:{
            social:"google"
        },
        success:function(){
            var auth3 = gapi.auth2.getAuthInstance();
            auth3.signOut().then(function () {
                console.log('User signed out.');
            });
            location.reload();
        }
    });
}

    $("#unlinkGooglePlus").click(unlinkGooglePlus);
    $("#unlinkVK").click(unlinkVK);
    onLoad();