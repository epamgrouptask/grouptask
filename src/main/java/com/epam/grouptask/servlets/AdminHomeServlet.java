package com.epam.grouptask.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;

import java.io.IOException;

/**
 * Created by Andrian on 17.01.2016.
 */
public class AdminHomeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession ses = request.getSession(true);
        User user = (User) ses.getAttribute("user");
        UserService us = new UserService();
        user = us.getUserById(user.getId());
        request.setAttribute("user", user);
        request.getRequestDispatcher("/pages/adminHome.jsp").forward(request, response);
    }
}
