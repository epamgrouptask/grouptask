package com.epam.grouptask.servlets;

import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.uploadUtil.UploadStream;
import com.epam.grouptask.utils.StatisticController;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.lang.reflect.Field;
import java.util.UUID;

/**
 * Created by 1 on 10.02.2016.
 */
@MultipartConfig
public class UploadBigFileServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(GetDeleteUploadServlet.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("user");
        FileOutputStream fos = UploadStream.getStream(user.getId());
        if (fos == null){
            fos = new FileOutputStream(new File(UUID.randomUUID().toString()));
        }
        InputStream in = request.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(in,16*1024);
        byte[] data = new byte[1024];
        while (bis.read(data)>0){
            fos.write(data);
            fos.flush();
        }
        UploadStream.addStream(fos,user.getId());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = null;
        User user = (User) request.getSession().getAttribute("user");
        FileOutputStream fos = UploadStream.getStream(user.getId());
        if (fos != null){
            Field pathField = null;
            try {
                pathField = FileOutputStream.class.getDeclaredField("path");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            pathField.setAccessible(true);
            try {
                path = (String) pathField.get(fos);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            fos.close();
        }
        File uploadFile = new File(path);
        CustomFile file = null;
        request.setCharacterEncoding("utf-8");
        String cloudStr = request.getParameter("cloud");
        if(cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = request.getParameter("clientId");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        file.setSize(uploadFile.length());
        file.setPath(request.getParameter("path"));
        file.setName(request.getParameter("name"));
        if (cloudStr.equals("DROPBOX")) {
            file.setPath(file.getPath() + "/" + file.getName());
        }
        int BUFFER_SIZE = 16 * 1024;
        InputStream fileContent = new BufferedInputStream(new FileInputStream(uploadFile), BUFFER_SIZE);
        try {
            boolean uploaded = file.uploadFile(fileContent);
            if (uploaded) {
                StatisticController.incrementUpload(cloudStr, user.getId());
            }
        } catch (FileTooLargeException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().print("err");
        } finally {
            fileContent.close();
            uploadFile.delete();
        }
    }
}
