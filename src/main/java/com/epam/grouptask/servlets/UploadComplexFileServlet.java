package com.epam.grouptask.servlets;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.utils.StatisticController;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 1 on 08.02.2016.
 */
@MultipartConfig
public class UploadComplexFileServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(GetDeleteUploadServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        /*Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);*/
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = request.getParameter("cloud");
        String clientId = request.getParameter("clientId");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        String jsonStr = request.getParameter("clouds");
        Long size = Long.valueOf(request.getParameter("size"));
        file.setSize(size);
        Map<String, Long> sizes = new HashMap<>();
        try {
            JSONObject json = new JSONObject(jsonStr);
            JSONArray jsonArray = new JSONArray(json.getString("parts"));
            int sumProcents = 0;
            long lastSize = size;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String cloud = obj.getString("cloud");
                int percent = obj.getInt("percent");
                long perc = size * percent / 100;
                sumProcents += percent;
                lastSize -= perc;
                if (i == jsonArray.length()) {
                    sizes.put(cloud, lastSize);
                } else {
                    sizes.put(cloud, perc);
                }
            }
            if (sumProcents != 100){
                //todo: send error
                return;
            }
        } catch (JSONException e) {
            LOG.warn(e);
            e.printStackTrace();
        }

        file.setPath(request.getParameter("path"));
        file.setName(request.getParameter("name"));
        Part filePart = request.getPart("file");            //get file data (is and name)
        String fileName = filePart.getSubmittedFileName();
        fileName = java.net.URLDecoder.decode(fileName,"UTF-8");
        int BUFFER_SIZE = 16 * 1024;
        InputStream fileContent = new BufferedInputStream(filePart.getInputStream(), BUFFER_SIZE);
        file.setName(fileName);

        try {
            boolean uploaded = file.uploadFileWithSplit(fileContent, sizes);
            if (uploaded) {
                StatisticController.incrementUpload(cloudStr, user.getId());
                response.getWriter().write("ok");
            }
        } catch (FileTooLargeException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
