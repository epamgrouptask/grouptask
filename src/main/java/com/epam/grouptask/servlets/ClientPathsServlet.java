package com.epam.grouptask.servlets;

import com.dropbox.core.DbxException;
import com.epam.grouptask.cloudcontroller.DriveController;
import com.epam.grouptask.cloudcontroller.DropboxController;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.ClientProgram;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ClientProgramService;
import com.epam.grouptask.services.UserService;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Executors;

/**
 * Created by Andrian on 30.01.2016.
 */
public class ClientPathsServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(ClientPathsServlet.class);

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String data = java.net.URLDecoder.decode(br.readLine(), "UTF-8");
        String[] divided = data.split("[=&]");
        Map<String, String> parameters = new HashMap<>();
        for (int i = 0; i < divided.length - 1; i += 2) {
            parameters.put(divided[i], divided[i + 1]);
        }
        String clientId = parameters.get("clientId");
        String path = parameters.get("path");
        ClientProgramService cps = new ClientProgramService();
        cps.removePathByClient(clientId, path);
        Integer userId = cps.getUserId(clientId);

        final CustomFile finalFile = new CustomFile(StorageType.CLIENT, userId, clientId);
        Executors.newSingleThreadExecutor().execute(() -> {
            finalFile.updateFileTree();
        });
        response.getWriter().write("ok");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String userId = request.getParameter("userId");
        String clientId = request.getParameter("clientId");
        String pathStr = request.getParameter("paths");
        List<String> paths = new ArrayList<>();
        for (String str : pathStr.split(",")) {
            paths.add(str);
        }
        ClientProgram clientProgram = new ClientProgram();
        clientProgram.setPaths(paths);
        clientProgram.setUserId(Integer.parseInt(userId));
        clientProgram.setClientId(clientId);
        ClientProgramService service = new ClientProgramService();
        service.setPaths(clientProgram);
        request.setAttribute("clientId", clientId);
        request.setAttribute("userId", userId);
        request.setAttribute("paths", paths);

        final CustomFile finalFile = new CustomFile(StorageType.CLIENT, Integer.valueOf(userId), clientId);
        Executors.newSingleThreadExecutor().execute(() -> {
            finalFile.updateFileTree();
        });
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession ses = request.getSession(true);
        User user = (User) ses.getAttribute("user");
        UserService us = new UserService();
        request.setAttribute("user", user);
        String id = request.getParameter("clientId");
        String tab = request.getParameter("tab");
        ClientProgramService service = new ClientProgramService();
        List<String> paths = service.getPathByClient(id);
        request.setAttribute("paths", paths);
        DriveController driveController = null;
        DropboxController dropboxController = null;
        try {
            driveController = new DriveController(user.getId());
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        try {
            dropboxController = new DropboxController(user.getId());
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        if (driveController != null) {
            Long driveFree = driveController.getFreeSize();
            Long total = driveController.getTotalSize();
            request.setAttribute("driveFree", driveFree);
            request.setAttribute("totalDrive", total);
        } else {
            request.setAttribute("driveFree", 0);
            request.setAttribute("totalDrive", 0);
        }
        if (dropboxController != null) {
            Long dropFree = null;
            try {
                dropFree = dropboxController.getFreeSize();
            } catch (DbxException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
            request.setAttribute("dropboxFree", dropFree);
            Long total = dropboxController.getTotalSize();
            request.setAttribute("totalDrop", total);
        } else {
            request.setAttribute("dropboxFree", 0);
            request.setAttribute("totalDrop", 0);
        }
        if (tab == null) {
            tab = "";
        }
        switch (tab) {
            case "paths":
                request.setAttribute("tabPaths", "tab-pane fade in active");
                request.setAttribute("tabProfile", "tab-pane fade");
                request.setAttribute("tabStorages", "tab-pane fade");
                break;
            default:
                request.setAttribute("tabPaths", "tab-pane fade");
                request.setAttribute("tabProfile", "tab-pane fade in active");
                request.setAttribute("tabStorages", "tab-pane fade");
        }
        request.setAttribute("clientId", id);
        ClientProgramService cps = new ClientProgramService();
        String clientname = cps.getClientById(user.getId(), id).getName();
        request.setAttribute("clientname", clientname);

        Locale locale = null;
        String loc = request.getParameter("language");
        if (loc == null) {
            loc = service.getUserLocale(id);
        } else {
            int userId = service.getUserId(id);
            us.changeLocale(loc, userId);
        }
        locale = new Locale(loc);
        ResourceBundle resourceBundle = null;
        try {
            resourceBundle = ResourceBundle.getBundle("Text", locale);
        } catch (MissingResourceException e) {
            if(locale.toString().equals("uk_UA") || locale.toString().equals("uk_ua")){
                locale = new Locale("uk");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else if(locale.toString().equals("en_US") || locale.toString().equals("en_us")){
                locale = new Locale("en");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else {
                e.printStackTrace();
            }
        }
        request.setAttribute("bundle", resourceBundle);

        request.getRequestDispatcher("/pages/clientprofile.jsp").forward(request, response);
    }
}
