package com.epam.grouptask.servlets;

import com.epam.grouptask.model.User;
import com.epam.grouptask.utils.ActiveStorages;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Andrian on 25.01.2016.
 */
public class ActiveStorageServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(ActiveStorageServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        User user = (User) session.getAttribute("user");
        int userId = user.getId();
        JSONObject active = ActiveStorages.getActive(userId);
        response.getWriter().print(active);
    }
}
