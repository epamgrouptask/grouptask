package com.epam.grouptask.servlets;

import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ClientProgramService;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Created by 1 on 14.02.2016.
 */
public class GetClientPathsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle resourceBundle = null;
        try {
            resourceBundle = ResourceBundle.getBundle("Text", locale);
        } catch (MissingResourceException e) {
            if(locale.toString().equals("uk_UA") || locale.toString().equals("uk_ua")){
                locale = new Locale("uk");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else if(locale.toString().equals("en_US") || locale.toString().equals("en_us")){
                locale = new Locale("en");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else {
                e.printStackTrace();
            }
        }

        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        User user = (User) session.getAttribute("user");
        ClientProgramService cps = new ClientProgramService();
        List<String> paths = new ArrayList<>();
        String clientId = request.getParameter("clientId");
        paths = cps.getPathByClient(clientId);
        JSONArray jsonArray = new JSONArray();
        for (String path : paths) {
            jsonArray.put(path);
        }
        response.getWriter().write(jsonArray.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
