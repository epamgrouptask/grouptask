package com.epam.grouptask.servlets;

import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.StatisticController;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.Executors;

/**
 * Created by Andrian on 17.01.2016.
 */
@MultipartConfig
public class GetDeleteUploadServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(GetDeleteUploadServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get all files and folders by path
        HttpSession session = request.getSession();
        Locale locale = null;
        /*try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);*/
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = request.getParameter("cloud");                //cloud ''
        if (cloudStr != null) cloudStr = cloudStr.toUpperCase();

        String clientId = request.getParameter("clientId");
        //clientId = clientId.toLowerCase();
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        String path = request.getParameter("path");
        file.setPath(java.net.URLDecoder.decode(path, "UTF-8"));                     //path ''

        List<CustomFile> files = file.getDirectory();
        JSONArray jsonArray = new JSONArray();
        if (files != null) {
            for (int i = 0; i < files.size(); i++) {
                try {
                    String jsString = files.get(i).toJson();
                    jsonArray.put(new JSONObject(jsString));
                } catch (JSONException e) {
                    LOG.warn(e);
                    e.printStackTrace();
                }
            }
        }

        StatisticController.incrementGet(cloudStr, user.getId());
        response.getWriter().write(jsonArray.toString());
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //delete file or directory
        HttpSession session = request.getSession();
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            LOG.warn(e);
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String data = br.readLine();
        data = java.net.URLDecoder.decode(data, "UTF-8");
        String[] divided = data.split("[=&]");
        Map<String, String> parameters = new HashMap<>();
        for (int i = 0; i < divided.length; i += 2) {
            parameters.put(divided[i], divided[i + 1]);
        }

        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = parameters.get("cloud");
        if (cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = parameters.get("clientId");
        String type = parameters.get("type");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        file.setFileId(parameters.get("fileId"));//request.getParameter("fileId"));//path ''
        if (type.equals("fil")) {
            file.setType("file");
        } else if (type.equals("fol")) {
            file.setType("folder");
        }
        file.setPath(parameters.get("path"));//request.getParameter("path"));                     //path ''
        file.setName(parameters.get("name"));
        boolean deleted = file.delete();
        if (deleted) {
            int delete = StatisticController.incrementDelete(cloudStr, user.getId());
            if (delete % 15 == 0) {           //if was deleted 15 files for this day
                UserService userService = new UserService();
                userService.changeBlocked(user.getId(), true);
            }
            if (file.getType().equals("folder")) {
                final CustomFile finalFile = file;
                Executors.newSingleThreadExecutor().execute(() -> {
                    finalFile.updateFileTree();
                });
            }
            response.getWriter().write("ok");
        } else {
            response.getWriter().write("err");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //upload file to server
        HttpSession session = request.getSession();
        /*Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);*/
        //response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = request.getParameter("cloud");
        if (cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = request.getParameter("clientId");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        file.setSize(Long.valueOf(request.getParameter("size")));
        System.out.println(request.getParameter("path") + " " + request.getParameter("name"));
        file.setPath(request.getParameter("path"));
        //file.setName(request.getParameter("name"));
        Part filePart = request.getPart("file");            //get file data (is and name)
        String fileName = filePart.getSubmittedFileName();
        if (cloudStr.equals("DROPBOX")) {
            if (!file.getPath().equals("/")) {
                file.setPath(file.getPath() + "/" + fileName);
            } else {
                file.setPath(file.getPath() + fileName);
            }
        }
        int BUFFER_SIZE = 16 * 1024;
        InputStream fileContent = new BufferedInputStream(filePart.getInputStream(), BUFFER_SIZE);

        file.setName(fileName);
        try {
            boolean uploaded = file.uploadFile(fileContent);
            if (uploaded) {
                StatisticController.incrementUpload(cloudStr, user.getId());
                response.getWriter().write("ok");
            } else {
                response.getWriter().write("err");
            }
        } catch (FileTooLargeException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().print("err");
        }
    }
}
