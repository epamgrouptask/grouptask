package com.epam.grouptask.servlets.localLogin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.grouptask.utils.HashChecker;
import com.epam.grouptask.model.User;
import com.epam.grouptask.notification.EmailSender;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class PasswordRestoreServlet
 */

public class PasswordRestoreServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(PasswordRestoreServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StringBuffer requestURL = request.getRequestURL();
        ValidationCodeService vcs = new ValidationCodeService();
        if (request.getQueryString() != null) {
            requestURL.append("?").append(request.getQueryString());
        }
        String completeURL = requestURL.toString();
        System.out.println(completeURL);
        String login = completeURL.substring(completeURL.lastIndexOf("/") + 1, completeURL.indexOf("_"));
        String code = completeURL.substring(completeURL.lastIndexOf("/") + 1);
        try {
            if (!vcs.valid(code)) {
                if (completeURL.contains("GroupTask")) {
                    response.sendRedirect("/GroupTask/checkcode/error");
                } else {
                    response.sendRedirect("/checkcode/error");
                }
            } else {
                request.setAttribute("login", login);
                request.setAttribute("code", code);
                request.getRequestDispatcher("/pages/restorePass.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("email");
		String login = request.getParameter("username");
		UserService us = new UserService();
		User user = null;
		user = us.getUserSameByNameEmail(login, email,0);
		if (user != null) {
			if(!email.equals("") && !login.equals("")){
				if(email.equals(user.getEmail()) && login.equals(user.getUserName())){					
				}else{
					response.getWriter().print("no user");
					return;
				}
			}
			ValidationCodeService vcs = new ValidationCodeService();
			String code = null;
			if(user.getEmail() == null){
				response.getWriter().print("no email");          //!!!!!!
			} else {
                try {
                    code = user.getUserName() + "_restore_" + HashChecker.getHash(user.getEmail() + user.getPassword());
                } catch (NoSuchAlgorithmException e) {
                    LOG.warn(e);
                    e.printStackTrace();
                }
                vcs.insertCode(code, user.getId());
                StringBuffer requestURL = request.getRequestURL();
                if (request.getQueryString() != null) {
                    requestURL.append("?").append(request.getQueryString());
                }

                Locale locale = null;
                String loc = request.getParameter("language");

                if(loc == null){
                    loc = us.getUserLocale(user.getId());
                }
                locale = new Locale(loc);
                ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", locale);

                String completeURL = requestURL.toString();
                completeURL = completeURL.substring(0, completeURL.lastIndexOf("/"));
                completeURL += "/restorepass/" + code;
                EmailSender.sendEmail(user.getEmail(), "moiraalira2015@gmail.com", "moira_moira",
                        resourceBundle.getString("restore_servlet.restore_password") + completeURL, "CloudsMerger");            //resource bundle
                response.getWriter().print("sent");
            }
        } else {
            response.getWriter().print("no user");
        }
    }
}
