package com.epam.grouptask.servlets.localLogin;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.grouptask.utils.HashChecker;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class CheckRestoreServlet
 */
public class CheckRestoreServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(CheckRestoreServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckRestoreServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().append("Served at: ").append(request.getContextPath());
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pass = request.getParameter("password");
        String login = request.getParameter("username");
        String code = request.getParameter("code");
        StringBuffer requestURL = request.getRequestURL();
        requestURL = request.getRequestURL();
        if (request.getQueryString() != null) {
            requestURL.append("?").append(request.getQueryString());
        }
        String url = requestURL.toString();
        ValidationCodeService vcs = new ValidationCodeService();
        UserService us = new UserService();

        try {
            pass = HashChecker.getHash(pass);
        } catch (NoSuchAlgorithmException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        us.changePassByLogin(login, pass);
        vcs.delete(code);
        response.sendRedirect("/home");
    }

}
