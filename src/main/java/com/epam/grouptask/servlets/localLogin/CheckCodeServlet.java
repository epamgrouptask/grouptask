package com.epam.grouptask.servlets.localLogin;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;
import org.apache.log4j.Logger;

/**
 * Servlet implementation class CheckCodeServlet
 */
public class CheckCodeServlet extends HttpServlet {
	private static final Logger LOG = Logger.getLogger(CheckCodeServlet.class);
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuffer requestURL = request.getRequestURL();
		HttpSession ses = request.getSession(true);
		if (request.getQueryString() != null) {
		    requestURL.append("?").append(request.getQueryString());
		}
		String code = requestURL.toString();
		code = code.substring(code.lastIndexOf("/")+1);
		ValidationCodeService vcs = new ValidationCodeService();
		try {
			if(!vcs.valid(code)){
				response.sendRedirect("error");
			}else{
				UserService us = new UserService();
				User user = us.getUserByCode(code);

				ses.setAttribute("user", user);
				vcs.delete(code);
				us.setConfirmed(user.getUserName());
				requestURL = request.getRequestURL();
				if (request.getQueryString() != null) {
				    requestURL.append("?").append(request.getQueryString());
				}
				response.sendRedirect("/home");
			}
		} catch (SQLException e) {
			LOG.warn(e);
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
        HttpSession ses = request.getSession(false);
        User user = (User) ses.getAttribute("user");
        String requestCode = request.getParameter("code");

        if(user != null) {
            ValidationCodeService codeService = new ValidationCodeService();
			boolean exist = codeService.isExist(requestCode);
            if (exist) {
                UserService userService = new UserService();
                userService.changeBlocked(user.getId(), false);
                ValidationCodeService validationCodeService = new ValidationCodeService();
                validationCodeService.delete(requestCode);
                response.getWriter().print("confirmed");
            } else {
                response.getWriter().print("bad");
            }
        }
	}

}
