package com.epam.grouptask.servlets.localLogin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.grouptask.utils.CodeIdGenerator;
import com.epam.grouptask.utils.HashChecker;
import com.epam.grouptask.model.User;
import com.epam.grouptask.notification.EmailSender;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Andrian on 17.01.2016.
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(LoginServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		if (password != null) {
			try {
				password = HashChecker.getHash(password);
			} catch (NoSuchAlgorithmException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
		}
		JSONObject jsonResp = new JSONObject();
		User user;
		UserService userService = new UserService();
		if ((user = userService.getUserByName(username)) == null) {
			try {
				jsonResp.put("err", "no user by login");
			} catch (JSONException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
			response.getWriter().print(jsonResp);
		} else {
			if (user.getPassword().equals(password)) {
				if (user.isConfirmed()) {
					HttpSession ses = request.getSession(true);
					ses.setAttribute("user", user);
					LOG.info(user.getUserName() + " has login to system, his id = " + user.getId());
					try {
						switch (user.getRole()) {
						case USER:
							jsonResp.put("redirect", "/user/home");
							break;
						case ADMIN:
							jsonResp.put("redirect", "/admin/home");
							break;
						}
					} catch (JSONException e) {
						LOG.warn(e);
						e.printStackTrace();
					}
					response.getWriter().print(jsonResp);
				} else {
					try {
						jsonResp.put("err", "not confirmed");
					} catch (JSONException e) {
						LOG.warn(e);
						e.printStackTrace();
					}
					response.getWriter().print(jsonResp);
				}
			} else {
				try {
					jsonResp.put("err", "not correct password");
				} catch (JSONException e) {
					LOG.warn(e);
					e.printStackTrace();
				}
				response.getWriter().print(jsonResp);
			}
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String email = request.getParameter("email");
		String username = request.getParameter("username");
		UserService userService = new UserService();
		User sameUser = userService.getUserSameByNameEmail(username, email, user.getId());
		if (sameUser != null) {
			if (sameUser.getUserName().equals(username)) {
				response.getWriter().write("nameFail");
				return;
			} else {
				response.getWriter().write("emailFail");
				return;
			}
		}
		String code = CodeIdGenerator.generateBlockedCode();
		EmailSender.sendEmail(user.getEmail(), "moiraalira2015@gmail.com", "moira_moira", "enter code " + code,
				"CloudsMerger");
		ValidationCodeService codeService = new ValidationCodeService();
		codeService.insertCode(code, user.getId());
		response.getWriter().write("success");
	}
}
