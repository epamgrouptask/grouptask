package com.epam.grouptask.servlets.localLogin;

import com.epam.grouptask.model.Role;
import com.epam.grouptask.model.User;
import com.epam.grouptask.notification.EmailSender;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;
import com.epam.grouptask.utils.CodeIdGenerator;
import com.epam.grouptask.utils.HashChecker;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Servlet implementation class RegistrationServlet
 */
public class RegistrationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(RegistrationServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String password = request.getParameter("password");
        String username = request.getParameter("username");
        String email = request.getParameter("email");
        String fullname = request.getParameter("fullname");
        UserService userService = new UserService();
        User user = null;
        User sameUser = null;
        if (!email.equals("") & username.equals("") && (fullname.equals("") && password.equals("")) ||
                (fullname == null && password == null)) {
            User userToResend = userService.getUserByName(username);
            if (userToResend == null) {
                response.getWriter().print("loginFail");
            } else {
            	if((sameUser = userService.getUserSameByNameEmail(username, email,0)) != null && sameUser.getId()!=userToResend.getId()){
            		response.getWriter().print("emailFail");
            		return;
            	}
                String generatedCode = CodeIdGenerator.generateCode(username, email);
                ValidationCodeService vcs = new ValidationCodeService();
                while (vcs.isExist(generatedCode)) {
                    generatedCode = CodeIdGenerator.generateCode(username, email);
                }
                vcs.insertCode(generatedCode, userToResend.getId());
                StringBuffer requestURL = request.getRequestURL();
                if (request.getQueryString() != null) {
                    requestURL.append("?").append(request.getQueryString());
                }

                Locale locale = null;
                String loc = request.getParameter("language");

                if(loc == null){
                    loc = userService.getUserLocale(userToResend.getId());
                }
                locale = new Locale(loc);
                ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", locale);

                String completeURL = requestURL.toString();
                completeURL = completeURL.substring(0, completeURL.lastIndexOf("/"));
                completeURL += "/checkcode/" + generatedCode;
                EmailSender.sendEmail(email, "moiraalira2015@gmail.com", "moira_moira",                //TODO: reformat from bundle
                        resourceBundle.getString("registration_servlet.register")
                                + completeURL,
                        "CloudsMerger");
                userService.changeEmailByLogin(username, email);
                response.getWriter().print("sent");
            }
            return;
        }
        if (!"".equals(password) && password != null) {
            try {
                password = HashChecker.getHash(password);
            } catch (NoSuchAlgorithmException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        if ((sameUser = userService.getUserSameByNameEmail(username, email,0)) == null) {
            user = new User();
            user.setRole(Role.USER);
            user.setPassword(password);
            user.setUserName(username);
            user.setEmail(email);
            user.setFullname(fullname);
            userService.insertUser(user);
            String generatedCode = CodeIdGenerator.generateCode(username, email);
            ValidationCodeService vcs = new ValidationCodeService();
            while (vcs.isExist(generatedCode)) {
                generatedCode = CodeIdGenerator.generateCode(username, email);
            }
            user = userService.getUserSameByNameEmail(username, email,0);
            vcs.insertCode(generatedCode, user.getId());
            StringBuffer requestURL = request.getRequestURL();
            if (request.getQueryString() != null) {
                requestURL.append("?").append(request.getQueryString());
            }

            Locale locale = null;
            String loc = request.getParameter("language");

            if(loc == null){
                loc = userService.getUserLocale(user.getId());
            }
            locale = new Locale(loc);
            ResourceBundle resourceBundle = ResourceBundle.getBundle("Text", locale);

            String completeURL = requestURL.toString();
            completeURL = completeURL.substring(0, completeURL.lastIndexOf("/"));
            completeURL += "/checkcode/" + generatedCode;
            EmailSender.sendEmail(user.getEmail(), "moiraalira2015@gmail.com", "moira_moira",
                    resourceBundle.getString("registration_servlet.register")
                            + completeURL,
                    "CloudsMerger");
            response.getWriter().write("sent");
        } else {
            if (sameUser.getUserName().equals(username)) {
                response.getWriter().write("nameFail");
            } else {
                response.getWriter().write("emailFail");
            }
        }
    }
}
