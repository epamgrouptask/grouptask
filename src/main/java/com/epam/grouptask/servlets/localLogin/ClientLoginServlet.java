package com.epam.grouptask.servlets.localLogin;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.HashChecker;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ClientLoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(ClientLoginServlet.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Locale locale = null;
        String loc = request.getParameter("language");
        if (loc != null) {
            locale = new Locale(loc);
        } else {
            locale = new Locale("en");
        }
        ResourceBundle resourceBundle = null;
        try {
            resourceBundle = ResourceBundle.getBundle("Text", locale);
        } catch (MissingResourceException e) {
            if(locale.toString().equals("uk_UA") || locale.toString().equals("uk_ua")){
                locale = new Locale("uk");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else if(locale.toString().equals("en_US") || locale.toString().equals("en_us")){
                locale = new Locale("en");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else {
                e.printStackTrace();
            }
        }
        request.setAttribute("bundle", resourceBundle);
        request.getRequestDispatcher("/pages/main.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String password = request.getParameter("password");
        String username = request.getParameter("username");
        if (password != null) {
            try {
                password = HashChecker.getHash(password);
            } catch (NoSuchAlgorithmException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        User user;
        UserService userService = new UserService();
        if ((user = userService.getUserByName(username)) == null) {
            response.getWriter().print("no user by login");
        } else {
            if (user.getPassword().equals(password)) {
                if (user.isConfirmed()) {
                    HttpSession ses = request.getSession(true);
                    ses.setAttribute("user", user);
                   /* ClientProgramService service = new ClientProgramService();
                    List<String> paths = service.getPathByClient(service.ge);
                    request.setAttribute("paths", paths);
                    JsonObject ob = new JsonObject();*/
                    response.getWriter().print(user.getId());
                } else {
                    response.getWriter().print("not confirmed");
                }
            } else {
                response.getWriter().print("not correct password");
            }
        }
    }

}
