package com.epam.grouptask.servlets;

import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by 1 on 03.02.2016.
 */
public class FileTreeServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(FileTreeServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            LOG.warn(e);
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);

        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        CustomFile file = null;

        User user = (User) session.getAttribute("user");

        String cloudStr = request.getParameter("cloud");
        if(cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = request.getParameter("clientId");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            // response.getWriter().write("err"); //todo: json response
        }

        CustomDirectory customDirectory = file.getFileTree();
        if(!"CLIENT".equals(cloudStr)) {
            //CustomDirectory customDirectory = file.getFileTree();
            if (cloudStr.equals("COMPLEX")) {
                String json = customDirectory.nodesToJson();
                response.getWriter().write(json);
            } else {
                response.getWriter().write("[" + customDirectory.toJson() + "]");
            }
        } else {
            response.getWriter().write(customDirectory.nodesToJson());
            //response.getWriter().write(file.getJsonFileTree());
        }
    }
}
