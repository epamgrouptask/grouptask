package com.epam.grouptask.servlets.profile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.HashChecker;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Andrian on 17.01.2016.
 */
public class AdminProfileServlet extends HttpServlet {
	private static final Logger LOG = Logger.getLogger(AdminProfileServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String password = request.getParameter("password");
		String username = request.getParameter("username");
		String email = request.getParameter("email");
		int id = Integer.valueOf(request.getParameter("id"));
		String fullname = request.getParameter("fullname");
		String newpassword = request.getParameter("newPass");
		UserService userService = new UserService();
		User user = null;
		User sameUser = null;
		if (password != null) {
			try {
				password = HashChecker.getHash(password);
			} catch (NoSuchAlgorithmException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
		}
		user = userService.getUserById(id);
		if (newpassword != null) {
			if (password.equals(user.getPassword())) {
				try {
					userService.changePassByLogin(user.getUserName(), HashChecker.getHash(newpassword));
				} catch (NoSuchAlgorithmException e) {
					LOG.warn(e);
					e.printStackTrace();
				}
				response.getWriter().write("okPass");
			} else {
				response.getWriter().write("failPass");
			}
			return;
		}
		if (user.getPassword().equals(password)) {
			if ((sameUser = userService.getUserSameByNameEmail(username, email,user.getId())) == null) {
				userService.changeEUFbyId(fullname, username, email, id);
				response.getWriter().write("ok");

			} else {

				if (sameUser.getUserName().equals(username)) {
					response.getWriter().write("nameFail");

				} else {
					response.getWriter().write("emailFail");
				}

			}
		} else {
			response.getWriter().write("passFail");
		}
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
