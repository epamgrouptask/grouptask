package com.epam.grouptask.servlets.profile;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.grouptask.utils.HashChecker;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;

import org.apache.log4j.Logger;

/**
 * Created by Andrian on 17.01.2016.
 */
public class UserProfileServlet extends HttpServlet {
	private static final Logger LOG = Logger.getLogger(AdminProfileServlet.class);

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String email = request.getParameter("email");
		int id = Integer.valueOf(request.getParameter("id"));
		String fullname = request.getParameter("fullname");
		String newpassword = request.getParameter("newPass");
		String code = request.getParameter("code");
		UserService userService = new UserService();
		User user = null;
		User sameUser = null;
		
		if (password != null) {
			try {
				password = HashChecker.getHash(password);
			} catch (NoSuchAlgorithmException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
		}
		user = userService.getUserById(id);
		if (newpassword!=null) {
			if (password.equals(user.getPassword())) {
				try {
					userService.changePassByLogin(user.getUserName(), HashChecker.getHash(newpassword));
				} catch (NoSuchAlgorithmException e) {
					LOG.warn(e);
					e.printStackTrace();
				}
				response.getWriter().write("okPass");
			} else {
				response.getWriter().write("failPass");
			}
			return;
		}
		ValidationCodeService vcs = new ValidationCodeService();
		try {
			if (user.getPassword().equals(password) || vcs.valid(code)) {
				if(code!=null){
					vcs.delete(code);
				}
				if ((sameUser = userService.getUserSameByNameEmail(username, email,user.getId())) == null) {
					userService.changeEUFbyId(fullname, username, email, id);
					response.getWriter().write("ok");

				} else {

					if (sameUser.getUserName().equals(username)) {
						response.getWriter().write("nameFail");

					} else {
						response.getWriter().write("emailFail");
					}

				}
			} else {
				if(!vcs.valid(code)){
					response.getWriter().write("codeFail");
				}else{
					response.getWriter().write("passFail");
				}
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession ses = request.getSession(true);
        UserService us = new UserService();
        User user = (User) ses.getAttribute("user");
        User modified = us.getUserById(user.getId());
        request.setAttribute("user", modified);
        request.getRequestDispatcher("/pages/profile.jsp").forward(request, response);
    }
}
