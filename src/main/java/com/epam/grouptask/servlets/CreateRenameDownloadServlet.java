package com.epam.grouptask.servlets;

import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.StatisticController;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;

/**
 * Created by Andrian on 17.01.2016.
 */
public class CreateRenameDownloadServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(CreateRenameDownloadServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //rename file or directory
        HttpSession session = request.getSession();
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            LOG.warn(e);
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = request.getParameter("cloud");                //cloud ''
        if (cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = request.getParameter("clientId");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        String type = request.getParameter("type");
        if (type.equals("fil")) {
            file.setType("file");
        } else if (type.equals("fol")) {
            file.setType("folder");
        }
        file.setPath(request.getParameter("path"));                     //path ''
        file.setFileId(request.getParameter("fileId"));
        String newName = request.getParameter("newName");

        //CHANGE PATH
        boolean renamed = file.rename(newName);
        if (renamed) {
            StatisticController.incrementRename(cloudStr, user.getId());
            if (file.getType().equals("folder")) {
                final CustomFile finalFile = file;
                Executors.newSingleThreadExecutor().execute(() -> {
                    finalFile.updateFileTree();
                });
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //create folder
        HttpSession session = request.getSession();
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            LOG.warn(e);
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String data = br.readLine();
        data = java.net.URLDecoder.decode(data, "UTF-8");
        String[] divided = data.split("[=&]");
        Map<String, String> parameters = new HashMap<>();
        for (int i = 0; i < divided.length; i += 2) {
            parameters.put(divided[i], divided[i + 1]);
        }
        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = parameters.get("cloud");
        if (cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = parameters.get("clientId");// cloud ''
        if (clientId != null) {
            clientId = java.net.URLDecoder.decode(parameters.get("clientId"), "UTF-8");// request.getParameter("clientId");
        }
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");      //todo: json
        }
        String correctPath = "";
        if (file.getStorageType() == StorageType.DRIVE) {
            correctPath = "/";
        }
        file.setPath(correctPath + java.net.URLDecoder.decode(parameters.get("path"), "UTF-8"));//request.getParameter("path"));                     //path ''
        file.setFileId(parameters.get("fileId"));//request.getParameter("fileId"));

        String newName = java.net.URLDecoder.decode(parameters.get("newName"), "UTF-8");//request.getParameter("newName");
        boolean created = file.createDirectory(newName);
        if (created) {
            StatisticController.incrementCreate(cloudStr, user.getId());

            final CustomFile finalFile = file;
            Executors.newSingleThreadExecutor().execute(() -> {
                finalFile.updateFileTree();
            });

            response.getWriter().write("created");
        } else {
            response.getWriter().write("err");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //download file from cloud|client
        HttpSession session = request.getSession();
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            LOG.warn(e);
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);

        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        CustomFile file = null;

        User user = (User) session.getAttribute("user");

        String cloudStr = request.getParameter("cloud");
        if (cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = request.getParameter("clientId");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err"); //todo: json response
        }
        String fileId = request.getParameter("fileId");
        if (cloudStr.equals("DROPBOX")) {
            file.setName(fileId.substring(fileId.lastIndexOf("/") + 1));
            file.setPath(fileId);
        } else {
            file.setName(request.getParameter("name"));
            file.setPath(request.getParameter("path"));
        }
        file.setFileId(fileId);
        ServletContext ctx = getServletContext();
        String mimeType = ctx.getMimeType(file.getName());
        response.reset();

        ServletOutputStream os = response.getOutputStream();

        response.setContentType(mimeType != null ? mimeType : "application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + getContentDespositionFilename(file.getName()) + "\"");

        int download = StatisticController.incrementDownload(cloudStr, user.getId());
        if (download % 20 == 0) {
            UserService userService = new UserService();        //every 20 downloaded files
            userService.changeBlocked(user.getId(), true);
        }

        file.downloadFile(os);
    }

    public String getContentDespositionFilename(String fileName) {
        try {
            byte[] fileNameBytes = fileName.getBytes("utf-8");
            String dispositionFileName = "";
            for (byte b : fileNameBytes) {
                dispositionFileName += (char) (b & 0xff);
            }
            return dispositionFileName;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return fileName;
    }
}
