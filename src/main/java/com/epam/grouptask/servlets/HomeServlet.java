package com.epam.grouptask.servlets;

import com.epam.grouptask.model.User;
import com.epam.grouptask.socketServer.Server;
import com.epam.grouptask.socketServer.SocketChannelManager;
import com.epam.grouptask.utils.StatSaver;
import com.epam.grouptask.utils.StatisticController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Servlet implementation class HomeServlet
 */
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Server server = SocketChannelManager.getServer();
        StatSaver.setTime(1);

        HttpSession ses = request.getSession(true);
        User user = (User) ses.getAttribute("user");
        if (user != null) {
            request.setAttribute("login", user.getUserName());
            request.setAttribute("confirmed", user.isConfirmed());
        }
        request.getRequestDispatcher("/pages/home.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
