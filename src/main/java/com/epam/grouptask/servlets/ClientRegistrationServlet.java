package com.epam.grouptask.servlets;

import com.epam.grouptask.model.ClientProgram;
import com.epam.grouptask.services.ClientProgramService;
import com.epam.grouptask.utils.CodeIdGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Andrian on 27.01.2016.
 */
public class ClientRegistrationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //HttpSession ses = request.getSession(false);
        //User user = (User) ses.getAttribute("user");
        Integer id = Integer.valueOf(request.getParameter("id"));
        String name = request.getParameter("name");
        String mac = request.getParameter("mac");

        String clientId = CodeIdGenerator.generateClientId();

        ClientProgramService service = new ClientProgramService();
        List<ClientProgram> programList = service.getAllClientsByUserId(id, 0);

        boolean exist = false;
        if (programList != null) {
            for (ClientProgram clientProgram : programList) {
                if (clientProgram.getMac().equals(mac)) {
                    if (clientProgram.isRemoved()) {
                        service.setRemoved(clientProgram.getClientId(), false);
                    }
                    clientId = clientProgram.getClientId();
                    exist = true;
                    break;
                }
            }
        }
        if (!exist) {
            ClientProgram clientProgram = new ClientProgram();              //get from parameters
            clientProgram.setClientId(clientId);
            clientProgram.setUserId(id);
            clientProgram.setMac(mac);
            clientProgram.setName(name);
            service.setClientWithoutPaths(clientProgram);
        }
        response.getWriter().print(clientId);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //set client program removed
        HttpSession session = request.getSession();
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String data = br.readLine();
        data = java.net.URLDecoder.decode(data, "UTF-8");
        String[] divided = data.split("[=&]");
        Map<String, String> parameters = new HashMap<>();
        for (int i = 0; i < divided.length; i += 2) {
            parameters.put(divided[i], divided[i + 1]);
        }
        ClientProgramService cps = new ClientProgramService();
        cps.setRemoved(parameters.get("clientId"), true);

    }
}
