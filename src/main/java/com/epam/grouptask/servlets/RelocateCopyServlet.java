package com.epam.grouptask.servlets;

import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ClientProgramService;
import com.epam.grouptask.utils.StatisticController;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;

/**
 * Created by Andrian on 17.01.2016.
 */
public class RelocateCopyServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(RelocateCopyServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //move file or folder
        HttpSession session = request.getSession();                                 //
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            LOG.warn(e);
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = request.getParameter("cloud");                //cloud ''
        String clientId = request.getParameter("clientId");
        if (cloudStr != null){
            cloudStr = cloudStr.toUpperCase();
        }
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        file.setPath(request.getParameter("path"));
        if(cloudStr != null) cloudStr = cloudStr.toUpperCase();
        file.setFileId(request.getParameter("fileId"));
        file.setName(request.getParameter("name"));

        String toPath = request.getParameter("toPath");
        String toCloudStr = request.getParameter("toCloud");
        String toClientId = null;
        if (toCloudStr != null){
            toCloudStr = toCloudStr.toUpperCase();
        } else {
            response.getWriter().print("err");
        }
        if(!"DROPBOX".equals(toCloudStr) && !"DRIVE".equals(toCloudStr) && !"COMPLEX".equals(toCloudStr) && !"BOX".equals(toCloudStr)){
            toClientId = toCloudStr;
            toCloudStr = "CLIENT";
            ClientProgramService clp = new ClientProgramService();
            toClientId = clp.getClientIdByNameAndUser(user.getId(), toClientId);
        }
        StorageType toCloud = StorageType.valueOf(toCloudStr);
        String type = request.getParameter("type");
        if (type.equals("fil")) {
            file.setType("file");
        } else if (type.equals("fol")) {
            file.setType("folder");
        }
        boolean moved = false;
        try {
            moved = file.move(toPath, toCloud, toClientId);
            if (moved) {
                StatisticController.incrementMove(cloudStr, user.getId());

                if (file.getType().equals("folder")) {
                    final CustomFile finalFile = file;
                    Executors.newSingleThreadExecutor().execute(() -> {
                        finalFile.updateFileTree();
                    });
                }

                response.getWriter().write("ok");
            }else{
                response.getWriter().write("err");
            }
        } catch (FileTooLargeException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        //response.getWriter().print(moved);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //copy file or folder
        HttpSession session = request.getSession();                                 //
        Locale locale = null;
        try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            LOG.warn(e);
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String data = br.readLine();
        data = java.net.URLDecoder.decode(data, "UTF-8");
        String[] divided = data.split("[=&]");
        Map<String, String> parameters = new HashMap<>();
        for (int i = 0; i < divided.length; i += 2) {
            parameters.put(divided[i], divided[i + 1]);
        }

        CustomFile file = null;
        User user = (User) session.getAttribute("user");

        String cloudStr = parameters.get("cloud");
        if(cloudStr != null) cloudStr = cloudStr.toUpperCase();
        String clientId = parameters.get("clientId");
        try {
            file = new CustomFile(StorageType.valueOf(cloudStr), user.getId(), clientId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().write("err");
        }
        file.setPath(parameters.get("path"));                     //path ''
        file.setFileId(parameters.get("fileId"));
        file.setName(parameters.get("name"));

        String toPath = parameters.get("toPath");
        String toCloudStr = parameters.get("toCloud");
        String toClientId = null;
        if (toCloudStr != null){
            toCloudStr = toCloudStr.toUpperCase();
        } else {
            response.getWriter().print("err");
        }
        if(!"DROPBOX".equals(toCloudStr) && !"DRIVE".equals(toCloudStr) && !"COMPLEX".equals(toCloudStr) && !"BOX".equals(toCloudStr)){
            toClientId = toCloudStr;
            toCloudStr = "CLIENT";
            ClientProgramService clp = new ClientProgramService();
            toClientId = clp.getClientIdByNameAndUser(user.getId(), toClientId);
        }
        StorageType toCloud = StorageType.valueOf(toCloudStr);
        String type = parameters.get("type");
        if (type.equals("fil")) {
            file.setType("file");
        } else if (type.equals("fol")) {
            file.setType("folder");
        }
        try {
            boolean copied = file.copy(toPath, toCloud, toClientId);
            if (copied) {
                StatisticController.incrementCopy(cloudStr, user.getId());

                if (file.getType().equals("folder")) {
                    final CustomFile finalFile = file;
                    Executors.newSingleThreadExecutor().execute(() -> {
                        finalFile.updateFileTree();
                    });
                }

                response.getWriter().write("ok");
            }else{
                response.getWriter().write("err");
            }
        } catch (FileTooLargeException e) {
            LOG.warn(e);
            e.printStackTrace();
            //response
        }
    }
}
