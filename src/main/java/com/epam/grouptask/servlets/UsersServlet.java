package com.epam.grouptask.servlets;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Andrian on 06.02.2016.
 */
public class UsersServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(UsersServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        UserService userService = new UserService();
        List<User> users = userService.getAllUsers();
        JSONArray usersJson = new JSONArray();
        if(users != null){
            for (User user : users) {
                String jsString = user.toJson();
                try {
                    usersJson.put(new JSONObject(jsString));
                } catch (JSONException e) {
                    LOG.warn(e);
                    e.printStackTrace();
                }
            }
        }
        response.getWriter().print(usersJson);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       request.getRequestDispatcher("/pages/AllUsers.jsp").forward(request, response);
    }
}
