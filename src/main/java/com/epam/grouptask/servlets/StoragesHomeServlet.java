package com.epam.grouptask.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Andrian on 17.01.2016.
 */
public class StoragesHomeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get parameter with type of storage (drive, drop, client-id, all)
        request.getRequestDispatcher("/pages/cloud.jsp").forward(request, response);
    }
}
