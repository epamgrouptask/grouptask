package com.epam.grouptask.servlets;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.grouptask.model.User;
import com.epam.grouptask.notification.EmailSender;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;
import com.epam.grouptask.utils.CodeIdGenerator;

/**
 * Created by Andrian on 12.02.2016.
 */
public class BlockUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        String userIdStr = request.getParameter("userId");
        Integer userId = null;
        if(userIdStr != null){
            userId = Integer.parseInt(userIdStr);
            UserService userService = new UserService();
            userService.changeBlocked(userId, true);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    }
    
}
