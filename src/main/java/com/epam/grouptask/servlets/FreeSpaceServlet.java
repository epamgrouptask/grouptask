package com.epam.grouptask.servlets;

import com.epam.grouptask.cloudcontroller.BoxController;
import com.epam.grouptask.cloudcontroller.ClientController;
import com.epam.grouptask.cloudcontroller.DriveController;
import com.epam.grouptask.cloudcontroller.DropboxController;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.SizeDisplayer;
import com.epam.grouptask.model.User;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Andrian on 22.01.2016.
 */
public class FreeSpaceServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(FreeSpaceServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        Locale locale = null;
        /*try {
            locale = (Locale) session.getAttribute("language");
        } catch (ClassCastException e) {
            locale = new Locale((String) session.getAttribute("language"));
        }
        ResourceBundle rb = ResourceBundle.getBundle("Text", locale);*/
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        User user = (User) session.getAttribute("user");
        DriveController driveController = null;
        DropboxController dropboxController = null;
        BoxController boxController = null;
//        ClientController clientController = null;
        try {
            driveController = new DriveController(user.getId());
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        try {
            dropboxController = new DropboxController(user.getId());
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        try {
            boxController = new BoxController(user.getId());
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        JSONObject freeSpace = new JSONObject();
        Long summary = Long.valueOf(0);           //in bytes
        Long summaryTotal = Long.valueOf(0);
        try {
            if (driveController != null) {
                Long driveFree = driveController.getFreeSize();
                summary += driveFree;
                Long total = driveController.getTotalSize();
                summaryTotal += total;
                freeSpace.put("drive", driveFree);
                freeSpace.put("totalDrive", total);
            } else {
                freeSpace.put("drive", 0);
                freeSpace.put("totalDrive", 0);
            }
            if (dropboxController != null) {
                Long dropFree = dropboxController.getFreeSize();
                summary += dropFree;
                freeSpace.put("dropbox", dropFree);
                Long total = dropboxController.getTotalSize();
                summaryTotal += total;
                freeSpace.put("totalDrop", total);
            } else {
                freeSpace.put("dropbox", 0);
                freeSpace.put("totalDrop", 0);
            }
            if (boxController != null) {
                Long boxFree = boxController.getFreeSize();
                summary += boxFree;
                freeSpace.put("box", boxFree);
                Long total = boxController.getTotalSize();
                summaryTotal += total;
                freeSpace.put("totalBox", total);
            } else {
                freeSpace.put("box", 0);
                freeSpace.put("totalBox", 0);
            }
            Map<String, Long> freeSizes = null;
            try {
                freeSizes = ClientController.getAllFreeSize(user.getId());
                for (Long size : freeSizes.values()) {
                    if (size != null) {
                        summary += size;
                    }
                }
                JSONObject clientFree = new JSONObject(freeSizes);
                freeSpace.put("clientFree", clientFree);
            } catch (Exception e) {
                LOG.warn(e);
                e.printStackTrace();
            }
            Map<String, Long> totalSizes = null;
            try {
                totalSizes = ClientController.getAllTotalSize(user.getId());
                for (Long size : totalSizes.values()) {
                    if (size != null) {
                        summaryTotal += size;
                    }
                }
                JSONObject clientTotalSizes = new JSONObject(totalSizes);
                freeSpace.put("clientTotal", clientTotalSizes);
            } catch (NotLinkedProfileException e) {
                LOG.warn(e);
                e.printStackTrace();
            }

            freeSpace.put("summary", summary);
            freeSpace.put("summaryTotal", summaryTotal);
        } catch (JSONException e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().print("err");
        } catch (Exception e) {
            LOG.warn(e);
            e.printStackTrace();
            response.getWriter().print("err");
        }
        response.getWriter().print(freeSpace);
    }
}
