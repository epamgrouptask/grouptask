package com.epam.grouptask.servlets;

import com.epam.grouptask.protocol.Protocol;
import com.epam.grouptask.socketServer.Server;
import com.epam.grouptask.socketServer.SocketChannelManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrian on 23.01.2016.
 */
public class SocketTestServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Server server = SocketChannelManager.getServer();
        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("GET");
        String clientId = "a123456";
        protocol.setClientID(clientId);
        server.setRequest(protocol);
        while ((protocol = server.getResponse(clientId)) == null) {
        }
        //File file = protocol.getDirectory().get(0);
        System.out.println(protocol);
    }
}
