package com.epam.grouptask.servlets.social;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 1 on 19.01.2016.
 */
public class SocialLoginServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(SocialLoginServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String fullname = request.getParameter("fullname");
        String email = request.getParameter("email");
        String social = request.getParameter("social");
        System.out.println(id + " " + fullname + " " + email + " " + social);
        UserService userService = new UserService();
        User user = null;
        User socialUser = null;

        if (social.equals("google")) {
            socialUser = userService.getUserByGoogle(id);
            user = userService.loginOrSignupGoogle(id, fullname, email);
            //userService.setConfirmed(user.getUserName());
            LOG.info(user.getUserName() + " has login to system, his id = " + user.getId());
        } else {
            if (social.equals("vk")) {
                socialUser = userService.getUserByVk(id);
                user = userService.loginOrSignupVk(id, fullname, email);
                //userService.setConfirmed(user.getUserName());
                LOG.info(user.getUserName() + "  has login to system, his id = " + user.getId());
            }
        }
        if (socialUser != null && socialUser.isConfirmed()) {
            HttpSession ses = request.getSession(true);
            ses.setAttribute("user", user);
            response.getWriter().print("ok");
        } else {
            response.getWriter().print("email");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String social = request.getParameter("social");
        request.setAttribute("id", id);
        request.setAttribute("social", social);
        request.getRequestDispatcher("/pages/email.jsp").forward(request, response);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String data = br.readLine();
        data = java.net.URLDecoder.decode(data,"UTF-8");
        String[] divided = data.split("[=&]");
        Map<String, String> parameters = new HashMap<>();
        for (int i = 0; i < divided.length; i += 2) {
            parameters.put(divided[i], divided[i + 1]);
        }
        String id = parameters.get("id");
        String social = parameters.get("social");
        String email = parameters.get("email");

        UserService userService = new UserService();
        User user = null;
        if (social.equals("google")) {
            user = userService.getUserByGoogle(id);
            userService.changeEmailByLogin(user.getUserName(), email);
            user = userService.getUserByGoogle(id);
            userService.setConfirmed(user.getUserName());
            LOG.info(user.getUserName() + " has login to system, his id = " + user.getId());
        } else {
            if (social.equals("vk")) {
                user = userService.getUserByVk(id);
                userService.changeEmailByLogin(user.getUserName(), email);
                user = userService.getUserByVk(id);
                userService.setConfirmed(user.getUserName());
                LOG.info(user.getUserName() + "  has login to system, his id = " + user.getId());
            }
        }
        HttpSession ses = request.getSession(true);
        ses.setAttribute("user", user);
        response.getWriter().print("ok");
    }
}
