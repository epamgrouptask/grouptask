package com.epam.grouptask.servlets.social;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by 1 on 24.01.2016.
 */
public class UnlinkSocialServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String social = request.getParameter("social");
        UserService userService = new UserService();
        HttpSession ses = request.getSession(false);
        User user = (User) ses.getAttribute("user");
        if (social.equals("google")) {
            user = userService.unlinkGoogle(user.getId());
        } else {
            if (social.equals("vk")) {
                user = userService.unlinkVk(user.getId());
            }
        }
        ses.setAttribute("user", user);
        response.getWriter().print("ok");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
