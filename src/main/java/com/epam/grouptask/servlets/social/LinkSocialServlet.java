package com.epam.grouptask.servlets.social;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.UserLinkChecker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by 1 on 24.01.2016.
 */
public class LinkSocialServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String fullname = request.getParameter("fullname");
        String email = request.getParameter("email");
        String social = request.getParameter("social");
        UserService userService = new UserService();
        HttpSession ses = request.getSession(false);
        User user = (User) ses.getAttribute("user");

        User another = null;
        if (social.equals("google")) {
            another = userService.getUserByGoogle(id);
            if (another == null) {
                user = userService.linkGoogle(user.getId(), id);
            } else {
                user = UserLinkChecker.updateUser(user, another);
            }
        } else {
            if (social.equals("vk")) {
                another = userService.getUserByVk(id);
                if (another == null) {
                    user = userService.linkVk(user.getId(), id);
                } else {
                    user = UserLinkChecker.updateUser(user, another);
                }
            }
        }
        ses.setAttribute("user", user);
        response.getWriter().print("ok");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
