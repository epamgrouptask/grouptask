package com.epam.grouptask.servlets.social;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Andrian on 29.01.2016.
 */
public class ClientSocialLoginServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(ClientSocialLoginServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String fullname = request.getParameter("fullname");
        String email = request.getParameter("email");
        String social = request.getParameter("social");
        System.out.println(id + " " + fullname + " " + email + " " + social);
        UserService userService = new UserService();
        User user = null;
        if ("google".equals(social)) {
            user = userService.loginOrSignupGoogle(id, fullname, email);
            userService.setConfirmed(user.getUserName());
            LOG.info(user.getUserName() + " has login to system, his id = " + user.getId());
        } else {
            if ("vk".equals(social)) {
                user = userService.loginOrSignupVk(id, fullname, email);
                userService.setConfirmed(user.getUserName());
                LOG.info(user.getUserName() + " has login to system, his id = " + user.getId());
            }
        }
        HttpSession ses = request.getSession(true);
        ses.setAttribute("user", user);
        response.getWriter().print(user.getId());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
