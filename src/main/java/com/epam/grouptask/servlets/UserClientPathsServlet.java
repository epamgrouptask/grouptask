package com.epam.grouptask.servlets;

import com.epam.grouptask.services.ClientProgramService;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Andrian on 10.02.2016.
 */
public class UserClientPathsServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("clientId");
        ClientProgramService service = new ClientProgramService();
        List<String> paths = service.getPathByClient(id);
        JSONArray jsonArray = new JSONArray(paths);
        response.getWriter().print(jsonArray);
    }
}
