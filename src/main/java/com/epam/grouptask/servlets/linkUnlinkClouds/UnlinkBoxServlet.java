package com.epam.grouptask.servlets.linkUnlinkClouds;

import com.epam.grouptask.cloudcontroller.BoxController;
import com.epam.grouptask.cloudcontroller.ComplexController;
import com.epam.grouptask.cloudcontroller.InitController;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ComplexFileService;
import com.epam.grouptask.services.ProgramDirectoryService;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.ProgramDirectoryUtil;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Oleg on 31.01.2016 15:16.
 */
public class UnlinkBoxServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(UnlinkBoxServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("user");
        JSONArray jsonArray = new JSONArray();
        Map<String, String> map = new ProgramDirectoryService().getUserDirectories(user.getId());
        BoxController box = new BoxController(user.getId());
        List<CustomFile> folder = box.getDirectory(map.get("BOX"));
        ComplexFileService service = new ComplexFileService();

        if (folder != null && folder.size() > 0) {
            for (CustomFile customFile : folder) {
                if (service.getDividedFilenameByPath(customFile.getName()) == null) {
                    continue;
                } else {
                    try {
                        jsonArray.put(new JSONObject().put("name", service.getDividedFilenameByPath(customFile.getName())));
                    } catch (JSONException e) {
                        LOG.warn(e);
                        e.printStackTrace();
                    }
                }
            }
        }

        response.getWriter().write(jsonArray.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("user");
        UserService userService = new UserService();
        //String refreshToken = user.getBoxRefreshToken();
        Properties properties = InitController.getProperties();

        Map<String, String> map = new ProgramDirectoryService().getUserDirectories(user.getId());
        BoxController box = new BoxController(user.getId());
        List<CustomFile> folder = box.getDirectory(map.get("BOX"));
        ComplexFileService service = new ComplexFileService();
        ComplexController controller = new ComplexController(user.getId());

        if (user != null) {
            if (folder != null && folder.size() > 0) {
                for (CustomFile customFile : folder) {
                    if(service.getDividedFilenameByPath(customFile.getPath()) != null) {
                        controller.delete(service.getDividedFilenameByPath(customFile.getPath()));
                        //service.deleteSplittedFileWhenUnlink(user.getId(), service.getDividedFilenameByPath(customFile.getPath()));
                    }
                }
            }

            ProgramDirectoryUtil.deleteProgramDirectory(user, StorageType.BOX);
            user = userService.unlinkBox(user.getId());
        }

        request.getSession().setAttribute("user", user);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("clientId", properties.getProperty("box_client_id"));
            jsonObject.put("clientSecret", properties.getProperty("box_client_secret"));
            jsonObject.put("refreshToken", properties.getProperty("refreshToken"));
        } catch (JSONException e) {
            LOG.warn(e);
            e.printStackTrace();
        }

        response.getWriter().write(jsonObject.toString());
    }
}
