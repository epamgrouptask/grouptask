package com.epam.grouptask.servlets.linkUnlinkClouds;

import com.box.sdk.BoxAPIRequest;
import com.box.sdk.BoxJSONResponse;
import com.epam.grouptask.cloudcontroller.BoxController;
import com.epam.grouptask.cloudcontroller.InitController;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.ProgramDirectoryUtil;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by Oleg on 30.01.2016 15:25.
 */
public class LinkBoxServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(LinkBoxServlet.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JSONObject json = new JSONObject();
        try {
            json.put("redirect", "/user/profile");
        } catch (JSONException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        response.getWriter().write(json.toString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String requestFromUrl = request.getParameter("request");
        Integer index = requestFromUrl.lastIndexOf("=");
        String authCode = requestFromUrl.substring(index + 1);
        Properties properties = InitController.getProperties();
        String clientId = properties.getProperty("box_client_id");
        String clientSecret = properties.getProperty("box_client_secret");
        JSONObject json = null;
        String accessToken = null;
        String refreshToken = null;
        HttpSession session = request.getSession(true);
        UserService userService = new UserService();

        URL url = new URL("https://app.box.com/api/oauth2/token/");
        BoxAPIRequest boxAPIRequest = new BoxAPIRequest(url, "POST");
        boxAPIRequest.setBody("grant_type=authorization_code&code=" + authCode +
                "&client_id=" + clientId + "&client_secret=" + clientSecret);
        //boxAPIRequest.setBody();
        BoxJSONResponse responseJSON = (BoxJSONResponse) boxAPIRequest.send();
        String stringJSON = responseJSON.getJSON();
        User user = (User) session.getAttribute("user");

        try {
            json = new JSONObject(stringJSON);
            accessToken = json.getString("access_token");
            refreshToken = json.getString("refresh_token");
        } catch (JSONException e) {
            LOG.warn(e);
            e.printStackTrace();
        }

        if (user != null) {
            user = userService.linkBox(user.getId(), accessToken, refreshToken);
            if(user != null){
                ProgramDirectoryUtil.createProgramDirectory(user, StorageType.BOX);
            }
            session.setAttribute("user", user);
        }
        response.sendRedirect("/user/profile");
        //response.sendRedirect("/user/profile");
    }
}
