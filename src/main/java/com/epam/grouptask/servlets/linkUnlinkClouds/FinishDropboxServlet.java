package com.epam.grouptask.servlets.linkUnlinkClouds;

import com.dropbox.core.*;
import com.epam.grouptask.cloudcontroller.InitController;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.ProgramDirectoryUtil;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

/**
 * Created by 1 on 20.01.2016.
 */
public class FinishDropboxServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(FinishDropboxServlet.class);
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DbxAuthFinish authFinish = null;
        Properties properties = InitController.getProperties();
        DbxAppInfo appInfo = new DbxAppInfo(properties.getProperty("dpbxappkey"), properties.getProperty("dpbxappsecret"));
        javax.servlet.http.HttpSession session = request.getSession(true);
        String sessionKey = "dropbox-auth-csrf-token";
        DbxSessionStore csrfTokenStore = new DbxStandardSessionStore(session, sessionKey);
        DbxRequestConfig config = new DbxRequestConfig("JavaTutorial/1.0",
                Locale.getDefault().toString());
        DbxWebAuth auth = new DbxWebAuth(config, appInfo,properties.getProperty("address") + "/linkDropbox1",csrfTokenStore);
        try {
            authFinish = auth.finish(request.getParameterMap());
        }catch (Exception e){
            LOG.warn(e);
            e.printStackTrace();
        }
        String accessToken = authFinish.accessToken;
        UserService us = new UserService();
        HttpSession ses = request.getSession(true);
        User user=(User) ses.getAttribute("user");
        if(user!=null) {
            user = us.linkDropbox(user.getId(), accessToken);
            if(user != null){
                ProgramDirectoryUtil.createProgramDirectory(user, StorageType.DROPBOX);
            }
        }
        response.sendRedirect("/user/profile");
    }
}
