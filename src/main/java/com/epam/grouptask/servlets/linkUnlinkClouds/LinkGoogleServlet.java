package com.epam.grouptask.servlets.linkUnlinkClouds;

import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.utils.ProgramDirectoryUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by 1 on 21.01.2016.
 */
public class LinkGoogleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String refresh = request.getParameter("refresh");
        String access = request.getParameter("access");
        UserService us = new UserService();
        HttpSession ses = request.getSession(true);
        User user = (User) ses.getAttribute("user");
        if (user != null) {
            user = us.linkDrive(user.getId(), access, refresh);
            if (user != null) {
                ProgramDirectoryUtil.createProgramDirectory(user, StorageType.DRIVE);
            }
            ses.setAttribute("user", user);
        }
        response.sendRedirect("/user/profile");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String code = request.getParameter("code");
        request.setAttribute("code", code);
        request.getRequestDispatcher("pages/finishLinkGoogle.jsp").forward(request, response);
    }

}