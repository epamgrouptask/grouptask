package com.epam.grouptask.servlets.linkUnlinkClouds;


import com.dropbox.core.*;
import com.dropbox.core.http.StandardHttpRequestor;
import com.dropbox.core.v2.DbxClientV2;
import com.epam.grouptask.cloudcontroller.InitController;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * Created by 1 on 20.01.2016.
 */
public class LinkDropboxServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Start the authorization process with Dropbox.
        Properties properties = InitController.getProperties();
        DbxAppInfo appInfo = new DbxAppInfo(properties.getProperty("dpbxappkey"), properties.getProperty("dpbxappsecret"));
        javax.servlet.http.HttpSession session = request.getSession(true);
        String sessionKey = "dropbox-auth-csrf-token";
        DbxSessionStore csrfTokenStore = new DbxStandardSessionStore(session, sessionKey);
        DbxRequestConfig config = new DbxRequestConfig("JavaTutorial/1.0",
                Locale.getDefault().toString());
        DbxWebAuth auth = new DbxWebAuth(config, appInfo,properties.getProperty("address") + "/linkDropbox1",csrfTokenStore);

        String authorizePageUrl = auth.start();
        // Redirect the user to the Dropbox website so they can approve our application.
        // The Dropbox website will send them back to /dropbox-auth-finish when they're done.
        response.getWriter().write(authorizePageUrl);
    }
}
