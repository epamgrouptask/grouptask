package com.epam.grouptask.servlets;

import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.grouptask.services.ClientProgramService;
import com.epam.grouptask.services.UserService;

/**
 * Servlet implementation class ClientProfileServlet
 */

public class ClientProfileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("newName");
        String clientId = request.getParameter("clientId");
        ClientProgramService cps = new ClientProgramService();
        cps.rename(name, clientId);

        Locale locale = null;
        String loc = request.getParameter("language");
        ClientProgramService service = new ClientProgramService();
        if(loc == null) {
            loc = service.getUserLocale(clientId);
        } else {
            int userId = service.getUserId(clientId);
            UserService us = new UserService();
            us.changeLocale(loc, userId);
        }
        locale = new Locale(loc);
        ResourceBundle resourceBundle = null;
        try {
            resourceBundle = ResourceBundle.getBundle("Text", locale);
        } catch (MissingResourceException e) {
            if(locale.toString().equals("uk_UA") || locale.toString().equals("uk_ua")){
                locale = new Locale("uk");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else if(locale.toString().equals("en_US") || locale.toString().equals("en_us")){
                locale = new Locale("en");
                resourceBundle = ResourceBundle.getBundle("Text", locale);
            }else {
                e.printStackTrace();
            }
        }

        request.setAttribute("bundle", resourceBundle);

        request.getRequestDispatcher("/pages/clientprofile.jsp").forward(request, response);
    }

}
