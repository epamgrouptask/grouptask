package com.epam.grouptask.servlets;

import com.epam.grouptask.model.Role;
import com.epam.grouptask.services.UserService;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Andrian on 03.02.2016.
 */
public class ChangeRoleServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(ChangeRoleServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        UserService userService = new UserService();
        String userId = request.getParameter("userId");
        int userID = Integer.valueOf(userId);
        Role role = Role.valueOf(request.getParameter("newRole"));

        int changed = userService.changeRole(userID, role);

        JSONObject jsonResponse = new JSONObject();
        if (changed != 0) {
            try {
                jsonResponse.put("redirect", "/admin/users");
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
            response.getWriter().print(jsonResponse);
        } else {
            try {
                jsonResponse.put("err", "Not changed");
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
            response.getWriter().print(jsonResponse);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
