package com.epam.grouptask.servlets;

import com.epam.grouptask.model.OperationCount;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.StatService;
import com.epam.grouptask.services.UserService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Andrian on 11.02.2016.
 */
public class AllStatServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        response.setCharacterEncoding("utf-8");
        request.setCharacterEncoding("utf-8");

        StatService ss = new StatService();
        LinkedHashMap<Integer, OperationCount> stat = ss.getAllStat();
        JSONObject statJson = null;
        statJson = new JSONObject(stat);
        response.getWriter().print(statJson);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/pages/allStat.jsp").forward(request, response);
    }
}
