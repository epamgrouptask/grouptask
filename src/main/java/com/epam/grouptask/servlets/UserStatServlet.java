package com.epam.grouptask.servlets;

import com.epam.grouptask.model.OperationCount;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.StatService;
import com.epam.grouptask.services.UserService;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Andrian on 03.02.2016.
 */
public class UserStatServlet extends HttpServlet {
    private static final Logger LOG = Logger.getLogger(UserStatServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	int id = Integer.valueOf(request.getParameter("id"));
    	UserService us = new UserService();
    	User user = us.getUserById(id);
        response.getWriter().write(user.toJson());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        HttpSession ses = request.getSession(true);
        User user = (User) ses.getAttribute("user");
        String userIdStr = request.getParameter("userId");
        Integer userId =  null;
        if(user != null && userIdStr == null) {
            userId = user.getId();
        }
        if (userIdStr != null) {
            userId = Integer.valueOf(userIdStr);
        }
        String date1Str = request.getParameter("date1");
        String date2Str = request.getParameter("date2");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = df.parse(date1Str);
            date2 = df.parse(date2Str);
        } catch (ParseException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        if (date1 == null || date2 == null) {
            //response with err
        } else {
            StatService statService = new StatService();
            Map<Long, Map<String, OperationCount>> stat = statService.getStatForUserInPeriod(userId, date1, date2);
            JSONObject JSstat = new JSONObject(stat);
            response.getWriter().write(JSstat.toString());
        }
    }
}
