package com.epam.grouptask.uploadUtil;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrian on 02.02.2016.
 */
public class UploadStream {
    public static Map<Integer, FileOutputStream> streamMap = new HashMap<>();

    public static void addStream(FileOutputStream fileOutputStream, int userId) {
        streamMap.put(userId, fileOutputStream);
    }

    public static FileOutputStream getStream(int userId) {
        return streamMap.remove(userId);
    }

    public static FileOutputStream removeStream(int userId) {

        return streamMap.remove(userId);

    }
}
