package com.epam.grouptask.protocol;

import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Andrian on 18.01.2016.
 */
public class Protocol implements Serializable {
    private static final long serialVersionUID = 5950169519310163575L;

    private String clientID = null;
    private String path = null;
    private String typeOfQuery = null;
    private String newpath = null;
    private String nameOfFile = null;
    private List<CustomFile> directory = null;
    private String uuid = null;
    private CustomDirectory tree;
    private Long size = null;
    private String error = null;
    private boolean response = false;
    private boolean boolResp;

    public boolean getBoolResp() {
        return boolResp;
    }

    public void setBoolResp(boolean boolResp) {
        this.boolResp = boolResp;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTypeOfQuery() {
        return typeOfQuery;
    }

    public void setTypeOfQuery(String typeOfQuery) {
        this.typeOfQuery = typeOfQuery;
    }

    public String getNewpath() {
        return newpath;
    }

    public void setNewpath(String newpath) {
        this.newpath = newpath;
    }

    public String getNameOfFile() {
        return nameOfFile;
    }

    public void setNameOfFile(String nameOfFile) {
        this.nameOfFile = nameOfFile;
    }

    public List<CustomFile> getDirectory() {
        return directory;
    }

    public void setDirectory(List<CustomFile> directory) {
        this.directory = directory;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isResponse() {
        return response;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }

    public CustomDirectory getTree() {
        return tree;
    }

    public void setTree(CustomDirectory tree) {
        this.tree = tree;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "Protocol{" +
                "clientID='" + clientID + '\'' +
                ", path='" + path + '\'' +
                ", typeOfQuery='" + typeOfQuery + '\'' +
                ", newpath='" + newpath + '\'' +
                ", nameOfFile='" + nameOfFile + '\'' +
                ", directory=" + directory +
                ", size=" + size +
                ", error='" + error + '\'' +
                ", response=" + response +
                ", boolResp=" + boolResp +
                '}';
    }
}
