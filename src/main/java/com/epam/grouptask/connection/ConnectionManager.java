package com.epam.grouptask.connection;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {
	private static final Logger LOG = Logger.getLogger(ConnectionManager.class);
	private static DataSource dataSource = null;
	private static Context context = null;
	private static Connection connection = null;

	public static synchronized Connection getConnection() {
		try {
			if (dataSource == null) {
				context = (Context) new InitialContext().lookup("java:comp/env");
				dataSource = (DataSource) context.lookup("jdbc/GroupTask");
				connection = dataSource.getConnection();
			}
			return dataSource.getConnection();
		} catch (SQLException | NamingException e) {
			LOG.error(e);
			e.printStackTrace();
			return null;
		}
	}

	private ConnectionManager() {
	}
}
