package com.epam.grouptask.exception;

/**
 * Created by Andrian on 22.01.2016.
 */
public class NotLinkedProfileException extends RuntimeException{
    public NotLinkedProfileException() {
        super();
    }

    public NotLinkedProfileException(String message) {
        super(message);
    }

    public NotLinkedProfileException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotLinkedProfileException(Throwable cause) {
        super(cause);
    }

    protected NotLinkedProfileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
