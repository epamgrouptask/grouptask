package com.epam.grouptask.exception;

/**
 * Created by Andrian on 22.01.2016.
 */
public class FileTooLargeException extends Exception {
    public FileTooLargeException() {
        super();
    }

    public FileTooLargeException(String message) {
        super(message);
    }

    public FileTooLargeException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileTooLargeException(Throwable cause) {
        super(cause);
    }

    protected FileTooLargeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
