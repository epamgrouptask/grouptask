package com.epam.grouptask.cloudcontroller;

import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxFiles;
import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;

/**
 * Created by Oleg on 18.01.2016.
 */
public abstract class CloudsController {

    public abstract boolean copy(String fromPath, String toPath);

    public abstract boolean move(String fromPath, String toPath, StorageType toCloud, String toClientId);

    public abstract boolean rename(String path, String newName);

    public abstract boolean uploadFile(InputStream in, String path, Long size) throws FileTooLargeException;

    public abstract boolean downloadFile(OutputStream out, String path);

    public abstract boolean createDirectory(String path, String name);

    public abstract List<CustomFile> getDirectory(String path);

    public abstract boolean delete(String path);

    public abstract Long getFreeSize() throws Exception;

    public abstract boolean toRecycleBin(String path);

    public abstract String getRoot();
    
	public abstract Long getTotalSize() throws IOException ;

    public abstract void getFileTree(CustomDirectory root) throws DbxException;
}
