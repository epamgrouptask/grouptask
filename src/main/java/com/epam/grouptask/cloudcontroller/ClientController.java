package com.epam.grouptask.cloudcontroller;

import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.ClientProgram;
import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.protocol.Protocol;
import com.epam.grouptask.services.ClientProgramService;
import com.epam.grouptask.socketServer.Server;
import com.epam.grouptask.socketServer.SocketChannelManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * Created by Andrian on 22.01.2016.
 */
public class ClientController extends CloudsController {
    private static final Logger LOG = Logger.getLogger(ClientController.class);
    private ClientProgram clientProgram;
    private Boolean active;

    public ClientController(int userId, String clientId) throws NotLinkedProfileException {
        ClientProgramService service = new ClientProgramService();
        clientProgram = service.getClientById(userId, clientId);
        if (clientProgram == null) {
            throw new NotLinkedProfileException();
        }
        Server server = SocketChannelManager.getServer();
        active = server.isConnected(clientId);
        if (!active) {
            throw new NotLinkedProfileException();
        }
    }

    public ClientProgram getClientProgram() {
        return clientProgram;
    }

    @Override
    public boolean copy(String fromPath, String toPath) {
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        boolean copied = false;
        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Copy");
        protocol.setPath(fromPath);
        protocol.setNewpath(toPath);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        try {
            server.setRequest(protocol);
            while ((protocol = server.getResponse(uuid)) == null) {
            }  //dirty hack
            copied = protocol.getBoolResp();
        } catch (IOException e) {
            LOG.warn(e);
            LOG.warn(e);
            e.printStackTrace();
        }
        return copied;
    }

    @Override
    public Long getFreeSize() {
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Size");
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        Long size = null;
        try {
            server.setRequest(protocol);
            while ((protocol = server.getResponse(uuid)) == null) {
            }  //dirty hack
            size = protocol.getSize();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return size < 0 || size == null ? 0 : size;
    }

    public static Map<String, Boolean> getAllClientsByUser(int userId) throws NotLinkedProfileException {
        ClientProgramService service = new ClientProgramService();
        List<ClientProgram> clients = service.getAllClientsByUserId(userId, 0);
        if (clients == null) {
            throw new NotLinkedProfileException();
        }
        Map<String, Boolean> clientIds = new HashMap<>();
        Server server = SocketChannelManager.getServer();
        for (ClientProgram clientProgram : clients) {
            clientIds.put(clientProgram.getClientId(), server.isConnected(clientProgram.getClientId()));
        }
        return clientIds;
    }

    public static Map<ClientProgram, Boolean> getAllClientsWithStatusByUser(int userId) throws NotLinkedProfileException {
        ClientProgramService service = new ClientProgramService();
        List<ClientProgram> clients = service.getAllClientsByUserId(userId, 1);
        if (clients == null) {
            throw new NotLinkedProfileException();
        }
        Map<ClientProgram, Boolean> clientIds = new HashMap<>();
        Server server = SocketChannelManager.getServer();
        for (ClientProgram clientProgram : clients) {
            clientIds.put(clientProgram, server.isConnected(clientProgram.getClientId()));
        }

        return clientIds;
    }

    public static Map<String, Long> getAllTotalSize(int userId) throws NotLinkedProfileException {
        Map<String, Long> sizes = new HashMap<>();
        ClientProgramService service = new ClientProgramService();
        List<ClientProgram> clients = service.getAllClientsByUserId(userId, 0);
        if (clients == null) {
            throw new NotLinkedProfileException();
        }
        Server server = SocketChannelManager.getServer();
        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Total");
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        for (ClientProgram clientProgram : clients) {                   //send request to all connected clients
            if (server.isConnected(clientProgram.getClientId())) {
                protocol.setClientID(clientProgram.getClientId());
                try {
                    server.setRequest(protocol);
                    Protocol response = null;
                    while ((response = server.getResponse(uuid)) == null) {
                    }  //dirty hack
                    sizes.put(clientProgram.getClientId(), response.getSize());
                } catch (IOException e) {
                    LOG.warn(e);
                    e.printStackTrace();
                }
            }
        }
        return sizes;
    }

    public static Map<String, Long> getAllFreeSize(int userId) {
        Map<String, Long> sizes = new HashMap<>();
        ClientProgramService service = new ClientProgramService();
        List<ClientProgram> clients = service.getAllClientsByUserId(userId, 0);
        if (clients == null) {
            return sizes;
        }
        Server server = SocketChannelManager.getServer();
        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Size");
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        for (ClientProgram clientProgram : clients) {                   //send request to all connected clients
            if (server.isConnected(clientProgram.getClientId())) {
                protocol.setClientID(clientProgram.getClientId());
                try {
                    server.setRequest(protocol);
                    Protocol response = null;
                    while ((response = server.getResponse(uuid)) == null) {
                    }  //dirty hack
                    sizes.put(clientProgram.getClientId(), response.getSize());
                } catch (IOException e) {
                    LOG.warn(e);
                    e.printStackTrace();
                }
            }
        }
        return sizes;
    }

    @Override
    public boolean move(String fromPath, String toPath, StorageType toCloud, String toClientId) {           //todo: delete 2 parameters
        boolean moved = false;

        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        //if (toCloud == StorageType.CLIENT && toClientId == clientProgram.getClientId()) {
        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Move");
        protocol.setPath(fromPath);
        protocol.setNewpath(toPath);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        try {
            server.setRequest(protocol);
            while ((protocol = server.getResponse(uuid)) == null) {
            }  //dirty hack
            moved = protocol.getBoolResp();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        //}
        return moved;
    }

    @Override
    public boolean rename(String path, String newName) {
        boolean renamed = false;
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Rename");
        protocol.setPath(path);
        protocol.setNewpath(newName);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        try {
            server.setRequest(protocol);
            while ((protocol = server.getResponse(uuid)) == null) {
            }  //dirty hack
            renamed = protocol.getBoolResp();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return renamed;
    }

    @Override
    public boolean uploadFile(InputStream in, String path, Long size) throws FileTooLargeException {
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }
        ClientProgramService clp = new ClientProgramService();
        List<String> paths = clp.getPathByClient(clientProgram.getClientId());
        if (paths == null) {
            return false;
        } else {
            boolean contains = false;
            for (String p : paths) {
                String slash = String.valueOf("\\\\");
                String twoSlash = String.valueOf("/");
                path = path.replaceAll(slash, twoSlash);
                p = p.replaceAll(slash, twoSlash);
                if(path.startsWith(p)){
                    contains = true;
                    break;
                }
            }
            if(!contains){
                return false;
            }
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Upload");
        protocol.setPath(path);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        boolean uploaded = false;
        try {
            server.setRequest(protocol);
            /*ServerForFile serverForFile = new ServerForFile(false);
            serverForFile.setInput(in);
            serverForFile.run();*/
            SocketChannelManager.getServerForFile().setInput(in);
            SocketChannelManager.setIsDownload(false);
            /*while (SocketChannelManager.getServerForFile().getDownloadToServer() != null) {

            }*/ //wait to download file
            Protocol resp = null;
            while ((resp = server.getResponse(uuid)) == null) {
            }
            uploaded = resp.getBoolResp();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return uploaded;
    }

    @Override
    public boolean downloadFile(OutputStream out, String path) {
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Download");
        protocol.setPath(path);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        Long respSize = null;
        try {
            server.setRequest(protocol);
            SocketChannelManager.getServerForFile().setOutput(out);
            SocketChannelManager.setIsDownload(true);
            while (SocketChannelManager.getServerForFile().getDownloadToServer() != null) {

            } //wait to download file
            //ServerForFile serverForFile = new ServerForFile();
            //serverForFile.setOutput(out);
            //serverForFile.run();
            Protocol resp = null;
            while ((resp = server.getResponse(uuid)) == null) {
            }
            respSize = resp.getSize();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return respSize == null ? false : true;
    }

    @Override
    public boolean createDirectory(String path, String newName) {
        boolean created = false;
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Create");
        protocol.setPath(path + "/" + newName);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        try {
            server.setRequest(protocol);
            while ((protocol = server.getResponse(uuid)) == null) {
            }  //dirty hack
            created = protocol.getBoolResp();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return created;
    }

    @Override
    public List<CustomFile> getDirectory(String path) {         //if root get all things from paths or display folders(paths)
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        List<CustomFile> files = new ArrayList<>();
        protocol.setTypeOfQuery("Get");
        protocol.setPath(path);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        try {
            server.setRequest(protocol);
            Protocol resp = null;
            while ((resp = server.getResponse(uuid)) == null) {
            }  //dirty hack
            files = resp.getDirectory();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return files;
    }

    @Override
    public boolean delete(String path) {
        boolean deleted = false;
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Delete");
        protocol.setPath(path);
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        try {
            server.setRequest(protocol);
            while ((protocol = server.getResponse(uuid)) == null) {
            }  //dirty hack
            deleted = protocol.getBoolResp();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return deleted;
    }

    @Override
    public boolean toRecycleBin(String path) {
        return false;
    }

    @Override
    public String getRoot() {
        return "/";
    }

    @Override
    public Long getTotalSize() throws IOException {
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Total");
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        Long size = null;
        try {
            server.setRequest(protocol);
            while ((protocol = server.getResponse(uuid)) == null) {
            }  //dirty hack
            size = protocol.getSize();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return size < 0 || size == null ? 0 : size;
    }

    @Override
    public void getFileTree(CustomDirectory root) {
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Tree");
        protocol.setClientID(clientProgram.getClientId());
        String uuid = UUID.randomUUID().toString();
        protocol.setUuid(uuid);
        //protocol.setTree(root.toJson());
        protocol.setTree(root);
        Protocol resp = null;
        try {
            server.setRequest(protocol);
            while ((resp = server.getResponse(uuid)) == null) {
            }  //dirty hack
            root.setInnerDir(resp.getTree().getInnerDir());
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
    }

    public String getJsonFileTree(CustomDirectory root) {
        Server server = SocketChannelManager.getServer();
        if (!server.isConnected(clientProgram.getClientId())) {
            throw new NotLinkedProfileException("Client not connected");
        }

        Protocol protocol = new Protocol();
        protocol.setTypeOfQuery("Tree");
        protocol.setClientID(clientProgram.getClientId());
        String tree = root.toJson();
        //protocol.setTree(tree);
        Protocol resp = null;
        try {
            server.setRequest(protocol);
            while ((resp = server.getResponse(clientProgram.getClientId())) == null) {
            }  //dirty hack
            //tree = resp.getTree();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return tree;
    }
}
