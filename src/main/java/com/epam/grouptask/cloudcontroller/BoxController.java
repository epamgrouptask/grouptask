package com.epam.grouptask.cloudcontroller;

import com.box.sdk.*;
import com.dropbox.core.DbxException;
import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ProgramDirectoryService;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Oleg on 29.01.2016 12:51.
 */
public class BoxController extends CloudsController {

    private static final Logger LOG = Logger.getLogger(BoxController.class);
    private User user = null;
    private Properties properties = null;
    private String clientId = null;
    private String clientSecret = null;
    private BoxAPIConnection api = null;
    String accessToken = null;
    String refreshToken = null;

    public BoxController(Integer userId) throws NotLinkedProfileException {
        user = InitController.getUser(userId);
        if (user.getBoxRefreshToken() == null) {
            throw new NotLinkedProfileException();
        }
        properties = InitController.getProperties();
        clientId = properties.getProperty("box_client_id");
        clientSecret = properties.getProperty("box_client_secret");
        accessToken = user.getBoxAccessToken();
        refreshToken = user.getBoxRefreshToken();

        api = new BoxAPIConnection(clientId, clientSecret, accessToken, refreshToken);
        api.setAutoRefresh(false);
        api = InitController.refreshTokenBox(user.getId(), api);
    }

    @Override
    public boolean copy(String fromPath, String toPath) {
        boolean copied;

        try {
            BoxFolder toFolder = new BoxFolder(api, toPath);
            BoxFile file = new BoxFile(api, fromPath);

            file.copy(toFolder);
            copied = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            copied = false;
        }
        return copied;
    }

    @Override
    public boolean move(String fromPath, String toPath, StorageType toCloud, String toClientId) {

        boolean copied;

        try {
            BoxFile file = new BoxFile(api, fromPath);
            BoxFolder destination = new BoxFolder(api, toPath);

            file.move(destination);
            copied = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            copied = false;
        }
        return copied;
    }

    @Override
    public boolean rename(String path, String newName) {
        boolean renamed;

        try {
            BoxFile file = new BoxFile(api, path);
            BoxFile.Info info = file.new Info();
            info.setName(newName);
            file.updateInfo(info);
            renamed = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            renamed = false;
        }
        return renamed;
    }

    @Override
    public boolean uploadFile(InputStream in, String path, Long size) throws FileTooLargeException {

        boolean uploaded = false;
        Long freeSize = null;
        try {
            freeSize = getFreeSize();
        } catch (Exception e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        if (freeSize > size) {
            try {
                String name = String.valueOf(path.subSequence(path.lastIndexOf("/") + 1, path.length()));
                String parentId = String.valueOf(path.subSequence(0, path.lastIndexOf("/")));
                if(parentId.equals("/") || parentId.equals("")){
                    parentId = getRoot();
                }
                BoxFolder destination = new BoxFolder(api, parentId);
                destination.uploadFile(in, name);
                uploaded = true;
                try {
                    in.close();
                } catch (IOException e) {
                    LOG.warn(e);
                }
            } catch (BoxAPIException e) {
                LOG.warn(e);
                uploaded = false;
            }
        }
        return uploaded;
    }

    @Override
    public boolean downloadFile(OutputStream out, String path) {

        boolean downloaded;

        try {
            BoxFile file = new BoxFile(api, path);
            BoxFile.Info info = file.getInfo();
            file.download(out);
            downloaded = true;
            try {
                out.flush();
            } catch (IOException e) {
                LOG.warn(e);
            }
        } catch (BoxAPIException e) {
            LOG.warn(e);
            downloaded = false;
        }
        return downloaded;
    }

    @Override
    public boolean createDirectory(String path, String name) {

        boolean created;
        if(path.equals("")){
            path = "0";
        }
        try {
            BoxFolder destination = new BoxFolder(api, path);
            destination.createFolder(name);
            created = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            created = false;
        }
        return created;
    }

    @Override
    public List<CustomFile> getDirectory(String path) {

        List<CustomFile> files = new ArrayList<>();
        CustomFile file = null;
        BoxFile.Info fileInfo = null;
        BoxFolder.Info folderInfo = null;
        BoxFile curentFile = null;
        BoxFolder curentFolder = null;
        if ("".equals(path) || "/".equals(path)) {
            path = "0";
        }
        BoxFolder folder = new BoxFolder(api, path);

        for (BoxItem.Info itemInfo : folder) {
            if (itemInfo instanceof BoxFile.Info) {
                fileInfo = (BoxFile.Info) itemInfo;
                try {
                    file = new CustomFile(StorageType.BOX, user.getId(), null);
                } catch (NotLinkedProfileException e) {
                    LOG.warn(e);
                }
                curentFile = new BoxFile(api, fileInfo.getID());
                file.setFileId(fileInfo.getID());
                file.setName(curentFile.getInfo().getName());
                file.setLastChange(curentFile.getInfo().getContentModifiedAt());
                file.setSize(curentFile.getInfo().getSize());
                file.setType("file");
                file.setPath(curentFile.getInfo().getParent().getID());

                files.add(file);
            } else if (itemInfo instanceof BoxFolder.Info) {
                folderInfo = (BoxFolder.Info) itemInfo;
                try {
                    file = new CustomFile(StorageType.BOX, user.getId(), null);
                } catch (NotLinkedProfileException e) {
                    LOG.warn(e);
                }
                curentFolder = new BoxFolder(api, folderInfo.getID());
                file.setFileId(folderInfo.getID());
                file.setName(curentFolder.getInfo().getName());
                file.setLastChange(curentFolder.getInfo().getContentModifiedAt());
                file.setSize(curentFolder.getInfo().getSize());
                file.setType("folder");
                file.setPath(curentFolder.getInfo().getParent().getID());

                files.add(file);
            }
        }
        return files;
    }

    @Override
    public boolean delete(String path) {

        boolean deleted;

        try {
            BoxFile file = new BoxFile(api, path);
            file.delete();
            deleted = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            deleted = false;
        }

        return deleted;
    }

    @Override
    public Long getFreeSize() throws Exception {

        BoxUser user = BoxUser.getCurrentUser(api);
        BoxUser.Info info = user.getInfo();

        Long freeSize = info.getSpaceAmount() - info.getSpaceUsed();
        return freeSize;
    }

    @Override
    public boolean toRecycleBin(String path) {

        boolean deleted;

        try {
            BoxFile file = new BoxFile(api, path);
            file.delete();

            deleted = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            deleted = false;
        }

        return deleted;
    }

    @Override
    public String getRoot() {

        BoxFolder rootFolder = BoxFolder.getRootFolder(api);
        return rootFolder.getID();
    }

    @Override
    public Long getTotalSize() throws IOException {

        BoxUser user = BoxUser.getCurrentUser(api);
        BoxUser.Info info = user.getInfo();
        Long getTotalSize = info.getSpaceAmount();

        return getTotalSize;
    }

    @Override
    public void getFileTree(CustomDirectory root) {

        String path = root.getPath();
        List<CustomFile> files = new ArrayList<>();
        CustomFile file = null;
        BoxFolder.Info folderInfo = null;
        BoxFolder curentFolder = null;
        BoxFolder folder = new BoxFolder(api, path);
        Map<String, String> map = new ProgramDirectoryService().getUserDirectories(user.getId());

        if (folder == null || folder.getInfo().getSize() == 0) {
        } else {
            for (BoxItem.Info itemInfo : folder) {
                if (itemInfo instanceof BoxFolder.Info) {
                    folderInfo = (BoxFolder.Info) itemInfo;
                    if (folderInfo.getID().equals(map.get("BOX"))) {
                        continue;
                    }
                    CustomDirectory customDirectory = new CustomDirectory(folderInfo.getName(), folderInfo.getID());
                    getFileTree(customDirectory);
                    root.addDirectory(customDirectory);
                }
            }
        }

    }

    public boolean copyFolder(String fromPath, String toPath) {

        boolean copied;

        try {
            BoxFolder folder = new BoxFolder(api, fromPath);
            BoxFolder destination = new BoxFolder(api, toPath);
            folder.copy(destination, folder.getInfo().getName());
            copied = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            copied = false;
        }

        return copied;
    }

    public boolean moveFolder(String fromPath, String toPath, StorageType toCloud, String toClientId) {

        boolean moved;

        try {
            BoxFolder folder = new BoxFolder(api, fromPath);
            BoxFolder destination = new BoxFolder(api, toPath);
            folder.move(destination);
            moved = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            moved = false;
        }
        return moved;
    }

    public boolean renameFolder(String path, String newName) {

        boolean renamed;

        try {
            BoxFolder folder = new BoxFolder(api, path);
            BoxFolder.Info info = folder.new Info();
            info.setName(newName);
            folder.updateInfo(info);
            renamed = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            renamed = false;
        }
        return renamed;
    }

    public boolean toRecycleBinFolder(String path) {

        boolean deleted;

        try {
            BoxFolder folder = new BoxFolder(api, path);
            folder.delete(true);
            deleted = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            deleted = false;
        }

        return deleted;
    }

    public String getFileIdByName(String name, String path) {

        String id = null;
        BoxFolder folder = new BoxFolder(api, path);
        BoxFolder.Info folderInfo = null;
        BoxFolder currentFolder = null;
        BoxFile.Info fileInfo = null;
        BoxFile currentFile = null;

        for (BoxItem.Info itemInfo : folder) {
            if (itemInfo instanceof BoxFolder.Info) {
                folderInfo = (BoxFolder.Info) itemInfo;
                currentFolder = new BoxFolder(api, folderInfo.getID());

                if (currentFolder.getInfo().getName().equals(name)) {
                    id = currentFolder.getInfo().getID();
                    break;
                }
            } else if (itemInfo instanceof BoxFile.Info) {
                fileInfo = (BoxFile.Info) itemInfo;
                currentFile = new BoxFile(api, fileInfo.getID());

                if (currentFile.getInfo().getName().equals(name)) {
                    id = currentFile.getInfo().getID();
                    break;
                }
            }
        }
        return id;
    }

    public boolean deleteFolder(String path) {

        boolean deleted;

        try {
            BoxFolder folder = new BoxFolder(api, path);
            folder.delete(true);
            deleted = true;
        } catch (BoxAPIException e) {
            LOG.warn(e);
            deleted = false;
        }

        return deleted;
    }
}
