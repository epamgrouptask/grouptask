package com.epam.grouptask.cloudcontroller;

import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxAPIRequest;
import com.box.sdk.BoxJSONResponse;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Properties;

/**
 * Created by Oleg on 17.01.2016.
 */
public class InitController {
    private static final Logger LOG = Logger.getLogger(InitController.class);

    public static User getUser(Integer userId) {

        User user = new UserService().getUserById(userId);
        return user;
    }

    public static Properties getProperties() {
        Properties properties = new Properties();
        try {
            InputStream inputStream = InitController.class.getClassLoader().getResourceAsStream("cloud_config.properties");
            properties.load(inputStream);
        } catch (java.io.IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return properties;
    }

    public static void refreshToken(Integer id) {
        Properties properties = InitController.getProperties();
        String clientId = properties.getProperty("drive_auth_client_id");
        String clientSecret = properties.getProperty("drive_auth_client_secret");
        final long MILLISECONDS_IN_HOUR = 3 * 60 * 60 * 1000;
        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        java.sql.Date date = new java.sql.Date(currentDate.getTime());
        User user = new UserService().getUserById(id);
        Timestamp userDate = user.getDriveAccessTokenCreate();
        long time = date.getTime() - userDate.getTime();
        if (MILLISECONDS_IN_HOUR > time) {
            return;
        }
        try {
            TokenResponse response =
                    new GoogleRefreshTokenRequest(new NetHttpTransport(), new JacksonFactory(),
                            user.getDriveRefreshToken(), clientId,
                            clientSecret).execute();
            new UserService().linkDrive(id, response.getAccessToken(), user.getDriveRefreshToken());

        } catch (Exception e) {
            LOG.warn(e);
            e.printStackTrace();
        }
    }

    public static BoxAPIConnection refreshTokenBox(Integer id, BoxAPIConnection api){

        Properties properties = InitController.getProperties();
        JSONObject json = null;
        String clientId = properties.getProperty("box_client_id");
        String clientSecret = properties.getProperty("box_client_secret");
        final long MILLISECONDS_IN_HOUR = 3 * 60 * 60 * 1000;
        Calendar calendar = Calendar.getInstance();
        java.util.Date currentDate = calendar.getTime();
        java.sql.Date date = new java.sql.Date(currentDate.getTime());
        User user = new UserService().getUserById(id);
        Timestamp userDate = user.getBoxAccessTokenCreate();
        long time = date.getTime() - userDate.getTime();

        if (MILLISECONDS_IN_HOUR > time) {
            return api;
        }
        api.refresh();
        new UserService().linkBox(user.getId(), api.getAccessToken(), api.getRefreshToken());
        /*URL url = null;
        try {
            url = new URL("https://app.box.com/api/oauth2/token/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        BoxAPIRequest boxAPIRequest = new BoxAPIRequest(url, "POST");
        boxAPIRequest.setBody("grant_type=refresh_token&refresh_token=" +
                api.getRefreshToken() +"&client_id=" + api.getClientID() + "&client_secret=" +
                api.getClientSecret());
        BoxJSONResponse responseJSON = (BoxJSONResponse) boxAPIRequest.send();
        String stringJSON = responseJSON.getJSON();
        try {
            json = new JSONObject(stringJSON);
            api.setAccessToken(json.getString("access_token"));
            api.setRefreshToken(json.getString("refresh_token"));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        return api;
    }
}
