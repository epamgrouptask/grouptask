package com.epam.grouptask.cloudcontroller;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v1.DbxEntry;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.DbxFiles;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ProgramDirectoryService;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DropboxController extends CloudsController {
    private static final Logger LOG = Logger.getLogger(DropboxController.class);
    private User user = null;
    private Properties properties = null;
    private String appKey = null;
    private String accessToken = null;
    private DbxRequestConfig config = null;
    private DbxClientV2 client = null;

    public DropboxController(Integer userId) throws NotLinkedProfileException {
        user = InitController.getUser(userId);
        if (user.getDrbxToken() == null) {
            throw new NotLinkedProfileException();
        }
        properties = InitController.getProperties();
        appKey = properties.getProperty("dpbxappkey");
        accessToken = user.getDrbxToken();
        config = new DbxRequestConfig(appKey, user.getLocale());
        client = new DbxClientV2(config, accessToken);
    }

    @Override
    public boolean copy(String fromPath, String toPath) {

        try {
            client.files.copy(fromPath, toPath);
        } catch (DbxException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean move(String fromPath, String toPath, StorageType toCloud, String toClientId) {

        try {
            client.files.move(fromPath, toPath);
        } catch (DbxException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean rename(String path, String newName) {

        String[] elements = path.split("/");
        elements[elements.length - 1] = newName;
        StringBuilder newPath = new StringBuilder("");

        for (int i = 0; i < elements.length; i++) {
            newPath.append(elements[i]);
            if (i < elements.length - 1) {
                newPath.append("/");
            }
        }

        move(path, newPath.toString(), StorageType.DROPBOX, null);

        return true;
    }

    @Override
    public boolean uploadFile(InputStream in, String path, Long size) {
        try {
            try {
                client.files.uploadBuilder(path).run(in);
            } finally {
                in.close();
            }
        } catch (DbxFiles.UploadException ex) {
            LOG.warn("Error uploading to Dropbox: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        } catch (DbxException ex) {
            LOG.warn("Error uploading to Dropbox: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        } catch (IOException ex) {
            LOG.warn("Error reading from file" + ":" + ex.getMessage());
            ex.printStackTrace();
            return false;
        } catch (NullPointerException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean downloadFile(OutputStream os, String path) {
        try {
            try {
                client.files.downloadBuilder(path).run(os);
                os.flush();
            } finally {
//                os.close();
            }
        } catch (DbxFiles.UploadException ex) {
            LOG.warn("Error downloading from Dropbox: " + ex.getMessage());
            return false;
        } catch (DbxException ex) {
            LOG.warn("Error downloading from Dropbox: " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            LOG.warn("Error writting file \"" + path + "\": " + ex.getMessage());
            return false;
        }
        return true;
    }



    @Override
    public boolean createDirectory(String path, String name) {
        if(!path.endsWith("/")){
            path += "/" + name;
        }
        else{
            path += name;
        }
        if (!path.startsWith("/")){
            path = "/" + path;
        }
        try {
            client.files.createFolder(path);
        } catch (DbxFiles.CreateFolderException e){
            LOG.warn(e);
            return false;
        } catch(DbxException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public List<CustomFile> getDirectory(String path) {
        if(!path.startsWith("/")) {
            path = path.equals("") ? path : ("/" + path);
        }
        List<CustomFile> customFiles = new ArrayList<>();
        CustomFile customFile = null;
        JSONObject json;
        String dateTime;
        Date lastModifiedDate = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        try {
            ArrayList<DbxFiles.Metadata> folders = client.files.listFolder(path).entries;
            for (DbxFiles.Metadata folder : folders) {
                json = new JSONObject(folder.toJson(true));

                customFile = new CustomFile(StorageType.DROPBOX, user.getId(), null);
                customFile.setName(json.getString("name"));
                customFile.setFileId(json.getString("id"));
                customFile.setPath(json.getString("path_lower"));
                customFile.setType(json.getString(".tag"));
                if (customFile.getType().equals("file")) {
                    customFile.setSize(Long.parseLong(json.getString("size")));
                    dateTime = json.getString("server_modified");
                    try {
                        lastModifiedDate = format.parse(dateTime);
                    } catch (ParseException e) {
                        LOG.warn(e);
                    }
                    customFile.setLastChange(lastModifiedDate);
                }

                customFiles.add(customFile);
            }
        } catch (DbxException e) {
            LOG.warn(e);
            return null;
        } catch (JSONException e) {
            LOG.warn(e);
            return null;
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            return null;
        }
        return customFiles;
    }

    @Override
    public boolean delete(String path) {

        try {
            client.files.delete(path);
        } catch (DbxException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean toRecycleBin(String path) {
        try {
            client.files.delete(path);
        } catch (DbxException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public String getRoot() {
        return "";
    }
    @Override
    public Long getTotalSize() {
        try {
            return client.users.getSpaceUsage().allocation.getIndividual().allocated;
        } catch (DbxException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long getFreeSize() throws DbxException {
        long spaceUsage = client.users.getSpaceUsage().used;
        long size = client.users.getSpaceUsage().allocation.getIndividual().allocated;
        long free = size - spaceUsage;
        if (free < 0) {
            free = 0;
        }
        return free;
    }

    @Override
    public void getFileTree(CustomDirectory root) throws DbxException {
        JSONObject json;
        Map<String, String> map = new ProgramDirectoryService().getUserDirectories(user.getId());
        try {
            ArrayList<DbxFiles.Metadata> folders = client.files.listFolder(root.getPath()).entries;
            for (DbxFiles.Metadata folder : folders) {
                json = new JSONObject(folder.toJson(true));
                if (json.getString(".tag").equals("file")){continue;}
                if(json.getString("path_lower").equals(map.get("DROPBOX").toLowerCase())){
                    continue;
                }
                CustomDirectory customDirectory = new CustomDirectory(json.getString("name"), json.getString("path_lower"));
                getFileTree(customDirectory);
                root.addDirectory(customDirectory);
            }
        } catch (DbxException e) {
            LOG.warn(e);

        } catch (JSONException e) {
            LOG.warn(e);
        }
    }
}
