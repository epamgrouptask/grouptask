package com.epam.grouptask.cloudcontroller;

import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ProgramDirectoryService;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.*;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Oleg on 17.01.2016.
 */
public class DriveController extends CloudsController {
    private static final Logger LOG = Logger.getLogger(DriveController.class);

    private final String APPLICATION_NAME = "Clouds Merge";
    private final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private HttpTransport HTTP_TRANSPORT;
    private final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE_METADATA_READONLY);
    private User user = null;
    private Properties properties = null;
    private String clientId = null;
    private String clientSecret = null;
    private Drive service = null;

    public DriveController(Integer userId) throws NotLinkedProfileException {
    	user = InitController.getUser(userId);
    	if (user.getDriveRefreshToken() == null) {
            throw new NotLinkedProfileException();
        }
    	InitController.refreshToken(userId);
    	user = InitController.getUser(userId);
        properties = InitController.getProperties();
        clientId = properties.getProperty("drive_client_id");
        clientSecret = properties.getProperty("drive_client_secret");
        service = getDriveService();
    }

    {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        } catch (Throwable t) {
            LOG.warn(t);
        }
    }

    private Credential authorize() {

        Credential credential = new GoogleCredential.Builder()
                .setClientSecrets(clientId, clientSecret)
                .setJsonFactory(JSON_FACTORY).setTransport(HTTP_TRANSPORT).build()
                .setRefreshToken(user.getDriveRefreshToken())
                .setAccessToken(user.getDriveAccessToken());

        return credential;
    }

    private Drive getDriveService() {
        Credential credential = authorize();
        return new Drive.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }


    @Override
    public boolean copy(String originFileId, String parentId) {
        ParentReference parentReference = new ParentReference();
        ParentList parents = null;
        String parentFolderId = null;
        try {
            parents = service.parents().list(originFileId).execute();
            parentFolderId = parents.getItems().get(0).getId();
            parentReference.setId(parentId);
            service.parents().insert(originFileId, parentReference).execute();
        } catch (IOException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean move(String fileId, String folderId, StorageType toCloud, String toClientId) {
        ParentReference parentReference = new ParentReference();
        ParentList parents = null;
        String parentFolderId = null;
        try {
            parents = service.parents().list(fileId).execute();
            parentFolderId = parents.getItems().get(0).getId();
            parentReference.setId(folderId);
            service.parents().insert(fileId, parentReference).execute();
            service.parents().delete(fileId, parentFolderId).execute();
        } catch (IOException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean rename(String fileId, String newName) {

        File file = new File();
        file.setTitle(newName);

        try {
            com.google.api.services.drive.Drive.Files.Patch patch = service.files().patch(fileId, file);
            patch.setFields("title");
            patch.execute();
        } catch (IOException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean uploadFile(InputStream in, String path, Long size) {
        //path equals FolderId
        File body = new File();
        String name = String.valueOf(path.subSequence(path.lastIndexOf("/") + 1, path.length()));
        String parentId = String.valueOf(path.subSequence(0, path.lastIndexOf("/")));
        body.setTitle(name);
        if(parentId.equals("/") || parentId.equals("")){
            parentId = getRoot();
        }
        body.setParents(Arrays.asList(new ParentReference().setId(parentId)));

        Path path1 = Paths.get(path);
        try {
            InputStreamContent content = new InputStreamContent(Files.probeContentType(path1), in);
            service.files().insert(body, content).execute();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean downloadFile(OutputStream out, String fileId) {
        try {
            service.files().get(fileId).executeMediaAndDownloadTo(out);
            //out.flush();
        } catch (IOException e) {
            e.printStackTrace();
            LOG.warn(e);
            return false;
        } finally {
//            try {
//                out.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
        return true;
    }

    @Override
    public boolean createDirectory(String folderId, String name) {
        //paste path into this method
        File body = new File();
        body.setTitle(name);
        body.setMimeType("application/vnd.google-apps.folder");
        try {
            if (folderId != null && folderId.length() > 0) {
                body.setParents(Arrays.asList(new ParentReference().setId(folderId)));
            }
            service.files().insert(body).execute();
        } catch (IOException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    public String getFileIdByName(String name, String path) {
        String id = null;

        String query;
        FileList result = null;
        if (path.equals("") || path.equals("root")) {
            query = "'" + getRoot() + "' in parents and trashed = " + "false";
        } else if (path.equals("recyclebin")) {
            query = "'" + getRoot() + "' in parents and trashed = " + "true";
        } else {
            query = "'" + path + "' in parents and trashed = false";
        }
        try {
            result = service.files().list().setQ(query).execute();
        } catch (IOException e) {
            LOG.warn(e);
        }
        List<File> files = result.getItems();
        for (File file : files) {
            if (file.getTitle().equals(name)) {
                id = file.getId();
                break;
            }
        }
        return id;
    }

    @Override
    public List<CustomFile> getDirectory(String path) {
        List<CustomFile> customFiles = new ArrayList<>();
        CustomFile customFile = null;
        FileList result = null;
        String query;
        String dateTime;
        Date lastModifiedDate = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        if (path.equals("") || path.equals("root")) {
            query = "'" + getRoot() + "' in parents and trashed = " + "false";
        } else if (path.equals("recyclebin")) {
            query = "'" + getRoot() + "' in parents and trashed = " + "true";
        } else {
            query = "'" + path + "' in parents and trashed = false";
        }
        try {
            result = service.files().list().setQ(query).execute();
        } catch (IOException e) {
            LOG.warn(e);
        }
        if(result == null) return null;
        List<File> files = result.getItems();
        if (files == null || files.size() == 0) {
            System.out.println("No files found.");
        } else {
            for (File file : files) {
                try {
                    customFile = new CustomFile(StorageType.DRIVE, user.getId(), clientId);
                } catch (NotLinkedProfileException e) {
                    LOG.warn(e);
                    return null;
                }
                customFile.setName(file.getTitle());
                customFile.setFileId(file.getId());
                dateTime = file.getModifiedDate().toString();
                try {
                    lastModifiedDate = format.parse(dateTime);
                } catch (ParseException e) {
                    LOG.warn(e);
                    e.printStackTrace();
                }
                customFile.setLastChange(lastModifiedDate);
                customFile.setSize(file.getFileSize());
                customFile.setType(file.getMimeType());
                customFile.setPath(file.getParents().get(0).getId());
                customFiles.add(customFile);
            }
            return customFiles;
        }
        return null;
    }

    @Override
    public boolean delete(String fileId) {

        try {
            service.files().delete(fileId).execute();
        } catch (IOException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public boolean toRecycleBin(String fileId) {
        try {
            service.files().trash(fileId).execute();
        } catch (IOException e) {
            LOG.warn(e);
            return false;
        }
        return true;
    }

    @Override
    public Long getFreeSize() throws IOException {
        About about = service.about().get().execute();
        long total = about.getQuotaBytesTotal();
        long quota = about.getQuotaBytesUsed();

        long free = total - quota;
        if (free < 0) {
            free = 0;
        }
        return free;
    }
    
    @Override
    public Long getTotalSize() throws IOException {
        About about = service.about().get().execute();
        long total = about.getQuotaBytesTotal();
        return total;
    }
    
    @Override
    public void getFileTree(CustomDirectory root) {
        String path = "'"+root.getPath()+"' in parents and trashed = false";
        FileList result = null;
        Map<String, String> map = new ProgramDirectoryService().getUserDirectories(user.getId());

        try {
            result = service.files().list().setQ(path).execute();
        } catch (IOException e) {
            LOG.warn(e);
        }
        List<File> files = result.getItems();
        if (files == null || files.size() == 0) {
        } else {
            for (File file : files) {
                if (!file.getMimeType().equals("application/vnd.google-apps.folder")) {
                    continue;
                }
                if(file.getId().equals(map.get("DRIVE"))){
                    continue;
                }
                CustomDirectory customDirectory = new CustomDirectory(file.getTitle(), file.getId());
                getFileTree(customDirectory);
                root.addDirectory(customDirectory);
            }
        }
    }

    @Override
    public String getRoot() {
        String rootId = null;
        try {
            About about = service.about().get().execute();
            rootId = about.getRootFolderId();
        } catch (IOException e) {
            LOG.warn(e);
        }
        return rootId;
    }
}
