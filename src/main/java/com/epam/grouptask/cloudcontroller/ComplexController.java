package com.epam.grouptask.cloudcontroller;

import com.dropbox.core.DbxException;
import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.*;
import com.epam.grouptask.services.ComplexFileService;
import com.epam.grouptask.services.ProgramDirectoryService;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Andrian on 21.01.2016.
 */
public class ComplexController extends CloudsController {
    private static final Logger LOG = Logger.getLogger(ComplexController.class);

    private DriveController driveController;
    private DropboxController dropboxController;
    private BoxController boxController;
    private List<ClientController> clientsControllers = new ArrayList<>();
    private int userId;

    private class UploadMaster implements Runnable {
        int count;
        FileInputStream fis;
        String path;
        String cloud;
        long size;
        Divided divided;
        ComplexFile complexFile;

        public UploadMaster(File file, String path, String cloud, long size, int count, ComplexFile complexFile) throws FileNotFoundException {
            this.fis = new FileInputStream(file);
            this.path = path;
            this.cloud = cloud;
            this.size = size;
            this.count = count;
            this.complexFile = complexFile;
        }

        @Override
        public void run() {
            Divided divivded = new Divided();
            divivded.setCloud(cloud);
            divivded.setOrder(count);
            divivded.setPath(path);
            complexFile.addPart(divivded);
            switch (cloud) {
                case "DROPBOX":
                    dropboxController.uploadFile(fis, path, size);
                    break;
                case "DRIVE":
                    driveController.uploadFile(fis, path, size);
                    break;
                case "BOX":
                    try {
                        boxController.uploadFile(fis, path, size);
                    } catch (FileTooLargeException e) {
                        LOG.warn(e);
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public ComplexController(int userId) {
        this.userId = userId;
        try {
            driveController = new DriveController(userId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            driveController = null;
        }
        try {
            dropboxController = new DropboxController(userId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            dropboxController = null;
        }
        try {
            boxController = new BoxController(userId);
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
            boxController = null;
        }

        try {
            Map<String, Boolean> clients = ClientController.getAllClientsByUser(userId);
            for (Map.Entry<String, Boolean> entry : clients.entrySet()) {
                if (entry.getValue()) {
                    clientsControllers.add(new ClientController(userId, entry.getKey()));
                }
            }
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
    }

    @Override
    public Long getFreeSize() throws Exception {
        return null;
    }

    @Override
    public boolean copy(String fromPath, String toPath) {
        return false;
    }

    @Override
    public boolean move(String fromPath, String toPath, StorageType toCloud, String toClientId) {
        return false;
    }

    @Override
    public boolean rename(String path, String newName) {
        ComplexFileService service = new ComplexFileService();
        return (service.rename(path, userId, newName) != 1);
    }

    @Override
    public boolean uploadFile(InputStream in, String path, Long size) {
        throw new UnsupportedOperationException();
        //what cloud is prefer to upload first
        /*long driveFreeSize = 0;
        long drbxFreeSize = 0;
        if (driveController != null) {
            try {
                driveFreeSize = driveController.getFreeSize();
            } catch (IOException e) {
                LOG.warn(e);
                driveFreeSize = 0;
            }
        }
        if (dropboxController != null) {
            try {
                drbxFreeSize = dropboxController.getFreeSize();
            } catch (DbxException e) {
                LOG.warn(e);
                drbxFreeSize = 0;
            }
        }
        if (driveFreeSize + drbxFreeSize <= size) {
            throw new FileTooLargeException();
        }*/
        //in different thread
        //driveController.uploadFile()
        //dropboxController
    }

    public boolean uploadFileWithSplit(InputStream in, String path, Long size, Map<String, Long> sizes) throws FileTooLargeException {
        Map<String, String> directorisPath;
        long driveFreeSize = 0;
        long drbxFreeSize = 0;
        long boxFreeSize = 0;
        if (driveController != null) {
            try {
                driveFreeSize = driveController.getFreeSize();
            } catch (IOException e) {
                LOG.warn(e);
                e.printStackTrace();
                driveFreeSize = 0;
            }
        }
        if (dropboxController != null) {
            try {
                drbxFreeSize = dropboxController.getFreeSize();
            } catch (DbxException e) {
                LOG.warn(e);
                e.printStackTrace();
                drbxFreeSize = 0;
            }
        }
        if (boxController != null) {
            try {
                boxFreeSize = boxController.getFreeSize();
            } catch (Exception e) {
                LOG.warn(e);
                e.printStackTrace();
                boxFreeSize = 0;
            }
        }
        if (driveFreeSize + drbxFreeSize + boxFreeSize <= size) {
            throw new FileTooLargeException();
        }
        try {
            int count = 0;
            directorisPath = new ProgramDirectoryService().getUserDirectories(userId);
            String name = String.valueOf(path.subSequence(path.lastIndexOf("/") + 1, path.length()));
            String parentId = String.valueOf(path.subSequence(0, path.lastIndexOf("/")));
            ComplexFile complexFile = new ComplexFile();
            complexFile.setName(name);
            complexFile.setUserId(userId);
            complexFile.setCreated(new Date());
            complexFile.setSize(size);
            List<Divided> parts = new ArrayList<>();
            complexFile.setParts(parts);
            BufferedInputStream bis = new BufferedInputStream(in, 16 * 1024);
            File tempFile = new File(String.valueOf(UUID.randomUUID()));
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(tempFile));
            ExecutorService es = Executors.newFixedThreadPool(4);
            String storage;
            Long fileSize;
            Iterator entries = sizes.entrySet().iterator();
            Map.Entry thisEntry = (Map.Entry) entries.next();
            storage = (String) thisEntry.getKey();
            fileSize = (long) thisEntry.getValue();
            byte[] data = new byte[16 * 1024];
            while ((bis.read(data)) >= 0) {
                if (tempFile.length() >= fileSize) {
                    UploadMaster uploadMaster = new UploadMaster(tempFile, directorisPath.get(storage) + "/" + tempFile.getName(), storage, fileSize, count, complexFile);
                    es.execute(uploadMaster);
                    thisEntry = (Map.Entry) entries.next();
                    count++;
                    tempFile = new File(String.valueOf(UUID.randomUUID()));
                    bos = new BufferedOutputStream(new FileOutputStream(tempFile));
                    storage = (String) thisEntry.getKey();
                    fileSize = (long) thisEntry.getValue();
                }
                bos.write(data);
            }
            UploadMaster uploadMaster = new UploadMaster(tempFile, directorisPath.get(storage) + "/" + tempFile.getName(), storage, fileSize, count, complexFile);
            es.execute(uploadMaster);
            ComplexFileService cfs = new ComplexFileService();
            cfs.saveComplexFile(complexFile);
            es.shutdown();
            try {
                es.awaitTermination(30, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean downloadFile(OutputStream out, String path) {
        ComplexFile complexFile = new ComplexFileService().getComplexFile(path, userId);
        List<Divided> parts = complexFile.getParts();
        Collections.sort(parts);
        for (Divided part : parts) {
            String name = String.valueOf(part.getPath().subSequence(part.getPath().lastIndexOf("/") + 1, part.getPath().length()));
            switch (part.getCloud()) {
                case "DROPBOX":
                    if (dropboxController != null) {
                        dropboxController.downloadFile(out, part.getPath());
                    }
                    break;
                case "DRIVE":
                    if (driveController != null) {
                        String parentId = String.valueOf(part.getPath().subSequence(0, part.getPath().lastIndexOf("/")));
                        String fileid = driveController.getFileIdByName(name, parentId);
                        driveController.downloadFile(out, fileid);
                    }
                    break;
                case "BOX":
                    if (boxController != null) {
                        String parentId = String.valueOf(part.getPath().subSequence(0, part.getPath().lastIndexOf("/")));
                        String fileid = boxController.getFileIdByName(name, parentId);
                        boxController.downloadFile(out, fileid);
                    }
                    break;
            }
        }
        try {
            out.close();
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean createDirectory(String path, String newName) {
        return false;
    }

    @Override
    public List<CustomFile> getDirectory(String path) {
        List<CustomFile> driveFiles;
        List<CustomFile> dropboxFiles;
        List<CustomFile> boxFiles;
        List<CustomFile> clentsFiles = new ArrayList<>();

        List<CustomFile> filesFromAllStorage = new ArrayList<>();

        if (driveController != null) {
            driveFiles = driveController.getDirectory(path);
            if(driveFiles != null) {
                filesFromAllStorage.addAll(driveFiles);
            }
        }
        if (dropboxController != null) {
            dropboxFiles = dropboxController.getDirectory(path);
            if(dropboxFiles != null) {
                filesFromAllStorage.addAll(dropboxFiles);
            }
        }
        if (boxController != null) {
            boxFiles = boxController.getDirectory(path);
            if(boxFiles != null) {
                filesFromAllStorage.addAll(boxFiles);
            }
        }
        if (clientsControllers != null) {
            if (clientsControllers.size() > 0) {
                for (ClientController clientController : clientsControllers) {
                    List<CustomFile> list = clientController.getDirectory(path);
                    if(list != null) {
                        clentsFiles.addAll(list);
                    }
                }
                filesFromAllStorage.addAll(clentsFiles);
            }
        }
        filesFromAllStorage.addAll(getComplexFiles());

        return filesFromAllStorage;
    }

    private List<CustomFile> getComplexFiles() {
        ComplexFileService service = new ComplexFileService();
        List<ComplexFile> complexFileList = service.getUsersFile(userId);
        List<CustomFile> files = new ArrayList<>();
        if (complexFileList != null) {
            for (ComplexFile complexFile : complexFileList) {
                CustomFile file = new CustomFile(StorageType.COMPLEX, userId, null);
                file.setName(complexFile.getName());
                file.setSize(complexFile.getSize());
                file.setLastChange(complexFile.getCreated());
                file.setType("file");
                files.add(file);
            }
        }
        return files;
    }

    @Override
    public boolean delete(String path) {
        ComplexFileService service = new ComplexFileService();
        ComplexFile complexFile = service.getComplexFile(path, userId);
        List<Divided> parts = complexFile.getParts();
        boolean deleted = false;
        if(parts != null) {
            for (Divided part : parts) {                        //physical delete
                switch (part.getCloud()) {
                    case "DROPBOX":
                        if (dropboxController != null) {
                            deleted = dropboxController.delete(part.getPath());
                        }
                        break;
                    case "DRIVE":
                        if (driveController != null) {
                            String name = String.valueOf(part.getPath().subSequence(part.getPath().lastIndexOf("/") + 1, part.getPath().length()));
                            String parentId = String.valueOf(part.getPath().subSequence(0, part.getPath().lastIndexOf("/")));
                            String fileid = driveController.getFileIdByName(name, parentId);
                            deleted = driveController.delete(fileid);
                        }
                        break;
                    case "BOX":
                        if (boxController != null) {
                            String name = String.valueOf(part.getPath().subSequence(part.getPath().lastIndexOf("/") + 1, part.getPath().length()));
                            String parentId = String.valueOf(part.getPath().subSequence(0, part.getPath().lastIndexOf("/")));
                            String fileid = boxController.getFileIdByName(name, parentId);
                            deleted = boxController.delete(fileid);
                        }
                        break;
                }
            }
            service.deleteFile(path, userId);       //delete from db
        }
        return deleted;
    }

    @Override
    public boolean toRecycleBin(String path) {
        return false;
    }

    @Override
    public String getRoot() {
        return "";
    }

    @Override
    public Long getTotalSize() throws IOException {
        /*long driveFreeSize = 0;
        long drbxFreeSize = 0;
        if (driveController != null) {
            try {
                driveFreeSize = driveController.getFreeSize();
            } catch (IOException e) {
                LOG.warn(e);
                driveFreeSize = 0;
            }
        }
        if (dropboxController != null) {
            try {
                drbxFreeSize = dropboxController.getFreeSize();
            } catch (DbxException e) {
                LOG.warn(e);
                drbxFreeSize = 0;
            }
        }*/
        return null;
    }

    @Override
    public void getFileTree(CustomDirectory root) {
        if (driveController != null) {
            CustomDirectory customDirectory = new CustomDirectory("Drive", driveController.getRoot());
            driveController.getFileTree(customDirectory);
            root.addDirectory(customDirectory);
        }
        if (dropboxController != null) {
            try {
                CustomDirectory customDirectory = new CustomDirectory("Dropbox", dropboxController.getRoot());
                dropboxController.getFileTree(customDirectory);
                root.addDirectory(customDirectory);
            } catch (DbxException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        if (boxController != null) {
            CustomDirectory customDirectory = new CustomDirectory("Box", boxController.getRoot());
            boxController.getFileTree(customDirectory);
            root.addDirectory(customDirectory);
        }
        if (clientsControllers != null) {
            if (clientsControllers.size() > 0) {
                for (ClientController clientController : clientsControllers) {
                    CustomDirectory customDirectory = new CustomDirectory(clientController.getClientProgram().getClientId(), clientController.getRoot());
                    clientController.getFileTree(customDirectory);
                    root.addDirectory(customDirectory);
                }
            }
        }
    }
}
