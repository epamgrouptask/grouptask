package com.epam.grouptask.model;

public enum Role {
	ADMIN("ADMIN"), USER("USER");

	private String name;

	Role(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
