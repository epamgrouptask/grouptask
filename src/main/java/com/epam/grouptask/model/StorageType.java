package com.epam.grouptask.model;

public enum StorageType {
    DRIVE("DRIVE"), DROPBOX("DROPBOX"), CLIENT("CLIENT"), COMPLEX("COMPLEX"), BOX("BOX");
    private String name;

    StorageType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}