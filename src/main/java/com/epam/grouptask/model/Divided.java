package com.epam.grouptask.model;

/**
 * Created by Andrian on 26.01.2016.
 */
public class Divided implements Comparable{
    private String path;
    private String cloud;
    private int order;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getCloud() {
        return cloud;
    }

    public void setCloud(String cloud) {
        this.cloud = cloud;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int compareTo(Object o) {
        Divided divided = (Divided) o;
        if (this.order<divided.order){
            return -1;
        }else if (this.order>divided.order){
            return 1;
        }else {
            return 0;
        }
    }
}
