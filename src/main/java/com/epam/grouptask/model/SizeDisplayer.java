/**
 * 
 */
package com.epam.grouptask.model;

import java.math.BigDecimal;

/**
 * @author Viktoria
 */
public class SizeDisplayer {
	public static String displaySize(Long size) {
		String[] sizeDimention = { "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
		int i = 0;
		long sizeIn = size;
		BigDecimal result;
		while (sizeIn > 1024) {
			if (i > sizeDimention.length) {
				break;
			}
			sizeIn /= 1024;
			i++;
		}
		result = new BigDecimal(size / Math.pow(1024, i));
		return result + " " + sizeDimention[i];
	}
}
