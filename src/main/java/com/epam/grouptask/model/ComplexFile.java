package com.epam.grouptask.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Andrian on 26.01.2016.
 */
public class ComplexFile {
    private Integer id;
    private String name;
    private int userId;
    private Long size;
    private Date created;
    private List<Divided> parts = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<Divided> getParts() {
        return parts;
    }

    public void setParts(List<Divided> parts) {
        this.parts = parts;
    }

    public void addPart(Divided part) {
        parts.add(part);
    }
}
