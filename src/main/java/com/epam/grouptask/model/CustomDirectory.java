package com.epam.grouptask.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 28.01.2016.
 */
public class CustomDirectory implements Serializable {
    private static final long serialVersionUID = -3388365165122445953L;

    String name;
    String path;
    List<CustomDirectory> innerDir = new ArrayList<>();

    public CustomDirectory(String name,String path) {
        this.name = name;
        this.path = path;
    }

    public void addDirectory(CustomDirectory customDirectory){
        innerDir.add(customDirectory);
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<CustomDirectory> getInnerDir() {
        return innerDir;
    }

    public void setInnerDir(List<CustomDirectory> innerDir) {
        this.innerDir = innerDir;
    }

    @Override
    public String toString() {
        return  "name: " + name +
                "{" + innerDir +
                "}";
    }

    public String toJson(){
        name = name.replaceAll("\"", "'");
        String slash = String.valueOf("\\\\");
        String twoSlash = String.valueOf("/");
        path = path == null ? "/" : path.replaceAll(slash, twoSlash);
        name = name == null ? "Unknown" : name.replaceAll(slash, twoSlash);

        String json = "{ \"text\": \""+name+"\", \"path\" : \""+path+"\"";
        if (innerDir.isEmpty()){
            json+="}";
        }else{
            json+=", \"nodes\": [";
            for (CustomDirectory customDirectory : innerDir) {
                json+= customDirectory.toJson();
                json+=",";
            }
            json = json.substring(0,json.length()-1);
            json+="]}";
        }
        return json;
    }
    public String nodesToJson(){
        String json = "[";
        for (CustomDirectory customDirectory : innerDir) {
            json+= customDirectory.toJson();
            json+=",";
        }
        json = json.substring(0,json.length()-1);
        json+="]";
        return json;
    }
}
