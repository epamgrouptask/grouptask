package com.epam.grouptask.model;

import com.epam.grouptask.services.UserService;

/**
 * Created by Andrian on 01.02.2016.
 */
public class OperationCount {
    private int getDir = 0;
    private int delete = 0;
    private int upload = 0;
    private int create = 0;
    private int rename = 0;
    private int move = 0;
    private int copy = 0;
    private int download = 0;
    private long downloadedSize = 0; //in kB
    private long uploadedSize = 0; //in kB

    public void incrementGet() {
        getDir++;
    }

    public void incrementDel() {
        delete++;
    }

    public void incrementUpload() {
        upload++;
    }

    public void incrementCreate() {
        create++;
    }

    public void incrementRename() {
        rename++;
    }

    public void incrementMove() {
        move++;
    }

    public void incrementCopy() {
        copy++;
    }

    public void incrementDownload() {
        download++;
    }

    public void incrementDownloadSize(long size) {
        downloadedSize += size;
    }

    public void incrementUploadSize(long size){
        uploadedSize += size;
    }

    public long getDownloadedSize() {
        return downloadedSize;
    }

    public void setDownloadedSize(long downloadedSize) {
        this.downloadedSize = downloadedSize;
    }

    public long getUploadedSize() {
        return uploadedSize;
    }

    public void setUploadedSize(long uploadedSize) {
        this.uploadedSize = uploadedSize;
    }

    public int getGetDir() {
        return getDir;
    }

    public void setGetDir(int getDir) {
        this.getDir = getDir;
    }

    public int getDelete() {
        return delete;
    }

    public void setDelete(int delete) {
        this.delete = delete;
    }

    public int getUpload() {
        return upload;
    }

    public void setUpload(int upload) {
        this.upload = upload;
    }

    public int getCreate() {
        return create;
    }

    public void setCreate(int create) {
        this.create = create;
    }

    public int getRename() {
        return rename;
    }

    public void setRename(int rename) {
        this.rename = rename;
    }

    public int getMove() {
        return move;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public int getCopy() {
        return copy;
    }

    public void setCopy(int copy) {
        this.copy = copy;
    }

    public int getDownload() {
        return download;
    }

    public void setDownload(int download) {
        this.download = download;
    }

    @Override
    public String toString() {
        return "{" +
                "\"getDir\": " + "\"" + getDir + "\"" +
                ", \"delete\": \"" + delete + "\"" +
                ", \"upload\": \"" + upload + "\"" +
                ", \"create\": \"" + create + "\"" +
                ", \"rename\": \"" + rename + "\"" +
                ", \"move\": \"" + move + "\"" +
                ", \"copy\": \"" + copy + "\"" +
                ", \"download\": \"" + download + "\"" +
                ", \"downloadedSize\": \"" + downloadedSize + "\"" +
                ", \"uploadedSize\": \"" + uploadedSize + "\"" +
                "}";
    }
}
