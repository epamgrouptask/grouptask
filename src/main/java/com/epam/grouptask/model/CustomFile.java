package com.epam.grouptask.model;

import com.dropbox.core.DbxException;
import com.epam.grouptask.cloudcontroller.*;
import com.epam.grouptask.exception.FileTooLargeException;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.services.ClientProgramService;
import com.epam.grouptask.services.FileTreeService;
import com.epam.grouptask.services.ProgramDirectoryService;
import com.epam.grouptask.utils.ActiveStorages;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CustomFile implements Serializable {
    private static final long serialVersionUID = 1150169519310163575L;
    private static final Logger LOG = Logger.getLogger(CustomFile.class);

    private Integer userId;
    private String name;
    private String path;
    private String type;
    private Long size;
    private Date lastChange;
    private transient StorageType storageType;
    private transient StorageType typeInComplex;
    private String fileId;
    private String clientId;
    private transient CloudsController cloud;
    private transient Map<String, Double> parts;

    public CustomFile(StorageType storageType, int user, String clientId) throws NotLinkedProfileException {
        this.storageType = storageType;
        userId = user;
        this.clientId = clientId;
        switch (storageType) {
            case DROPBOX:
                cloud = new DropboxController(userId);
                break;
            case DRIVE:
                cloud = new DriveController(userId);
                break;
            case CLIENT:
                cloud = new ClientController(userId, clientId);
                break;
            case COMPLEX:
                cloud = new ComplexController(userId);
                break;
            case BOX:
                cloud = new BoxController(userId);
                break;
        }

    }

    public boolean copy(String toPath, StorageType toStorage, String toClientId) throws FileTooLargeException {
        boolean copied = false;
        File file = null;
        OutputStream download = null;
        InputStream upload = null;
        if (toStorage == storageType) {
            switch (storageType) {
                case DROPBOX:
                    if ("/".equals(toPath)) {
                        toPath = "";
                    }
                    copied = cloud.copy(path, toPath + "/" + name);
                    break;
                case DRIVE:
                    copied = cloud.copy(fileId, toPath);
                    break;
                case BOX:
                    if (type.equals("file")) {
                        copied = cloud.copy(fileId, toPath);
                    } else if (type.equals("folder")) {
                        copied = ((BoxController) cloud).copyFolder(fileId, toPath);
                    }
                    break;
                case CLIENT:
                    if (clientId != null && (clientId.equals(toClientId) || toClientId == null)) {
                        copied = cloud.copy(path, toPath);
                    } else if (clientId != null && !clientId.equals(toClientId)) {
                        try {
                            ClientController anotherClient = new ClientController(userId, toClientId);
                            file = new File(UUID.randomUUID().toString());
                            download = new BufferedOutputStream(new FileOutputStream(file));
                            downloadFile(download);
                            download.flush();
                            download.close();
                            size = file.length();
                            upload = new BufferedInputStream(new FileInputStream(file));
                            if ("/".equals(toPath)) {
                                toPath = "";
                            }
                            copied = anotherClient.uploadFile(upload, toPath + "/" + name, size);            //toPath = parent id
                            upload.close();
                            file.delete();
                        } catch (IOException e) {
                            LOG.warn(e);
                            e.printStackTrace();
                        } catch (FileTooLargeException e) {
                            LOG.warn(e);
                            e.printStackTrace();
                        }
                    }
                    break;
                case COMPLEX:
                    throw new UnsupportedOperationException();
            }
        } else {
            try {
                switch (toStorage) {
                    case DROPBOX:
                        DropboxController drbx = new DropboxController(userId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        Long s = drbx.getFreeSize();
                        if ("/".equals(toPath)) {
                            toPath = "";
                        }
                        copied = drbx.uploadFile(upload, toPath + "/" + name, size);
                        upload.close();
                        boolean deleted = file.delete();
                        break;
                    case DRIVE:
                        DriveController drive = new DriveController(userId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        if ("/".equals(toPath)) {
                            toPath = "";
                        }
                        copied = drive.uploadFile(upload, toPath + "/" + name, size);            //toPath = parent id
                        upload.close();
                        file.delete();
                        break;
                    case CLIENT:
                        ClientController client = new ClientController(userId, toClientId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        if (toPath.endsWith("/") || toPath.endsWith("\\")) {
                            copied = client.uploadFile(upload, toPath + name, size);
                        } else {
                            copied = client.uploadFile(upload, toPath + "/" + name, size);
                        }
                        upload.close();
                        file.delete();
                        break;
                    case COMPLEX:
                        throw new UnsupportedOperationException();
                    case BOX:
                        BoxController box = new BoxController(userId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        if ("/".equals(toPath)) {
                            toPath = "";
                        }
                        copied = box.uploadFile(upload, toPath + "/" + name, size);            //toPath = parent id
                        upload.close();
                        file.delete();
                        break;
                }
            } catch (DbxException e) {
                LOG.warn(e);
                e.printStackTrace();
            } catch (IOException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return copied;
    }

    public boolean move(String toPath, StorageType toCloud, String toClientId) throws FileTooLargeException {
        //toPath for DRIVE it`s folderId
        boolean moved = false;
        File file = null;
        OutputStream download = null;
        InputStream upload = null;
        if (toCloud == storageType) {
            switch (storageType) {
                case DROPBOX:
                    moved = cloud.move(path, toPath, toCloud, toClientId);
                    break;
                case DRIVE:
                    moved = cloud.move(fileId, toPath, toCloud, toClientId);
                    break;
                case BOX:
                    if (type.equals("file")) {
                        moved = cloud.move(fileId, toPath, toCloud, toClientId);
                    } else if (type.equals("folder")) {
                        moved = ((BoxController) cloud).moveFolder(fileId, toPath, toCloud, toClientId);
                    }
                    break;
                case CLIENT:
                    if (clientId != null && clientId.equals(toClientId) || toClientId == null) {
                        moved = cloud.move(path, toPath, toCloud, toClientId);
                    } else if (clientId != null && !clientId.equals(toClientId)) {
                        try {
                            ClientController anotherClient = new ClientController(userId, toClientId);
                            file = new File(UUID.randomUUID().toString());
                            download = new BufferedOutputStream(new FileOutputStream(file));
                            downloadFile(download);
                            download.flush();
                            download.close();
                            size = file.length();
                            upload = new BufferedInputStream(new FileInputStream(file));
                            if ("/".equals(toPath)) {
                                toPath = "";
                            }
                            moved = anotherClient.uploadFile(upload, toPath + "/" + name, size);            //toPath = parent id
                            upload.close();
                            file.delete();
                            delete();
                        } catch (IOException e) {
                            LOG.warn(e);
                            e.printStackTrace();
                        }
                    }
                    break;
                case COMPLEX:
                    throw new UnsupportedOperationException();
            }
        } else {
            try {
                switch (toCloud) {
                    case DROPBOX:
                        DropboxController drbx = new DropboxController(userId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        Long s = drbx.getFreeSize();
                        if ("/".equals(toPath)) {
                            toPath = "";
                        }
                        moved = drbx.uploadFile(upload, toPath + "/" + name, size);
                        upload.close();
                        boolean deleted = file.delete();
                        deleted = delete();
                        break;
                    case DRIVE:
                        DriveController drive = new DriveController(userId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        if ("/".equals(toPath)) {
                            toPath = "";
                        }
                        moved = drive.uploadFile(upload, toPath + "/" + name, size);            //toPath = parent id
                        upload.close();
                        file.delete();
                        delete();
                        break;
                    case CLIENT:
                        ClientController client = new ClientController(userId, toClientId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        if (toPath.endsWith("/") || toPath.endsWith("\\")) {
                            moved = client.uploadFile(upload, toPath + name, size);
                        } else {
                            moved = client.uploadFile(upload, toPath + "/" + name, size);
                        }
                        //moved = client.uploadFile(upload, toPath + "/" + name, size);
                        upload.close();
                        file.delete();
                        delete();
                        break;
                    case COMPLEX:
                        throw new UnsupportedOperationException();
                    case BOX:
                        BoxController box = new BoxController(userId);
                        file = new File(UUID.randomUUID().toString());
                        download = new BufferedOutputStream(new FileOutputStream(file));
                        if (storageType == StorageType.CLIENT) {
                            cloud.downloadFile(download, path);
                        } else {
                            downloadFile(download);
                        }
                        download.flush();
                        download.close();
                        size = file.length();
                        upload = new BufferedInputStream(new FileInputStream(file));
                        if ("/".equals(toPath)) {
                            toPath = "";
                        }
                        moved = box.uploadFile(upload, toPath + "/" + name, size);            //toPath = parent id
                        upload.close();
                        file.delete();
                        delete();
                        break;
                }
            } catch (DbxException e) {
                LOG.warn(e);
                e.printStackTrace();
            } catch (IOException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return moved;
    }

    public boolean rename(String newName) {
        //for Drive path equals file Id
        boolean renamed = false;
        if (storageType == StorageType.DRIVE) {
            renamed = cloud.rename(fileId, newName);
        } else if (storageType == StorageType.DROPBOX || storageType == StorageType.CLIENT) {
            renamed = cloud.rename(path, newName);
        } else if (storageType == StorageType.COMPLEX) {           //for complex path will be name of file
            renamed = cloud.rename(name, newName);
        } else if (storageType == StorageType.BOX) {
            if (type.equals("file")) {
                renamed = cloud.rename(fileId, newName);
            } else if (type.equals("folder")) {
                renamed = ((BoxController) cloud).renameFolder(fileId, newName);
            }
        }
        return renamed;
    }

    //Delete parameter path
    public boolean createDirectory(String newName) {
        boolean created = false;
        if (storageType == StorageType.DROPBOX) {
            created = cloud.createDirectory(path, newName);
        } else if (storageType == StorageType.DRIVE || storageType == StorageType.BOX) {
            //path equals FOLDER_NAME
            created = cloud.createDirectory(fileId, newName);
        } else if (storageType == StorageType.CLIENT) {
            created = cloud.createDirectory(path, newName);
        } else if (storageType == StorageType.COMPLEX) {
            throw new UnsupportedOperationException();
        }
        return created;
    }

    public boolean uploadFile(InputStream in) throws FileTooLargeException {    //upload file(String path)
        boolean uploaded = false;
        try {
            if (size > cloud.getFreeSize()) {
                throw new FileTooLargeException();
            }
            if (path.equals("") || path.equals("/")) {
                path = getRoot();
            }
        } catch (Exception e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        if (storageType == StorageType.DRIVE || storageType == StorageType.CLIENT || storageType == StorageType.BOX) {
            if(path.endsWith("/") || path.endsWith("\\")){
                uploaded = cloud.uploadFile(in, path + name, size);
            } else {
                uploaded = cloud.uploadFile(in, path + "/" + name, size);     //for drive path = parent id
            }
        } else if (storageType == StorageType.DROPBOX) {
            uploaded = cloud.uploadFile(in, path, size);
        }
        return uploaded;
    }

    public boolean uploadFileWithSplit(InputStream in, Map<String, Long> sizes) throws FileTooLargeException {
        boolean uploaded = ((ComplexController) cloud).uploadFileWithSplit(in, path + name, size, sizes);
        return uploaded;
    }

    public boolean downloadFile(OutputStream os) {
        boolean downloaded = false;
        //download files from cloud
        if (storageType == StorageType.DRIVE || storageType == StorageType.BOX) {
            downloaded = cloud.downloadFile(os, fileId);
        }
        if (storageType == StorageType.DROPBOX) {
            downloaded = cloud.downloadFile(os, path);
        }
        if (storageType == StorageType.CLIENT) {
            if (path.endsWith("/") || path.endsWith("\\")) {
                downloaded = cloud.downloadFile(os, path + name);
            } else {
                downloaded = cloud.downloadFile(os, path + '/' + name);
            }
        }
        if (storageType == StorageType.COMPLEX) {
            downloaded = cloud.downloadFile(os, name);
        }
        return downloaded;
    }

    public List<CustomFile> getDirectory() {
        List<CustomFile> list = null;
        list = cloud.getDirectory(path);
        if (list != null) {
            Map<String, String> map = new ProgramDirectoryService().getUserDirectories(userId);
            //String pathInCloud = map.get(storageType.toString());
            if (map != null) {
                CustomFile currentFile;
                for (int i = 0; i < list.size(); i++) {
                    currentFile = list.get(i);
                    if (currentFile.getStorageType() == StorageType.DRIVE) {
                        if (currentFile.getFileId().equals(map.get(currentFile.getStorageType().toString()))) {
                            list.remove(i);
                        }
                    }

                    if (i < list.size()) {
                        currentFile = list.get(i);
                        if (currentFile.getStorageType() == StorageType.DROPBOX) {
                            if (currentFile.getPath().equals(map.get(currentFile.getStorageType().toString()).toLowerCase())) {
                                list.remove(i);
                            }
                        }
                    }

                    if (i < list.size()) {
                        currentFile = list.get(i);
                        if (currentFile.getStorageType() == StorageType.BOX) {
                            if (currentFile.getFileId().equals(map.get(currentFile.getStorageType().toString()))) {
                                list.remove(i);
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    public List<CustomFile> getAllDirectoriesWithProgramDirectory() {
        List<CustomFile> list = null;
        list = cloud.getDirectory(path);

        return list;
    }

    public boolean toRecyclerBin() {
        //path equals File or Folder id in Drive
        boolean recycler = false;
        if (storageType == StorageType.DRIVE) {
            recycler = cloud.toRecycleBin(fileId);
        } else if (storageType == StorageType.BOX) {
            recycler = cloud.delete(fileId);
        } else {
            throw new UnsupportedOperationException();
        }
        return recycler;
    }

    public boolean delete() {
        boolean deleted = false;
        if (storageType == StorageType.DROPBOX) {
            deleted = cloud.delete(path);
        } else if (storageType == StorageType.DRIVE) {
            deleted = cloud.delete(fileId);
        } else if (storageType == StorageType.BOX) {
            if (type.equals("file")) {
                deleted = ((BoxController) cloud).delete(fileId);
            } else if (type.equals("folder")) {
                deleted = ((BoxController) cloud).deleteFolder(fileId);
            }
        } else if (storageType == StorageType.CLIENT) {
            deleted = cloud.delete(path);
        } else if (storageType == StorageType.COMPLEX) {
            deleted = cloud.delete(name);
        } else if (storageType == StorageType.BOX) {
            deleted = cloud.toRecycleBin(fileId);
        }
        return deleted;
    }

    public String toJson() {
        name = name.replaceAll("\"", "'");
        String slash = String.valueOf("\\\\");
        String twoSlash = String.valueOf("/");
        path = path == null ? "/" : path.replaceAll(slash, twoSlash);
        name = name == null ? "Unknown" : name.replaceAll(slash, twoSlash);

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String strDate = lastChange == null ? null : df.format(lastChange);
        return "{" +
                "\"userId\": " + "\"" + userId + "\"" +
                ", \"name\": \"" + name + "\"" +
                ", \"path\": \"" + path + "\"" +
                ", \"type\": \"" + type + "\"" +
                ", \"size\": \"" + size + "\"" +
                ", \"lastChange\": \"" + strDate + "\"" +
                ", \"storageType\": \"" + storageType + "\"" +
                ", \"fileId\": \"" + fileId + "\"" +
                ", \"clientId\": \"" + clientId + "\"" +
                "}";
    }

    @Override
    public String toString() {
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String strDate = lastChange == null ? null : df.format(lastChange);
        return "CustomFile{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", type='" + type + '\'' +
                ", size=" + size +
                ", lastChange=" + strDate +
                ", storageType=" + storageType +
                ", fileId='" + fileId + '\'' +
                ", clientId='" + clientId + '\'' +
                '}';
    }

    public String getRoot() {
        return cloud.getRoot();
    }

    public CustomDirectory getFileTree() {
        FileTreeService service = new FileTreeService();

        CustomDirectory customDirectory = null;
        if (storageType == StorageType.CLIENT) {
            customDirectory = service.getStorageFileTree(userId, clientId);
            if (customDirectory == null) {
                customDirectory = updateFileTree();
            }
        } else if (storageType == StorageType.COMPLEX) {
            customDirectory = service.getUserTrees(userId);
            if (customDirectory == null || customDirectory.getInnerDir().size() <= 0) {
                customDirectory = updateFileTree();
            } else {
                List<CustomDirectory> clouds = customDirectory.getInnerDir();
                JSONObject jsObject = ActiveStorages.getActive(userId);
                if (jsObject.length() != clouds.size()) {
                    try {
                        JSONArray jsonArray = jsObject.getJSONArray("clients");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            boolean isAvailable = false;
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            for (CustomDirectory d : clouds) {
                                try {
                                    if (jsonObject.getString("name").equals(d.getName()) && jsonObject.getBoolean("status")) {
                                        isAvailable = true;
                                        break;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (!isAvailable && jsonObject.getBoolean("status")) {
                                customDirectory = updateFileTree();
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            customDirectory = service.getStorageFileTree(userId, storageType.toString());
            if (customDirectory == null) {
                customDirectory = updateFileTree();
            }
        }
        return customDirectory;
    }

    public CustomDirectory updateFileTree() {
        CustomDirectory customDirectory = new CustomDirectory("/", getRoot());
        try {
            cloud.getFileTree(customDirectory);
        } catch (DbxException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        FileTreeService service = new FileTreeService();
        if (storageType == StorageType.CLIENT) {
            service.updateTree(userId, clientId, customDirectory);
        } else if (storageType == StorageType.COMPLEX) {
            List<CustomDirectory> clouds = customDirectory.getInnerDir();
            for (CustomDirectory d : clouds) {
                String cloudName = d.getName();
                CustomDirectory directory = new CustomDirectory("/", "/");
                directory.setInnerDir(d.getInnerDir());
                service.updateTree(userId, cloudName, directory);
                if (!cloudName.equals("Box") && !cloudName.equals("Drive") && !cloudName.equals("Dropbox")) {
                    d.setName(new ClientProgramService().getClientById(userId, cloudName).getName());
                }
            }
        } else {
            service.updateTree(userId, storageType.toString(), customDirectory);
        }
        return customDirectory;
    }

    public String getJsonFileTree() {
        CustomDirectory customDirectory = new CustomDirectory("/", getRoot());
        String tree = null;
        if (storageType == StorageType.CLIENT) {
            tree = ((ClientController) cloud).getJsonFileTree(customDirectory);
        }
        return tree;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public CloudsController getCloud() {
        return cloud;
    }

    public Date getLastChange() {
        return lastChange;
    }

    public void setLastChange(Date lastChange) {
        this.lastChange = lastChange;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public StorageType getTypeInComplex() {
        return typeInComplex;
    }

    public void setTypeInComplex(StorageType typeInComplex) {
        this.typeInComplex = typeInComplex;
    }
}