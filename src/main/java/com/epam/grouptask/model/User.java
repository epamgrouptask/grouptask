package com.epam.grouptask.model;

import java.sql.Timestamp;

public class User {
    private int id;
    private String vkId;
    private String googleId;
    private String driveAccessToken;
    private String driveRefreshToken;
    private String drbxToken;
    private String email;
    private String userName;
    private String password;
    private Role role;
    private String locale;
    private String fullname;
    private Timestamp driveAccessTokenCreate;
    private boolean confirmed;
    private String boxRefreshToken;
    private String boxAccessToken;
    private Timestamp boxAccessTokenCreate;
    private boolean blocked;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    public String getDriveAccessToken() {
        return driveAccessToken;
    }

    public void setDriveAccessToken(String driveAccessToken) {
        this.driveAccessToken = driveAccessToken;
    }

    public String getDrbxToken() {
        return drbxToken;
    }

    public void setDrbxToken(String drbxToken) {
        this.drbxToken = drbxToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDriveRefreshToken() {
        return driveRefreshToken;
    }

    public void setDriveRefreshToken(String driveRefreshToken) {
        this.driveRefreshToken = driveRefreshToken;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Timestamp getDriveAccessTokenCreate() {
        return driveAccessTokenCreate;
    }

    public void setDriveAccessTokenCreate(Timestamp driveAccessTokenCreate) {
        this.driveAccessTokenCreate = driveAccessTokenCreate;
    }

    public String getBoxRefreshToken() {
        return boxRefreshToken;
    }

    public void setBoxRefreshToken(String boxRefreshToken) {
        this.boxRefreshToken = boxRefreshToken;
    }

    public String getBoxAccessToken() {
        return boxAccessToken;
    }

    public void setBoxAccessToken(String boxAccessToken) {
        this.boxAccessToken = boxAccessToken;
    }

    public Timestamp getBoxAccessTokenCreate() {
        return boxAccessTokenCreate;
    }

    public void setBoxAccessTokenCreate(Timestamp boxAccessTokenCreate) {
        this.boxAccessTokenCreate = boxAccessTokenCreate;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public String toJson() {
        return "{" +
                "\"id\": \"" + id + "\"" +
                ", \"vkId\": \"" + vkId + '\"' +
                ", \"googleId\": \"" + googleId + '\"' +
                ", \"driveAccessToken\": \"" + driveAccessToken + '\"' +
                ", \"driveRefreshToken\": \"" + driveRefreshToken + '\"' +
                ", \"drbxToken\": \"" + drbxToken + '\"' +
                ", \"email\":\"" + email + '\"' +
                ", \"userName\": \"" + userName + '\"' +
                ", \"role\": \"" + role + '\"' +
                ", \"locale\": \"" + locale + '\"' +
                ", \"fullname\": \"" + fullname + '\"' +
                ", \"driveAccessTokenCreate\": \"" + driveAccessTokenCreate + "\"" +
                ", \"confirmed\": \"" + confirmed + "\"" +
                ", \"boxRefreshToken\": \"" + boxRefreshToken + '\"' +
                ", \"boxAccessToken\": \"" + boxAccessToken + '\"' +
                ", \"boxAccessTokenCreate\" :\"" + boxAccessTokenCreate + "\"" +
                ", \"blocked\": \"" + blocked + "\"" +
                '}';
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", vkId='" + vkId + '\'' +
                ", googleId='" + googleId + '\'' +
                ", driveAccessToken='" + driveAccessToken + '\'' +
                ", driveRefreshToken='" + driveRefreshToken + '\'' +
                ", drbxToken='" + drbxToken + '\'' +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", locale='" + locale + '\'' +
                ", fullname='" + fullname + '\'' +
                ", driveAccessTokenCreate=" + driveAccessTokenCreate +
                ", confirmed=" + confirmed +
                ", boxRefreshToken='" + boxRefreshToken + '\'' +
                ", boxAccessToken='" + boxAccessToken + '\'' +
                ", boxAccessTokenCreate=" + boxAccessTokenCreate +
                ", blocked=" + blocked +
                '}';
    }
}
