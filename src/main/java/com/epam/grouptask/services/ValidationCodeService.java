/**
 * 
 */
package com.epam.grouptask.services;

import java.sql.Connection;
import java.sql.SQLException;

import com.epam.grouptask.connection.ConnectionManager;
import com.epam.grouptask.dao.ValidationCodeDao;
import org.apache.log4j.Logger;

public class ValidationCodeService {
	private static final Logger LOG = Logger.getLogger(ValidationCodeService.class);
	private Connection conn = null;
	private ValidationCodeDao valCodeDao = null;

	public boolean isExist(String code) {
		conn = ConnectionManager.getConnection();
		boolean exists = false;
		valCodeDao = new ValidationCodeDao();
		try {
			exists = valCodeDao.isExist(conn, code);
		} catch (SQLException e) {
			LOG.warn(e);
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
		}
		return exists;
	}
	public void insertCode(String code, int userId){
		conn = ConnectionManager.getConnection();
		valCodeDao = new ValidationCodeDao();
		try {
			valCodeDao.insertCode(code, conn, userId);
		} catch (SQLException e) {
			LOG.warn(e);
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
		}
	}
	public boolean valid( String code) throws SQLException {
		boolean isValid=false;
		conn = ConnectionManager.getConnection();
		valCodeDao = new ValidationCodeDao();
		try {
			isValid=valCodeDao.valid(conn, code);
		} catch (SQLException e) {
			LOG.warn(e);
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
		}
		return isValid;
	}
	
	public int delete( String code){
		int deleted=0;
		conn = ConnectionManager.getConnection();
		valCodeDao = new ValidationCodeDao();
		try {
			deleted=valCodeDao.delete(conn, code);
		} catch (SQLException e) {
			LOG.warn(e);
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				LOG.warn(e);
				e.printStackTrace();
			}
		}
		return deleted;
	}
}
