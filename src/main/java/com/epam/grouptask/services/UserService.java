package com.epam.grouptask.services;

import com.epam.grouptask.connection.ConnectionManager;
import com.epam.grouptask.dao.UserDao;
import com.epam.grouptask.model.Role;
import com.epam.grouptask.model.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class UserService {
    private static final Logger LOG = Logger.getLogger(UserService.class);
    private Connection conn = null;
    private UserDao userDao = null;

    public int changeLocale(String locale, int userId) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        int changed = 0;
        try {
            changed = userDao.updateLocale(conn, locale, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return changed;
    }

    public List<User> getAllUsers() {
        List<User> users = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            users = userDao.getAllUsers(conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return users;
    }

    public User getUserById(int id) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            user = userDao.getUserById(id, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User getUserByCode(String code) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            user = userDao.getUserByCode(code, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User getUserSameByNameEmail(String username, String email, int id) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            user = userDao.getSameUserByLoginEmail(username, email, id, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User getUserByName(String username) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            user = userDao.getUserByLogin(username, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public void insertUser(User user) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            userDao.inserUser(user, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    public User getUserByVk(String id) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            user = userDao.vkLogin(conn, id);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User getUserByGoogle(String id) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            user = userDao.googleLogin(conn, id);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User loginOrSignupVk(String id, String fullname, String email) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            user = userDao.vkLogin(conn, id);
            if (user == null) {
                saved = userDao.setUserVk(conn, id, fullname, email);
                if (saved == 1) {
                    user = userDao.vkLogin(conn, id);
                }
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User loginOrSignupGoogle(String id, String fullname, String email) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            user = userDao.googleLogin(conn, id);
            if (user == null) {
                saved = userDao.setUserGoogle(conn, id, fullname, email);
                if (saved == 1) {
                    user = userDao.googleLogin(conn, id);
                }
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User linkVk(int userId, String vkId) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.linkVk(conn, userId, vkId);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User linkGoogle(int userId, String googleId) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.linkGoogle(conn, userId, googleId);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User unlinkDrive(int userId) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.unlinkDrive(conn, userId);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User unlinkDropbox(int userId) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.unlinkDropbox(conn, userId);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User unlinkGoogle(int userId) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.unlinkGoogle(conn, userId);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User unlinkVk(int userId) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.unlinkVk(conn, userId);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User unlinkBox(int userId) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.unlinkBox(conn, userId);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User linkDropbox(int userId, String token) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.linkDropbox(conn, userId, token);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User linkDrive(int userId, String accessToken, String refreshToken) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.linkDrive(conn, userId, accessToken, refreshToken);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public User linkBox(int userId, String accessToken, String refreshToken) {
        User user = null;
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();

        int saved = 0;
        try {
            saved = userDao.linkBox(conn, userId, accessToken, refreshToken);
            if (saved == 1) {
                user = userDao.getUserById(userId, conn);
            }
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return user;
    }

    public void changeEmailByLogin(String login, String email) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            userDao.changeEmailByLogin(login, email, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    public void changePassByLogin(String login, String pass) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            userDao.changePassByLogin(login, pass, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    public void changeEUFbyId(String fullname, String username, String email, int id) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            userDao.changeEUFbyId(fullname, username, email, id, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    public void setConfirmed(String login) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        try {
            userDao.setConfirmed(login, conn);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    public String haveDropbox(int userId) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        String token = null;
        try {
            token = userDao.haveDropbox(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return token;
    }

    public String haveDrive(int userId) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        String token = null;
        try {
            token = userDao.haveDrive(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return token;
    }

    public String haveBox(int userId) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        String token = null;
        try {
            token = userDao.haveBox(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return token;
    }

    public int deleteUser(int userId) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        int deleted = 0;
        try {
            deleted = userDao.deleteRow(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return deleted;
    }

    public int updateUser(User user) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        int update = 0;
        try {
            update = userDao.updateUser(conn, user);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return update;
    }

    public int changeRole(int userId, Role newRole) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        int changed = 0;
        try {
            changed = userDao.changeRole(conn, userId, newRole);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }

        return changed;
    }

    public int changeBlocked(int userId, boolean blocked) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        int changed = 0;
        try {
            changed = userDao.changeBlock(conn, userId, blocked);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return changed;
    }

    public String getUserLocale(int userId) {
        conn = ConnectionManager.getConnection();
        userDao = new UserDao();
        String locale = null;

        try {
            locale = userDao.getUserLocale(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return locale;
    }
}
