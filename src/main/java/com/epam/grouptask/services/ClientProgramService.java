package com.epam.grouptask.services;

import com.epam.grouptask.connection.ConnectionManager;
import com.epam.grouptask.dao.ClientProgramDao;
import com.epam.grouptask.model.ClientProgram;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andrian on 24.01.2016.
 */
public class ClientProgramService {
    private static final Logger LOG = Logger.getLogger(ClientProgramService.class);
    private Connection conn = null;
    private ClientProgramDao clientProgramDao = null;

    public String getClientIdByNameAndUser(int userId, String name){
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        String clientId = null;
        try {
            clientId = clientProgramDao.getClientIdByUserAndName(conn, name, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return clientId;
    }

    public boolean setRemoved (String clientId, boolean isRemoved) {
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        boolean removed = false;
        try {
        	removed = clientProgramDao.setRemovedClient(conn, clientId, isRemoved);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return removed;
    }
    
    public String getUserLocale(String clientId){
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        String locale = null;
        try {
            locale = clientProgramDao.getUserLocale(conn, clientId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return locale;
    }

    public Integer getUserId (String clientId) {
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        Integer id = null;
        try {
            id = clientProgramDao.getUser(conn, clientId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return id;
    }

    public List<String> getPathByClient(String id) {
        List<String> paths = null;
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        try {
        	paths = clientProgramDao.getPathsByClient(conn, id);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return paths;
    }

    public List<ClientProgram> getAllClientsByUserId(int id,int mode) {
        List<ClientProgram> clients = null;
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        try {
            clients = clientProgramDao.getAllClientsByUserId(conn, id,mode);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return clients;
    }
    
    public ClientProgram getClientById(int userId, String clientId) {
        ClientProgram clientProgram = null;
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        try {
            clientProgram = clientProgramDao.getClientById(conn, userId, clientId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return clientProgram;
    }

    public int setClientWithoutPaths(ClientProgram clientProgram) {
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();
        int saved = 0;
        try {
            saved = clientProgramDao.saveClientProgram(conn, clientProgram.getClientId(), clientProgram.getUserId(), clientProgram.getMac(), clientProgram.getName());
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return saved;
    }

    public void setPaths(ClientProgram clientProgram) {
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        Integer id=null;
		try {
			id = clientProgramDao.getIdByUserAndClient(conn, clientProgram.getClientId(), clientProgram.getUserId());
		} catch (SQLException e2) {
            LOG.warn(e2);
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
        try {
            conn.setAutoCommit(false);
            if (id == null) {
                conn.rollback();
                return;
            }
            clientProgramDao.savePathsById(conn, clientProgram.getPaths(), id);
            conn.commit();
        } catch (SQLException e) {
            LOG.warn(e);
            try {
                conn.rollback();
            } catch (SQLException e1) {
                LOG.warn(e1);
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    public void setClientWithPaths(ClientProgram clientProgram) {
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        try {
            conn.setAutoCommit(false);

            int saved = clientProgramDao.saveClientProgram(conn, clientProgram.getClientId(), clientProgram.getUserId(), clientProgram.getMac(), clientProgram.getName());
            Integer id = clientProgramDao.getIdByUserAndClient(conn, clientProgram.getClientId(), clientProgram.getUserId());
            if (id == null) {
                conn.rollback();
                return;
            }
            clientProgramDao.savePathsById(conn, clientProgram.getPaths(), id);

            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            LOG.warn(e);
            try {
                conn.rollback();
            } catch (SQLException e1) {
                LOG.warn(e1);
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }
    
    public void removePathByClient(String clientId,String path){
        conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        try {
            conn.setAutoCommit(false);
            clientProgramDao.removePathByClient(conn, clientId, path);
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            LOG.warn(e);
            try {
                conn.rollback();
            } catch (SQLException e1) {
                LOG.warn(e1);
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }
    public void rename(String newName,String clientId){
    	conn = ConnectionManager.getConnection();
        clientProgramDao = new ClientProgramDao();

        try {
            conn.setAutoCommit(false);
            clientProgramDao.rename(conn, newName, clientId);
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            LOG.warn(e);
            try {
                conn.rollback();
            } catch (SQLException e1) {
                LOG.warn(e1);
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }
}
