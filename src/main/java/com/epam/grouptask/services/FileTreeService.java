package com.epam.grouptask.services;

import com.epam.grouptask.connection.ConnectionManager;
import com.epam.grouptask.dao.FileTreeDao;
import com.epam.grouptask.model.CustomDirectory;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Andrian on 18.02.2016.
 */
public class FileTreeService {
    private static final Logger LOG = Logger.getLogger(StatService.class);

    Connection conn = null;
    FileTreeDao ftd = null;

    public CustomDirectory getStorageFileTree(int userId, String storage){
        CustomDirectory customDirectory = null;

        conn = ConnectionManager.getConnection();
        ftd = new FileTreeDao();

        try {
            customDirectory = ftd.getStorage(conn, userId, storage);
        } catch (SQLException | IOException | ClassNotFoundException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return customDirectory;
    }

    public CustomDirectory getUserTrees(int userId){
        CustomDirectory customDirectory = null;

        conn = ConnectionManager.getConnection();
        ftd = new FileTreeDao();

        try {
            customDirectory = ftd.getAllStorages(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return customDirectory;
    }

    public int updateTree(int userId, String storage, CustomDirectory cd){
        conn = ConnectionManager.getConnection();
        ftd = new FileTreeDao();
        int updated = 0;
        try {
            updated = ftd.updateTrees(conn, cd, userId, storage);
        } catch (SQLException | IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return updated;
    }
}
