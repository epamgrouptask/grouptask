package com.epam.grouptask.services;

import com.epam.grouptask.connection.ConnectionManager;
import com.epam.grouptask.dao.ComplexFileDao;
import com.epam.grouptask.model.ComplexFile;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Andrian on 26.01.2016.
 */
public class ComplexFileService {
    private static final Logger LOG = Logger.getLogger(ComplexFile.class);
    private Connection conn;
    private ComplexFileDao complexFileDao;

    public void saveComplexFile(ComplexFile complexFile) {
        conn = ConnectionManager.getConnection();
        complexFileDao = new ComplexFileDao();

        try {
            conn.setAutoCommit(false);

            complexFileDao.setFile(conn, complexFile);
            Integer id = complexFileDao.getIdByNameAndUser(conn, complexFile.getName(), complexFile.getUserId());
            if (id == null) {
                conn.rollback();
                return;
            }
            complexFileDao.saveDividedParts(conn, id, complexFile.getParts());
            conn.commit();
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            LOG.warn(e);
            try {
                conn.rollback();
            } catch (SQLException e1) {
                LOG.warn(e1);
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }

    }

    public ComplexFile getComplexFile(String name, int userId) {
        conn = ConnectionManager.getConnection();
        complexFileDao = new ComplexFileDao();

        ComplexFile complexFile = null;
        try {
            Integer id = complexFileDao.getIdByNameAndUser(conn, name, userId);
            if (id == null) {
                return complexFile;
            }
            complexFile = complexFileDao.getFileByUser(conn, name, userId);
            complexFile.setParts(complexFileDao.getPartsForFile(conn, id));
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }

        return complexFile;
    }

    public List<ComplexFile> getUsersFile(int userId) {
        conn = ConnectionManager.getConnection();
        complexFileDao = new ComplexFileDao();
        List<ComplexFile> files = null;
        try {
            files = complexFileDao.getAllUserFiles(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return files;
    }

    public int deleteFile(String fileName, int userId) {
        conn = ConnectionManager.getConnection();
        complexFileDao = new ComplexFileDao();
        int deleted = 0;

        try {
            Integer id = complexFileDao.getIdByNameAndUser(conn, fileName, userId);
            if (id == null) {
                return deleted;
            }
            deleted = complexFileDao.deleteParts(conn, id);
            if (deleted <= 0) {
                return deleted;
            }
            deleted = complexFileDao.deleteFile(conn, fileName, userId);

        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return deleted;
    }

    public int rename(String fileName, int userId, String newName) {
        conn = ConnectionManager.getConnection();
        complexFileDao = new ComplexFileDao();
        int rename = 0;
        try {
            rename = complexFileDao.rename(conn, fileName, userId, newName);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return rename;
    }

    public String getDividedFilenameByPath(String path) {

        conn = ConnectionManager.getConnection();
        complexFileDao = new ComplexFileDao();
        String name = null;

        try {
            name = complexFileDao.getDividedFilenameByPath(conn, path);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return name;
    }

    public void deleteSplittedFileWhenUnlink(int userId, String filename){

        conn = ConnectionManager.getConnection();
        complexFileDao = new ComplexFileDao();
        List<Integer> listId;

        try {
            listId = complexFileDao.getFileIdByNameAndUser(conn, filename, userId);
            for (Integer integer : listId) {
                complexFileDao.deleteParts(conn, integer);
            }
            complexFileDao.deleteFile(conn, filename, userId);
        }catch(SQLException e){
            LOG.warn(e);
            e.printStackTrace();
        }finally {
            try{
                conn.close();
            }catch (SQLException e){
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }
}
