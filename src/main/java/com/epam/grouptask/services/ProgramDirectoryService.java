package com.epam.grouptask.services;

import com.epam.grouptask.connection.ConnectionManager;
import com.epam.grouptask.dao.ProgramDirectoriesDao;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Andrian on 26.01.2016.
 */
public class ProgramDirectoryService {
    private static final Logger LOG = Logger.getLogger(ProgramDirectoryService.class);
    private Connection conn;
    private ProgramDirectoriesDao programDirectoriesDao;

    public Map<String, String> getUserDirectories(int userId) {
        conn = ConnectionManager.getConnection();
        programDirectoriesDao = new ProgramDirectoriesDao();

        Map<String, String> directories = null;
        try {
            directories = programDirectoriesDao.getUserDirectories(conn, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }

        return directories;
    }

    public int setDirectory(String cloud, String directory, int userId){
        conn = ConnectionManager.getConnection();
        programDirectoriesDao = new ProgramDirectoriesDao();

        int inserted = 0;
        try {
            inserted = programDirectoriesDao.setDirectory(conn, cloud, directory, userId);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return inserted;
    }

    public int deleteDirectory(String cloud, int userId){
        conn = ConnectionManager.getConnection();
        programDirectoriesDao = new ProgramDirectoriesDao();

        int deleted = 0;
        try{
            deleted = programDirectoriesDao.deleteDirectory(conn, cloud, userId);
        }catch(SQLException e){
            LOG.warn(e);
            e.printStackTrace();
        }finally {
            try {
                conn.close();
            }catch (SQLException e){
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return deleted;
    }
}
