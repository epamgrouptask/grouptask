package com.epam.grouptask.services;

import com.epam.grouptask.connection.ConnectionManager;
import com.epam.grouptask.dao.StatDao;
import com.epam.grouptask.model.OperationCount;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Andrian on 03.02.2016.
 */
public class StatService {
    private static final Logger LOG = Logger.getLogger(StatService.class);
    private Connection conn = null;
    private StatDao statDao = null;

    public LinkedHashMap<Integer, OperationCount> getAllStat(){
        conn = ConnectionManager.getConnection();
        statDao = new StatDao();

        LinkedHashMap<Integer, OperationCount> stat = null;
        try {
            stat = statDao.getAllStat(conn);
        } catch (SQLException e) {
            LOG.warn(e);
            try {
                conn.rollback();
            } catch (SQLException e1) {
                LOG.warn(e1);
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return stat;
    }

    public void setTodayStat(Map<Integer, Map<String, OperationCount>> stat) {
        conn = ConnectionManager.getConnection();
        statDao = new StatDao();
        try {
            statDao.setTodayStat(conn, stat);
        } catch (SQLException e) {
            LOG.warn(e);
            try {
                conn.rollback();
            } catch (SQLException e1) {
                LOG.warn(e1);
                e1.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    public Map<Long, Map<String, OperationCount>> getStatForUserInPeriod(int userId, Date date1, Date date2) {
        conn = ConnectionManager.getConnection();
        statDao = new StatDao();
        Map<Long, Map<String, OperationCount>> stat = null;
        try {
            stat = statDao.getStatForUserInPeriod(conn, userId, date1, date2);
        } catch (SQLException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return stat;
    }
}
