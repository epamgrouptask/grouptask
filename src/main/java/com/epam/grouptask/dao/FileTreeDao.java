package com.epam.grouptask.dao;

import com.epam.grouptask.model.CustomDirectory;
import com.epam.grouptask.socketServer.Server;
import com.epam.grouptask.socketServer.SocketChannelManager;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Andrian on 18.02.2016.
 */
public class FileTreeDao {
    private final String GET_ALL_TREES = "SELECT * FROM file_tree LEFT JOIN client_program ON storage_type = client_id WHERE userId = ?";
    private final String GET_STORAGE = "SELECT tree FROM file_tree WHERE userId = ? AND storage_type LIKE ?";

    private final String UPDATE = "REPLACE INTO file_tree (userId, storage_type, tree) VALUES (?,?,?)";

    public CustomDirectory getAllStorages(Connection c, int userId) throws SQLException {
        CustomDirectory customDirectory = new CustomDirectory("/", "");;

        PreparedStatement pstmt = c.prepareStatement(GET_ALL_TREES);
        pstmt.setInt(1, userId);

        ResultSet rs = pstmt.executeQuery();

        Server server = SocketChannelManager.getServer();
        while (rs.next()) {
            String storage = rs.getString("storage_type");
            if(!"BOX".equals(storage.toUpperCase()) && !"DRIVE".equals(storage.toUpperCase()) && !"DROPBOX".equals(storage.toUpperCase())){
                if(!server.isConnected(storage)){
                    continue;
                }
            }
            String name = rs.getString("name");
            byte[] bytes = rs.getBytes("tree");
            try {
                ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                ObjectInputStream ois = new ObjectInputStream(bais);
                CustomDirectory cd = (CustomDirectory) ois.readObject();
                cd.setName(name == null || name.equals("null") ? storage : name);
                customDirectory.addDirectory(cd);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return customDirectory;
    }

    public CustomDirectory getStorage(Connection c, int userId, String storage) throws SQLException, IOException, ClassNotFoundException {
        CustomDirectory customDirectory = null;

        PreparedStatement pstmt = c.prepareStatement(GET_STORAGE);
        pstmt.setInt(1, userId);
        pstmt.setString(2, storage);

        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            byte[] bytes = rs.getBytes(1);
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
            customDirectory = (CustomDirectory) ois.readObject();
        }
        pstmt.close();
        return customDirectory;
    }

    public int updateTrees(Connection c, CustomDirectory cd, int userId, String storage) throws SQLException, IOException {
        PreparedStatement pstmt = c.prepareStatement(UPDATE);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(cd);
        byte[] bytes = baos.toByteArray();
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);

        pstmt.setInt(1, userId);
        pstmt.setString(2, storage);
        pstmt.setBinaryStream(3, bais, bytes.length);

        int updated = pstmt.executeUpdate();
        pstmt.close();
        return updated;
    }
}
