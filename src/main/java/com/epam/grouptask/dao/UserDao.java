package com.epam.grouptask.dao;

import com.epam.grouptask.model.Role;
import com.epam.grouptask.model.User;
import com.epam.grouptask.transformers.UserTransformer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao {
    private PreparedStatement stmt;

    private final String GET_USERS = "SELECT * FROM user_info";

    private final String GET_USER_BY_NAME_EMAIL = "SELECT * FROM user_info WHERE (email LIKE ? or username LIKE ?) and id!=?";
    private final String GET_USER_BY_NAME = "SELECT * FROM user_info WHERE username LIKE ?";
    private final String GET_USER_BY_ID = "SELECT * FROM user_info WHERE id=?";
    private final String GET_USER_BY_CODE = "SELECT * FROM user_info JOIN validation_code ON validation_code.user_id = user_info.id where validation_code.code LIKE ?";
    private final String VK_LOGIN = "SELECT * FROM user_info WHERE vk_id = ?";
    private final String GOOGLE_LOGIN = "SELECT * FROM user_info WHERE google_id = ?";
    private final String GET_USER_LOCALE = "SELECT locale FROM user_info WHERE id = ?";

    private final String UNLINK_GOOGLE = "UPDATE user_info SET google_id = null WHERE id = ?";
    private final String UNLINK_VK = "UPDATE user_info SET vk_id = null WHERE id = ?";
    private final String CHANGE_EMAIL = "UPDATE user_info SET email = ? WHERE username like ?";
    private final String SET_CONFIRMED = "UPDATE user_info SET confirmed = ? WHERE username like ?";
    private final String CHANGE_PASS = "UPDATE user_info SET password = ? WHERE username like ?";
    private final String LINK_VK = "UPDATE user_info SET vk_id = ? WHERE id = ?";
    private final String LINK_GOOGLE = "UPDATE user_info SET google_id = ? WHERE id = ?";
    private final String CHANGE_EUF = "UPDATE user_info SET email = ?, username=?, fullname=? WHERE id LIKE ?";
    private final String LINK_DROP = "UPDATE user_info SET drbx_token = ? WHERE id = ?";
    private final String LINK_DRIVE = "UPDATE user_info SET drive_access_token = ?,drive_refresh_token=?, drive_access_token_create=NOW() WHERE id = ?";
    private final String UNLINK_DRIVE = "UPDATE user_info SET drive_access_token = NULL, drive_refresh_token=NULL WHERE id = ?";
    private final String UNLINK_DROPBOX = "UPDATE user_info SET drbx_token = NULL WHERE id = ?";
    private final String LINK_BOX = "UPDATE user_info SET box_access_token = ?, box_refresh_token = ?, box_access_token_create = NOW() WHERE id = ?";
    private final String UNLINK_BOX = "UPDATE user_info SET box_refresh_token = NULL , box_access_token = NULL WHERE id = ?";
    private final String UPDATE_USER = "UPDATE user_info SET vk_id = ?," +
            "google_id = ?," +
            "drive_access_token = ?," +
            "drive_refresh_token = ?, drive_access_token_create = ?, drbx_token = ?, email = ?," +
            "username = ?, password = ?, role = ?, locale = ?, fullname = ?, confirmed = ?," +
            "box_refresh_token = ?, box_access_token = ?, box_access_token_create = ? WHERE id = ?";        //need update all other tables
    private final String CHANGE_ROLE = "UPDATE user_info SET role = ? WHERE id = ?";
    private final String CHANGE_BLOCK = "UPDATE user_info SET blocked = ? WHERE id = ?";
    private final String CHANGE_LOCALE = "UPDATE user_info SET locale = ? WHERE id = ?";

    private final String INSERT_USER = "INSERT INTO user_info (email, username, password, fullname) VALUES (?,?,?,?);";
    private final String SET_USER_VK = "INSERT INTO user_info (vk_id, fullname, email) VALUES (?,?,?)";
    private final String SET_USER_GOOGLE = "INSERT INTO user_info (google_id, fullname, email) VALUES (?,?,?)";

    private final String DELETE_USER = "DELETE FROM user_info WHERE id = ?";

    public int updateLocale(Connection c, String locale, int id) throws SQLException {
        stmt = c.prepareStatement(CHANGE_LOCALE);
        stmt.setString(1, locale);
        stmt.setInt(2, id);

        int updated = stmt.executeUpdate();
        return updated;
    }

    public int updateUser(Connection c, User user) throws SQLException {
        stmt = c.prepareStatement(UPDATE_USER);
        stmt.setString(1, user.getVkId());
        stmt.setString(2, user.getGoogleId());
        stmt.setString(3, user.getDriveAccessToken());
        stmt.setString(4, user.getDriveRefreshToken());
        stmt.setTimestamp(5, user.getDriveAccessTokenCreate());
        stmt.setString(6, user.getDrbxToken());
        stmt.setString(7, user.getEmail());
        stmt.setString(8, user.getPassword());
        stmt.setString(9, user.getRole().getName());
        stmt.setString(10, user.getLocale());
        stmt.setString(11, user.getFullname());
        stmt.setBoolean(12, user.isConfirmed());
        stmt.setString(13, user.getBoxRefreshToken());
        stmt.setString(14, user.getBoxAccessToken());
        stmt.setTimestamp(15, user.getBoxAccessTokenCreate());

        stmt.setInt(16, user.getId());

        int updated = stmt.executeUpdate();
        return updated;
    }

    public User getUserById(int id, Connection conn) throws SQLException {
        User user = null;
        stmt = conn.prepareStatement(GET_USER_BY_ID);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            user = new UserTransformer().fromResultSetToObject(rs);
        }
        rs.close();
        return user;
    }

    public List<User> getAllUsers(Connection c) throws SQLException {
        List<User> users = new ArrayList<>();

        stmt = c.prepareStatement(GET_USERS);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            users.add(new UserTransformer().fromResultSetToObject(rs));
        }
        return users;
    }

    public User getUserByCode(String code, Connection conn) throws SQLException {
        User user = null;
        stmt = conn.prepareStatement(GET_USER_BY_CODE);
        stmt.setString(1, code);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            user = new UserTransformer().fromResultSetToObject(rs);
        }
        rs.close();
        return user;
    }

    public int setUserVk(Connection c, String id, String fullname, String email) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(SET_USER_VK);
        pstmt.setString(1, id);
        pstmt.setString(2, fullname);
        pstmt.setString(3, email);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int setUserGoogle(Connection c, String id, String fullname, String email) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(SET_USER_GOOGLE);
        pstmt.setString(1, id);
        pstmt.setString(2, fullname);
        pstmt.setString(3, email);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int linkGoogle(Connection c, int id, String googleId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(LINK_GOOGLE);
        pstmt.setString(1, googleId);
        pstmt.setInt(2, id);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int linkVk(Connection c, int id, String vkId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(LINK_VK);
        pstmt.setString(1, vkId);
        pstmt.setInt(2, id);

        int saved = pstmt.executeUpdate();
        pstmt.close();
        return saved;
    }

    public User getSameUserByLoginEmail(String username, String email, int id, Connection conn) throws SQLException {
        User user = null;
        stmt = conn.prepareStatement(GET_USER_BY_NAME_EMAIL);
        stmt.setString(2, username);
        stmt.setString(1, email);
        stmt.setInt(3, id);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            user = new UserTransformer().fromResultSetToObject(rs);
        }
        rs.close();
        return user;
    }

    public User getUserByLogin(String username, Connection conn) throws SQLException {
        User user = null;
        stmt = conn.prepareStatement(GET_USER_BY_NAME);
        stmt.setString(1, username);
        ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
            user = new UserTransformer().fromResultSetToObject(rs);
        }
        rs.close();
        return user;
    }

    public void inserUser(User user, Connection conn) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(INSERT_USER);
        stmt.setString(1, user.getEmail());
        stmt.setString(2, user.getUserName());
        stmt.setString(3, user.getPassword());
        stmt.setString(4, user.getFullname());
        stmt.executeUpdate();
    }

    public User vkLogin(Connection c, String vkId) throws SQLException {
        User user = null;
        PreparedStatement pstmt = c.prepareStatement(VK_LOGIN);
        pstmt.setString(1, vkId);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            user = new UserTransformer().fromResultSetToObject(rs);
        }
        rs.close();
        pstmt.close();
        return user;
    }

    public User googleLogin(Connection c, String googleId) throws SQLException {
        User user = null;
        PreparedStatement pstmt = c.prepareStatement(GOOGLE_LOGIN);
        pstmt.setString(1, googleId);
        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            user = new UserTransformer().fromResultSetToObject(rs);
        }
        rs.close();
        pstmt.close();
        return user;
    }

    public int linkDropbox(Connection c, int userId, String dropboxToken) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(LINK_DROP);
        pstmt.setString(1, dropboxToken);
        pstmt.setInt(2, userId);

        int saved = pstmt.executeUpdate();
        pstmt.close();
        return saved;
    }

    public int linkDrive(Connection c, int userId, String accessDriveToken, String refreshDriveToken) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(LINK_DRIVE);
        pstmt.setString(1, accessDriveToken);
        pstmt.setString(2, refreshDriveToken);
        pstmt.setInt(3, userId);
        int saved = pstmt.executeUpdate();
        pstmt.close();
        return saved;
    }

    public int linkBox(Connection c, int userId, String accessBoxToken, String refreshBoxToken) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(LINK_BOX);
        pstmt.setString(1, accessBoxToken);
        pstmt.setString(2, refreshBoxToken);
        pstmt.setInt(3, userId);
        int saved = pstmt.executeUpdate();
        pstmt.close();
        return saved;
    }

    public void changeEmailByLogin(String login, String email, Connection conn) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(CHANGE_EMAIL);
        stmt.setString(1, email);
        stmt.setString(2, login);
        stmt.executeUpdate();
    }

    public void changePassByLogin(String login, String pass, Connection conn) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(CHANGE_PASS);
        stmt.setString(1, pass);
        stmt.setString(2, login);
        stmt.executeUpdate();
    }

    public int setConfirmed(String login, Connection conn) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(SET_CONFIRMED);
        stmt.setBoolean(1, true);
        stmt.setString(2, login);
        int updated = stmt.executeUpdate();
        return updated;
    }

    public void changeEUFbyId(String fullname, String username, String email, int id, Connection conn) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(CHANGE_EUF);
        stmt.setString(1, email);
        stmt.setString(2, username);
        stmt.setString(3, fullname);
        stmt.setInt(4, id);
        stmt.executeUpdate();
    }

    public String haveDropbox(Connection c, int userId) throws SQLException {
        stmt = c.prepareStatement(GET_USER_BY_ID);
        stmt.setInt(1, userId);

        ResultSet rs = stmt.executeQuery();
        String token = null;
        if (rs.next()) {
            token = rs.getString("drbx_token");
        }
        return token;
    }

    public String haveDrive(Connection c, int userId) throws SQLException {
        stmt = c.prepareStatement(GET_USER_BY_ID);
        stmt.setInt(1, userId);

        ResultSet rs = stmt.executeQuery();
        String token = null;
        if (rs.next()) {
            token = rs.getString("drive_refresh_token");
        }
        return token;
    }

    public String haveBox(Connection c, int userId) throws SQLException {
        stmt = c.prepareStatement(GET_USER_BY_ID);
        stmt.setInt(1, userId);

        ResultSet rs = stmt.executeQuery();
        String token = null;
        if (rs.next()) {
            token = rs.getString("box_refresh_token");
        }
        return token;
    }

    public int unlinkGoogle(Connection conn, int userId) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(UNLINK_GOOGLE);
        pstmt.setInt(1, userId);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int unlinkVk(Connection conn, int userId) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(UNLINK_VK);
        pstmt.setInt(1, userId);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int unlinkDrive(Connection conn, int userId) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(UNLINK_DRIVE);
        pstmt.setInt(1, userId);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int unlinkDropbox(Connection conn, int userId) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(UNLINK_DROPBOX);
        pstmt.setInt(1, userId);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int unlinkBox(Connection conn, int userId) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(UNLINK_BOX);
        pstmt.setInt(1, userId);

        int saved = pstmt.executeUpdate();
        pstmt.close();

        return saved;
    }

    public int deleteRow(Connection c, int userId) throws SQLException {
        stmt = c.prepareStatement(DELETE_USER);
        stmt.setInt(1, userId);

        int deleted = stmt.executeUpdate();
        return deleted;
    }

    public int changeRole(Connection c, int userId, Role newRole) throws SQLException {
        stmt = c.prepareStatement(CHANGE_ROLE);
        stmt.setString(1, newRole.getName());
        stmt.setInt(2, userId);

        int updated = stmt.executeUpdate();

        stmt.close();

        return updated;
    }

    public int changeBlock(Connection c, int userId, boolean blocked) throws SQLException {
        stmt = c.prepareStatement(CHANGE_BLOCK);
        stmt.setBoolean(1, blocked);
        stmt.setInt(2, userId);

        int updated = stmt.executeUpdate();
        stmt.close();
        return updated;
    }

    public String getUserLocale(Connection c, int userId) throws SQLException {

        stmt = c.prepareStatement(GET_USER_LOCALE);
        stmt.setInt(1, userId);

        ResultSet rs = stmt.executeQuery();
        String locale = null;

        if(rs.next()){
            locale = rs.getString("locale");
        }

        return locale;
    }
}