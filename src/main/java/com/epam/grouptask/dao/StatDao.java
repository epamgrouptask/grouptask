package com.epam.grouptask.dao;

import com.epam.grouptask.model.OperationCount;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Andrian on 03.02.2016.
 */
public class StatDao {
    private final String SET_DAY_USER_STAT = "REPLACE INTO day_stat (day, get_directory, deleteDF, upload, download," +
            " create_dir, renameDF, move, copyDF, cloud, user_id, download_size, upload_size) VALUES (NOW(),?,?,?,?,?,?,?,?,?,?,?,?)";

    private final String GET_BY_USER_IN_PERIOD = "SELECT * FROM day_stat WHERE user_id = ? AND (day BETWEEN ? AND ?)";
    private final String ALL_USER_STAT = "SELECT user_id," +
            "  SUM(deleteDF)      del," +
            "  SUM(create_dir)    cr," +
            "  SUM(download)      dwnld," +
            "  SUM(upload)        upld," +
            "  SUM(copyDF)        cp," +
            "  SUM(get_directory) gd," +
            "  SUM(renameDF)      r," +
            "  SUM(move)          m " +
            "FROM day_stat " +
            "GROUP BY user_id " +
            "ORDER BY del DESC";

    public LinkedHashMap<Integer, OperationCount> getAllStat(Connection c) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(ALL_USER_STAT);
        ResultSet rs = pstmt.executeQuery();
        LinkedHashMap<Integer, OperationCount> stat = new LinkedHashMap<>();
        while (rs.next()){
            Integer userId = rs.getInt("user_id");
            OperationCount count = new OperationCount();
            count.setCopy(rs.getInt("cp"));
            count.setDelete(rs.getInt("del"));
            count.setUpload(rs.getInt("upld"));
            count.setDownload(rs.getInt("dwnld"));
            count.setCreate(rs.getInt("cr"));
            count.setGetDir(rs.getInt("gd"));
            count.setRename(rs.getInt("r"));
            count.setMove(rs.getInt("m"));

            stat.put(userId, count);
        }
        return stat;
    }

    public void setTodayStat(Connection c, Map<Integer, Map<String, OperationCount>> stat) throws SQLException {
        c.setAutoCommit(false);

        PreparedStatement pstmt = c.prepareStatement(SET_DAY_USER_STAT);

        for (Map.Entry<Integer, Map<String, OperationCount>> userMap : stat.entrySet()) {
            for (Map.Entry<String, OperationCount> op : userMap.getValue().entrySet()) {
                OperationCount operation = op.getValue();
                pstmt.setInt(1, operation.getGetDir());
                pstmt.setInt(2, operation.getDelete());
                pstmt.setInt(3, operation.getUpload());
                pstmt.setInt(4, operation.getDownload());
                pstmt.setInt(5, operation.getCreate());
                pstmt.setInt(6, operation.getRename());
                pstmt.setInt(7, operation.getMove());
                pstmt.setInt(8, operation.getCopy());
                pstmt.setString(9, op.getKey());
                pstmt.setInt(10, userMap.getKey());
                pstmt.setLong(11, operation.getDownloadedSize());
                pstmt.setLong(12, operation.getUploadedSize());

                pstmt.addBatch();
            }
        }
        pstmt.executeBatch();

        c.commit();
        c.setAutoCommit(true);
    }

    public Map<Long, Map<String, OperationCount>> getStatForUserInPeriod(Connection c, int userId, java.util.Date date1, java.util.Date date2) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(GET_BY_USER_IN_PERIOD);
        pstmt.setInt(1, userId);
        pstmt.setDate(2, new Date(date1.getTime()));
        pstmt.setDate(3, new Date(date2.getTime()));
        ResultSet rs = pstmt.executeQuery();

        Map<Long, Map<String, OperationCount>> stat = new HashMap<>();
        while (rs.next()) {
            Long date = rs.getDate("day").getTime();
            String cloud = rs.getString("cloud");
            OperationCount oc = new OperationCount();
            oc.setCopy(rs.getInt("copyDF"));
            oc.setGetDir(rs.getInt("get_directory"));
            oc.setDelete(rs.getInt("deleteDF"));
            oc.setUpload(rs.getInt("upload"));
            oc.setDownload(rs.getInt("download"));
            oc.setCreate(rs.getInt("create_dir"));
            oc.setRename(rs.getInt("renameDF"));
            oc.setMove(rs.getInt("move"));
            oc.setDownloadedSize(rs.getLong("download_size"));
            oc.setUploadedSize(rs.getLong("upload_size"));

            Map<String, OperationCount> m = stat.get(date);
            if (m == null) {
                stat.put(date, new HashMap<>());
                m = stat.get(date);
            }
            if (m.get(cloud) == null) {
                m.put(cloud, oc);
            }
        }
        return stat;
    }
}
