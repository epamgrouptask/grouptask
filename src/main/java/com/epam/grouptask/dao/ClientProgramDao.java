package com.epam.grouptask.dao;

import com.epam.grouptask.model.ClientProgram;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrian on 24.01.2016.
 */
public class ClientProgramDao {
    private PreparedStatement stmt;

    private final String GET_ALL_NOT_REM_CLIENTS = "SELECT * FROM client_program c LEFT JOIN user_file_path p ON c.id = p.client_id WHERE c.user = ? AND c.removed=?";
    private final String GET_ALL_CLIENTS = "SELECT * FROM client_program c LEFT JOIN user_file_path p ON c.id = p.client_id WHERE c.user = ?";
    private final String GET_CLIENT = "SELECT * FROM client_program c LEFT JOIN user_file_path p ON c.id = p.client_id WHERE c.user = ? AND c.client_id LIKE ?";
    private final String GET_USER_LOCALE = "SELECT locale FROM client_program c JOIN user_info u ON u.id = c.user WHERE client_id LIKE ?";
    private final String GET_ID = "SELECT id FROM client_program WHERE user = ? AND client_id LIKE ?";
    private final String GET_USER = "SELECT user FROM client_program WHERE client_id LIKE ?";
    private final String GET_PATHS = "SELECT * FROM user_file_path WHERE client_id = (SELECT id FROM client_program WHERE client_program.client_id= ?);";
    private final String GET_BY_USER_NAME = "SELECT client_id FROM client_program WHERE user = ? AND name LIKE ?";

    private final String SET_CLIENT = "INSERT INTO client_program (client_id, user, MAC_address, name) VALUES (?,?,?,?)";
    private final String SET_PATHS = "INSERT INTO user_file_path (client_id, path) SELECT * FROM (SELECT ?, ?) AS tmp WHERE NOT EXISTS (SELECT client_id FROM user_file_path WHERE path = ? and client_id=?) LIMIT 1;";

    private final String REMOVE_PATHS = "DELETE FROM user_file_path WHERE client_id= (SELECT id FROM client_program WHERE client_program.client_id=?) AND path=?;";

    private final String RENAMECLIENT = "UPDATE client_program SET name = ? WHERE client_id = ?";
    private final String SET_REMOVED = "UPDATE client_program SET  removed=?  WHERE client_id=?";

    public String getClientIdByUserAndName(Connection c, String name, int userId) throws SQLException {
        stmt = c.prepareStatement(GET_BY_USER_NAME);
        stmt.setInt(1, userId);
        stmt.setString(2, name);
        ResultSet rs = stmt.executeQuery();
        String clientId = null;
        if (rs.next()) {
            clientId = rs.getString(1);
        }
        return clientId;
    }

    public boolean setRemovedClient(Connection c, String clientId, boolean isRemoved) throws SQLException {
        stmt = c.prepareStatement(SET_REMOVED);
        stmt.setString(2, clientId);
        stmt.setBoolean(1, isRemoved);
        int updated = stmt.executeUpdate();
        if (updated != 0) {
            return true;
        }
        return false;
    }

    public String getUserLocale(Connection c, String clientId) throws SQLException {
        stmt = c.prepareStatement(GET_USER_LOCALE);
        stmt.setString(1, clientId);
        ResultSet rs = stmt.executeQuery();
        String locale = null;
        if (rs.next()) {
            locale = rs.getString(1);
        }
        return locale;
    }

    public Integer getUser(Connection c, String clientId) throws SQLException {
        stmt = c.prepareStatement(GET_USER);
        stmt.setString(1, clientId);
        ResultSet rs = stmt.executeQuery();
        Integer userId = null;
        if (rs.next()) {
            userId = rs.getInt(1);
        }
        return userId;
    }

    public List<ClientProgram> getAllClientsByUserId(Connection c, int id, int mode) throws SQLException {
        List<ClientProgram> clients = null; // mode
        switch (mode) { // 1 - all not removed clients
            case 0: // -1 - all removed
                stmt = c.prepareStatement(GET_ALL_CLIENTS); // 0 -
                stmt.setInt(1, id);
                break;
            case -1:
                stmt = c.prepareStatement(GET_ALL_NOT_REM_CLIENTS);
                stmt.setInt(1, id);
                stmt.setBoolean(2, true);
                break;
            case 1:
                stmt = c.prepareStatement(GET_ALL_NOT_REM_CLIENTS);
                stmt.setInt(1, id);
                stmt.setBoolean(2, false);
                break;
        }
        ResultSet rs = stmt.executeQuery();
        ClientProgram client = new ClientProgram();
        if (rs.next()) {
            clients = new ArrayList<>();

            client.setUserId(id);
            client.setClientId(rs.getString("c.client_id")); // c.client_id
            client.setPath(rs.getString("path"));
            client.setMac(rs.getString("MAC_address"));
            client.setName(rs.getString("name"));
            client.setRemoved(rs.getBoolean("removed"));
            clients.add(client);
            while (rs.next()) {
                String currentId = rs.getString("c.client_id");
                if (currentId.equals(client.getClientId())) {
                    client.setPath(rs.getString("path"));
                } else {
                    client = new ClientProgram();
                    client.setUserId(id);
                    client.setClientId(rs.getString("c.client_id")); // c.client_id
                    client.setPath(rs.getString("path"));
                    client.setMac(rs.getString("MAC_address"));
                    client.setName(rs.getString("name"));
                    client.setRemoved(rs.getBoolean("removed"));
                    clients.add(client);
                }
            }
        }
        return clients;
    }

    public ClientProgram getClientById(Connection c, int userId, String clientId) throws SQLException {
        stmt = c.prepareStatement(GET_CLIENT);
        stmt.setInt(1, userId);
        stmt.setString(2, clientId);

        ResultSet rs = stmt.executeQuery();
        ClientProgram client = null;
        if (rs.next()) {
            client = new ClientProgram();
            client.setUserId(userId);
            client.setClientId(rs.getString("c.client_id")); // c.client_id
            client.setMac(rs.getString("MAC_address"));
            client.setPath(rs.getString("path"));
            client.setName(rs.getString("name"));
            client.setRemoved(rs.getBoolean("removed"));
            while (rs.next()) {
                client.setPath(rs.getString("path"));
            }
        }
        return client;
    }

    public int saveClientProgram(Connection c, String clientId, int userId, String mac, String name)
            throws SQLException {
        stmt = c.prepareStatement(SET_CLIENT);
        stmt.setString(1, clientId);
        stmt.setInt(2, userId);
        stmt.setString(3, mac);
        stmt.setString(4, name);

        int inserted = stmt.executeUpdate();
        return inserted;
    }

    public void savePathsById(Connection c, List<String> paths, int id) throws SQLException {
        stmt = c.prepareStatement(SET_PATHS);
        for (String path : paths) {
            stmt.setInt(1, id);
            stmt.setString(2, path);
            stmt.setString(3, path);
            stmt.setInt(4, id);
            stmt.addBatch();
        }
        stmt.executeBatch();
    }

    public Integer getIdByUserAndClient(Connection c, String clientId, int userId) throws SQLException {
        stmt = c.prepareStatement(GET_ID);
        stmt.setInt(1, userId);
        stmt.setString(2, clientId);

        ResultSet rs = stmt.executeQuery();
        Integer id = null;
        if (rs.next()) {
            id = rs.getInt(1);
        }
        return id;
    }

    public List<String> getPathsByClient(Connection c, String clientId) throws SQLException {
        stmt = c.prepareStatement(GET_PATHS);
        stmt.setString(1, clientId);

        ResultSet rs = stmt.executeQuery();
        List<String> paths = null;
        if (rs.next()) {
            paths = new ArrayList<String>();
            paths.add(rs.getString("path"));
        }
        while (rs.next()) {
            paths.add(rs.getString("path"));
        }
        return paths;
    }

    public void removePathByClient(Connection c, String clientId, String path) throws SQLException {
        stmt = c.prepareStatement(REMOVE_PATHS);
        stmt.setString(1, clientId);
        stmt.setString(2, path);
        stmt.executeUpdate();
    }

    public void rename(Connection c, String newName, String clientId) throws SQLException {
        stmt = c.prepareStatement(RENAMECLIENT);
        stmt.setString(1, newName);
        stmt.setString(2, clientId);
        stmt.executeUpdate();
    }
}
