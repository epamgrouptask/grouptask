package com.epam.grouptask.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrian on 26.01.2016.
 */
public class ProgramDirectoriesDao {
    private final String GET_DIRECTORIES_FOR_USER = "SELECT * FROM program_directories_in_clouds WHERE user_id = ?";
    private final String SET_DIRECTORY = "INSERT INTO program_directories_in_clouds (cloud, directory_path, user_id) VALUES (?,?,?)";
    private final String DELETE_DIRECTORY = "DELETE FROM program_directories_in_clouds WHERE user_id = ? AND cloud LIKE ?";

    public Map<String, String> getUserDirectories(Connection c, int userId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(GET_DIRECTORIES_FOR_USER);
        pstmt.setInt(1, userId);

        ResultSet rs = pstmt.executeQuery();
        Map<String, String> directories = new HashMap<>();
        while (rs.next()) {
            directories.put(rs.getString("cloud"), rs.getString("directory_path"));
        }
        return directories;
    }

    public int setDirectory(Connection c, String cloud, String directory, int userId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(SET_DIRECTORY);
        pstmt.setString(1, cloud);
        pstmt.setString(2, directory);
        pstmt.setInt(3, userId);

        int inserted = pstmt.executeUpdate();
        return inserted;
    }

    public int deleteDirectory(Connection connection, String cloud, int userId) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(DELETE_DIRECTORY);
        stmt.setInt(1, userId);
        stmt.setString(2, cloud);

        int deleted = stmt.executeUpdate();
        return deleted;
    }
}
