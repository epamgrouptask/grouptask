package com.epam.grouptask.dao;

import com.epam.grouptask.model.ComplexFile;
import com.epam.grouptask.model.Divided;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by Andrian on 26.01.2016.
 */
public class ComplexFileDao {
    private final String SET_FILE_NAME = "INSERT INTO saved_file (file_name, user_id, file_size, created_date) VALUES (?,?,?,?)";
    private final String SET_DIVIDED_PARTS = "INSERT INTO divided (filepath, cloud, order_file, file_id) VALUES (?,?,?,?)";

    private final String GET_ID_BY_NAME_AND_USER = "SELECT id FROM saved_file WHERE file_name LIKE ? AND user_id = ?";
    private final String GET_PARTS = "SELECT * FROM divided WHERE file_id = ?";
    private final String GET_FILE_INFO = "SELECT * FROM saved_file WHERE file_name LIKE ? AND user_id = ?";
    private final String GET_ALL_FILES = "SELECT * FROM saved_file s JOIN divided d ON s.id = d.file_id WHERE user_id = ?";

    private final String DELETE_PARTS = "DELETE FROM divided WHERE file_id = ?";
    private final String DELETE_FILE = "DELETE FROM saved_file WHERE user_id = ? AND file_name LIKE ?";

    private final String RENAME_FILE = "UPDATE saved_file SET file_name = ? WHERE user_id = ? AND file_name LIKE ?";
    private final String GET_DIVIDED_FILENAME_BY_PATH = "SELECT file_name FROM saved_file s JOIN divided d ON s.id = d.file_id WHERE filepath LIKE '%";

    public int setFile(Connection c, ComplexFile complexFile) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(SET_FILE_NAME);
        pstmt.setString(1, complexFile.getName());
        pstmt.setInt(2, complexFile.getUserId());
        pstmt.setLong(3, complexFile.getSize());
        Timestamp timestamp = new Timestamp(complexFile.getCreated().getTime());
        pstmt.setTimestamp(4, timestamp);

        int saved = pstmt.executeUpdate();
        return saved;
    }

    public Integer getIdByNameAndUser(Connection c, String name, int userId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(GET_ID_BY_NAME_AND_USER);
        pstmt.setString(1, name);
        pstmt.setInt(2, userId);

        ResultSet rs = pstmt.executeQuery();
        Integer id = null;
        if (rs.next()) {
            id = rs.getInt(1);
        }
        return id;
    }

    public void saveDividedParts(Connection c, int fileId, List<Divided> parts) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(SET_DIVIDED_PARTS);

        for (Divided part : parts) {
            pstmt.setString(1, part.getPath());
            pstmt.setString(2, part.getCloud());
            pstmt.setInt(3, part.getOrder());
            pstmt.setInt(4, fileId);
            pstmt.addBatch();
        }

        pstmt.executeBatch();
    }

    public List<Divided> getPartsForFile(Connection c, int fileId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(GET_PARTS);
        pstmt.setInt(1, fileId);

        ResultSet rs = pstmt.executeQuery();

        List<Divided> parts = new ArrayList<>();
        while (rs.next()) {
            Divided part = new Divided();
            part.setCloud(rs.getString("cloud"));
            part.setPath(rs.getString("filepath"));
            part.setOrder(rs.getInt("order_file"));
            parts.add(part);
        }
        return parts;
    }

    public ComplexFile getFileByUser(Connection c, String name, int userId) throws SQLException {
        ComplexFile complexFile = null;
        PreparedStatement pstmt = c.prepareStatement(GET_FILE_INFO);
        pstmt.setString(1, name);
        pstmt.setInt(2, userId);

        ResultSet rs = pstmt.executeQuery();
        if(rs.next()){
            complexFile = new ComplexFile();
            complexFile.setName(rs.getString("file_name"));
            complexFile.setUserId(userId);
            complexFile.setCreated(new Date(rs.getTimestamp("created_date").getTime()));
            complexFile.setSize(rs.getLong("file_size"));
        }
        return complexFile;
    }

    public List<ComplexFile> getAllUserFiles(Connection c, int userId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(GET_ALL_FILES);
        pstmt.setInt(1, userId);

        ResultSet rs = pstmt.executeQuery();
        List<ComplexFile> files = null;
        if (rs.next()) {
            files = new ArrayList<>();
            ComplexFile complexFile = new ComplexFile();
            complexFile.setId(rs.getInt(1));
            complexFile.setUserId(userId);
            complexFile.setName(rs.getString("file_name"));
            complexFile.setSize(rs.getLong("file_size"));
            complexFile.setCreated(new Date(rs.getTimestamp("created_date").getTime()));

            Divided divided = new Divided();
            divided.setCloud(rs.getString("cloud"));
            divided.setOrder(rs.getInt("order_file"));
            divided.setPath(rs.getString("filepath"));

            complexFile.addPart(divided);
            files.add(complexFile);
            while (rs.next()) {
                Integer currentId = rs.getInt("s.id");
                if (currentId.equals(complexFile.getId())) {
                    divided = new Divided();
                    divided.setCloud(rs.getString("cloud"));
                    divided.setOrder(rs.getInt("order_file"));
                    divided.setPath(rs.getString("filepath"));

                    complexFile.addPart(divided);
                } else {
                    complexFile = new ComplexFile();
                    complexFile.setId(rs.getInt(1));
                    complexFile.setUserId(userId);
                    complexFile.setName(rs.getString("file_name"));
                    complexFile.setSize(rs.getLong("file_size"));
                    complexFile.setCreated(new Date(rs.getTimestamp("created_date").getTime()));

                    divided = new Divided();
                    divided.setCloud(rs.getString("cloud"));
                    divided.setOrder(rs.getInt("order_file"));
                    divided.setPath(rs.getString("filepath"));

                    complexFile.addPart(divided);
                    files.add(complexFile);
                }
            }
        }

        return files;
    }

    public int deleteParts(Connection c, int fileId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(DELETE_PARTS);
        pstmt.setInt(1, fileId);
        int deleted = pstmt.executeUpdate();
        return deleted;
    }

    public int deleteFile(Connection c, String name, int userId) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(DELETE_FILE);
        pstmt.setString(2, name);
        pstmt.setInt(1, userId);
        int deleted = pstmt.executeUpdate();
        return deleted;
    }

    public int rename(Connection c, String name, int userId, String newName) throws SQLException {
        PreparedStatement pstmt = c.prepareStatement(RENAME_FILE);
        pstmt.setString(3, name);
        pstmt.setInt(2, userId);
        pstmt.setString(1, newName);
        int renamed = pstmt.executeUpdate();
        return renamed;
    }

    public String getDividedFilenameByPath(Connection connection, String path) throws SQLException {

        String name = null;
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(GET_DIVIDED_FILENAME_BY_PATH + path + "%'");
        if(rs.next()) {
            name = rs.getString("file_name");
        }

        return name;
    }

    public List<Integer> getFileIdByNameAndUser(Connection c, String filename, Integer userId) throws SQLException {

        List<Integer> listId = new ArrayList<>();
        PreparedStatement stmt = c.prepareStatement(GET_ID_BY_NAME_AND_USER);
        stmt.setString(1, filename);
        stmt.setInt(2, userId);
        ResultSet rs = stmt.executeQuery();
        while(rs.next()){
            listId.add(rs.getInt("id"));
        }

        return listId;
    }
}
