package com.epam.grouptask.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Andrian on 17.01.2016.
 */
public class ValidationCodeDao {
	private final String GET_CODE = "SELECT * FROM validation_code WHERE code LIKE ?";
	private final String GET_CODE_BY_TIME = "SELECT * FROM validation_code WHERE code LIKE ? AND DATEDIFF(CURDATE(),date)<=1";

	private final String CODE_INSERT = "INSERT INTO validation_code values(0,?,?,?)";

	private String DELETE = "DELETE FROM validation_code WHERE code LIKE ?";

	public boolean isExist(Connection connection, String code) throws SQLException {
		PreparedStatement pstmt = connection.prepareStatement(GET_CODE);
		pstmt.setString(1, code);

		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			return true;
		} else {
			return false;
		}
	}
	
	public int delete(Connection c, String code) throws SQLException {
		PreparedStatement pstmt = c.prepareStatement(DELETE);
		pstmt.setString(1, code);
		int deleted = pstmt.executeUpdate();
		pstmt.close();
		return deleted;
	}

	public void insertCode(String code, Connection conn, int userId) throws SQLException {
		PreparedStatement stmt = conn.prepareStatement(CODE_INSERT);
		stmt.setInt(1, userId);
		stmt.setString(2, code);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR,1);
		java.sql.Date date = new java.sql.Date(cal.getTime().getTime());
		stmt.setDate(3, date);
		stmt.executeUpdate();
	}

	public boolean valid(Connection c, String code) throws SQLException {
		PreparedStatement pstmt = c.prepareStatement(GET_CODE_BY_TIME);
		pstmt.setString(1, code);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			return true;
		} else {
			return false;
		}
	}
}
