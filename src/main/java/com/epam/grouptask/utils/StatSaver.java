package com.epam.grouptask.utils;

import com.epam.grouptask.services.StatService;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Andrian on 03.02.2016.
 */
public class StatSaver {
    private static class Saver implements Runnable {
        @Override
        public void run() {
            StatService service = new StatService();
            service.setTodayStat(StatisticController.getStat());            //save to db users stats
        }
    }

    private static class Reset implements Runnable {
        @Override
        public void run() {
            StatisticController.reset();
        }
    }

    private static final ScheduledExecutorService EXECUTOR_SERVICE = Executors.newScheduledThreadPool(1);
    private static final ScheduledExecutorService resetService = Executors.newSingleThreadScheduledExecutor();

    static {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long howMany = (c.getTimeInMillis() - System.currentTimeMillis());
        resetService.scheduleAtFixedRate(new Reset(), howMany, 1000 * 60 * 60 * 24, TimeUnit.MILLISECONDS);           //every day in midnight
    }

    public static void setTime(int time) {
        EXECUTOR_SERVICE.scheduleAtFixedRate(new Saver(), 0, time, TimeUnit.HOURS);
    }
}
