package com.epam.grouptask.utils;

import com.epam.grouptask.cloudcontroller.BoxController;
import com.epam.grouptask.cloudcontroller.DriveController;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.CustomFile;
import com.epam.grouptask.model.StorageType;
import com.epam.grouptask.model.User;
import com.epam.grouptask.services.ProgramDirectoryService;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by Oleg on 25.01.2016 18:40.
 */
public class ProgramDirectoryUtil {
    private static final Logger LOG = Logger.getLogger(ProgramDirectoryUtil.class);

    public static void createProgramDirectory(User user, StorageType type) {
        String directoryName = "CloudsMerger";
        CustomFile customFile = null;
        List<CustomFile> files = null;
        try {
            customFile = new CustomFile(type, user.getId(), null);
            boolean created = false;
            String pathToSave = null;

            // if (created) {
            if (type == StorageType.DROPBOX) {
                customFile.setPath(customFile.getRoot());
                files = customFile.getAllDirectoriesWithProgramDirectory();
                for (CustomFile file : files) {
                    if(file.getName().equals(directoryName)){
                        pathToSave = "/" + directoryName;
                        created = true;
                    }
                }
                if(!created) {
                    created = customFile.createDirectory(directoryName);
                    pathToSave = "/" + directoryName;
                }
            } else if (type == StorageType.DRIVE) {
                customFile.setFileId(customFile.getRoot());
                customFile.setPath(customFile.getRoot());
                files = customFile.getAllDirectoriesWithProgramDirectory();
                for (CustomFile file : files) {
                    if(file.getName().equals(directoryName)){
                        pathToSave = ((DriveController) customFile.getCloud()).getFileIdByName(directoryName, customFile.getRoot());
                        created = true;
                    }
                }
                if(!created) {
                    created = customFile.createDirectory(directoryName);
                    pathToSave = ((DriveController) customFile.getCloud()).getFileIdByName(directoryName, customFile.getRoot());
                }
            } else if (type == StorageType.BOX) {
                customFile.setFileId(customFile.getRoot());
                customFile.setPath(customFile.getRoot());
                files = customFile.getAllDirectoriesWithProgramDirectory();
                for (CustomFile file : files) {
                    if (file.getName().equals(directoryName)) {
                        pathToSave = ((BoxController) customFile.getCloud()).getFileIdByName(directoryName, customFile.getRoot());
                        created = true;
                    }
                }
                if(!created) {
                    created = customFile.createDirectory(directoryName);
                    pathToSave = ((BoxController) customFile.getCloud()).getFileIdByName(directoryName, customFile.getRoot());
                }
            }
            if (created) {
                ProgramDirectoryService programDirectoryService = new ProgramDirectoryService();
                int inserted = programDirectoryService.setDirectory(type.toString(), pathToSave, user.getId());
            }
            // }
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
    }

    public static void deleteProgramDirectory(User user, StorageType type) {
        String directoryName = "CloudsMerger";
        CustomFile customFile = null;
        try {
            boolean deleted = false;
            customFile = new CustomFile(type, user.getId(), null);
            String pathToDelete = null;
            if (type == StorageType.DROPBOX) {
                pathToDelete = "/" + directoryName;
                customFile.setPath(pathToDelete);
            } else if (type == StorageType.DRIVE) {
                //customFile.setFileId(customFile.getRoot());
                pathToDelete = ((DriveController) customFile.getCloud()).getFileIdByName(directoryName, customFile.getRoot());
                customFile.setFileId(pathToDelete);
            } else if (type == StorageType.BOX) {
                // customFile.setFileId(customFile.getRoot());
                pathToDelete = ((BoxController) customFile.getCloud()).getFileIdByName(directoryName, customFile.getRoot());
                customFile.setFileId(pathToDelete);
            }
            customFile.setType("folder");
            deleted = customFile.delete();

            if (deleted) {
                ProgramDirectoryService programDirectoryService = new ProgramDirectoryService();
                int removed = programDirectoryService.deleteDirectory(type.toString(), user.getId());
            }
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
    }
}
