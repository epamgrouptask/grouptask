package com.epam.grouptask.utils;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Andrian on 27.01.2016.
 */
public class CodeIdGenerator {
    private static final Logger LOG = Logger.getLogger(CodeIdGenerator.class);

    public static String generateCode(String username, String email) {
        String base = username + email;
        char[] mas = base.toCharArray();
        StringBuilder sb = new StringBuilder();
        Random rand = new Random();
        int i = 60;
        while (i >= 0) {
            sb.append(mas[rand.nextInt(base.length())]);
            i--;
        }
        String code = null;
        try {
            code = HashChecker.getHash(sb.toString());
        } catch (NoSuchAlgorithmException e) {
            LOG.warn(e);
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            LOG.warn(e);
            e.printStackTrace();
        }
        return code;
    }

    public static String generateClientId() {
        return String.valueOf(UUID.randomUUID());
    }

    public static String generateBlockedCode() {
        String alphabet = "abcdefghiklmnopqrstvxyzABCDEFGHIKLMNOPQRSTVXYZ1234567890";
        Random r = new Random();
        StringBuilder code = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            code.append(alphabet.charAt(r.nextInt(alphabet.length())));
        }
        return code.toString();
    }
}
