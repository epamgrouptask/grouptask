package com.epam.grouptask.utils;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Andrian on 17.12.2015.
 */
public class HashChecker {
    private static final Logger LOG = Logger.getLogger(HashChecker.class);

    public static boolean checkTwoHashes(String hash1, String hash2){
        return hash1.equals(hash2);
    }

    public static boolean checkPassWithHash(String pass, String hash) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        return hash.equals(getHash(pass));
    }

    public static String getHash(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] hash = md.digest(text.getBytes("UTF-8"));
        md.update(text.getBytes("UTF-8"));
        StringBuffer hexString = new StringBuffer();

        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
