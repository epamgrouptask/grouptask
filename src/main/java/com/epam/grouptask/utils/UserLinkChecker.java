package com.epam.grouptask.utils;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;

/**
 * Created by Andrian on 01.02.2016.
 */
public class UserLinkChecker {
    private static final Logger LOG = Logger.getLogger(UserLinkChecker.class);

    public static User updateUser(User user, User another) {
        Class clazz = user.getClass();

        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                Object currentField = field.get(user);
                Object anotherField = field.get(another);

                field.set(user, currentField == null ? anotherField : currentField);

            } catch (IllegalAccessException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        UserService service = new UserService();
        service.deleteUser(another.getId());
        service.updateUser(user);
        return user;
    }
}
