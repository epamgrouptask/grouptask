package com.epam.grouptask.utils;

import com.epam.grouptask.cloudcontroller.ClientController;
import com.epam.grouptask.exception.NotLinkedProfileException;
import com.epam.grouptask.model.ClientProgram;
import com.epam.grouptask.services.UserService;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Created by Andrian on 18.02.2016.
 */
public class ActiveStorages {
    private static final Logger LOG = Logger.getLogger(ActiveStorages.class);

    public static JSONObject getActive(int userId) {
        UserService userService = new UserService();
        String drbx = userService.haveDropbox(userId);
        String drive = userService.haveDrive(userId);
        String box = userService.haveBox(userId);
        Collection<JSONObject> clientsJO = new ArrayList<JSONObject>();
        JSONObject active = new JSONObject();
        Map<ClientProgram, Boolean> clients = null;

        try {
            clients = ClientController.getAllClientsWithStatusByUser(userId);
            List list = new LinkedList(clients.entrySet());

            Collections.sort(list, new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
                }
            });
            Map sortedMap = new LinkedHashMap();
            for (Iterator iterator = list.iterator(); iterator.hasNext(); ) {
                Map.Entry entry = (Map.Entry) iterator.next();
                sortedMap.put(entry.getKey(), entry.getValue());
            }
            clients = sortedMap;
        } catch (NotLinkedProfileException e) {
            LOG.warn(e);
            e.printStackTrace();
        }

        if (clients != null) {
            try {
                for (Map.Entry<ClientProgram, Boolean> client : clients.entrySet()) {
                    JSONObject cl = new JSONObject();
                    cl.put("clientId", client.getKey().getClientId());
                    cl.put("mac", client.getKey().getMac());
                    cl.put("name", client.getKey().getName());
                    cl.put("status", client.getValue());
                    clientsJO.add(cl);
                }
                active.put("clients", clientsJO);
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        if (drbx == null) {
            try {
                active.put("dropbox", false);
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        } else {
            try {
                active.put("dropbox", true);
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        if (drive == null) {
            try {
                active.put("drive", false);
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        } else {
            try {
                active.put("drive", true);
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        if (box == null) {
            try {
                active.put("box", false);
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        } else {
            try {
                active.put("box", true);
            } catch (JSONException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
        return active;
    }
}
