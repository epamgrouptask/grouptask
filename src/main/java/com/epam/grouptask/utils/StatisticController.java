package com.epam.grouptask.utils;

import com.epam.grouptask.model.OperationCount;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrian on 01.02.2016.
 */
public class StatisticController {
    private static Map<Integer, Map<String, OperationCount>> stat = new HashMap<>();        //string - cloud

    public static int incrementGet(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementGet();
        return oc.getGetDir();
    }

    public static int incrementDelete(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementDel();
        return oc.getDelete();
    }

    public static int incrementDownload(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementDownload();
        return oc.getDownload();
    }

    public static int incrementUpload(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementUpload();
        return oc.getUpload();
    }

    public static int incrementCopy(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementCopy();
        return oc.getCopy();
    }

    public static int incrementMove(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementMove();
        return oc.getMove();
    }

    public static int incrementCreate(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementCreate();
        return oc.getCreate();
    }

    public static int incrementRename(String cloud, int userId) {
        Map<String, OperationCount> cloudStat = stat.get(userId);
        if (cloudStat == null) {
            init(cloud, userId);
            cloudStat = stat.get(userId);
        }
        OperationCount oc = cloudStat.get(cloud);
        if (oc == null) {
            cloudStat.put(cloud, new OperationCount());
            oc = cloudStat.get(cloud);
        }
        oc.incrementRename();
        return oc.getRename();
    }

    public static Map<Integer, Map<String, OperationCount>> getStat() {
        return stat;
    }

    public static void reset() {
        stat = new HashMap<>();
    }

    private static void init(String cloud, int userId) {
        Map<String, OperationCount> op = new HashMap<>();
        op.put(cloud, new OperationCount());
        stat.put(userId, op);
    }
}
