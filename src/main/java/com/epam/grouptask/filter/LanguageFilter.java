package com.epam.grouptask.filter;

import com.epam.grouptask.model.User;
import com.epam.grouptask.services.UserService;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by Andrian on 11.02.2016.
 */
public class LanguageFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(LanguageFilter.class);
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        req.setCharacterEncoding("utf-8");
        HttpSession session = ((HttpServletRequest) req).getSession(false);
        if (session != null) {
            User user = (User) session.getAttribute("user");
            if (user != null) {
                String lang = null;
                try {
                    lang = (String) session.getAttribute("language");
                } catch (ClassCastException e){
                    LOG.warn(e);
                    lang = ((Locale) session.getAttribute("language")).toString();
                } catch (IllegalStateException e){
                    LOG.warn(e);
                    chain.doFilter(req, resp);
                    //return;
                }
                if (lang != null){
                    if(!lang.contains("en") && !lang.contains("uk_UA")){
                        lang = "en";
                    }
                    if("/user/home".equals(((HttpServletRequest) req).getRequestURI())){
                        session.setAttribute("language", user.getLocale());
                    } else {
                        UserService us = new UserService();
                        int ins = us.changeLocale(lang, user.getId());
                        if (ins > 0) {
                            if("/logout".equals(((HttpServletRequest) req).getRequestURI())){
                                //chain.doFilter(req, resp);
                                //return;
                            } else {
                                try {
                                    session.setAttribute("language", lang);
                                } catch (IllegalStateException e) {
                                    LOG.warn(e);
                                }
                            }
                        }
                    }
                } else {
//                    UserService us = new UserService();
                    lang = user.getLocale();
                    if(lang != null) {
                        session.setAttribute("language", lang);
                    }
                }
            }
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
