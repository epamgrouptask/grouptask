package com.epam.grouptask.filter;

import com.epam.grouptask.model.Role;
import com.epam.grouptask.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Andrian on 28.01.2016.
 */
public class AdminFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpSession session = ((HttpServletRequest) req).getSession(false);

        if (session == null) {
            ((HttpServletResponse) resp).sendRedirect("/home");
        } else {
            User curUser = (User) session.getAttribute("user");
            if (curUser == null || curUser.getRole() != Role.ADMIN)
                //((HttpServletRequest) req).getRequestDispatcher("/pages/index.jsp").forward(req, resp);
                ((HttpServletResponse) resp).sendRedirect("/home");
            else
                chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
