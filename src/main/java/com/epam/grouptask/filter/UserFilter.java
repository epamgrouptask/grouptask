package com.epam.grouptask.filter;

import com.epam.grouptask.model.Role;
import com.epam.grouptask.model.User;
import com.epam.grouptask.notification.EmailSender;
import com.epam.grouptask.services.ClientProgramService;
import com.epam.grouptask.services.UserService;
import com.epam.grouptask.services.ValidationCodeService;
import com.epam.grouptask.utils.CodeIdGenerator;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by Andrian on 28.01.2016.
 */
public class UserFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        resp.setCharacterEncoding("utf-8");
        req.setCharacterEncoding("utf-8");
        HttpSession session = ((HttpServletRequest) req).getSession(false);
        if (session == null) {
            ((HttpServletResponse) resp).sendRedirect("/home");
        } else {
            User curUser = (User) session.getAttribute("user");
            if (curUser != null) {
                UserService userService = new UserService();
                curUser = userService.getUserById(curUser.getId());

                Locale locale = null;
                String loc = req.getParameter("language");

                if(loc == null){
                    loc = userService.getUserLocale(curUser.getId());
                }
                locale = new Locale(loc);
                ResourceBundle resourceBundle = null;
                try {
                    resourceBundle = ResourceBundle.getBundle("Text", locale);
                } catch (MissingResourceException e) {
                    if(locale.toString().equals("uk_UA") || locale.toString().equals("uk_ua")){
                        locale = new Locale("uk");
                        resourceBundle = ResourceBundle.getBundle("Text", locale);
                    }else if(locale.toString().equals("en_US") || locale.toString().equals("en_us")){
                        locale = new Locale("en");
                        resourceBundle = ResourceBundle.getBundle("Text", locale);
                    }else {
                        e.printStackTrace();
                    }
                }

                HttpServletRequest httpRequest = (HttpServletRequest) req;
                String method = httpRequest.getMethod();
                String reqStr = httpRequest.getRequestURI();

                if (curUser.isBlocked() && (reqStr.contains("getdeleteupload") || reqStr.contains("createrenamedownload") || reqStr.contains("relocatecopy"))) {                               //if user blocked in system
                    String code = CodeIdGenerator.generateBlockedCode();
                    EmailSender.sendEmail(curUser.getEmail(), "moiraalira2015@gmail.com", "moira_moira",
                            resourceBundle.getString("user_filter.email") + code, "CloudsMerger");
                    ValidationCodeService codeService = new ValidationCodeService();
                    codeService.insertCode(code, curUser.getId());
                    resp.getWriter().print("blocked");
                } else {
                    if (curUser.isBlocked() && method.equalsIgnoreCase("GET")) {
                        req.setAttribute("req", reqStr);
                        String code = CodeIdGenerator.generateBlockedCode();
                        EmailSender.sendEmail(curUser.getEmail(), "moiraalira2015@gmail.com", "moira_moira",
                                resourceBundle.getString("user_filter.email") + code, "CloudsMerger");
                        ValidationCodeService codeService = new ValidationCodeService();
                        codeService.insertCode(code, curUser.getId());
                        httpRequest.getRequestDispatcher("/pages/blocked.jsp").forward(req, resp);
                    } else {
                        if (curUser.getRole() != Role.USER) {
                            //((HttpServletRequest) req).getRequestDispatcher("/pages/index.jsp").forward(req, resp);
                            ((HttpServletResponse) resp).sendRedirect("/home");
                        } else {
                            chain.doFilter(req, resp);
                        }
                    }
                }
            } else {
                ((HttpServletResponse) resp).sendRedirect("/home");
                //chain.doFilter(req, resp);
            }
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
