package com.epam.grouptask.transformers;

import com.epam.grouptask.model.Role;
import com.epam.grouptask.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserTransformer {
    public User fromResultSetToObject(ResultSet rs) throws SQLException {
        User result = null;
        result = new User();
        result.setId(rs.getInt("id"));
        result.setVkId(rs.getString("vk_id"));
        result.setGoogleId(rs.getString("google_id"));
        result.setDriveAccessToken(rs.getString("drive_access_token"));
        result.setDriveRefreshToken(rs.getString("drive_refresh_token"));
        result.setDrbxToken(rs.getString("drbx_token"));
        result.setEmail(rs.getString("email"));
        result.setUserName(rs.getString("username"));
        result.setPassword(rs.getString("password"));
        result.setRole(Role.valueOf(rs.getString("role")));
        result.setLocale(rs.getString("locale"));
        result.setFullname(rs.getString("fullname"));
        result.setConfirmed(rs.getBoolean("confirmed"));
        result.setDriveAccessTokenCreate(rs.getTimestamp("drive_access_token_create"));
        result.setBoxRefreshToken(rs.getString("box_refresh_token"));
        result.setBoxAccessToken(rs.getString("box_access_token"));
        result.setBoxAccessTokenCreate(rs.getTimestamp("box_access_token_create"));
        result.setBlocked(rs.getBoolean("blocked"));
        return result;
    }
}
