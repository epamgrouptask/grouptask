package com.epam.grouptask.socketServer;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

/**
 * Created by Andrian on 23.01.2016.
 */
public class ServerForFile implements Runnable {
    private static final Logger LOG = Logger.getLogger(ServerForFile.class);
    private ServerSocket serverSocket;
    private int port = 2222;

    private int bufferSize = 16 * 1024;
    private Boolean downloadToServer = null;

    private InputStream input;
    private OutputStream output;

    class Transfer extends Thread {
        private int bufferSize = 16 * 1024;

        private Socket server;
        private boolean isDownload;

        private InputStream input;
        private OutputStream output;

        Transfer(Socket s, boolean d, InputStream is, OutputStream os) {
            server = s;
            isDownload = d;
            input = is;
            output = os;
        }

        private void uploadToClient() throws IOException {
            //Socket server = serverSocket.accept();
            System.out.println("Just connected to "
                    + server.getRemoteSocketAddress());
            byte[] bytes = new byte[bufferSize];

            OutputStream out = new BufferedOutputStream(server.getOutputStream(), bufferSize);

            int count;
            while ((count = input.read(bytes)) > 0) {
                out.write(bytes, 0, count);
                out.flush();
            }
            input.close();
            out.close();
            server.close();
        }

        private void downloadToServer() throws IOException {
            //Socket server = serverSocket.accept();
            System.out.println("Just connected to "
                    + server.getRemoteSocketAddress());
            //File file = new File("1.exe");
            byte[] bytes = new byte[bufferSize];
            InputStream in = new BufferedInputStream(server.getInputStream(), bufferSize);
            int count = 0;
            while ((count = in.read(bytes)) > 0) {
                output.write(bytes, 0, count);
                output.flush();
            }
            output.close();
            in.close();
            System.out.println("Sended");
            server.close();
            //downloadToServer = null;
            //serverSocket.close();
        }

        @Override
        public void run() {
            if (isDownload) {
                try {
                    downloadToServer();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    uploadToClient();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public ServerForFile() {
        Properties properties = new Properties();
        try {
            //InputStream inputStream = ServerForFile.class.getClassLoader().getResourceAsStream("serv.properties");
            properties.load(getClass().getClassLoader().getResourceAsStream("cloud_config.properties"));
            port = Integer.parseInt(properties.getProperty("fileServerPort"));
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        try {
            serverSocket = new ServerSocket(port);
            serverSocket.setSoTimeout(60000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void setDownloadToServer(boolean downloadToServer) {
        this.downloadToServer = downloadToServer;
    }

    public synchronized Boolean getDownloadToServer() {
        return downloadToServer;
    }

    public InputStream getInput() {
        return input;
    }

    public void setInput(InputStream input) {
        this.input = input;
    }

    public OutputStream getOutput() {
        return output;
    }

    public void setOutput(OutputStream output) {
        this.output = output;
    }

    private void uploadToClient() throws IOException, InterruptedException {
        Socket server = serverSocket.accept();
        Transfer transfer = new Transfer(server, false, input, null);
        downloadToServer = null;
        transfer.start();
        //transfer.join();
        /*System.out.println("Just connected to "
                + server.getRemoteSocketAddress());
        byte[] bytes = new byte[bufferSize];

        OutputStream out = new BufferedOutputStream(server.getOutputStream(), bufferSize);

        int count;
        while ((count = input.read(bytes)) > 0) {
            out.write(bytes, 0, count);
            out.flush();
        }
        input.close();
        server.close();*/

        //serverSocket.close();
    }

    private void downloadToServer() throws IOException, InterruptedException {
        Socket server = serverSocket.accept();
        Transfer transfer = new Transfer(server, true, null, output);
        downloadToServer = null;
        transfer.start();
        transfer.join();
        /*System.out.println("Just connected to "
                + server.getRemoteSocketAddress());

        //File file = new File("1.exe");
        byte[] bytes = new byte[bufferSize];
        InputStream in = new BufferedInputStream(server.getInputStream(), bufferSize);
        //OutputStream out = new BufferedOutputStream(new FileOutputStream(file), bufferSize);
        int count = 0;
        while ((count = in.read(bytes)) > 0) {
            output.write(bytes, 0, count);
            output.flush();
        }
        output.close();
        in.close();
        System.out.println("Sended");
        server.close();*/

        //serverSocket.close();
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                try {
                    if (downloadToServer != null) {
                        if (downloadToServer) {
                            downloadToServer();
                        } else {
                            uploadToClient();
                        }
                    }
                } catch (IOException e) {
                    LOG.warn(e);
                    downloadToServer = null;
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    downloadToServer = null;
                    e.printStackTrace();
                }
            }
        }
    }
}
