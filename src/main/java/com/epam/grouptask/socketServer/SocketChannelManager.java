package com.epam.grouptask.socketServer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Andrian on 17.01.2016.
 */
public class SocketChannelManager {
    private static Server server;
    private static ServerForFile serverForFile;
    private static ExecutorService es = Executors.newFixedThreadPool(2);

    static {
        if (server == null) {
            server = new Server();
            es.execute(server);
        }
        if (serverForFile == null) {
            serverForFile = new ServerForFile();
            es.execute(serverForFile);
        }
    }

    public static synchronized Server getServer() {
        return server;
    }

    public static synchronized ServerForFile getServerForFile(){
        return serverForFile;
    }

    public static synchronized void setIsDownload(boolean isDownload){
        if(serverForFile != null){
            serverForFile.setDownloadToServer(isDownload);
        }
    }

    public static synchronized void restart(){
        server = new Server();
        es.execute(server);
    }

    private SocketChannelManager() {
    }
}
