package com.epam.grouptask.socketServer;

import com.epam.grouptask.protocol.Protocol;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.DeflaterInputStream;
import java.util.zip.DeflaterOutputStream;

/**
 * Created by Andrian on 10.10.2015.
 */

public class Server implements Runnable {
    private static final Logger LOG = Logger.getLogger(Server.class);
    public String ADDRESS = "127.0.0.1";
    public int PORT = 8561;
    public long TIMEOUT = 10000;

    private ServerSocketChannel serverChannel;
    private Selector selector;

    private Map<SocketChannel, byte[]> dataTracking = new HashMap<>();
    private Map<String, SocketChannel> sockets = new HashMap<>();
    private Map<String, Protocol> response = new HashMap<>();

    public Server() {
        init();
    }

    private void init() {
        System.out.println("initializing server");
        if (selector != null) return;
        if (serverChannel != null) return;

        Properties properties = new Properties();
        try {
            //InputStream inputStream = ServerForFile.class.getClassLoader().getResourceAsStream("serv.properties");
            properties.load(getClass().getClassLoader().getResourceAsStream("cloud_config.properties"));
            ADDRESS = properties.getProperty("asyncServerAddress");
            PORT = Integer.parseInt(properties.getProperty("asyncServerPort"));
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        try {
            selector = Selector.open();
            serverChannel = ServerSocketChannel.open();
            serverChannel.configureBlocking(false);
            serverChannel.socket().bind(new InetSocketAddress(ADDRESS, PORT));

            serverChannel.register(selector, SelectionKey.OP_ACCEPT);
        } catch (ClosedChannelException e) {
            LOG.error(e);
            e.printStackTrace();
        } catch (IOException e) {
            LOG.error(e);
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        System.out.println("Now accepting connections...");
        try {
            // A run the server as long as the thread is not interrupted.
            while (!Thread.currentThread().isInterrupted()) { //!Thread.currentThread().isInterrupted()
                selector.select(TIMEOUT);

                Iterator<SelectionKey> keys = selector.selectedKeys().iterator();
                while (keys.hasNext()) {
                    SelectionKey currentKey = keys.next();
                    if (!currentKey.isValid()) {
                        continue;
                    }
                    if (currentKey.isAcceptable()) {
                        // a connection was accepted by a ServerSocketChannel
                        System.out.println("Accepting connection");
                        accept(currentKey);
                    }
                    if (currentKey.isWritable()) {
                        System.out.println("Writing...");
                        write(currentKey);
                    }
                    if (currentKey.isReadable()) {
                        System.out.println("Reading connection");
                        read(currentKey);
                    }
                    keys.remove();
                }
            }
        } catch (IOException e) {
            LOG.warn(e);
            e.printStackTrace();
        } finally {
            closeConnection();
        }

    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();

        byte[] data = dataTracking.get(channel);
        dataTracking.remove(channel);
        ByteBuffer bb = ByteBuffer.wrap(data);
        channel.write(bb);

        key.interestOps(SelectionKey.OP_READ);

        ExecutorService es = Executors.newFixedThreadPool(1);
        //ServerForFile serverForFile = new ServerForFile();
    }

    private void closeConnection() {
        System.out.println("Closing server down");
        if (selector != null) {
            try {
                selector.close();
                serverChannel.socket().close();
                serverChannel.close();
                SocketChannelManager.restart();
            } catch (IOException e) {
                LOG.warn(e);
                e.printStackTrace();
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);

        byte[] message = new String("ready").getBytes();
        //socketChannel.write(ByteBuffer.wrap(message));
        dataTracking.put(socketChannel, dataWithItLength(message));
        socketChannel.register(selector, SelectionKey.OP_WRITE);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer readBuffer = ByteBuffer.allocate(4);
        readBuffer.clear();

        ByteBuffer message;

        int read;
        int lengthOfMessage;
        try {
            read = channel.read(readBuffer);
            readBuffer.flip();
            lengthOfMessage = readBuffer.getInt();
            message = ByteBuffer.allocate(lengthOfMessage);
            message.clear();
            read = channel.read(message);
        } catch (IOException e) {
            LOG.warn(e);
            System.out.println("Reading problem, closing connection");
            //fileTracking.remove(key.channel());
            key.cancel();
            channel.close();
            deleteChannel(channel);
            return;
        }
        if (read == -1) {
            System.out.println("Nothing was there to be read, closing connection");
            channel.close();
            key.cancel();
            deleteChannel(channel);
            return;
        }
        message.flip();
        byte[] buff = new byte[lengthOfMessage];
        message.get(buff, 0, read);
        String stringData = new String(buff);

        ByteArrayInputStream bis = new ByteArrayInputStream(buff);
        ObjectInput in = new ObjectInputStream(bis);

        Protocol protocol = null;   //response from client
        try {
            synchronized (in){
                protocol = (Protocol) in.readObject();
                in.close();
                bis.close();
                if (protocol.getClientID() != null) {
                    sockets.put(protocol.getClientID(), channel);
                    if (protocol.isResponse()) {
                        response.put(protocol.getUuid(), protocol);
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            LOG.warn(e);
            e.printStackTrace();
        }

        System.out.println("Get: " + protocol);    //LOG
    }

    private void deleteChannel(SocketChannel channel){
        Set<Map.Entry<String, SocketChannel>> entrySet = sockets.entrySet();
        for (Map.Entry<String, SocketChannel> entry : entrySet) {
            if (entry.getValue().equals(channel)) {
                sockets.remove(entry.getKey());
            }
        }
    }

    public Protocol getResponse(String uuid) {
        Protocol resp = response.get(uuid);
        if (resp != null) {
            response.remove(uuid);
        }
        return resp;
    }

    public boolean setRequest(Protocol protocol) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos);
        out.writeObject(protocol);

        byte b[] = bos.toByteArray();
        SocketChannel channel = sockets.get(protocol.getClientID());
        if (channel == null) {
            return false;
        }
        dataTracking.put(channel, dataWithItLength(b));
        channel.register(selector, SelectionKey.OP_WRITE);
        out.close();
        bos.close();
        return true;
    }

    public boolean isConnected(String client) {
        if (sockets.get(client) != null) {
            return true;
        } else {
            return false;
        }
    }

    private byte[] dataWithItLength(byte[] data) {
        byte[] newData = new byte[data.length + 4];
        System.arraycopy(ByteBuffer.allocate(4).putInt(data.length).array(),
                0, newData, 0, 4);
        System.arraycopy(data, 0, newData, 4, data.length);
        return newData;
    }

}