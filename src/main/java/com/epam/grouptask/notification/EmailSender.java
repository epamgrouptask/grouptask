/**
 * 
 */
package com.epam.grouptask.notification;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

import org.apache.log4j.Logger;
import org.apache.log4j.chainsaw.Main;

import javax.activation.*;

/**
 * @author Viktoria
 */
public class EmailSender {
	private static final Logger LOG = Logger.getLogger(EmailSender.class);
	public static void sendEmail(String emailTo, final String emailFrom, final String password, String text,
			String subject) {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");// 465

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailFrom, password);
			}
		});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(emailFrom));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
			message.setSubject(subject);
			message.setText(text);
			Transport.send(message);

		} catch (MessagingException e) {
			LOG.warn(e);
			throw new RuntimeException(e);
		}
	}
}
